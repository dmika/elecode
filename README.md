# elecode (electrical engineering code)
=============

This is software useful for some applications in electrical engineering. The author uses this code mainly for calculations related to electrical grounding characteristics and soil properties. However, most of the code can be used for a wide range of applications. If you have any questions or found an error in the code, you can contact me via e-mail: vkd@tuta.io.

***



## Compiling elecode

elecode can be compiled in several different ways: as a library, as a command-line interface (CLI) application, as a graphical user interface (GUI) application, and as a CLI-GUI application.

GUI uses Qt 5 and OpenGL libraries. Therefore, the libraries should be installed before compilation (not required for library or CLI-only application).

The code (library, CLI, GUI) was tested with Debian 11 (GCC 10). Library and CLI were tested with MSYS2 on Windows 10. In addition, library and CLI were tested with Raspberry Pi (GCC 10).

To compile CLI-GUI application:

    cd ./gui
    qmake
    make

To compile CLI application:

    cd ./cli
    make

For library:

    make

If you want to turn all warnings into errors, please add `dev` after the make command. In this case, however, compilation stops even if a single warning exists (warning flags are quite strict).

***



## File structure

### fdtd

This part of the project contains a code for calculations with the [Finite Difference Time Domain method](https://en.wikipedia.org/wiki/Finite-difference_time-domain_method). A detailed description of the method could be found in the book of A. Taflove and S. C. Hagness, "Computational Electrodynamics: The Finite-Difference Time-Domain Method", Third Edition, 2005.

### flashover

The code for insulation flashover related calculations.

### permittivity

The code can be helpful for calculations related to the permittivity of different materials. The author uses it for calculations and measurements of soil permittivity. Various numerical methods were implemented in the code. For some details, see the papers mentioned in the code.

### common

This part mostly contains either math-related code or auxiliary tools that could be used by different parts of the project.

### cli

The code for the CLI application.

### gui

The GUI application code.

### doc

Documentation-related files.

***



## License

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).



## Manual

### Introduction
This is a short reference manual for the main elecode capabilities.
For a typical FDTD simulation, the command usually looks like this:
```sh
elecode model.elm
```

where `model.elm` is the model file. By default, elecode creates `model.elo` file with the simulation result (if other output name was not specified in the `model.elm` file).

***



### Model file commands
There are two types of commands: mandatory and optional.
The full list of mandatory commands is presented in the Table:
| Command | Description |
| ------ | ------ |
| [volume](#volume) | Sets main parameters of the simulation volume. |
| [calctime](#calctime) | Sets simulation time. |

The full list of optional commands is presented in the Table:
| Command | Description |
| ------ | ------ |
| [output](#output) | Sets output file parameters. |
| [threads](#threads) | Sets number of threads. |
| [abc](#abc) | Sets absorbing boundary conditions (ABC). |
| [block](#block) | Models lossy dielectric block. |
| [debye](#debye) | Defines Debye parameters. |
| [wire](#wire) | Models thin wire. |
| [resistor](#resistor) | Models resistor. |
| [capacitor](#capacitor) | Models capacitor. |
| [inductor](#inductor) | Models inductor. |
| [source](#source) | Models EM field source. |
| [function](#function) | Defines source function. |
| [calculate](#calculate) | Calculates a value. |

Comment symbol is `#`.
Below, these commands are described in more detail. Parameters in square brackets are optional (or represent several values).

#### <a name="volume"></a>volume

    volume (x, y, z, dl, [coef])
`volume` command sets volume size in meters (with dimensions _x_, _y_ and _z_) and cell size _dl_ (in meters). Coefficient _coef_ is used when smaller time steps are required. _coef_ should be more than 0 and less than 1.

#### <a name="calctime"></a>calctime

    calctime (period)
`calctime` sets simulation time _period_ (in seconds).

#### <a name="output"></a>output

    output (dt, [fileName], [interval])
`output` sets output time step _dt_. _fileName_ sets output file name. _interval_ sets interval (in FDTD loop steps) for writing output result into file. If `output` is not used, _dt_ equals to FDTD time step.

#### <a name="threads"></a>threads

    threads (number)
`threads` command sets the _number_ of threads for the calculation. Acceptable values are 1 or more. If `threads` is not used, _number_ equals to 1.

#### <a name="abc"></a>abc

Two ABC types were implemented: [UPML](https://doi.org/10.1006/jcph.1994.1159) and [CPML](https://doi.org/10.1002/1098-2760(20001205)27:5%3C334::AID-MOP14%3E3.0.CO;2-A).

    abc (upml, d)
Sets UPML absorbing boundary conditions with thickness _d_ (in meters).

    abc (cpml, d, sigmaCoef, kappaMax, aMax, m, ma)
Sets CPML absorbing boundary conditions with thickness _d_ (in meters) and specific CPML parameters. The description of the parameters can be found in the book "Computational Electrodynamics: The Finite-Difference Time-Domain Method". _sigmaCoef_ is the ratio $`\sigma_{max}/\sigma_{opt}`$. _kappaMax_ is the $`\kappa`$ value, _aMax_ is $`\alpha_{max}`$. _m_ is the scaling order for $`\sigma`$ and $`\kappa`$. _ma_ is the scaling order for $`\alpha`$.

#### <a name="block"></a>block

    block (x1, y1, z1, x2, y2, z2, epsilon, sigma, [label])
`block` sets lossy dielectric block with permittivity _epsilon_ and conductivity _sigma_. _x1_, _y1_, _z1_ - coordinates of the first point. _x2_, _y2_, _z2_ - coordinates of the second point. _label_ is optional. It is used when Debye parameters are required.

#### <a name="debye"></a>debye

    debye (label, eps1, tau1, eps2, tau2, eps3, tau3...)
`debye` defines Debye parameters to approximate the complex permittivity:
```math
\hat{\epsilon}_r(\omega) = \epsilon_\infty + \sum_{p=1}^{n} \frac{\Delta\epsilon_p}{1+j\omega\tau_p},
```
where _tau_ corresponds to $`\tau`$, and _eps_ corresponds to $`\epsilon`$. $`\epsilon_\infty`$ is the permittivity in the block command.
_label_ should coincide with that of the `block` command.

#### <a name="wire"></a>wire

Several thin wire models were implemented.

    wire (staircase, x1, y1, z1, x2, y2, z2)
Models staircase wire. _x1_, _y1_, _z1_ - coordinates of the first point. _x2_, _y2_, _z2_ - coordinates of the second point.

    wire (thin, x1, y1, z1, x2, y2, z2, diameter, [subtype])
Models wire with particular _diameter_ (in meters) according to [model](https://doi.org/10.1109/TAP.2005.860008). _subtype_ sets optional parameters. Using _subtype_ t corrects the magnetic field one cell beyond the second tip of the wire. This is required to eliminate instability when two wires are separated by a single cell.

    wire (oblique, x1, y1, z1, x2, y2, z2, diameter, [optional parameters])
Models oblique wire with particular _diameter_ according to the [model](https://doi.org/10.1109/TAP.2011.2180304). Wires are automatically connected in nodes when their tips have common coordinates. Several optional parameters can be used: volcalc, curcalc (followed by segment number), infend, cursrc (followed by label and cell number). volcalc is used to calculate voltage between the wire tip and the closest node. curcalc calculates current in a particular wire segment. infend models infinite wire (at the second wire tip). cursrc sets the current source in a particular wire segment.

Wires located in the Debye media are modeled according to [model](https://doi.org/10.2528/PIERM16081804).

#### <a name="resistor"></a>resistor

    resistor (x1, y1, z1, x2, y2, z2, resistance)
models `resistor` with particular _resistance_. _x1_, _y1_, _z1_ - coordinates of the first point. _x2_, _y2_, _z2_ - coordinates of the second point.

#### <a name="capacitor"></a>capacitor

    capacitor (x1, y1, z1, x2, y2, z2, capacitance, [conductance])
models a non-ideal `capacitor` with particular _capacitance_ and _conductance_ (optional parameter). _x1_, _y1_, _z1_ - coordinates of the first point. _x2_, _y2_, _z2_ - coordinates of the second point.

#### <a name="inductor"></a>inductor

    inductor (x1, y1, z1, x2, y2, z2, inductance)
models an `inductor` with particular _inductance_. _x1_, _y1_, _z1_ - coordinates of the first point. _x2_, _y2_, _z2_ - coordinates of the second point.

#### <a name="source"></a>source

    source (type, x1, y1, z1, x2, y2, z2, parameter, label)
models voltage, current, or field `source` _type_. _x1_, _y1_, _z1_ - coordinates of the first point. _x2_, _y2_, _z2_ - coordinates of the second point. _parameter_ is either resistance (for voltage source) or conductance (for current source). _label_ is required for setting the waveform function. ehard and esoft _types_ are used for modeling hard and soft electric field sources.

    source (tfsf, theta, phi, psi, dist, label)
This command models a plane wave using the total-field / scattered-field technique. Angles _theta_, _phi_, and _psi_ are set according to the book "Computational Electrodynamics: The Finite-Difference Time-Domain Method". _dist_ is the distance from the volume border (in meters).

#### <a name="function"></a>function

This command defines source `function`:

    function (label, type, [function parameters])
Several _types_ are supported: sine, gaussian, heidler, cigre, ramp, step, custom.

    function (label, sine, frequency, [amplitude])
This command models sine waveform with particular _frequency_. By default, amplitude equals to 1 (if it is not set).

    function (label, gaussian, b, c, [amplitude], [subtype])
This command models [Gaussian function](https://en.wikipedia.org/wiki/Gaussian_function) with parameters _b_ and _c_.

Default _subtype_ s corresponds to the function:
```math
f(t) = f_{max} \text{exp} \left( - \frac{(t-b)^2}{2c^2} \right)
```
_amplitude_ corresponds to $`f_{max}`$.

_subtype_ d corresponds to the function:
```math
f(t) = f_{max} \text{exp} \left( - \frac{(t-b)^2}{2c^2} \right) \frac{b-t}{c^2}
```

_subtype_ n corresponds to the function:
```math
f(t) = f_{max} \text{exp} \left( - \frac{(t-b)^2}{c^2} \right) \frac{b-t}{c}
```

    function (label, heidler, subtype, T, tau, eta, n, [amplitude], [Ts])
The parameters of this function are set according to the [work](https://doi.org/10.1002/ETEP.4450120209).

_subtype_ s corresponds to the function:
```math
f(t) = \frac{f_{max}}{\eta}\frac{\left(\frac{t}{T}\right)^n}{1+\left(\frac{t}{T}\right)^n}\text{exp}\left(-\frac{t}{\tau}\right)+T_s
```
_T_ is the front time coefficient, _tau_ (corresponding to $`\tau`$) is the decay time coefficient, _eta_ (corresponding to $`\eta`$) is the correction coefficient. _amplitude_ corresponds to $`f_{max}`$.
_Ts_ (in seconds) is used when it is necessary to compensate for the offset $`T_{off}`$ mentioned in the [work](https://doi.org/10.1002/ETEP.4450120209). By default, _Ts_ equals to 0.

For the _subtype_ mtle (MTLE lightning channel model):

    function (label, heidler, mtle, lambda, speed, T, tau, n, amplitude, [T, tau, n, amplitude, ...])
_lambda_ is the decay constant in meters, _speed_ is the propagation speed in m/s. Number of Heidler functions can be one or more (not limited in the command).

    function (label, cigre, subtype, front, timeToHalf, [steepness], [amplitude])
The parameters of this function are set according to the [guide](https://e-cigre.org/publication/063-guide-to-procedures-for-estimating-the-lightning-performance-of-transmission-lines).

_subtype_ f corresponds to the function:
```math
f(t) = \begin{cases}
At + Bt^n & \text{if }t < t_n\\
I_1 e^{-(t-t_n)/t_1} - I_2 e^{-(t-t_n)/t_2} & \text{if }t \geqslant t_n
\end{cases}
```
where
```math
\begin{split}
& A = \frac{1}{n-1} \left( 0.9\frac{f_{max}n}{t_n} - S_m \right), \\
& B = \frac{1}{t^n_n (n-1)} \left( S_m t_n - 0.9f_{max} \right), \\
& I_1 = \frac{t_1t_2}{t_1-t_2} \left( S_m + 0.9\frac{f_{max}}{t_2} \right), \\
& I_2 = \frac{t_1t_2}{t_1-t_2} \left( S_m + 0.9\frac{f_{max}}{t_1} \right), \\
& n = 1 + 2(S_N-1)(2+1/S_N), \\
& t_n = 0.6t_f[3S^2_N/(1+S^2_N)], \\
& S_N = S_mt_f/f_{max}, \\
& t_1 = (t_h-t_n)/\ln2, \\
& t_2 = 0.1f_{max}/S_m.
\end{split}
```
_subtype_ s corresponds to the function:
```math
f(t) = \begin{cases}
S_m t & \text{if }t < t_n\\
I_1 e^{-(t-t_n)/t_1} - I_2 e^{-(t-t_n)/t_2} & \text{if }t \geqslant t_n
\end{cases}
```
where
```math
\begin{split}
& I_1 = \frac{t_1t_2}{t_1-t_2} \left( S_m + 0.9\frac{f_{max}}{t_2} \right), \\
& I_2 = \frac{t_1t_2}{t_1-t_2} \left( S_m + 0.9\frac{f_{max}}{t_1} \right), \\
& t_n = 0.9t_f, \\
& t_1 = (t_h-t_n)/\ln2, \\
& t_2 = 0.1f_{max}/S_m.
\end{split}
```
_front_ (corresponding to $`t_f`$) and _timeToHalf_ (corresponding to $`t_h`$) are set in seconds, _steepness_ (corresponding to $`S_m`$) is set in A/s, _amplitude_ (corresponding to $`f_{max}`$) is set in amperes.

    function (label, ramp, front, timeToHalf, [amplitude])
The parameters of this function correspond to a simple ramp waveform.

    function (label, step, front)
This command models step function with sine waveform at the front.

For the custom:

    function (label, custom, dt, [function values])
It models custom function. _dt_ is the time step (in seconds). Function values can be separated by the space symbols or commas.

#### <a name="calculate"></a>calculate

    calculate (current, x1, y1, z1, x2, y2, z2)
Calculates current as integral of the magnetic field. _x1_, _y1_, _z1_ - coordinates of the first point. _x2_, _y2_, _z2_ - coordinates of the second point.

    calculate (voltage, x1, y1, z1, x2, y2, z2, x3, y3, z3...)
Calculates voltage as the line integral of the electric field along the path defined by the points. Points should connect lines located along the FDTD electric field grid. Number of the points is not limited.

##### _Example_
Typical `*.elm` file might look like this:

    threads (2)
    volume (2, 2, 2, 0.05)
    calctime (20e-9)
    output (1e-10)
    abc (cpml, 0.4, 1, 1, 0.01, 3, 1)
    source (current, 1, 1, 1, 1, 1, 1.05, 0, myfunc)
    calculate (voltage, 1, 1.5, 1, 1, 1.5, 1.05)
    function (myfunc, gaussian, 5e-9, 1e-9, 1, n)

***



### CLI commands

If `elecode` command is used without arguments, the program starts as GUI application (assuming, it is compiled with GUI). Otherwise, the program starts in CLI mode.

#### Soil properties

Resistivity and permittivity can be calculated if `model.elo` is formatted as:

    time1 current1 voltage1
    time2 current2 voltage2
    time3 current3 voltage3
    ...   ...      ...

In order to set array dimensions, there is a command

    elecode model.elo [-a a b] [-s scaleFactor]
where _a_ and _b_ are dimensions (in meters) for the array with perpendicular dipole location:

<div align="center">
  <center><img src="doc/array.svg"  width="120" height="120"/></center>
</div>

_scaleFactor_ scales resistivity by the certain factor which allows to use relative resistivity according to the [method](https://doi.org/10.1109/TEMC.2011.2106790). The output is `model.fre` file formatted as:

    frequency1 resistivity1 permittivity1
    frequency2 resistivity2 permittivity2
    frequency3 resistivity3 permittivity3
    ...        ...          ...

Complex permittivity is calculated with:

    elecode model.fre
The output is `model.fee` file formatted as:

    frequency1 permittivity_im1 permittivity_re1
    frequency2 permittivity_im2 permittivity_re2
    frequency3 permittivity_im3 permittivity_re3
    ...        ...          ...

#### Debye parameters

This command calculates Debye parameters corresponding to data in the `*.fee` file:

    elecode model.fee [-r n]
or

    elecode model.fee [minFreq] [maxFreq] [Np] [n]

Debye parameters are calculated according to the [method](https://dx.doi.org/10.1109/TAP.2007.900230). _n_ is number of repeats for calculations (default is 150). After _n_ repeats, the best result is chosen (that fits reference permittivity). _minFreq_ and _maxFreq_ are the minimum and maximum frequencies, correspondingly (in Hz); default values are 10e3 and 4e6. _Np_ is the number of Debye functions (default is 4).

In addition, Debye parameters can be calculated for a particular soil model using this command:

    elecode -mav resistivity [minFreq] [maxFreq] [Np] [n]
where _resistivity_ is low-frequency resistivity. Instead of `-mav`, either `-mp` or `-mm` can be used. `-mav` corresponds to the [model](https://dx.doi.org/10.1109/TEMC.2014.2313977). `-mp` corresponds to the [model](https://dx.doi.org/10.1109/ISEMC.1999.810203). `-mm` corresponds to the Messier model. Parameters used for the models `-mp` and `-mm` are the same as in the [work](https://dx.doi.org/10.1109/TEMC.2013.2271913). Other arguments for the command are the same as above.

#### Depth of investigation

For calculating depth of investigation (DOI), this command is used:

    elecode -d a b
where _a_ and _b_ are dimensions (in meters) for the array with perpendicular dipole location. It is calculated based on [work](https://dx.doi.org/10.1190/1.1440226) and [work](https://dx.doi.org/10.1190/1.1442728).

#### Backflashover probability

For calculating backflashover probability, this command is used:

    elecode model.elo -pf clear [correlationCoef]
where _clear_ is clearance in meters. Instead of `-pf` (first stroke), `-ps` (subsequent stroke) can be used. _correlationCoef_ sets correlation coefficient between amplitude and steepness. Insulation breakdown is determined with the [method](https://doi.org/10.1109/61.25625).

The command expects `model.elo` file formatted as:

    time1 current1 voltage1
    time2 current2 voltage2
    time3 current3 voltage3
    ...   ...      ...



### GUI

The GUI window consists of a model file editor, 3D model view, console output, and error panel. It can be used to create FDTD models and check model files for errors.


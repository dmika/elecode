///
/// \file bfp.h
/// \brief Calculation of the back flashover probability.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef BFP_H__
#define BFP_H__


#include "integration.h"
#include "waveform.h"
#include "breakdown.h"
#include <deque>
#include <vector>
#include <numbers>


struct AmplitudeSteepness {
    AmplitudeSteepness() = default;
    AmplitudeSteepness(float amp, float st) : amplitude{amp}, steepness{st} {}
    float amplitude{0.0};
    float steepness{0.0};
};


std::deque<AmplitudeSteepness> calcAsc(LightningWaveform *wave, const std::vector<float> &voltResponse, float dt, float gap, float lowAmp = 5.0e3, float highAmp = 80.0e3, float lowSteep = 6.0e9, float highSteep = 100.0e9);
std::deque<AmplitudeSteepness> calcAscFast(LightningWaveform *wave, const std::vector<float> &voltResponse, float dt, float gap, float lowAmp = 5.0e3, float highAmp = 80.0e3, float lowSteep = 6.0e9, float highSteep = 100.0e9);
float calcAmplitude(LightningWaveform *wave, const std::vector<float> &voltResponse, float dt, float gap, float lowAmp = 5.0e3, float highAmp = 80.0e3, float steep = 24.3e9, float front = 0.0);
float calcProbabilityAsc(const std::deque<AmplitudeSteepness> &asc, float amplitudeMedian, float amplitudeBeta, float steepnessMedian, float steepnessBeta);
float getProbability(float value, float is, float sigma);
double calcProbabilityAscCorrel(const std::deque<AmplitudeSteepness> &asc, double amplitudeMedian, double amplitudeBeta, double steepnessMedian, double steepnessBeta, double correl);
double getProbabilityCorrel(double x1, double x2, double mu1, double mu2, double sigma1, double sigma2, double rho);


#endif    // BFP_H__


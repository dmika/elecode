///
/// \file breakdown.h
/// \brief Different breakdown models. See "A new calculation method of breakdown voltage-time characteristics
/// of long air gaps" by T. Shindo, T. Suzuki; "Performance of large air gaps under lightning overvoltages:
/// experimental study and analysis of accuracy predetermination methods" by A. Pigini, G. Rizzi, E. Garbagnati,
/// A. Porrino, G. Baldo, G. Pesavento.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef BREAKDOWN_H__
#define BREAKDOWN_H__


#include "interpolation.h"
#include <stdexcept>
#include <vector>
#include <cmath>


struct TimeVoltage {
    float time;
    float volt;
};


enum class Polarity {
    positive,
    negative
};


enum class GapType {
    rodplane,
    rodrod,
    insString
};


bool breakdownShindo(std::vector<TimeVoltage> &pulse, float gapDist, Polarity pol = Polarity::positive, GapType type = GapType::rodrod, float *breakTime = nullptr);
bool breakdownPigini(std::vector<TimeVoltage> &pulse, float gapDist, Polarity pol = Polarity::positive, GapType type = GapType::rodrod, float *breakTime = nullptr);
float calcE50Pigini(float gapDist, Polarity pol, GapType type);
bool breakdownPigini(std::vector<TimeVoltage> &pulse, float gapDist, float e50, float *breakTime = nullptr);


#endif    // BREAKDOWN_H__


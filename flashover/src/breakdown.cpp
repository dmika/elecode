///
/// \file breakdown.cpp
/// \brief Different breakdown models. See "A new calculation method of breakdown voltage-time characteristics
/// of long air gaps" by T. Shindo, T. Suzuki; "Performance of large air gaps under lightning overvoltages:
/// experimental study and analysis of accuracy predetermination methods" by A. Pigini, G. Rizzi, E. Garbagnati,
/// A. Porrino, G. Baldo, G. Pesavento.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "breakdown.h"


bool breakdownShindo(std::vector<TimeVoltage> &pulse, float gapDist, Polarity pol, GapType type, float *breakTime)
{
    float e0, A, B, k1, k2;
    if (type == GapType::rodplane) {
        e0 = 400.0e3;
        if (pol == Polarity::positive) {
            A = 0.5;
            B = 0.3;
            k1 = 2.0e-7;
            k2 = 3.0e-3;
        }
        else {
            throw std::invalid_argument("no data for negative rod-plane configuration (function breakdownShindo).");
        }
    }
    else if (type == GapType::rodrod) {
        e0 = 450.0e3;
        if (pol == Polarity::positive) {
            A = 0.5;
            B = 0.42;
            k1 = 1.0e-7;
            k2 = 2.5e-3;
        }
        else {
            A = 0.5;
            B = 0.5;
            k1 = 0.5e-7;
            k2 = 5.0e-3;
        }
    }
    else {
        throw std::invalid_argument("only rod-rod and rod-plane gap types are allowed (function breakdownShindo).");
    }
    
    float x = 0.0;  // leader length
    float v = 0.0;  // leader velocity
    float Vp = 0.0; // peak voltage (MV)
    float c1 = 5.0e-10;
    for (unsigned cnt = 0; cnt < pulse.size(); ++cnt) {
        if (pulse[cnt].volt > Vp) {
            Vp = pulse[cnt].volt;
        }
    }
    Vp *= 1.0e-6;
    for (unsigned cnt = 1; cnt < pulse.size(); ++cnt) {
        float Ts = A/(Vp/gapDist-B);
        if (pulse[cnt].time*1.0e6 >= Ts) {
            float i = c1 * pulse[cnt].volt * v;
            if (type == GapType::rodplane) {
                v = k1 * pulse[cnt].volt*pulse[cnt].volt/(gapDist-x) + k2 * pulse[cnt].volt*i*(x/gapDist)/(gapDist-x);
            }
            else if (type == GapType::rodrod) {
                v = k1 * pulse[cnt].volt*pulse[cnt].volt/(gapDist-2.0*x) + k2 * pulse[cnt].volt*i*(x/gapDist)/(gapDist-2.0*x);
            }
            x += v*(pulse[cnt].time-pulse[cnt-1].time);
            
            if (x >= gapDist) {
                if (breakTime) {
                    *breakTime = pulse[cnt].time;
                }
                return true;
            }
            if (pulse[cnt].volt < e0*(gapDist-x)) {
                return false;
            }
        }
    }
    
    return false;
}


bool breakdownPigini(std::vector<TimeVoltage> &pulse, float gapDist, Polarity pol, GapType type, float *breakTime)
{
    float e50 = calcE50Pigini(gapDist, pol, type);
    return breakdownPigini(pulse, gapDist, e50, breakTime);
}


float calcE50Pigini(float gapDist, Polarity pol, GapType type)
{
    std::vector<float> posCoefE50E50rp = {1.0, 1.00633, 1.01962, 1.03291, 1.04747, 1.06013, 1.07392, 1.08646, 1.10063, 1.11519,
        1.12848, 1.14177, 1.15557, 1.16962, 1.18291, 1.19873, 1.21253};
    std::vector<float> negCoefE50E50rp = {1.0, 0.969656, 0.944293, 0.919565, 0.895978, 0.870109, 0.846014, 0.819638, 0.793768, 0.779438,
        0.779438, 0.779438, 0.778551, 0.778551, 0.77817, 0.779438, 0.778804};
    std::vector<float> posE50rp = {0.518959, 0.522845, 0.522046, 0.522046, 0.522817, 0.521693, 0.521164, 0.521164, 0.521164, 0.521164, 0.52166, 0.522046,
        0.521164, 0.520627, 0.521329, 0.520282, 0.521164, 0.520806, 0.520282, 0.520282, 0.5194, 0.5194, 0.520282, 0.522046, 0.522046, 0.522625,
        0.522046, 0.522046, 0.521164, 0.521164, 0.521164, 0.521164, 0.521605, 0.520282, 0.520282, 0.521412, 0.520282, 0.520282, 0.520282, 0.520282,
        0.521164, 0.521164, 0.521164, 0.520282, 0.519731, 0.520475, 0.520282, 0.520282, 0.520282, 0.519042, 0.5194, 0.518519, 0.520282, 0.518519,
        0.518519, 0.5194, 0.5194, 0.518519, 0.518519, 0.518519, 0.518519, 0.518519, 0.519731, 0.517637, 0.518078, 0.518684, 0.518519, 0.518519,
        0.518519, 0.518519, 0.517637, 0.516755, 0.516975, 0.516755, 0.516755, 0.516755, 0.516755, 0.516755, 0.516755, 0.516755, 0.518519, 0.518519,
        0.518519, 0.518519, 0.518519, 0.5194, 0.5194, 0.5194, 0.517637, 0.518519, 0.518519, 0.518519, 0.518519, 0.518519, 0.517637, 0.518519,
        0.518519, 0.517637, 0.518519, 0.517637, 0.517637, 0.517637, 0.5194, 0.5194, 0.518871};
    std::vector<float> negE50rp = {0.962081, 0.962081, 0.954784, 0.942372, 0.937831, 0.926808, 0.915212, 0.908333, 0.897884, 0.890101, 0.882562, 0.874295,
        0.86627, 0.858058, 0.850639, 0.846054, 0.836861, 0.831713, 0.824515, 0.820712, 0.816711, 0.810825, 0.80657, 0.804156, 0.798942, 0.796296,
        0.790432, 0.786828, 0.783069, 0.77866, 0.776014, 0.771803, 0.768607, 0.766314, 0.762897, 0.760141, 0.757496, 0.754332, 0.75172, 0.747795,
        0.745855, 0.741623, 0.739859, 0.736618, 0.733907, 0.731922, 0.729277, 0.727513, 0.724515, 0.721671, 0.719577, 0.716931, 0.715168, 0.714352,
        0.71164, 0.709579, 0.707231, 0.705467, 0.702822, 0.701058, 0.698413, 0.696858, 0.695613, 0.694004, 0.690476, 0.689749, 0.688867, 0.686067,
        0.684921, 0.684303, 0.68254, 0.680875, 0.679012, 0.678131, 0.676367, 0.673721, 0.673721, 0.671958, 0.670392, 0.670029, 0.670194, 0.66843,
        0.666667, 0.665928, 0.665785, 0.663139, 0.662257, 0.659612, 0.659612, 0.658455, 0.657209, 0.656966, 0.655203, 0.655203, 0.654321, 0.652557, 
        0.651675, 0.650794, 0.649912, 0.64903, 0.64903, 0.647035, 0.647266, 0.646384, 0.646384};
    std::vector<float> gapFactorArray(posCoefE50E50rp.size());
    std::vector<float> gapDistanceArray(posE50rp.size());
    for (unsigned cnt = 0; cnt < gapFactorArray.size(); ++cnt) {
        gapFactorArray[cnt] = 1.0 + static_cast<float>(cnt) * 0.05;
    }
    for (unsigned cnt = 0; cnt < gapDistanceArray.size(); ++cnt) {
        gapDistanceArray[cnt] = 0.8 + static_cast<float>(cnt) * 0.05;
    }
    
    float gapFactor = 1.0;
    float e50 = 0.0;
    
    if (type == GapType::rodplane) {
        gapFactor = 1.0;
    }
    else if (type == GapType::rodrod) {
        gapFactor = 1.3;
    }
    else if (type == GapType::insString) {
        gapFactor = 1.4;
    }
    
    if (pol == Polarity::positive) {
        float e50coef = makeLinearInterpolation(gapFactorArray, posCoefE50E50rp, gapFactor);
        float e50rp = makeLinearInterpolation(gapDistanceArray, posE50rp, gapDist);
        e50 = e50rp*e50coef;
    }
    else {
        float e50coef = makeLinearInterpolation(gapFactorArray, negCoefE50E50rp, gapFactor);
        float e50rp = makeLinearInterpolation(gapDistanceArray, negE50rp, gapDist);
        e50 = e50rp*e50coef;
    }
    
    float volts = 1.0e6;
    return e50*volts;
}


bool breakdownPigini(std::vector<TimeVoltage> &pulse, float gapDist, float e50, float *breakTime)
{
    float kv = 0.001;
    e50 = e50*kv;
    unsigned cnt = 0;
    
    for (cnt = 0; cnt < pulse.size() && pulse[cnt].volt*kv < e50*gapDist; ++cnt);
    
    // l - leader length
    // vl - leader velocity
    for (float l = 0.0, vl = 0.0; cnt < pulse.size()-1; ++cnt) {
        float voltkv = pulse[cnt].volt*kv;
        l += vl*(pulse[cnt+1].time - pulse[cnt].time);
        vl = 170.0 * gapDist * (voltkv/(gapDist-l) - e50) * exp(1.5*1.0e-3 * voltkv / gapDist);
        
        if (l >= gapDist) {
            if (breakTime) {
                *breakTime = pulse[cnt].time;
            }
            return true;
        }
    }
    
    return false;
}


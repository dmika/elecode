///
/// \file bfp.cpp
/// \brief Calculation of the back flashover probability.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "bfp.h"


std::deque<AmplitudeSteepness> calcAsc(LightningWaveform *wave, const std::vector<float> &voltResponse, float dt, float gap, float lowAmp, float highAmp, float lowSteep, float highSteep)
{
    std::deque<AmplitudeSteepness> asc;
    std::vector<float> curshape(voltResponse.size());
    float dsteep = 0.1e9;
    float damp = 0.1e3;
    
    for (float steep = lowSteep; steep < highSteep; steep += dsteep) {
        for (float amp = lowAmp; amp < highAmp; amp += damp) {
            wave->setParameters(steep, amp);
            for (unsigned cnt = 0; cnt < curshape.size(); ++cnt) {
                curshape[cnt] = wave->getValue(static_cast<float>(cnt)*dt);
            }
            std::vector<float> volt = calcResponseToArbitraryCause(voltResponse, curshape);
            std::vector<TimeVoltage> timevolt;
            for (unsigned cnt = 0; cnt < volt.size(); ++cnt) {
                timevolt.push_back(TimeVoltage(static_cast<float>(cnt)*dt, volt[cnt]));
            }
            if (breakdownPigini(timevolt, gap, Polarity::negative, GapType::insString)) {
                asc.push_front(AmplitudeSteepness(amp*1e-3, steep*1e-9));
                break;
            }
        }
    }

    return asc;
}


std::deque<AmplitudeSteepness> calcAscFast(LightningWaveform *wave, const std::vector<float> &voltResponse, float dt, float gap, float lowAmp, float highAmp, float lowSteep, float highSteep)
{
    std::deque<AmplitudeSteepness> asc;
    std::vector<float> curshape(voltResponse.size());
    float dsteep = 0.1e9;
    float damp = 0.1e3;
    
    float curflash = lowAmp;
    for (float steep = highSteep; steep >= lowSteep; steep -= dsteep) {
        float amp;
        for (amp = curflash; amp < highAmp; amp += damp) {
            wave->setParameters(steep, amp);
            for (unsigned cnt = 0; cnt < curshape.size(); ++cnt) {
                curshape[cnt] = wave->getValue(static_cast<float>(cnt)*dt);
            }
            std::vector<float> volt = calcResponseToArbitraryCause(voltResponse, curshape);
            std::vector<TimeVoltage> timevolt;
            for (unsigned cnt = 0; cnt < volt.size(); ++cnt) {
                timevolt.push_back(TimeVoltage(static_cast<float>(cnt)*dt, volt[cnt]));
            }
            if (breakdownPigini(timevolt, gap, Polarity::negative, GapType::insString)) {
                asc.push_back(AmplitudeSteepness(amp*1e-3, steep*1e-9));
                curflash = amp - 1.0e3;
                if (curflash < lowAmp) curflash = lowAmp;
                break;
            }
        }
        if (amp >= highAmp) break;
    }

    return asc;
}


float calcAmplitude(LightningWaveform *wave, const std::vector<float> &voltResponse, float dt, float gap, float lowAmp, float highAmp, float steep, float front)
{
    std::vector<float> curshape(voltResponse.size());
    float damp = 0.01e3;
    float current = 0.0;
    
    for (float amp = lowAmp; amp < highAmp; amp += damp) {
        if (front > 0.0) {
            float cigreSmToS3090 = 24.3/7.2;
            steep = cigreSmToS3090*amp/front;
            wave->setParameters(steep, amp, 0.0, front);
        }
        else {
            wave->setParameters(steep, amp);
        }
        for (unsigned cnt = 0; cnt < curshape.size(); ++cnt) {
            curshape[cnt] = wave->getValue(static_cast<float>(cnt)*dt);
        }
        std::vector<float> volt = calcResponseToArbitraryCause(voltResponse, curshape);
        std::vector<TimeVoltage> timevolt;
        for (unsigned cnt = 0; cnt < volt.size(); ++cnt) {
            timevolt.push_back(TimeVoltage(static_cast<float>(cnt)*dt, volt[cnt]));
        }
        if (breakdownPigini(timevolt, gap, Polarity::negative, GapType::insString)) {
            current = amp;
            break;
        }
    }

    return current*1.0e-3;
}


float calcProbabilityAsc(const std::deque<AmplitudeSteepness> &asc, float amplitudeMedian, float amplitudeBeta, float steepnessMedian, float steepnessBeta)
{
    float prob = getProbability(asc[0].amplitude, amplitudeMedian, amplitudeBeta) *
        getProbability(asc[0].steepness, steepnessMedian, steepnessBeta);
    for (unsigned cnt = 0; cnt < asc.size()-1; ++cnt) {
        float Ip = getProbability((asc[cnt+1].amplitude+asc[cnt].amplitude)*0.5, amplitudeMedian, amplitudeBeta);
        float as1p = getProbability(asc[cnt+1].steepness, steepnessMedian, steepnessBeta);
        float as0p = getProbability(asc[cnt].steepness, steepnessMedian, steepnessBeta);
        prob += Ip * (as1p - as0p);
    }
    return prob;
}


float getProbability(float value, float is, float sigma)
{
    float z = (log(value)-log(is)) / sigma;
    return 0.5 * erfcf(z / sqrt(2.0));
}


double calcProbabilityAscCorrel(const std::deque<AmplitudeSteepness> &asc, double amplitudeMedian, double amplitudeBeta, double steepnessMedian, double steepnessBeta, double correl)
{
    double da = 0.1;
    double di = 0.01;
    double amax = 1000.0;
    double imax = 500.0;
    double prob = 0.0;

    for (double a = static_cast<double>(asc[0].steepness)+da*0.5; a < amax; a += da) {
        for (double i = static_cast<double>(asc[0].amplitude)+di*0.5; i < imax; i += di) {
            prob += getProbabilityCorrel(i, a, amplitudeMedian, steepnessMedian, amplitudeBeta, steepnessBeta, correl) * di * da;
        }
    }
    for (unsigned cnt = 0; cnt < asc.size()-1; ++cnt) {
        da = fabs(static_cast<double>(asc[cnt].steepness) - static_cast<double>(asc[cnt+1].steepness));
        double a = static_cast<double>(asc[cnt+1].steepness)+da*0.5;
        for (double i = static_cast<double>(asc[cnt].amplitude)+0.5*di; i < imax; i += di) {
            prob += getProbabilityCorrel(i, a, amplitudeMedian, steepnessMedian, amplitudeBeta, steepnessBeta, correl) * di * da;
        }
    }
    
    return prob;
}


double getProbabilityCorrel(double x1, double x2, double mu1, double mu2, double sigma1, double sigma2, double rho)
{
    using namespace std::numbers;
    double z1 = (log(x1)-log(mu1)) / sigma1;
    double z2 = (log(x2)-log(mu2)) / sigma2;
    double q = (z1*z1 - 2.0*rho*z1*z2 + z2*z2) / (1.0-rho*rho);
    return exp(-q*0.5) / (2.0*pi*x1*x2*sigma1*sigma2*sqrt(1.0-rho*rho));
}


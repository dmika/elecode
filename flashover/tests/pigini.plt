set xrange [0:10e-6]
set yrange [0:2]
set style line 1 lt 1 lw 2 pt 3 lc rgb "black";
set grid;
plot './pigini_figc7x.txt' using 1:2 w l ls 1, \
'./pigini_figc7.txt' using 1:($2*1e-6) w l ls 1;

///
/// \file pigini.cpp
/// \brief Testing Pigini model.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "breakdown.h"


int main(void)
{
    std::ifstream inf("pigini_figc7.txt");
    std::vector<TimeVoltage> pulse;
    
    while (!(inf.eof())) {
        std::string str;
        getline (inf, str);
        if (!str.empty()) {
            std::istringstream iss(str);
            TimeVoltage tv;
            iss >> tv.time;
            iss >> tv.volt;
            pulse.push_back(tv);
        }
    }
    
    float time = 0.0;
    breakdownPigini(pulse, 2.0, 520.0e3, &time);
    
    float ref = 8.1e-6;
    float tol = 0.03;
    float error = (time - ref) / ref;
    
    if (fabs(error) < tol) {
        std::cout << "Pigini Fig.C7 test OK.";
    }
    else {
        std::cout << "Pigini Fig.C7 test FAIL.";
    }
    std::cout << " Time = " << time << ". Error = " << error*100.0 << " %" << std::endl;
    
    return 0;
}


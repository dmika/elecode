///
/// \file pso.cpp
/// \brief Basic particle swarm optimization algorithm. See "Particle Swarm Optimization
/// in Electromagnetics" by Jacob Robinson and Yahya Rahmat-Samii.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "pso.h"


Swarm::Swarm(std::function<float(const std::vector<float>&, const std::vector<Dimension>&)> func,
             const std::vector<ReferenceValue> &ref, const std::vector<Dimension> &dim, unsigned particlesNum)
    : mFunction{func}, mReferenceValues{ref}, d{dim}
{
    std::random_device rd;
    mGen = std::make_shared<std::mt19937>(rd());
    init(particlesNum);
}


void Swarm::init(unsigned particlesNum)
{
    if (particlesNum == 0) {
        throw std::invalid_argument("Zero number of particles.");
    }
    if (mReferenceValues.size() == 0) {
        throw std::invalid_argument("Zero ReferenceValues size.");
    }
    if (d.size() == 0) {
        throw std::invalid_argument("Zero Dimensions size.");
    }

    gBest = d;
    mParticles.clear();
    for (unsigned cnt = 0; cnt < particlesNum; ++cnt) {
        mParticles.push_back(Particle(this));
    }
    gBestFitness = std::numeric_limits<float>::max();
}


void Swarm::makeStep(void)
{
    for (auto &particle : mParticles) {
        particle.makeStep();
    }
}


void Swarm::makeIterations(unsigned iterationsNum)
{
    for (unsigned cnt = 0; cnt < iterationsNum; ++cnt) {
        makeStep();
    }
}


Particle Swarm::getParameters(void)
{
    float bestFitness = std::numeric_limits<float>::max();
    unsigned bestParticle = 0;
    for (unsigned part = 0; part < mParticles.size(); ++part) {
        if (mParticles[part].getFitness() < bestFitness) {
            bestFitness = mParticles[part].getFitness();
            bestParticle = part;
        }
    }
    return mParticles[bestParticle];
}


// choose best fit parameters from n repeats
Particle Swarm::calcParametersWithRepeats(unsigned repeats, unsigned iterationsNum)
{
    if (repeats == 0) {
        throw std::invalid_argument("Zero repeats.");
    }

    Particle dpsave(this);
    for (unsigned cnt = 0; cnt < repeats; ++cnt) {
        makeIterations(iterationsNum);
        Particle dp = getParameters();
        if (dpsave.getFitness() > dp.getFitness()) {
            dpsave = dp;
        }
        init(mParticles.size());
    }
    return dpsave;
}


Particle::Particle(Swarm *swarm)
{
    mSwarm = swarm;
    d = mSwarm->getBest();
    for (unsigned cnt = 0; cnt < d.size(); ++cnt) {
        std::uniform_real_distribution<float> dismx(d[cnt].xmin, d[cnt].xmax);
        std::uniform_real_distribution<float> dismv(d[cnt].vmin, d[cnt].vmax);
        d[cnt].x = dismx(*mSwarm->getGen());
        d[cnt].v = dismv(*mSwarm->getGen());
    }
    pBest.resize(d.size());
}


void Particle::makeStep(void)
{
    // apply boundary conditions, if needed

    float fitn = evaluateFitness();
    if (fitn < mSwarm->getBestFitness()) {
        mSwarm->setBestFitness(fitn);
        mSwarm->setBest(d);
    }
    if (fitn < pBestFitness) {
        pBestFitness = fitn;
        pBest = d;
    }

    updateVelocity();
    updatePosition();
}


float Particle::evaluateFitness(void)
{
    float error = 0.0;
    std::vector<ReferenceValue> referenceValues = mSwarm->getReferenceValues();
    for (unsigned cnt = 0; cnt < referenceValues.size(); ++cnt) {
        float ref = referenceValues[cnt].f;
        float calc = mSwarm->getFunctionValue(referenceValues[cnt].x, d);
        error += (ref-calc)*(ref-calc);
    }
    return error;
}


void Particle::updateVelocity(void)
{
    std::uniform_real_distribution<float> dis(0.0, 1.0);
    for (unsigned cnt = 0; cnt < d.size(); ++cnt) {
        d[cnt].v = mw*d[cnt].v + mc1*dis(*mSwarm->getGen())*(pBest[cnt].x-d[cnt].x) +
                                 mc2*dis(*mSwarm->getGen())*(mSwarm->getBest()[cnt].x-d[cnt].x);
    }

    // constrain veolcity if needed
    float vsum = 0.0;
    for (const auto &mvi : d) {
        vsum += mvi.v*mvi.v;
    }
    vsum = sqrt(vsum);
    float vmaxTot = 0.0;
    for (const auto &mvi : d) {
        if (mvi.vmax > vmaxTot) {
            vmaxTot = mvi.vmax;
        }
    }
    if (vsum > vmaxTot) {
        float mul = vmaxTot / vsum;
        //mul *= 0.3; // coefficient can be adjusted
        for (auto &mvi : d) {
            mvi.v *= mul;
        }
    }
}


void Particle::updatePosition(void)
{
    for (unsigned cnt = 0; cnt < d.size(); ++cnt) {
        float dt = 1.0;
        d[cnt].x += d[cnt].v*dt;
    }
}


///
/// \file interpolation.cpp
/// \brief Interpolation methods.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "interpolation.h"


std::vector<float> makeLinearInterpolationOfArray(const std::vector<float> &inx, const std::vector<float> &iny, float outdx)
{
    if (inx.size() != iny.size()) {
        throw std::invalid_argument("Input vectors have different sizes (function makeLinearInterpolationOfArray).");
    }
    if (outdx <= 0.0) {
        throw std::invalid_argument("Increment is not a positive number (function makeLinearInterpolationOfArray).");
    }
    
    std::vector<float> outy;
    float outxcur = 0.0;
    float dxs, dY, dYs;
    for (unsigned inxcnt = 1; inxcnt < inx.size(); ++inxcnt) {
        if (inx[inxcnt] > inx[inxcnt-1] || inx[inxcnt] < inx[inxcnt-1]) {
            while(outxcur <= inx[inxcnt]) {
                dxs = outxcur - inx[inxcnt-1];
                dY = iny[inxcnt] - iny[inxcnt-1];
                dYs = dxs * dY / (inx[inxcnt] - inx[inxcnt-1]);
                outy.push_back(iny[inxcnt-1] + dYs);
                outxcur += outdx;
            }
        }
    }
    
    return outy;
}


float makeLinearInterpolation(const std::vector<float> &inx, const std::vector<float> &iny, float outx)
{
    if (inx.size() != iny.size()) {
        throw std::invalid_argument("Input vectors have different sizes (function makeLinearInterpolationOfArray).");
    }
    
    float outy = 0.0;
    float dxs, dY, dYs;
    for (unsigned inxcnt = 1; inxcnt < inx.size(); ++inxcnt) {
        if ((inx[inxcnt] > inx[inxcnt-1] || inx[inxcnt] < inx[inxcnt-1]) && outx <= inx[inxcnt]) {
            dxs = outx - inx[inxcnt-1];
            dY = iny[inxcnt] - iny[inxcnt-1];
            dYs = dxs * dY / (inx[inxcnt] - inx[inxcnt-1]);
            outy = iny[inxcnt-1] + dYs;
            break;
        }
    }
    
    return outy;
}


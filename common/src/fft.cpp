///
/// \file fft.cpp
/// \brief FFT algorithms.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "fft.h"


std::vector<std::complex<float>> fft(std::vector<std::complex<float>> in, float *ratio)
{
    int size = static_cast<int>(in.size());
    if (size == 0) {
        throw std::invalid_argument("Vector size is zero (function fft).");
    }
    else if (!(size & (size - 1))) {
        return fftct(in);
    }
    else {
        std::vector<std::complex<float>> out = interpolation(in, ratio);
        return fftct(out);
    }
}


std::vector<std::complex<float>> ifft(std::vector<std::complex<float>> in, float *ratio)
{
    int size = static_cast<int>(in.size());
    if (size == 0) {
        throw std::invalid_argument("Vector size is zero (function fft).");
    }
    else if (!(size & (size - 1))) {
        std::vector<std::complex<float>> tmp = ifftct(in);
        float mul = 1.0/static_cast<float>(in.size());
        for (auto &t : tmp) t *= mul;
        return tmp;
    }
    else {
        std::vector<std::complex<float>> out = interpolation(in, ratio);
        std::vector<std::complex<float>> tmp = ifftct(out);
        float mul = 1.0/static_cast<float>(out.size());
        for (auto &t : tmp) t *= mul;
        return tmp;
    }
}


//radix-2 decimation-in-time Cooley-Tukey FFT algorithm
std::vector<std::complex<float>> fftct(std::vector<std::complex<float>> in)
{
    using namespace std::complex_literals;
    using namespace std::numbers;
    unsigned hsize = in.size()/2;
    if (in.size() <= 1) {
        return in;
    }
    else {
        std::vector<std::complex<float>> even(hsize);
        std::vector<std::complex<float>> odd(hsize);
        for (unsigned cnt = 0; cnt < hsize; ++cnt) {
            even[cnt] = in[cnt*2];
            odd[cnt] = in[cnt*2+1];
        }
        auto fir = fftct(even);
        auto sec = fftct(odd);
        for (unsigned cnt = 0; cnt < hsize; ++cnt) {
            auto eo = std::exp(std::complex<float>(-2.0*pi*1.0i)*static_cast<float>(cnt)/static_cast<float>(in.size())) * sec[cnt];
            in[cnt] = fir[cnt] + eo;
            in[hsize+cnt] = fir[cnt] - eo;
        }
    }
    return in;
}


std::vector<std::complex<float>> ifftct(std::vector<std::complex<float>> in)
{
    using namespace std::complex_literals;
    using namespace std::numbers;
    unsigned hsize = in.size()/2;
    if (in.size() <= 1) {
        return in;
    }
    else {
        std::vector<std::complex<float>> even(hsize);
        std::vector<std::complex<float>> odd(hsize);
        for (unsigned cnt = 0; cnt < hsize; ++cnt) {
            even[cnt] = in[cnt*2];
            odd[cnt] = in[cnt*2+1];
        }
        auto fir = ifftct(even);
        auto sec = ifftct(odd);
        for (unsigned cnt = 0; cnt < hsize; ++cnt) {
            auto eo = std::exp(std::complex<float>(2.0*pi*1.0i)*static_cast<float>(cnt)/static_cast<float>(in.size())) * sec[cnt];
            in[cnt] = fir[cnt] + eo;
            in[hsize+cnt] = fir[cnt] - eo;
        }
    }
    return in;
}


std::vector<std::complex<float>> zeroPadding(std::vector<std::complex<float>> in, InputData data)
{
    unsigned nextPowerOfTwo = 1;
    while (nextPowerOfTwo <= in.size()) {
        nextPowerOfTwo = nextPowerOfTwo << 1;
    }
    unsigned diff = nextPowerOfTwo - in.size();
    std::vector<std::complex<float>> out(nextPowerOfTwo);
    
    if (data == InputData::front) {
        for (unsigned cnt = 0; cnt < diff; ++cnt) {
            out[cnt] = 0.0;
        }
        for (unsigned cnt = diff; cnt < nextPowerOfTwo; ++cnt) {
            out[cnt] = in[cnt-diff];
        }
    }
    else if (data == InputData::back) {
        for (unsigned cnt = 0; cnt < in.size(); ++cnt) {
            out[cnt] = in[cnt];
        }
        for (unsigned cnt = in.size(); cnt < nextPowerOfTwo; ++cnt) {
            out[cnt] = 0.0;
        }
    }
    
    return out;
}


//removing the first/last part of the vector to make the size 2^x
std::vector<std::complex<float>> truncation(std::vector<std::complex<float>> in, InputData data)
{
    int prevPowerOfTwo = static_cast<int>(in.size());
    while (prevPowerOfTwo & (prevPowerOfTwo-1)) {
        prevPowerOfTwo = prevPowerOfTwo & (prevPowerOfTwo-1);
    }
    
    if (data == InputData::front) {
        std::vector<std::complex<float>> out(in.begin() + prevPowerOfTwo, in.end());
        return out;
    }
    else {
        std::vector<std::complex<float>> out(in.begin(), in.begin() + prevPowerOfTwo);
        return out;
    }
}


std::vector<std::complex<float>> interpolation(std::vector<std::complex<float>> in, float *ratio)
{
    if (in.size() < 2) {
        throw std::invalid_argument("Vector size should not be less than 2 (function interpolation).");
    }

    unsigned nextPowerOfTwo = 1;
    while (nextPowerOfTwo <= in.size()) {
        nextPowerOfTwo = nextPowerOfTwo << 1;
    }
    
    std::vector<std::complex<float>> out(nextPowerOfTwo);
    
    float dInterp = static_cast<float>(in.size()-1) / static_cast<float>(nextPowerOfTwo-1);
    unsigned xoutcnt = 0;
    
    for (unsigned xincnt = 1; xincnt < in.size(); ++xincnt) {
        while (xoutcnt*dInterp <= static_cast<float>(xincnt)) {
            float dxs = xoutcnt*dInterp - static_cast<float>(xincnt-1);
            std::complex<float> dY = in[xincnt] - in[xincnt-1];
            std::complex<float> dYs = dxs * dY;
            out[xoutcnt] = in[xincnt-1] + dYs;
            xoutcnt++;
        }
    }
    
    if (ratio) {
        *ratio = dInterp;
    }
    return out;
}


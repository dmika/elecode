///
/// \file polynomial.cpp
/// \brief Handling polynomials.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "polynomial.h"


std::vector<long double> multiplyPolynomials(const std::vector<long double> &a, const std::vector<long double> &b)
{
    std::vector<long double> prod(a.size()+b.size()-1);
    for (auto &i : prod) {
        i = 0.0;
    }
    for (long unsigned i = 0; i < a.size(); ++i) {
        for (long unsigned j = 0; j < b.size(); ++j) {
            prod[i+j] += a[i]*b[j];
        }
    }
    return prod;
}


bool findRootNewton(const std::vector<long double> &pol, long double &root)
{
    long double tol = 1.0e-19;
    long unsigned n = 1e8;
    
    for (long unsigned k = 0; k < n; ++k) {
        long double fx = pol[0], fdx = 0.0;
        for (long unsigned i = 1; i < pol.size(); ++i) {
            fx += pol[i]*powl(root, static_cast<long double>(i));
            fdx += static_cast<long double>(i)*pol[i]*powl(root, static_cast<long double>(i)-1.0);
        }
        if (fdx == 0.0) {
            return false;
        }
        else if (fabs(fx) < tol) {
            return true;
        }
        root = root-fx/fdx;
    }
    
    return false;
}


void forwardDeflation(std::vector<long double> &polynomial, const long double &x)
{
    long double p = polynomial[polynomial.size()-1];
    for (long int i = polynomial.size()-2; i >= 0; --i) {
        long double tmp = polynomial[i];
        polynomial[i] = p;
        p = x*p + tmp;
    }
    polynomial.pop_back();
}


std::vector<long double> findRootsWithDeflation(std::function<bool(std::vector<long double>, long double&)> rootfunc, std::vector<long double> pol)
{
    std::vector<long double> roots(pol.size()-1);
    for (long unsigned j = pol.size()-1; j >= 1; --j) {
        long double x = 0.0;
        rootfunc(pol, x);
        roots[j-1] = x;
        forwardDeflation(pol, x);
    }
    return roots;
}


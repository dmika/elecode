///
/// \file vectors.cpp
/// \brief Basic math for vectors (geometry).
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include <cmath>
#include "vectors.h"


float getVectorLength(Vector vec)
{
    return sqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
}


float getDistanceBetweenPoints(Vector first, Vector second)
{
    return getVectorLength(second - first);
}


double getVectorLength(XyPointDouble vec)
{
    return sqrt(vec.x*vec.x + vec.y*vec.y);
}


double getDistanceBetweenPoints(XyPointDouble first, XyPointDouble second)
{
    return getVectorLength(second - first);
}


Vector getDirectionFromPointToPoint(Vector start, Vector end)
{
    Vector diff = end - start;
    float length = getVectorLength(diff);
    if (length > 0.0) {
        return diff/length;
    }
    else {
        throw std::invalid_argument("Vector length should be bigger than zero (function getDirectionFromPointToPoint).");
    }
}


Vector crossProduct(Vector a, Vector b)
{
    Vector c;
    c.x = a.y * b.z - a.z * b.y;
    c.y = a.z * b.x - a.x * b.z;
    c.z = a.x * b.y - a.y * b.x;
    return c;
}


float dotProduct(Vector a, Vector b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}


float getDistanceFromPointToLine(Vector point, Vector firstLinePoint, Vector secondLinePoint)
{
    // Line is defined by two points
    // Distance = |(Point-FirstPoint)x(Point-SecondPoint)| / |SecondPoint-FirstPoint|, where x - cross product
    float distLine = getDistanceBetweenPoints(firstLinePoint, secondLinePoint);
    if (distLine > 0.0) {
        return getVectorLength(crossProduct(point - firstLinePoint, point - secondLinePoint)) / distLine;
    }
    else {
        throw std::invalid_argument("Line length should be bigger than zero (function getDistanceFromPointToLine).");
    }
}


bool getRayPlaneIntersectionPoint(Vector rayOrig, Vector rayDir, Vector planeOrig, Vector planeDir, Vector &inters)
{
    float dist = dotProduct(planeOrig - rayOrig, planeDir) / dotProduct(rayDir, planeDir);
    inters = rayDir * dist + rayOrig;
    
    if (dist > 0.000001) {
        return true;
    }
    else {
        return false;
    }
}


bool getRayTriangleIntersectionPoint(Vector rayOrig, Vector rayDir, Vector a, Vector b, Vector c, Vector &inters)
{
    Vector planeDir = crossProduct(b-a, c-a);
    Vector i;
    getRayPlaneIntersectionPoint(rayOrig, rayDir, a, planeDir, i);
    
    Vector ab = b-a;
    Vector cb = b-c;
    Vector v = ab - cb * dotProduct(cb, ab) / dotProduct(cb, cb);
    float x = 1.0 - dotProduct(v, i-a) / dotProduct(v, ab);
    
    Vector ba = a-b;
    Vector ca = a-c;
    v = ba - ca * dotProduct(ca, ba) / dotProduct(ca, ca);
    float y = 1.0 - dotProduct(v, i-b) / dotProduct(v, ba);
    
    if (x >= 0.0 && x <= 1.0 && y >= 0.0 && y <= 1.0) {
        inters = i;
        return true;
    }
    else {
        return false;
    }
}


Vector rotateVectorWithEulerAngles(Vector inv, float phi, float theta, float psi)
{
    float cos_phi = cos(phi);
    float sin_phi = sin(phi);
    float cos_theta = cos(theta);
    float sin_theta = sin(theta);
    float cos_psi = cos(psi);
    float sin_psi = sin(psi);
    
    Vector resultv;
    resultv.x = inv.x*cos_theta*cos_psi + inv.y*(cos_phi*sin_psi + sin_phi*sin_theta*cos_psi) +
                inv.z*(sin_phi*sin_psi - cos_phi*sin_theta*cos_psi);
    resultv.y = -inv.x*cos_theta*sin_psi + inv.y*(cos_phi*cos_psi - sin_phi*sin_theta*sin_psi) +
                inv.z*(sin_phi*cos_psi + cos_phi*sin_theta*sin_psi);
    resultv.z = inv.x*sin_theta - inv.y*sin_phi*cos_theta + inv.z*cos_phi*cos_theta;
    return resultv;
}


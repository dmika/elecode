///
/// \file integration.cpp
/// \brief Numerical integration.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "integration.h"


float integrateTrapezoidal(std::function<float(float)> func, float a, float b, unsigned stepsNum)
{
    if (stepsNum == 0) {
        throw std::invalid_argument("Number of steps should be larger than zero (function integrateTrapezoidal).");
    }

    float result;
    float delta = (b-a) / static_cast<float>(stepsNum);
    
    result = 0.5 * ( func(a) + func(b) );
    for (unsigned cnt = 1; cnt < stepsNum; ++cnt) {
        result += func(static_cast<float>(cnt)*delta);
    }
    
    return result*delta;
}


float integrateSimpson(std::function<float(float)> func, float a, float b, unsigned depth, float eps)
{
    float trap = 0.5*(b-a)*(func(a)+func(b));
    float m = 0.5*(b+a);
    float simp = (trap + 2.0*(b-a)*func(m))/3.0;
    if ((fabs(simp - trap) <= eps) || depth < 1) {
        return simp;
    }
    else {
        return integrateSimpson(func, a, m, depth-1, eps*0.5) + integrateSimpson(func, m, b, depth-1, eps*0.5);
    }
}


std::vector<EvalPoint> calcTanhSinhEvalPoints(unsigned n)
{
    using namespace std::numbers;
    std::vector<EvalPoint> xw(n);
    
    float h = 4.0/static_cast<float>(n+1);
    float sum = 0.0;
    int k = (1-static_cast<int>(n))/2;
    for(unsigned cnt = 0; cnt < n; ++cnt, ++k) {
        float kh = static_cast<float>(k)*h;
        float ps = 0.5*pi*sinh(kh);
        xw[cnt].x = tanh(ps);
        xw[cnt].w = 0.5*h*pi*cosh(kh) / (cosh(ps)*cosh(ps));
        sum += xw[cnt].w;
    }
    for (auto &w : xw) w.w *= 2.0/sum;
    
    return xw;
}


float integrateTanhSinh(std::function<float(float)> func, float a, float b, unsigned n)
{
    std::vector<EvalPoint> xw = calcTanhSinhEvalPoints(n);
    float res = 0.0;
    for (unsigned cnt = 0; cnt < xw.size(); ++cnt) {
        res += xw[cnt].w * func(0.5*(xw[cnt].x*(b-a)+a+b));
    }
    return 0.5*(b-a)*res;
}


std::vector<float> calcResponseToArbitraryCause(const std::vector<float> &responseToStepCause, const std::vector<float> &arbitraryCause)
{
    std::vector<float> responseToArbitraryCause;
    for(unsigned gcnt = 0; gcnt < responseToStepCause.size(); ++gcnt) {
        float response = 0.0;
        for(unsigned cnt = 0; cnt < gcnt; ++cnt) {
            response += responseToStepCause[gcnt-cnt] * (arbitraryCause[cnt+1] - arbitraryCause[cnt]);
        }
        responseToArbitraryCause.push_back(response);
    }
    return responseToArbitraryCause;
}


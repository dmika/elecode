///
/// \file waveform.cpp
/// \brief Different waveforms.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "waveform.h"


HeidlerParameters::HeidlerParameters(float front, float tail, float exp, float amplitude)
    : mFrontCoef{front}, mTailCoef{tail}, mExponent{exp}, mAmplitude{amplitude}
{
    mCorrectionCoef = std::exp(-(mFrontCoef/mTailCoef)*pow(mExponent*mTailCoef/mFrontCoef, 1.0/mExponent));
}


Sine::Sine(float frequency, float amplitude)
    : mFrequency{frequency}, mAmplitude{amplitude}
{
}


float Sine::getValue(float time)
{
    using namespace std::numbers;
    return mAmplitude * sin(2.0*pi*time*mFrequency);
}


SineStep::SineStep(float front, float amplitude)
    : mFront{front}, mAmplitude{amplitude}
{
}


float SineStep::getValue(float time)
{
    using namespace std::numbers;
    if (time <= mFront) {
        return mAmplitude * (0.5 + 0.5*sin(time*pi/mFront-0.5*pi));
    }
    else {
        return mAmplitude;
    }
}


Gaussian::Gaussian(float b, float c, float amplitude, char type)
    : mB{b}, mC{c}, mAmplitude{amplitude}, mType{type}
{
}


float Gaussian::getValue(float time)
{
    if (mType == 'd') {
        return mAmplitude * exp(-pow(time-mB, 2.0)/(2.0*mC*mC)) * (mB-time)/(mC*mC);
    }
    else if (mType == 'n') {
        // normalized differentiated gaussian (see Stephen D. Gedney Introduction to the
        // Finite-Difference Time-Domain (FDTD) Method for Electromagnetics)
        return mAmplitude * exp(-pow(time-mB, 2.0)/(mC*mC)) * (mB-time)/mC;
    }
    else {
        return mAmplitude * exp(-pow(time-mB, 2.0)/(2.0*mC*mC));
    }
}


Ramp::Ramp(float front, float timeToHalf, float amplitude)
    : mFront{front}, mTimeToHalf{timeToHalf}, mAmplitude{amplitude}
{
}


float Ramp::getValue(float time)
{
    float A1 = mAmplitude/mFront;
    float A2 = -0.5*mAmplitude / (mTimeToHalf-mFront);

    if (time <= mFront) {
        return A1*time;
    }
    else {
        return mAmplitude + A2*(time-mFront);
    }
}


void Ramp::setParameters(float steepness, float amplitude, float timeToHalf, float front)
{
    if (front > 0.0) {
        mFront = front;
    }
    else {
        mFront = amplitude/steepness;
    }
    if (timeToHalf > 0.0) {
        mTimeToHalf = timeToHalf;
    }
    mAmplitude = amplitude;
}


Cigre::Cigre(float front, float timeToHalf, float steepness, float amplitude, char subType)
    : mFront{front}, mTimeToHalf{timeToHalf}, mSteepness{steepness}, mAmplitude{amplitude}, mSubType{subType}
{
    calcVariables();
}


float Cigre::getValue(float time)
{
    float t = time * 1.0e6;
    float kAtoA = 1.0e3;
    if (t < tn) {
        if (mSubType == 'f') {
            return (A*t + B*pow(t, n)) * kAtoA;
        }
        else { //if (mSubType == 's')
            return Sm * t * kAtoA;
        }
    }
    else {
        return (I1 * exp(-(t-tn)/t1) - I2 * exp(-(t-tn)/t2)) * kAtoA;
    }
}


void Cigre::setParameters(float steepness, float amplitude, float timeToHalf, float front)
{
    if (front > 0.0) {
        mFront = front;
    }
    else if (mSteepnessType == SteepnessType::Sm && mSubType == 'f') {
        float cigreSmToS3090 = 24.3/7.2;
        float steepnessEstimate = steepness/cigreSmToS3090;
        mFront = amplitude/steepnessEstimate;
    }
    else {
        mFront = amplitude/steepness;
    }
    if (timeToHalf > 0.0) {
        mTimeToHalf = timeToHalf;
    }
    mSteepness = steepness;
    mAmplitude = amplitude;
    
    calcVariables();
}


void Cigre::calcVariables(void)
{
    float I = mAmplitude * 1.0e-3;
    float tf = mFront * 1.0e6;
    float th = mTimeToHalf * 1.0e6;
    Sm = mSteepness * 1.0e-9;
    
    if (mSubType == 'f') {
        float Sn = Sm * tf / I;
        n = 1.0 + 2.0*(Sn-1.0)*(2.0+1.0/Sn);
        tn = 0.6 * tf * (3.0*Sn*Sn/(1.0+Sn*Sn));
        A = (0.9*I*n/tn - Sm) / (n-1.0);
        B = (Sm*tn - 0.9*I) / (pow(tn,n)*(n-1.0));
    }
    else if (mSubType == 's') {
        if (tf > 0.0) {
            Sm = I/tf;
            tn = 0.9*tf;
        }
        else if (Sm > 0.0) {
            tf = I/Sm;
            tn = 0.9*tf;
        }
    }
    
    t1 = (th-tn) / log(2.0);
    t2 = 0.1 * I / Sm;
    I1 = t1*t2*(Sm+0.9*I/t2) / (t1-t2);
    I2 = t1*t2*(Sm+0.9*I/t1) / (t1-t2);
}


Heidler::Heidler(float front, float tail, float corr, float exp, float amplitude, float tshift)
    : mTimeShift{tshift}
{
    mParameters.push_back(HeidlerParameters(front, tail, exp, amplitude, corr));
}


Heidler::Heidler(std::vector<HeidlerParameters> params, float tshift)
    : mParameters{params}, mTimeShift{tshift}
{
}


float Heidler::getValue(float time)
{
    float t = time + mTimeShift;
    float val = 0.0;
    if (t >= 0.0) {
        for (const auto &p : mParameters) {
            val += (p.mAmplitude/p.mCorrectionCoef) * ( pow(t/p.mFrontCoef, p.mExponent) /
                   (1.0 + pow(t/p.mFrontCoef, p.mExponent)) ) * exp(-t/p.mTailCoef);
        }
    }
    return val;
}


ArrayWaveform::ArrayWaveform(std::vector<float> dataArray, float dataTimeInterval)
    : mDataArray{dataArray}, mDataTimeInterval{dataTimeInterval}
{
}


float ArrayWaveform::getValue(float time)
{
    unsigned step = time/mDataTimeInterval;
    if (step < mDataArray.size()-1) {
        float dts = time - step*mDataTimeInterval;
        float dI = mDataArray[step+1] - mDataArray[step];
        float dIs = dts * dI / mDataTimeInterval;
        return mDataArray[step] + dIs;
    }
    else if (mDataArray.size()) {
        return mDataArray[mDataArray.size()-1];
    }
    return 0.0;
}


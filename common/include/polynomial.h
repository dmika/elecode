///
/// \file polynomial.h
/// \brief Handling polynomials.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef POLYNOMIAL_H__
#define POLYNOMIAL_H__


#include <vector>
#include <functional>
#include <cmath>
#include <stdexcept>


std::vector<long double> multiplyPolynomials(const std::vector<long double> &a, const std::vector<long double> &b);
bool findRootNewton(const std::vector<long double> &pol, long double &root);
void forwardDeflation(std::vector<long double> &polynomial, const long double &x);
std::vector<long double> findRootsWithDeflation(std::function<bool(std::vector<long double>, long double&)> rootfunc, std::vector<long double> pol);


#endif    // POLYNOMIAL_H__


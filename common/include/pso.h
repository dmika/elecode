///
/// \file pso.h
/// \brief Basic particle swarm optimization algorithm. See "Particle Swarm Optimization
/// in Electromagnetics" by Jacob Robinson and Yahya Rahmat-Samii.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef PSO_H__
#define PSO_H__


#include "common.h"
#include <random>
#include <memory>
#include <functional>


class Swarm;
class Particle;
struct ReferenceValue;
struct Dimension;


struct ReferenceValue {
    ReferenceValue() = default;
    ReferenceValue(std::vector<float> xin, float fin) : x{xin}, f{fin} {}
    std::vector<float> x{};
    float f{0.0};
};


struct Dimension {
    Dimension() = default;
    Dimension(float xmi, float xma, float vmi, float vma)
        : xmin{xmi}, xmax{xma}, vmin{vmi}, vmax{vma} {}
    float x{0.0};
    float v{0.0};
    float xmin{0.0};
    float xmax{0.0};
    float vmin{0.0};
    float vmax{0.0};
};


class Swarm {
public:
    Swarm(std::function<float(const std::vector<float>&, const std::vector<Dimension>&)> func,
          const std::vector<ReferenceValue> &ref, const std::vector<Dimension> &dim, unsigned particlesNum = 30);

    void makeStep(void);
    void makeIterations(unsigned iterationsNum);
    Particle calcParametersWithRepeats(unsigned repeats = 100, unsigned iterationsNum = 3000);
    Particle getParameters(void);

    std::vector<ReferenceValue> getReferenceValues(void) const { return mReferenceValues; }
    float getFunctionValue(const std::vector<float> &x, const std::vector<Dimension> &param) { return mFunction(x, param); }
    float getBestFitness(void) const { return gBestFitness; }
    std::vector<Dimension> getBest(void) const { return gBest; }
    std::shared_ptr<std::mt19937> getGen(void) const { return mGen; }
    void setBestFitness(float gbf) { gBestFitness = gbf; }
    void setBest(std::vector<Dimension> gb) { gBest = gb; }

protected:
    void init(unsigned particlesNum);
    std::function<float(const std::vector<float>&, const std::vector<Dimension>&)> mFunction{};
    std::vector<ReferenceValue> mReferenceValues{};
    std::vector<Dimension> d{};
    std::vector<Particle> mParticles{};
    std::vector<Dimension> gBest{};
    float gBestFitness{std::numeric_limits<float>::max()};
    std::shared_ptr<std::mt19937> mGen{nullptr};
};


class Particle {
public:
    Particle(Swarm *swarm);
    ~Particle() {}
    Particle(const Particle&) = default;
    Particle& operator=(const Particle&) = default;

    void makeStep(void);
    float getFitness(void) const { return pBestFitness; }
    void setSwarm(Swarm* swarm) { mSwarm = swarm; }
    std::vector<Dimension> d{};

protected:
    float evaluateFitness(void);
    void updateVelocity(void);
    void updatePosition(void);
    Swarm *mSwarm{nullptr};
    std::vector<Dimension> pBest{};
    float pBestFitness{std::numeric_limits<float>::max()};
    float mc1{1.4};
    float mc2{1.4};
    float mw{0.5};
};


#endif    // PSO_H__


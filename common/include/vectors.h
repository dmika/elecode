///
/// \file vectors.h
/// \brief Basic math for vectors (geometry).
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef VECTORS_H__
#define VECTORS_H__


#include <stdexcept>


enum class XyzDirection
{
    x,
    y,
    z
};


template <class T> class Xy {
public:
    Xy() = default;
    Xy(T xin, T yin) : x{xin}, y{yin} {}

    Xy operator+(const Xy &in) {
        Xy result;
        result.x = x + in.x;
        result.y = y + in.y;
        return result;
    }

    Xy operator-(const Xy &in) {
        Xy result;
        result.x = x - in.x;
        result.y = y - in.y;
        return result;
    }

    Xy operator+(const T &in) {
        Xy result;
        result.x = x + in;
        result.y = y + in;
        return result;
    }

    Xy operator-(const T &in) {
        Xy result;
        result.x = x - in;
        result.y = y - in;
        return result;
    }

    Xy operator*(const T &in) {
        Xy result;
        result.x = x * in;
        result.y = y * in;
        return result;
    }

    Xy operator/(const T &in) {
        Xy result;
        result.x = x / in;
        result.y = y / in;
        return result;
    }

    T x{0};
    T y{0};
};


template <class T> class Xyz {
public:
    Xyz() = default;
    Xyz(T xin, T yin, T zin) : x{xin}, y{yin}, z{zin} {}

    Xyz operator+(const Xyz &in) {
        Xyz result;
        result.x = x + in.x;
        result.y = y + in.y;
        result.z = z + in.z;
        return result;
    }

    Xyz operator-(const Xyz &in) {
        Xyz result;
        result.x = x - in.x;
        result.y = y - in.y;
        result.z = z - in.z;
        return result;
    }

    Xyz operator+(const T &in) {
        Xyz result;
        result.x = x + in;
        result.y = y + in;
        result.z = z + in;
        return result;
    }

    Xyz operator-(const T &in) {
        Xyz result;
        result.x = x - in;
        result.y = y - in;
        result.z = z - in;
        return result;
    }

    Xyz operator*(const T &in) {
        Xyz result;
        result.x = x * in;
        result.y = y * in;
        result.z = z * in;
        return result;
    }

    Xyz operator/(const T &in) {
        Xyz result;
        result.x = x / in;
        result.y = y / in;
        result.z = z / in;
        return result;
    }

    T x{0};
    T y{0};
    T z{0};
};


using Vector = Xyz<float>;
using XyzPointFloat = Xyz<float>;
using XyzSizeFloat = Xyz<float>;
using XyzPointDouble = Xyz<double>;
using XyPointDouble = Xy<double>;


float getVectorLength(Vector vec);
float getDistanceBetweenPoints(Vector first, Vector second);
double getVectorLength(XyPointDouble vec);
double getDistanceBetweenPoints(XyPointDouble first, XyPointDouble second);
Vector getDirectionFromPointToPoint(Vector start, Vector end);
Vector crossProduct(Vector a, Vector b);
float dotProduct(Vector a, Vector b);
float getDistanceFromPointToLine(Vector point, Vector firstLinePoint, Vector secondLinePoint);
bool getRayPlaneIntersectionPoint(Vector rayOrig, Vector rayDir, Vector planeOrig, Vector planeDir, Vector &inters);
bool getRayTriangleIntersectionPoint(Vector rayOrig, Vector rayDir, Vector a, Vector b, Vector c, Vector &inters);
Vector rotateVectorWithEulerAngles(Vector inv, float phi, float theta, float psi);


#endif    // VECTORS_H__


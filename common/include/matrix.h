///
/// \file matrix.h
/// \brief Handling matrices.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef MATRIX_H__
#define MATRIX_H__


#include <vector>
#include <stdexcept>


// The algorithm cannot calculate some inverted matrices. Be careful.
// When accuracy is critical, use a more robust algorithm.
template <typename T>
std::vector<std::vector<T>> getInvertedMatrix(std::vector<std::vector<T>> matrix, bool allowNanResult = false)
{
    unsigned order = matrix.size();

    // augment
    for (unsigned i = 0; i < order; ++i) {
        matrix[i].resize(order*2, 0.0);
        matrix[i][order+i] = 1.0;
    }

    for (unsigned i = 0; i < order; ++i) {
        if (!allowNanResult && std::fpclassify(matrix[i][i]) == FP_ZERO) {
            throw std::invalid_argument("Division by zero.");
        }
        for (unsigned j = 0; j < order; ++j) {
            if (i != j) {
                T coef = matrix[j][i] / matrix[i][i];
                for (unsigned ii = 0; ii < order*2; ++ii) {
                    matrix[j][ii] -= coef * matrix[i][ii];
                }
            }
        }
    }

    for (unsigned i = 0; i < order; ++i) {
        T tmp = matrix[i][i];
        for (unsigned j = 0; j < order; ++j) {
            matrix[i][j] = matrix[i][order+j] / tmp;
        }
        matrix[i].resize(order);
    }

    return matrix;
}


template <typename T>
std::vector<T> solveMatrixEquation(const std::vector<std::vector<T>> &matrix, const std::vector<T> &known)
{
    long unsigned order = matrix.size();
    std::vector<T> result(order);
    
    std::vector<std::vector<T>> invmatrix = getInvertedMatrix(matrix);
    for (long unsigned i = 0; i < order; ++i) {
        T sum = 0;
        for (long unsigned j = 0; j < order; ++j) {
            sum += invmatrix[i][j]*known[j];
        }
        result[i] = sum;
    }
    
    return result;
}


#endif    // MATRIX_H__


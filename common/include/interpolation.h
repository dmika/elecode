///
/// \file interpolation.h
/// \brief Interpolation methods.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef INTERPOLATION_H__
#define INTERPOLATION_H__


#include <vector>
#include <stdexcept>


std::vector<float> makeLinearInterpolationOfArray(const std::vector<float> &inx, const std::vector<float> &iny, float outdx);
float makeLinearInterpolation(const std::vector<float> &inx, const std::vector<float> &iny, float outx);


#endif    // INTERPOLATION_H__


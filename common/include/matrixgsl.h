///
/// \file matrixgsl.h
/// \brief Handling matrices using GSL.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef MATRIXGSL_H__
#define MATRIXGSL_H__


#include <vector>
#include <stdexcept>
#include <gsl/gsl_linalg.h>


template <typename T>
std::vector<std::vector<T>> getInvertedMatrix(const std::vector<std::vector<T>> &matrix)
{
    long unsigned order = matrix.size();
    double *data = new double[order*order];
    for (long unsigned i = 0; i < order; ++i) {
        for (long unsigned j = 0; j < order; ++j) {
            data[i*order+j] = static_cast<double>(matrix[i][j]);
        }
    }
    double inva[order*order];
    gsl_matrix_view m = gsl_matrix_view_array(data, order, order);
    gsl_matrix_view inv = gsl_matrix_view_array(inva, order, order);
    gsl_permutation *p = gsl_permutation_alloc(order);
    int signum;
    gsl_linalg_LU_decomp(&m.matrix, p, &signum);    
    gsl_linalg_LU_invert(&m.matrix, p, &inv.matrix);
    std::vector<std::vector<T>> result = matrix;
    for (long unsigned i = 0; i < order; ++i) {
        for (long unsigned j = 0; j < matrix[i].size(); ++j) {
            result[i][j] = (long double)gsl_matrix_get(&inv.matrix, i, j);
        }
    }
    
    gsl_permutation_free(p);
    delete[] data;
    
    return result;
}


template <typename T>
std::vector<T> solveMatrixEquation(const std::vector<std::vector<T>> &matrix, const std::vector<T> &known)
{
    long unsigned order = matrix.size();
    double *data = new double[order*order];
    for (long unsigned i = 0; i < order; ++i) {
        for (long unsigned j = 0; j < order; ++j) {
            data[i*order+j] = static_cast<double>(matrix[i][j]);
        }
    }
    gsl_vector *inp = gsl_vector_alloc(order);
    gsl_vector *outp = gsl_vector_alloc(order);
    for (long unsigned i = 0; i < order; ++i) {
        inp->data[i] = known[i];
    }
    gsl_matrix_view m = gsl_matrix_view_array(data, order, order);
    gsl_permutation *p = gsl_permutation_alloc(order);
    int signum;
    gsl_linalg_LU_decomp (&m.matrix, p, &signum);    
    gsl_linalg_LU_solve (&m.matrix, p, inp, outp);
    std::vector<T> result(order);
    for (long unsigned i = 0; i < order; ++i) {
        result[i] = outp->data[i];
    }
    
    gsl_permutation_free(p);
    delete[] data;
    gsl_vector_free(inp);
    gsl_vector_free(outp);
    
    return result;
}


#endif    // MATRIXGSL_H__


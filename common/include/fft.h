///
/// \file fft.h
/// \brief FFT algorithms.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef FFT_H__
#define FFT_H__


#include <complex>
#include <vector>
#include <numbers>


enum class InputData {
    front,
    back
};


std::vector<std::complex<float>> fft(std::vector<std::complex<float>> in, float *ratio = nullptr);
std::vector<std::complex<float>> ifft(std::vector<std::complex<float>> in, float *ratio = nullptr);
std::vector<std::complex<float>> fftct(std::vector<std::complex<float>> in);
std::vector<std::complex<float>> ifftct(std::vector<std::complex<float>> in);
std::vector<std::complex<float>> zeroPadding(std::vector<std::complex<float>> in, InputData data = InputData::back);
std::vector<std::complex<float>> truncation(std::vector<std::complex<float>> in, InputData data = InputData::back);
std::vector<std::complex<float>> interpolation(std::vector<std::complex<float>> in, float *ratio = nullptr);


#endif    // FFT_H__


///
/// \file constants.h
/// \brief General physics and math constants.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef CONSTANTS_H__
#define CONSTANTS_H__


const float PI = 3.1415926536;
const float C0 = 2.99792458e8;
const float MU0 = 4.0*PI*1.0e-7;
const float EPS0 = 1.0/(MU0*C0*C0);


#endif    // CONSTANTS_H__


///
/// \file integration.h
/// \brief Numerical integration.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef INTEGRATION_H__
#define INTEGRATION_H__


#include <stdexcept>
#include <functional>
#include <numbers>
#include <cmath>
#include <vector>


struct EvalPoint {
    float x = 0.0;
    float w = 0.0;
};


float integrateTrapezoidal(std::function<float(float)> func, float a, float b, unsigned stepsNum);
float integrateSimpson(std::function<float(float)> func, float a, float b, unsigned depth, float eps = 1.0e-6);
std::vector<EvalPoint> calcTanhSinhEvalPoints(unsigned n);
float integrateTanhSinh(std::function<float(float)> func, float a, float b, unsigned n);
std::vector<float> calcResponseToArbitraryCause(const std::vector<float> &responseToStepCause, const std::vector<float> &arbitraryCause);


#endif    // INTEGRATION_H__


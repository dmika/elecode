///
/// \file waveform.h
/// \brief Different waveforms.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef WAVEFORM_H__
#define WAVEFORM_H__


#include <vector>
#include <cmath>
#include <numbers>


enum class SteepnessType
{
    Sm,
    S1090,
    S3090
};


struct HeidlerParameters {
    HeidlerParameters() = default;
    HeidlerParameters(float front, float tail, float exp, float amplitude);
    HeidlerParameters(float front, float tail, float exp, float amplitude, float corr)
        : mFrontCoef{front}, mTailCoef{tail}, mExponent{exp}, mAmplitude{amplitude}, mCorrectionCoef{corr} {}

    float mFrontCoef{1.0};
    float mTailCoef{1.0};
    float mExponent{10.0};
    float mAmplitude{1.0};
    float mCorrectionCoef{1.0};
};


class Waveform {
public:
    virtual ~Waveform() = default;

    virtual float getValue(float) { return 0.0; }
};


class Sine : public Waveform {
public:
    Sine() = default;
    Sine(float frequency, float amplitude = 1.0);

    virtual float getValue(float time);

    float mFrequency{0.0};
    float mAmplitude{1.0};
};


class SineStep : public Waveform {
public:
    SineStep() = default;
    SineStep(float front, float amplitude = 1.0);

    virtual float getValue(float time);

    float mFront{1.0};
    float mAmplitude{1.0};
};


class Gaussian : public Waveform {
public:
    Gaussian() = default;
    Gaussian(float b, float c, float amplitude = 1.0, char type = 's');

    virtual float getValue(float time);

    float mB{0.0};
    float mC{1.0};
    float mAmplitude{1.0};
    char mType{'s'};
};


class LightningWaveform : public Waveform {
public:
    LightningWaveform() = default;

    virtual void setParameters(float /*steepness*/, float /*amplitude*/, float timeToHalf = 0.0, float front = 0.0) { timeToHalf = timeToHalf; front = front; }
    
    void setSteepnessType(SteepnessType type) { mSteepnessType = type; }
    
    SteepnessType mSteepnessType{SteepnessType::Sm};
};


class Ramp : public LightningWaveform {
public:
    Ramp() = default;
    Ramp(float front, float timeToHalf, float amplitude = 1.0);

    virtual float getValue(float time);
    virtual void setParameters(float steepness, float amplitude, float timeToHalf = 0.0, float front = 0.0);

    float mFront{1.0};
    float mTimeToHalf{1.0};
    float mAmplitude{1.0};
};


class Cigre : public LightningWaveform {
public:
    Cigre() = default;
    Cigre(float front, float timeToHalf, float steepness = 0.0, float amplitude = 1.0, char subType = 'f');

    virtual float getValue(float time);
    virtual void setParameters(float steepness, float amplitude, float timeToHalf = 0.0, float front = 0.0);

    float mFront{1.0};
    float mTimeToHalf{1.0};
    float mSteepness{1.0};
    float mAmplitude{1.0};
    char mSubType{'f'};
    
private:
    void calcVariables(void);
    
    float Sm{};
    float n{};
    float tn{};
    float A{};
    float B{};
    float t1{};
    float t2{};
    float I1{};
    float I2{};
};


class Heidler : public LightningWaveform {
public:
    Heidler() = default;
    Heidler(float front, float tail, float corr, float exp, float amplitude = 1.0, float tshift = 0.0);
    Heidler(std::vector<HeidlerParameters> params, float tshift = 0.0);

    virtual float getValue(float time);

    std::vector<HeidlerParameters> mParameters{};
    float mTimeShift{0.0};
};


class ArrayWaveform : public Waveform {
public:
    ArrayWaveform() = default;
    ArrayWaveform(std::vector<float> dataArray, float dataTimeInterval);

    virtual float getValue(float time);

    std::vector<float> mDataArray{};
    float mDataTimeInterval{0.0};
};


#endif    // WAVEFORM_H__


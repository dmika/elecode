///
/// \file holland.cpp
/// \brief Holland wire model test.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "fdtd.h"


using namespace fdtd;


class TestWire : public TimeStepItem {
public:
    TestWire (std::shared_ptr<Model> model) {
        output.open("holland.txt");

        sectors = 41;
        xc = 30.0;
        yc = 50.5;
        zc = 50.5;
        x = 30;
        y = 50;
        z = 50;

        I = new float[sectors];
        U = new float[sectors+1];
        L = new float[sectors];
        C = new float[sectors+1];

        Ek = new float[sectors];

        Dlw = model->getCellSize();
        Vw = C0;
        Dtw = model->getTimeStepDuration();

        for (unsigned cnt = 0; cnt < sectors; ++cnt) {
            I[cnt] = U[cnt] = Ek[cnt] = 0.0;
            L[cnt] = 8.0e-7;
            C[cnt] = 1.0 / ( Vw * Vw * L[cnt] );
        }
        U[sectors] = 0.0;
        C[sectors] = 1.0 / ( Vw * Vw * L[sectors-1] );


        Ex = model->getBasicParameters().Ex;
        Cax = model->getBasicParameters().Cax;
        Cbx = model->getBasicParameters().Cbx;
    }

    TestWire(const TestWire&) = default;
    TestWire& operator=(const TestWire&) = default;
    TestWire(TestWire&&) = default;
    TestWire& operator=(TestWire&&) = default;
    
    virtual void doFirstHalfStepJob(unsigned) {
        WireToFdtd();
        CalcVoltage();
    };
    
    virtual void doSecondHalfStepJob(unsigned timeStepNumber) {
        FdtdToWire();
        CalcCurrent();
        output << timeStepNumber << "   " << I[20] << std::endl;
    };
    
    void WireToFdtd(void);
    void FdtdToWire(void);
    void CalcCurrent(void);
    void CalcVoltage(void);

    float *I{};
    float *U{};
    float *L{};
    float *C{};

    float *Ek{};

    float Dlw{};
    float Vw{};
    float Dtw{};

    float xc{}, yc{}, zc{};
    unsigned x{}, y{}, z{};
    unsigned sectors{};

    float ***Ex{};
    float ***Cax{};
    float ***Cbx{};

    std::ofstream output{};
};

void TestWire::WireToFdtd(void)
{
    for (unsigned seg = 0; seg < sectors; ++seg) {
        Ex[x+seg][y][z] -= 0.25 * I[seg] * (1.0 / Cax[x+seg][y][z]) * Cbx[x+seg][y][z];
        Ex[x+seg][y+1][z] -= 0.25 * I[seg] * (1.0 / Cax[x+seg][y+1][z]) * Cbx[x+seg][y+1][z];
        Ex[x+seg][y][z+1] -= 0.25 * I[seg] * (1.0 / Cax[x+seg][y][z+1]) * Cbx[x+seg][y][z+1];
        Ex[x+seg][y+1][z+1] -= 0.25 * I[seg] * (1.0 / Cax[x+seg][y+1][z+1]) * Cbx[x+seg][y+1][z+1];
    }
}

void TestWire::FdtdToWire(void)
{
    for (unsigned seg = 0; seg < sectors; ++seg) {
        Ek[seg] = 0.25 * Ex[x+seg][y][z] + 0.25 * Ex[x+seg][y+1][z] + 0.25 * Ex[x+seg][y][z+1] + 0.25 * Ex[x+seg][y+1][z+1];
    }
}

void TestWire::CalcCurrent(void)
{
    for (unsigned seg = 0; seg < sectors; ++seg) {
        I[seg] = I[seg] - ( Dtw * ( U[seg+1] - U[seg] ) / ( L[seg] * Dlw ) - ( Dtw * Ek[seg] ) / L[seg] );
    }
}

void TestWire::CalcVoltage(void)
{
    for (unsigned seg = 1; seg < sectors; ++seg) {
        U[seg] = U[seg] - Dtw * ( I[seg] - I[seg-1] ) / C[seg];
    }
    U[0] = U[0] - 2.0 * Dtw * I[0] / C[0];
    U[sectors] = U[sectors] + 2.0 * Dtw * I[sectors-1] / C[sectors];
}


int main(void)
{
    const float cellsize = 1.0;
    auto fdtdcalc = std::make_unique<Fdtd>(XyzSize(100, 100, 100), cellsize, 0.5);
    auto model = fdtdcalc->getModel();
    
    float dt = model->getTimeStepDuration();
    auto fieldSource = [&](unsigned counter) noexcept
    {
        const float t0 = 12.0e-8;
        const float tw = 4.0e-8;
        return ( -2*(counter*dt-t0)/tw ) * exp( - pow( (counter*dt-t0)/tw, 2.0 ) );
    };
    
    fdtdcalc->addAbc(AbcType::upml, 7);
    fdtdcalc->addTimeStepItem(std::make_shared<TfsfArea>(model, fieldSource, 90.0, 90.0-26.565051));
    fdtdcalc->addTimeStepItem(std::make_shared<TestWire>(model));
    fdtdcalc->calculateTillIteration(2000);
    
    return 0;
}


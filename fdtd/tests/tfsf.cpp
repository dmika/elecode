///
/// \file tfsf.cpp
/// \brief TFSF test.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "fdtd.h"


void saveResult(float ***ex);


void saveResult(float ***ex)
{
    std::ofstream output("tfsf.txt");
    for (int j = 0; j < 100; j += 3) {
        for (int i = 0; i < 100; i += 3) {
            output << ex[i][j][50] << "  ";
        }
        output << std::endl;
    }
    output.close();
}


int main(void)
{
    using namespace fdtd;

    const float cellsize = 1.0;
    auto fdtdcalc = std::make_unique<Fdtd>(XyzSize(100, 100, 100), cellsize);
    auto model = fdtdcalc->getModel();
    
    float dt = model->getTimeStepDuration();
    auto fieldSource = [&](unsigned counter) noexcept
    {
        const float t0 = 12.0e-8;
        const float tw = 4.0e-8;
        return ( -2.0*(counter*dt-t0)/tw ) * exp( - pow( (counter*dt-t0)/tw, 2.0 ) );
    };
    
    fdtdcalc->addAbc(AbcType::upml, 7);
    fdtdcalc->addTimeStepItem(std::make_shared<TfsfArea>(model, fieldSource, 90.0, 90.0-26.565051));
    fdtdcalc->calculateTillIteration(150);
    
    saveResult(model->getBasicParameters().Ex);
    
    return 0;
}


///
/// \file shape.cpp
/// \brief Modeling different shapes. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "shape.h"


namespace fdtd {


void addLossyDielectricBlockToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint,
                                    float epsilon, float sigma, std::vector<float> deltaEpsilon, std::vector<float> tau)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (firstPoint.x > volumeSize.x || firstPoint.y > volumeSize.y || firstPoint.z > volumeSize.z) {
        throw std::invalid_argument("firstPoint coordinates are outside of the calculation volume (function addLossyDielectricBlockToModel).");
    }
    if (secondPoint.x > volumeSize.x || secondPoint.y > volumeSize.y || secondPoint.z > volumeSize.z) {
        throw std::invalid_argument("secondPoint coordinates are outside of the calculation volume (function addLossyDielectricBlockToModel).");
    }

    BasicParameters b = model->getBasicParameters();
    DebyeAdeParameters d = model->getDebyeAdeParameters();
    float dl = model->getCellSize();
    float dti = model->getTimeStepDuration();
    unsigned maxPolesNum = model->getDebyeAdeParameters().PolesNumber;
    unsigned polesnum = deltaEpsilon.size();

    for (unsigned i = firstPoint.x; i < secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j <= secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k <= secondPoint.z; ++k) {
                float dbetasumx = 0.0;
                unsigned pole = 0;
                for (; pole < polesnum; ++pole) {
                    d.kpx[pole][i][j][k] = (1.0 - dti/(2.0*tau[pole])) / (1.0 + dti/(2.0*tau[pole]));
                    d.dbetapx[pole][i][j][k] = (EPS0*deltaEpsilon[pole]*dti/tau[pole]) / ((1.0 + dti/(2.0*tau[pole]))*dti);
                    dbetasumx += d.dbetapx[pole][i][j][k]*dti;
                }
                for (; pole < maxPolesNum; ++pole) {
                    d.kpx[pole][i][j][k] = 0.0;
                    d.dbetapx[pole][i][j][k] = 0.0;
                }
                b.Cax[i][j][k] = (2.0*EPS0*epsilon + dbetasumx - sigma*dti) / (2.0*EPS0*epsilon + dbetasumx + sigma*dti);
                b.Cbx[i][j][k] = 2.0*dti / ((2.0*EPS0*epsilon + dbetasumx + sigma*dti) * dl);
            }
        }
    }
    for (unsigned i = firstPoint.x; i <= secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j < secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k <= secondPoint.z; ++k) {
                float dbetasumy = 0.0;
                unsigned pole = 0;
                for (; pole < polesnum; ++pole) {
                    d.kpy[pole][i][j][k] = (1.0 - dti/(2.0*tau[pole])) / (1.0 + dti/(2.0*tau[pole]));
                    d.dbetapy[pole][i][j][k] = (EPS0*deltaEpsilon[pole]*dti/tau[pole]) / ((1.0 + dti/(2.0*tau[pole]))*dti);
                    dbetasumy += d.dbetapy[pole][i][j][k]*dti;
                }
                for (; pole < maxPolesNum; ++pole) {
                    d.kpy[pole][i][j][k] = 0.0;
                    d.dbetapy[pole][i][j][k] = 0.0;
                }
                b.Cay[i][j][k] = (2.0*EPS0*epsilon + dbetasumy - sigma*dti) / (2.0*EPS0*epsilon + dbetasumy + sigma*dti);
                b.Cby[i][j][k] = 2.0*dti / ((2.0*EPS0*epsilon + dbetasumy + sigma*dti) * dl);
            }
        }
    }
    for (unsigned i = firstPoint.x; i <= secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j <= secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k < secondPoint.z; ++k) {
                float dbetasumz = 0.0;
                unsigned pole = 0;
                for (; pole < polesnum; ++pole) {
                    d.kpz[pole][i][j][k] = (1.0 - dti/(2.0*tau[pole])) / (1.0 + dti/(2.0*tau[pole]));
                    d.dbetapz[pole][i][j][k] = (EPS0*deltaEpsilon[pole]*dti/tau[pole]) / ((1.0 + dti/(2.0*tau[pole]))*dti);
                    dbetasumz += d.dbetapz[pole][i][j][k]*dti;
                }
                for (; pole < maxPolesNum; ++pole) {
                    d.kpz[pole][i][j][k] = 0.0;
                    d.dbetapz[pole][i][j][k] = 0.0;
                }
                b.Caz[i][j][k] = (2.0*EPS0*epsilon + dbetasumz - sigma*dti) / (2.0*EPS0*epsilon + dbetasumz + sigma*dti);
                b.Cbz[i][j][k] = 2.0*dti / ((2.0*EPS0*epsilon + dbetasumz + sigma*dti) * dl);
            }
        }
    }
}


void addLosslessDielectricBlockToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float epsilon)
{
    addLossyDielectricBlockToModel(model, firstPoint, secondPoint, epsilon);
}


void addLosslessMagneticBlockToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float mu)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (firstPoint.x > volumeSize.x || firstPoint.y > volumeSize.y || firstPoint.z > volumeSize.z) {
        throw std::invalid_argument("firstPoint coordinates are outside of the calculation volume (function addLosslessMagneticBlockToModel).");
    }
    if (secondPoint.x > volumeSize.x || secondPoint.y > volumeSize.y || secondPoint.z > volumeSize.z) {
        throw std::invalid_argument("secondPoint coordinates are outside of the calculation volume (function addLosslessMagneticBlockToModel).");
    }

    BasicParameters b = model->getBasicParameters();
    float dl = model->getCellSize();
    float dti = model->getTimeStepDuration();

    for (unsigned i = firstPoint.x; i <= secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j < secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k < secondPoint.z; ++k) {
                b.Dbx[i][j][k] = dti / (MU0*mu*dl);
            }
        }
    }
    for (unsigned i = firstPoint.x; i < secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j <= secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k < secondPoint.z; ++k) {
                b.Dby[i][j][k] = dti / (MU0*mu*dl);
            }
        }
    }
    for (unsigned i = firstPoint.x; i < secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j < secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k <= secondPoint.z; ++k) {
                b.Dbz[i][j][k] = dti / (MU0*mu*dl);
            }
        }
    }
}


void addPerfectConductorBlockToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (firstPoint.x > volumeSize.x || firstPoint.y > volumeSize.y || firstPoint.z > volumeSize.z) {
        throw std::invalid_argument("firstPoint coordinates are outside of the calculation volume (function addPerfectConductorBlockToModel).");
    }
    if (secondPoint.x > volumeSize.x || secondPoint.y > volumeSize.y || secondPoint.z > volumeSize.z) {
        throw std::invalid_argument("secondPoint coordinates are outside of the calculation volume (function addPerfectConductorBlockToModel).");
    }

    BasicParameters b = model->getBasicParameters();

    for (unsigned i = firstPoint.x; i < secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j <= secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k <= secondPoint.z; ++k) {
                b.Cax[i][j][k] = b.Cbx[i][j][k] = 0.0;
            }
        }
    }
    for (unsigned i = firstPoint.x; i <= secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j < secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k <= secondPoint.z; ++k) {
                b.Cay[i][j][k] = b.Cby[i][j][k] = 0.0;
            }
        }
    }
    for (unsigned i = firstPoint.x; i <= secondPoint.x; ++i) {
        for (unsigned j = firstPoint.y; j <= secondPoint.y; ++j) {
            for (unsigned k = firstPoint.z; k < secondPoint.z; ++k) {
                b.Caz[i][j][k] = b.Cbz[i][j][k] = 0.0;
            }
        }
    }
}


}    // namespace fdtd


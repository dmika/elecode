///
/// \file abc.cpp
/// \brief Absorbing boundary conditions for the FDTD. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "abc.h"
#include <cmath>


namespace fdtd {


UpmlAbc::UpmlAbc(std::shared_ptr<Model> model, unsigned thickness)
{
    mModel = model;
    mModel->setAbcThickness(thickness);
    mModel->allocateUpmlParameters();
    initUpmlParameters(mModel);
    mModel->addItemToInit(this);
    mModel->initTimeStepItems();
}


void UpmlAbc::init(void)
{
    // set functions
    if (mModel->getAllocatedParameters().debye) {
        mFirstHalfStepFunction = upmlElectricDebyeAde;
    }
    else {
        mFirstHalfStepFunction = upmlElectric;
    }
    mSecondHalfStepFunction = upmlMagnetic;
}


CpmlAbc::CpmlAbc(std::shared_ptr<Model> model, unsigned thickness,
                 float sigma_coef, float kappa_max,
                 float a_max, float m, float ma, std::string border)
{
    mModel = model;
    mModel->allocateCpmlParameters();
    initCpmlParameters(mModel, sigma_coef, kappa_max, a_max, m, ma, "all"); // init default values
    initParameters(thickness, sigma_coef, kappa_max, a_max, m, ma, border);
    mModel->addItemToInit(this);
    mModel->initTimeStepItems();
}


void CpmlAbc::init(void)
{
    // set functions
    mFirstHalfStepFunction = cpmlElectric;
    mSecondHalfStepFunction = cpmlMagnetic;
}


void CpmlAbc::initParameters(unsigned thickness, float sigma_coef, float kappa_max,
                             float a_max, float m, float ma, std::string border)
{
    mModel->freeCpmlFieldParameters();
    if (border == "xb") {
        mModel->setAbcThicknessBot(XyzSize(thickness, mModel->getAbcThicknessBot().y, mModel->getAbcThicknessBot().z));
    }
    else if (border == "yb") {
        mModel->setAbcThicknessBot(XyzSize(mModel->getAbcThicknessBot().x, thickness, mModel->getAbcThicknessBot().z));
    }
    else if (border == "zb") {
        mModel->setAbcThicknessBot(XyzSize(mModel->getAbcThicknessBot().x, mModel->getAbcThicknessBot().y, thickness));
    }
    else if (border == "xt") {
        mModel->setAbcThicknessTop(XyzSize(thickness, mModel->getAbcThicknessTop().y, mModel->getAbcThicknessTop().z));
    }
    else if (border == "yt") {
        mModel->setAbcThicknessTop(XyzSize(mModel->getAbcThicknessTop().x, thickness, mModel->getAbcThicknessTop().z));
    }
    else if (border == "zt") {
        mModel->setAbcThicknessTop(XyzSize(mModel->getAbcThicknessTop().x, mModel->getAbcThicknessTop().y, thickness));
    }
    else { // border == "all"
        mModel->setAbcThickness(thickness);
    }
    mModel->allocateCpmlFieldParameters();
    initCpmlParameters(mModel, sigma_coef, kappa_max, a_max, m, ma, border);
}


void upmlElectric(std::shared_ptr<Model> model)
{
    XyzPoint top = model->getVolumeSize();
    XyzPoint low = model->getAbcThicknessBot() - 1;
    XyzPoint high = model->getVolumeSize() - model->getAbcThicknessTop();
    BasicParameters b = model->getBasicParameters();
    UpmlParameters u = model->getUpmlParameters();
    // -------------------------------- Ex --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 1; j < top.y; ++j) {
            for (unsigned k = 1; k < top.z; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_ps = u.Psx[i][j][k];
                    float tmp_p = u.Px[i][j][k];
                    
                    u.Psx[i][j][k] = b.Cax[i][j][k] * u.Psx[i][j][k] + b.Cbx[i][j][k] *
                        ( b.Hz[i][j][k] - b.Hz[i][j-1][k] - b.Hy[i][j][k] + b.Hy[i][j][k-1] );
                    
                    u.Px[i][j][k] = u.Cey[0][j] * u.Px[i][j][k] + u.Cey[1][j] * (u.Psx[i][j][k] - tmp_ps);
                    
                    b.Ex[i][j][k] = u.Cez[2][k] * b.Ex[i][j][k] + u.Cez[3][k] *
                        ( u.Cex[4][i] * u.Px[i][j][k] - u.Cex[5][i] * tmp_p );
                }
            }
        }
    }
    // -------------------------------- Ey --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 1; k < top.z; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_ps = u.Psy[i][j][k];
                    float tmp_p = u.Py[i][j][k];
                    
                    u.Psy[i][j][k] = b.Cay[i][j][k] * u.Psy[i][j][k] + b.Cby[i][j][k] *
                        ( b.Hx[i][j][k] - b.Hx[i][j][k-1] - b.Hz[i][j][k] + b.Hz[i-1][j][k] );
                    
                    u.Py[i][j][k] = u.Cez[0][k] * u.Py[i][j][k] + u.Cez[1][k] * (u.Psy[i][j][k] - tmp_ps);
                    
                    b.Ey[i][j][k] = u.Cex[2][i] * b.Ey[i][j][k] + u.Cex[3][i] *
                        ( u.Cey[4][j] * u.Py[i][j][k] - u.Cey[5][j] * tmp_p );
                }
            }
        }
    }
    // -------------------------------- Ez --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < top.x; ++i) {
        for (unsigned j = 1; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_ps = u.Psz[i][j][k];
                    float tmp_p = u.Pz[i][j][k];
                    
                    u.Psz[i][j][k] = b.Caz[i][j][k] * u.Psz[i][j][k] + b.Cbz[i][j][k] *
                        ( b.Hy[i][j][k] - b.Hy[i-1][j][k] - b.Hx[i][j][k] + b.Hx[i][j-1][k] );
                    
                    u.Pz[i][j][k] = u.Cex[0][i] * u.Pz[i][j][k] + u.Cex[1][i] * (u.Psz[i][j][k] - tmp_ps);
                    
                    b.Ez[i][j][k] = u.Cey[2][j] * b.Ez[i][j][k] + u.Cey[3][j] *
                        ( u.Cez[4][k] * u.Pz[i][j][k] - u.Cez[5][k] * tmp_p );
                }
            }
        }
    }
}


void upmlMagnetic(std::shared_ptr<Model> model)
{
    XyzPoint top = model->getVolumeSize();
    XyzPoint low = model->getAbcThicknessBot() - 1;
    XyzPoint high = model->getVolumeSize() - model->getAbcThicknessTop();
    BasicParameters b = model->getBasicParameters();
    UpmlParameters u = model->getUpmlParameters();
    // -------------------------------- Hx --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x+1; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_bs = u.Bsx[i][j][k];
                    float tmp_b = u.Bx[i][j][k];
                    
                    u.Bsx[i][j][k] = u.Bsx[i][j][k] + b.Dbx[i][j][k] *
                        ( b.Ey[i][j][k+1] - b.Ey[i][j][k] - b.Ez[i][j+1][k] + b.Ez[i][j][k] );
                    
                    u.Bx[i][j][k] = u.Chy[0][j] * u.Bx[i][j][k] + u.Chy[1][j] * (u.Bsx[i][j][k] - tmp_bs);
                    
                    b.Hx[i][j][k] = u.Chz[2][k] * b.Hx[i][j][k] + u.Chz[3][k] *
                        ( u.Chx[4][i] * u.Bx[i][j][k] - u.Chx[5][i] * tmp_b );
                }
            }
        }
    }
    // -------------------------------- Hy --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y+1; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_bs = u.Bsy[i][j][k];
                    float tmp_b = u.By[i][j][k];
                    
                    u.Bsy[i][j][k] = u.Bsy[i][j][k] + b.Dby[i][j][k] *
                        ( b.Ez[i+1][j][k] - b.Ez[i][j][k] - b.Ex[i][j][k+1] + b.Ex[i][j][k] );
                    
                    u.By[i][j][k] = u.Chz[0][k] * u.By[i][j][k] + u.Chz[1][k] * (u.Bsy[i][j][k] - tmp_bs);
                    
                    b.Hy[i][j][k] = u.Chx[2][i] * b.Hy[i][j][k] + u.Chx[3][i] *
                        ( u.Chy[4][j] * u.By[i][j][k] - u.Chy[5][j] * tmp_b );
                }
            }
        }
    }
    // -------------------------------- Hz --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z+1; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_bs = u.Bsz[i][j][k];
                    float tmp_b = u.Bz[i][j][k];
                    
                    u.Bsz[i][j][k] = u.Bsz[i][j][k] + b.Dbz[i][j][k] *
                        ( b.Ex[i][j+1][k] - b.Ex[i][j][k] - b.Ey[i+1][j][k] + b.Ey[i][j][k] );
                    
                    u.Bz[i][j][k] = u.Chx[0][i] * u.Bz[i][j][k] + u.Chx[1][i] * (u.Bsz[i][j][k] - tmp_bs);
                    
                    b.Hz[i][j][k] = u.Chy[2][j] * b.Hz[i][j][k] + u.Chy[3][j] *
                        ( u.Chz[4][k] * u.Bz[i][j][k] - u.Chz[5][k] * tmp_b );
                }
            }
        }
    }
}


void upmlElectricDebyeAde(std::shared_ptr<Model> model)
{
    XyzPoint top = model->getVolumeSize();
    XyzPoint low = model->getAbcThicknessBot() - 1;
    XyzPoint high = model->getVolumeSize() - model->getAbcThicknessTop();
    BasicParameters b = model->getBasicParameters();
    UpmlParameters u = model->getUpmlParameters();
    DebyeAdeParameters d = model->getDebyeAdeParameters();
    float cellsize = model->getCellSize();
    // -------------------------------- Ex --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 1; j < top.y; ++j) {
            for (unsigned k = 1; k < top.z; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_ps = u.Psx[i][j][k];
                    float tmp_p = u.Px[i][j][k];
                    
                    float jsumtmp = 0.0;
                    for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                        jsumtmp += (1.0 + d.kpx[pole][i][j][k]) * d.Jx[pole][i][j][k];
                    }
                    
                    u.Psx[i][j][k] = b.Cax[i][j][k] * u.Psx[i][j][k] + b.Cbx[i][j][k] *
                        ( b.Hz[i][j][k] - b.Hz[i][j-1][k] - b.Hy[i][j][k] + b.Hy[i][j][k-1] - 0.5 * jsumtmp * cellsize );
                    
                    u.Px[i][j][k] = u.Cey[0][j] * u.Px[i][j][k] + u.Cey[1][j] * (u.Psx[i][j][k] - tmp_ps);
                    
                    b.Ex[i][j][k] = u.Cez[2][k] * b.Ex[i][j][k] + u.Cez[3][k] *
                        ( u.Cex[4][i] * u.Px[i][j][k] - u.Cex[5][i] * tmp_p );
                    
                    for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                        d.Jx[pole][i][j][k] = d.kpx[pole][i][j][k] * d.Jx[pole][i][j][k] + d.dbetapx[pole][i][j][k] * (u.Psx[i][j][k] - tmp_ps);
                    }
                }
            }
        }
    }
    // -------------------------------- Ey --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 1; k < top.z; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_ps = u.Psy[i][j][k];
                    float tmp_p = u.Py[i][j][k];
                    
                    float jsumtmp = 0.0;
                    for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                        jsumtmp += (1.0 + d.kpy[pole][i][j][k]) * d.Jy[pole][i][j][k];
                    }
                    
                    u.Psy[i][j][k] = b.Cay[i][j][k] * u.Psy[i][j][k] + b.Cby[i][j][k] *
                        ( b.Hx[i][j][k] - b.Hx[i][j][k-1] - b.Hz[i][j][k] + b.Hz[i-1][j][k] - 0.5 * jsumtmp * cellsize );
                    
                    u.Py[i][j][k] = u.Cez[0][k] * u.Py[i][j][k] + u.Cez[1][k] * (u.Psy[i][j][k] - tmp_ps);
                    
                    b.Ey[i][j][k] = u.Cex[2][i] * b.Ey[i][j][k] + u.Cex[3][i] *
                        ( u.Cey[4][j] * u.Py[i][j][k] - u.Cey[5][j] * tmp_p );
                    
                    for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                        d.Jy[pole][i][j][k] = d.kpy[pole][i][j][k] * d.Jy[pole][i][j][k] + d.dbetapy[pole][i][j][k] * (u.Psy[i][j][k] - tmp_ps);
                    }
                }
            }
        }
    }
    // -------------------------------- Ez --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < top.x; ++i) {
        for (unsigned j = 1; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                if (!(i>low.x && i<high.x && j>low.y && j<high.y && k>low.z && k<high.z)) {
                    float tmp_ps = u.Psz[i][j][k];
                    float tmp_p = u.Pz[i][j][k];
                    
                    float jsumtmp = 0.0;
                    for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                        jsumtmp += (1.0 + d.kpz[pole][i][j][k]) * d.Jz[pole][i][j][k];
                    }

                    u.Psz[i][j][k] = b.Caz[i][j][k] * u.Psz[i][j][k] + b.Cbz[i][j][k] *
                        ( b.Hy[i][j][k] - b.Hy[i-1][j][k] - b.Hx[i][j][k] + b.Hx[i][j-1][k] - 0.5 * jsumtmp * cellsize );
                    
                    u.Pz[i][j][k] = u.Cex[0][i] * u.Pz[i][j][k] + u.Cex[1][i] * (u.Psz[i][j][k] - tmp_ps);
                    
                    b.Ez[i][j][k] = u.Cey[2][j] * b.Ez[i][j][k] + u.Cey[3][j] *
                        ( u.Cez[4][k] * u.Pz[i][j][k] - u.Cez[5][k] * tmp_p );
                    
                    for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                        d.Jz[pole][i][j][k] = d.kpz[pole][i][j][k] * d.Jz[pole][i][j][k] + d.dbetapz[pole][i][j][k] * (u.Psz[i][j][k] - tmp_ps);
                    }
                }
            }
        }
    }
}


void cpmlElectric(std::shared_ptr<Model> model)
{
    XyzPoint abcThicknessBot = model->getAbcThicknessBot();
    XyzPoint top = model->getVolumeSize();
    XyzPoint low = abcThicknessBot + 1;
    XyzPoint high = top - model->getAbcThicknessTop();
    BasicParameters b = model->getBasicParameters();
    CpmlParameters c = model->getCpmlParameters();
    // -------------------------------- x --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < low.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                unsigned psii = i;
                c.PsiEzx[psii][j][k] = c.Bex[i] * c.PsiEzx[psii][j][k] + c.Cex[i] * (b.Hy[i][j][k] - b.Hy[i-1][j][k]);
                c.PsiEyx[psii][j][k] = c.Bex[i] * c.PsiEyx[psii][j][k] + c.Cex[i] * (b.Hz[i][j][k] - b.Hz[i-1][j][k]);
                b.Ez[i][j][k] += b.Cbz[i][j][k] * c.PsiEzx[psii][j][k];
                b.Ey[i][j][k] -= b.Cby[i][j][k] * c.PsiEyx[psii][j][k];
            }
        }
    }
    #pragma omp parallel for
    for (unsigned i = high.x; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                unsigned psii = i - high.x + abcThicknessBot.x + 1;
                c.PsiEzx[psii][j][k] = c.Bex[i] * c.PsiEzx[psii][j][k] + c.Cex[i] * (b.Hy[i][j][k] - b.Hy[i-1][j][k]);
                c.PsiEyx[psii][j][k] = c.Bex[i] * c.PsiEyx[psii][j][k] + c.Cex[i] * (b.Hz[i][j][k] - b.Hz[i-1][j][k]);
                b.Ez[i][j][k] += b.Cbz[i][j][k] * c.PsiEzx[psii][j][k];
                b.Ey[i][j][k] -= b.Cby[i][j][k] * c.PsiEyx[psii][j][k];
            }
        }
    }
    // -------------------------------- y --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 1; j < low.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                unsigned psij = j;
                c.PsiExy[i][psij][k] = c.Bey[j] * c.PsiExy[i][psij][k] + c.Cey[j] * (b.Hz[i][j][k] - b.Hz[i][j-1][k]);
                c.PsiEzy[i][psij][k] = c.Bey[j] * c.PsiEzy[i][psij][k] + c.Cey[j] * (b.Hx[i][j][k] - b.Hx[i][j-1][k]);
                b.Ex[i][j][k] += b.Cbx[i][j][k] * c.PsiExy[i][psij][k];
                b.Ez[i][j][k] -= b.Cbz[i][j][k] * c.PsiEzy[i][psij][k];
            }
        }
    }
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = high.y; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                unsigned psij = j - high.y + abcThicknessBot.y + 1;
                c.PsiExy[i][psij][k] = c.Bey[j] * c.PsiExy[i][psij][k] + c.Cey[j] * (b.Hz[i][j][k] - b.Hz[i][j-1][k]);
                c.PsiEzy[i][psij][k] = c.Bey[j] * c.PsiEzy[i][psij][k] + c.Cey[j] * (b.Hx[i][j][k] - b.Hx[i][j-1][k]);
                b.Ex[i][j][k] += b.Cbx[i][j][k] * c.PsiExy[i][psij][k];
                b.Ez[i][j][k] -= b.Cbz[i][j][k] * c.PsiEzy[i][psij][k];
            }
        }
    }
    // -------------------------------- z --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 1; k < low.z; ++k) {
                unsigned psik = k;
                c.PsiEyz[i][j][psik] = c.Bez[k] * c.PsiEyz[i][j][psik] + c.Cez[k] * (b.Hx[i][j][k] - b.Hx[i][j][k-1]);
                c.PsiExz[i][j][psik] = c.Bez[k] * c.PsiExz[i][j][psik] + c.Cez[k] * (b.Hy[i][j][k] - b.Hy[i][j][k-1]);
                b.Ey[i][j][k] += b.Cby[i][j][k] * c.PsiEyz[i][j][psik];
                b.Ex[i][j][k] -= b.Cbx[i][j][k] * c.PsiExz[i][j][psik];
            }
        }
    }
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = high.z; k < top.z; ++k) {
                unsigned psik = k - high.z + abcThicknessBot.z + 1;
                c.PsiEyz[i][j][psik] = c.Bez[k] * c.PsiEyz[i][j][psik] + c.Cez[k] * (b.Hx[i][j][k] - b.Hx[i][j][k-1]);
                c.PsiExz[i][j][psik] = c.Bez[k] * c.PsiExz[i][j][psik] + c.Cez[k] * (b.Hy[i][j][k] - b.Hy[i][j][k-1]);
                b.Ey[i][j][k] += b.Cby[i][j][k] * c.PsiEyz[i][j][psik];
                b.Ex[i][j][k] -= b.Cbx[i][j][k] * c.PsiExz[i][j][psik];
            }
        }
    }
}


void cpmlMagnetic(std::shared_ptr<Model> model)
{
    XyzPoint abcThicknessBot = model->getAbcThicknessBot();
    XyzPoint top = model->getVolumeSize();
    XyzPoint low = abcThicknessBot;
    XyzPoint high = top - model->getAbcThicknessTop();
    BasicParameters b = model->getBasicParameters();
    CpmlParameters c = model->getCpmlParameters();
    // -------------------------------- x --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < low.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                unsigned psii = i;
                c.PsiHyx[psii][j][k] = c.Bhx[i] * c.PsiHyx[psii][j][k] + c.Chx[i] * (b.Ez[i+1][j][k] - b.Ez[i][j][k]);
                c.PsiHzx[psii][j][k] = c.Bhx[i] * c.PsiHzx[psii][j][k] + c.Chx[i] * (b.Ey[i+1][j][k] - b.Ey[i][j][k]);
                b.Hy[i][j][k] += b.Dby[i][j][k] * c.PsiHyx[psii][j][k];
                b.Hz[i][j][k] -= b.Dbz[i][j][k] * c.PsiHzx[psii][j][k];
            }
        }
    }
    #pragma omp parallel for
    for (unsigned i = high.x; i < top.x-1; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                unsigned psii = i - high.x + abcThicknessBot.x + 1;
                c.PsiHyx[psii][j][k] = c.Bhx[i] * c.PsiHyx[psii][j][k] + c.Chx[i] * (b.Ez[i+1][j][k] - b.Ez[i][j][k]);
                c.PsiHzx[psii][j][k] = c.Bhx[i] * c.PsiHzx[psii][j][k] + c.Chx[i] * (b.Ey[i+1][j][k] - b.Ey[i][j][k]);
                b.Hy[i][j][k] += b.Dby[i][j][k] * c.PsiHyx[psii][j][k];
                b.Hz[i][j][k] -= b.Dbz[i][j][k] * c.PsiHzx[psii][j][k];
            }
        }
    }
    // -------------------------------- y --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < low.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                unsigned psij = j;
                c.PsiHzy[i][psij][k] = c.Bhy[j] * c.PsiHzy[i][psij][k] + c.Chy[j] * (b.Ex[i][j+1][k] - b.Ex[i][j][k]);
                c.PsiHxy[i][psij][k] = c.Bhy[j] * c.PsiHxy[i][psij][k] + c.Chy[j] * (b.Ez[i][j+1][k] - b.Ez[i][j][k]);
                b.Hz[i][j][k] += b.Dbz[i][j][k] * c.PsiHzy[i][psij][k];
                b.Hx[i][j][k] -= b.Dbx[i][j][k] * c.PsiHxy[i][psij][k];
            }
        }
    }
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = high.y; j < top.y-1; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                unsigned psij = j - high.y + abcThicknessBot.y + 1;
                c.PsiHzy[i][psij][k] = c.Bhy[j] * c.PsiHzy[i][psij][k] + c.Chy[j] * (b.Ex[i][j+1][k] - b.Ex[i][j][k]);
                c.PsiHxy[i][psij][k] = c.Bhy[j] * c.PsiHxy[i][psij][k] + c.Chy[j] * (b.Ez[i][j+1][k] - b.Ez[i][j][k]);
                b.Hz[i][j][k] += b.Dbz[i][j][k] * c.PsiHzy[i][psij][k];
                b.Hx[i][j][k] -= b.Dbx[i][j][k] * c.PsiHxy[i][psij][k];
            }
        }
    }
    // -------------------------------- z --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < low.z; ++k) {
                unsigned psik = k;
                c.PsiHxz[i][j][psik] = c.Bhz[k] * c.PsiHxz[i][j][psik] + c.Chz[k] * (b.Ey[i][j][k+1] - b.Ey[i][j][k]);
                c.PsiHyz[i][j][psik] = c.Bhz[k] * c.PsiHyz[i][j][psik] + c.Chz[k] * (b.Ex[i][j][k+1] - b.Ex[i][j][k]);
                b.Hx[i][j][k] += b.Dbx[i][j][k] * c.PsiHxz[i][j][psik];
                b.Hy[i][j][k] -= b.Dby[i][j][k] * c.PsiHyz[i][j][psik];
            }
        }
    }
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = high.z; k < top.z-1; ++k) {
                unsigned psik = k - high.z + abcThicknessBot.z + 1;
                c.PsiHxz[i][j][psik] = c.Bhz[k] * c.PsiHxz[i][j][psik] + c.Chz[k] * (b.Ey[i][j][k+1] - b.Ey[i][j][k]);
                c.PsiHyz[i][j][psik] = c.Bhz[k] * c.PsiHyz[i][j][psik] + c.Chz[k] * (b.Ex[i][j][k+1] - b.Ex[i][j][k]);
                b.Hx[i][j][k] += b.Dbx[i][j][k] * c.PsiHxz[i][j][psik];
                b.Hy[i][j][k] -= b.Dby[i][j][k] * c.PsiHyz[i][j][psik];
            }
        }
    }
}


void initUpmlParameters(std::shared_ptr<Model> model)
{
    float dl = model->getCellSize();
    float dti = model->getTimeStepDuration();
    unsigned abcThickness = model->getAbcThicknessBot().x;
    XyzSize volumeSize = model->getVolumeSize();
    UpmlParameters u = model->getUpmlParameters();
    
    // base PML parameters
    float nu = sqrt(MU0/EPS0);                                // characteristic wave impedance
    float m, sigma_max, kappa_max;                            // polynomial grading parameters
    m = 4.0;
    sigma_max = 0.8 * (m + 1.0) / (nu * dl);
    kappa_max = 1.0;
    float dm = static_cast<float>(abcThickness) * dl;         // PML thickness in meters
    
    // PML parameters (lower parts)
    for (unsigned i = 0; i < abcThickness; ++i) {
        unsigned cell = abcThickness - i - 1;
        float sigma_tmp = sigma_max * pow((static_cast<float>(i)+1.0)*dl/dm, m);
        float kappa_tmp = 1.0 + (kappa_max - 1.0) * pow((static_cast<float>(i)+1.0)*dl/dm, m);
        // E field
        u.Cex[0][cell] = u.Cey[0][cell] = u.Cez[0][cell] =
            ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
            ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Cex[1][cell] = u.Cey[1][cell] = u.Cez[1][cell] =
            ( 2.0 * EPS0 ) / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Cex[2][cell] = u.Cey[2][cell] = u.Cez[2][cell] =
            ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
            ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Cex[3][cell] = u.Cey[3][cell] = u.Cez[3][cell] =
            1.0 / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        // H field
        u.Chx[4][cell] = u.Chy[4][cell] = u.Chz[4][cell] = 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti;
        u.Chx[5][cell] = u.Chy[5][cell] = u.Chz[5][cell] = 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti;
        
        sigma_tmp = sigma_max * pow((static_cast<float>(i)+0.5)*dl/dm, m);
        kappa_tmp = 1.0 + (kappa_max - 1.0) * pow((static_cast<float>(i)+0.5)*dl/dm, m);
        // E field 0.5
        u.Cex[4][cell] = u.Cey[4][cell] = u.Cez[4][cell] = 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti;
        u.Cex[5][cell] = u.Cey[5][cell] = u.Cez[5][cell] = 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti;
        // H field 0.5
        u.Chx[0][cell] = u.Chy[0][cell] = u.Chz[0][cell] =
            ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
            ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Chx[1][cell] = u.Chy[1][cell] = u.Chz[1][cell] =
            ( 2.0 * EPS0 ) / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Chx[2][cell] = u.Chy[2][cell] = u.Chz[2][cell] =
            ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
            ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Chx[3][cell] = u.Chy[3][cell] = u.Chz[3][cell] =
            1.0 / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
    }
    
    // PML parameters (higher parts)
    for (unsigned i = 0; i < abcThickness; ++i) {
        XyzPoint cell = volumeSize + i - abcThickness;
        float sigma_tmp = sigma_max * pow(static_cast<float>(i)*dl/dm, m);
        float kappa_tmp = 1.0 + (kappa_max - 1.0) * pow(static_cast<float>(i)*dl/dm, m);
        // E field
        u.Cex[0][cell.x] = u.Cey[0][cell.y] = u.Cez[0][cell.z] =
            ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
            ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Cex[1][cell.x] = u.Cey[1][cell.y] = u.Cez[1][cell.z] =
            ( 2.0 * EPS0 ) / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Cex[2][cell.x] = u.Cey[2][cell.y] = u.Cez[2][cell.z] =
            ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
            ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Cex[3][cell.x] = u.Cey[3][cell.y] = u.Cez[3][cell.z] =
            1.0 / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        // H field
        u.Chx[4][cell.x] = u.Chy[4][cell.y] = u.Chz[4][cell.z] = 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti;
        u.Chx[5][cell.x] = u.Chy[5][cell.y] = u.Chz[5][cell.z] = 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti;
        
        sigma_tmp = sigma_max * pow((static_cast<float>(i)+0.5)*dl/dm, m);
        kappa_tmp = 1.0 + (kappa_max - 1.0) * pow((static_cast<float>(i)+0.5)*dl/dm, m);
        // E field 0.5
        u.Cex[4][cell.x] = u.Cey[4][cell.y] = u.Cez[4][cell.z] = 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti;
        u.Cex[5][cell.x] = u.Cey[5][cell.y] = u.Cez[5][cell.z] = 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti;
        // H field 0.5
        u.Chx[0][cell.x] = u.Chy[0][cell.y] = u.Chz[0][cell.z] =
            ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
            ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Chx[1][cell.x] = u.Chy[1][cell.y] = u.Chz[1][cell.z] =
            ( 2.0 * EPS0 ) / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Chx[2][cell.x] = u.Chy[2][cell.y] = u.Chz[2][cell.z] =
            ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
            ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        u.Chx[3][cell.x] = u.Chy[3][cell.y] = u.Chz[3][cell.z] =
            1.0 / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
    }
}


void initCpmlParameters(std::shared_ptr<Model> model, float sigma_coef, float kappa_max, float a_max, float m, float ma, std::string border)
{
    CpmlParameters c = model->getCpmlParameters();
    
    if (border == "xb") {
        initBottomCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                                 model->getAbcThicknessBot().x, model->getVolumeSize().x, c.Bex, c.Cex, c.Bhx, c.Chx, c.dkex, c.dkhx);
    }
    else if (border == "yb") {
        initBottomCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                                 model->getAbcThicknessBot().y, model->getVolumeSize().y, c.Bey, c.Cey, c.Bhy, c.Chy, c.dkey, c.dkhy);
    }
    else if (border == "zb") {
        initBottomCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                                 model->getAbcThicknessBot().z, model->getVolumeSize().z, c.Bez, c.Cez, c.Bhz, c.Chz, c.dkez, c.dkhz);
    }
    else if (border == "xt") {
        initTopCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                              model->getAbcThicknessTop().x, model->getVolumeSize().x, c.Bex, c.Cex, c.Bhx, c.Chx, c.dkex, c.dkhx);
    }
    else if (border == "yt") {
        initTopCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                              model->getAbcThicknessTop().y, model->getVolumeSize().y, c.Bey, c.Cey, c.Bhy, c.Chy, c.dkey, c.dkhy);
    }
    else if (border == "zt") {
        initTopCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                              model->getAbcThicknessTop().z, model->getVolumeSize().z, c.Bez, c.Cez, c.Bhz, c.Chz, c.dkez, c.dkhz);
    }
    else { // border == "all"
        initBottomCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                                 model->getAbcThicknessBot().x, model->getVolumeSize().x, c.Bex, c.Cex, c.Bhx, c.Chx, c.dkex, c.dkhx);
        initBottomCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                                 model->getAbcThicknessBot().y, model->getVolumeSize().y, c.Bey, c.Cey, c.Bhy, c.Chy, c.dkey, c.dkhy);
        initBottomCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                                 model->getAbcThicknessBot().z, model->getVolumeSize().z, c.Bez, c.Cez, c.Bhz, c.Chz, c.dkez, c.dkhz);
        initTopCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                              model->getAbcThicknessTop().x, model->getVolumeSize().x, c.Bex, c.Cex, c.Bhx, c.Chx, c.dkex, c.dkhx);
        initTopCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                              model->getAbcThicknessTop().y, model->getVolumeSize().y, c.Bey, c.Cey, c.Bhy, c.Chy, c.dkey, c.dkhy);
        initTopCpmlParameters(sigma_coef, kappa_max, a_max, m, ma, model->getCellSize(), model->getTimeStepDuration(),
                              model->getAbcThicknessTop().z, model->getVolumeSize().z, c.Bez, c.Cez, c.Bhz, c.Chz, c.dkez, c.dkhz);
    }
}


void initBottomCpmlParameters(float sigma_coef, float kappa_max, float a_max, float m, float ma,
                              float dl, float dti, unsigned abcThickness, unsigned volumeSize,
                              float *Be, float *Ce, float *Bh, float *Ch, float *dke, float *dkh)
{
    // base PML parameters
    float nu = sqrt(MU0/EPS0);                                   // characteristic wave impedance
    float sigma_max = sigma_coef * 0.8 * (m + 1.0) / (nu * dl);
    float dm = static_cast<float>(abcThickness) * dl;            // PML thickness in meters
    
    // --------------------- CPML COEFFICIENTS ---------------------
    // E field PML parameters
    for (unsigned i = 0; i <= abcThickness; ++i) {
        unsigned cell = i;
        float sigma_tmp = sigma_max * pow(static_cast<float>(abcThickness-i) * dl / dm, m);
        float kappa_tmp = 1.0 + (kappa_max-1.0) * pow(static_cast<float>(abcThickness-i) * dl / dm, m);
        float a_tmp = a_max * pow(static_cast<float>(i) * dl / dm, ma);
        Be[cell] = exp(-(sigma_tmp/kappa_tmp + a_tmp) * dti / EPS0);
        if (sigma_tmp*kappa_tmp + kappa_tmp*kappa_tmp*a_tmp > 0.0) { // if it can be negative then != 0.0
            Ce[cell] = sigma_tmp * (Be[cell]-1.0) / (sigma_tmp*kappa_tmp + kappa_tmp*kappa_tmp*a_tmp);
        }
        else {
            Ce[cell] = 0.0;
        }
    }
    // H field PML parameters
    for (unsigned i = 0; i < abcThickness; ++i) {
        unsigned cell = i;
        float sigma_tmp = sigma_max * pow((static_cast<float>(abcThickness-i-1)+0.5) * dl / dm, m);
        float kappa_tmp = 1.0 + (kappa_max-1.0) * pow((static_cast<float>(abcThickness-i-1)+0.5) * dl / dm, m);
        float a_tmp = a_max * pow((static_cast<float>(i)+0.5) * dl / dm, ma);
        Bh[cell] = exp(-(sigma_tmp/kappa_tmp + a_tmp) * dti / EPS0);
        Ch[cell] = sigma_tmp * (Bh[cell]-1.0) / (sigma_tmp*kappa_tmp + kappa_tmp*kappa_tmp*a_tmp);
    }
    // --------------------- FDTD KAPPA ---------------------
    // default values
    for (unsigned cnt = 0; cnt < volumeSize/2; ++cnt) {
        dke[cnt] = dkh[cnt] = 1.0;
    }
    // E field PML parameters
    for (unsigned i = 0; i <= abcThickness; ++i) {
        unsigned cell = i;
        float kappa_tmp = 1.0 + (kappa_max-1.0) * pow(static_cast<float>(abcThickness-i) * dl / dm, m);
        dke[cell] = 1.0/kappa_tmp;
    }
    // H field PML parameters
    for (unsigned i = 0; i < abcThickness; ++i) {
        unsigned cell = i;
        float kappa_tmp = 1.0 + (kappa_max-1.0) * pow((static_cast<float>(abcThickness-i-1)+0.5) * dl / dm, m);
        dkh[cell] = 1.0/kappa_tmp;
    }
}


void initTopCpmlParameters(float sigma_coef, float kappa_max, float a_max, float m, float ma,
                           float dl, float dti, unsigned abcThickness, unsigned volumeSize,
                           float *Be, float *Ce, float *Bh, float *Ch, float *dke, float *dkh)
{
    // base PML parameters
    float nu = sqrt(MU0/EPS0);                                   // characteristic wave impedance
    float sigma_max = sigma_coef * 0.8 * (m + 1.0) / (nu * dl);
    float dm = static_cast<float>(abcThickness) * dl;            // PML thickness in meters
    unsigned high = volumeSize - abcThickness;
    
    // --------------------- CPML COEFFICIENTS ---------------------
    // E field PML parameters
    for (unsigned i = 0; i < abcThickness; ++i) {
        unsigned cell = high + i;
        float sigma_tmp = sigma_max * pow(static_cast<float>(i) * dl / dm, m);
        float kappa_tmp = 1.0 + (kappa_max-1.0) * pow(static_cast<float>(i) * dl / dm, m);
        float a_tmp = a_max * pow(static_cast<float>(abcThickness-i) * dl / dm, ma);
        Be[cell] = exp(-(sigma_tmp/kappa_tmp + a_tmp) * dti / EPS0);
        if (sigma_tmp*kappa_tmp + kappa_tmp*kappa_tmp*a_tmp > 0.0) { // if it can be negative then != 0.0
            Ce[cell] = sigma_tmp * (Be[cell]-1.0) / (sigma_tmp*kappa_tmp + kappa_tmp*kappa_tmp*a_tmp);
        }
        else {
            Ce[cell] = 0.0;
        }
    }
    // H field PML parameters
    for (unsigned i = 0; i < abcThickness; ++i) {
        unsigned cell = high + i;
        float sigma_tmp = sigma_max * pow((static_cast<float>(i) + 0.5) * dl / dm, m);
        float kappa_tmp = 1.0 + (kappa_max-1.0) * pow((static_cast<float>(i) + 0.5) * dl / dm, m);
        float a_tmp = a_max * pow((static_cast<float>(abcThickness-i) - 0.5) * dl / dm, ma);
        Bh[cell] = exp(-(sigma_tmp/kappa_tmp + a_tmp) * dti / EPS0);
        Ch[cell] = sigma_tmp * (Bh[cell]-1.0) / (sigma_tmp*kappa_tmp + kappa_tmp*kappa_tmp*a_tmp);
    }
    // --------------------- FDTD KAPPA ---------------------
    // default values
    for (unsigned cnt = volumeSize/2; cnt < volumeSize; ++cnt) {
        dke[cnt] = dkh[cnt] = 1.0;
    }
    dkh[volumeSize] = 1.0;
    // E field PML parameters
    for (unsigned i = 0; i < abcThickness; ++i) {
        unsigned cell = high + i;
        float kappa_tmp = 1.0 + (kappa_max-1.0) * pow(static_cast<float>(i) * dl / dm, m);
        dke[cell] = 1.0/kappa_tmp;
    }
    // H field PML parameters
    for (unsigned i = 0; i < abcThickness; ++i) {
        unsigned cell = high + i;
        float kappa_tmp = 1.0 + (kappa_max-1.0) * pow((static_cast<float>(i) + 0.5) * dl / dm, m);
        dkh[cell] = 1.0/kappa_tmp;
    }
}


}    // namespace fdtd


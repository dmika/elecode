///
/// \file basic.cpp
/// \brief Core FDTD parameters and functions. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "basic.h"


namespace fdtd {


Basic::Basic(std::shared_ptr<Model> model)
{
    mModel = model;
    mModel->allocateBasicParameters();
    initBasicParameters(mModel);
    makeMetallicWallBorder(mModel);
    mModel->addItemToInit(this);
    init();
}


void Basic::init()
{
    // set functions
    if (mModel->getAllocatedParameters().cpml && mModel->getAllocatedParameters().cpmlfield) {
        if (mModel->getAllocatedParameters().debye) {
            mFirstHalfStepFunction = basicElectricCpmlDebyeAde;
        }
        else {
            mFirstHalfStepFunction = basicElectricCpml;
        }
        mSecondHalfStepFunction = basicMagneticCpml;
    }
    else {
        if (mModel->getAllocatedParameters().debye) {
            mFirstHalfStepFunction = basicElectricDebyeAde;
        }
        else {
            mFirstHalfStepFunction = basicElectric;
        }
        mSecondHalfStepFunction = basicMagnetic;
    }
}


void basicElectric(std::shared_ptr<Model> model)
{
    XyzPoint bottom;
    XyzPoint top = model->getVolumeSize();
    unsigned add = 1;
    if (model->getAllocatedParameters().upml) {
        bottom = bottom + model->getAbcThicknessBot();
        top = top - model->getAbcThicknessTop();
        add = 0;
    }
    BasicParameters b = model->getBasicParameters();
    // -------------------------------- Ex --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x; i < top.x; ++i) {
        for (unsigned j = bottom.y+add; j < top.y; ++j) {
            for (unsigned k = bottom.z+add; k < top.z; ++k) {
                b.Ex[i][j][k] = b.Cax[i][j][k] * b.Ex[i][j][k] + b.Cbx[i][j][k] *
                    ( b.Hz[i][j][k] - b.Hz[i][j-1][k] - b.Hy[i][j][k] + b.Hy[i][j][k-1] );
            }
        }
    }
    // -------------------------------- Ey --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x+add; i < top.x; ++i) {
        for (unsigned j = bottom.y; j < top.y; ++j) {
            for (unsigned k = bottom.z+add; k < top.z; ++k) {
                b.Ey[i][j][k] = b.Cay[i][j][k] * b.Ey[i][j][k] + b.Cby[i][j][k] *
                    ( b.Hx[i][j][k] - b.Hx[i][j][k-1] - b.Hz[i][j][k] + b.Hz[i-1][j][k] );
            }
        }
    }
    // -------------------------------- Ez --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x+add; i < top.x; ++i) {
        for (unsigned j = bottom.y+add; j < top.y; ++j) {
            for (unsigned k = bottom.z; k < top.z; ++k) {
                b.Ez[i][j][k] = b.Caz[i][j][k] * b.Ez[i][j][k] + b.Cbz[i][j][k] *
                    ( b.Hy[i][j][k] - b.Hy[i-1][j][k] - b.Hx[i][j][k] + b.Hx[i][j-1][k] );
            }
        }
    }
}


void basicMagnetic(std::shared_ptr<Model> model)
{
    XyzPoint bottom;
    XyzPoint top = model->getVolumeSize();
    if (model->getAllocatedParameters().upml) {
        bottom = bottom + model->getAbcThicknessBot();
        top = top - model->getAbcThicknessTop();
    }
    BasicParameters b = model->getBasicParameters();
    // -------------------------------- Hx --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x; i < top.x; ++i) {
        for (unsigned j = bottom.y; j < top.y; ++j) {
            for (unsigned k = bottom.z; k < top.z; ++k) {
                b.Hx[i][j][k] = b.Hx[i][j][k] + b.Dbx[i][j][k] *
                    ( b.Ey[i][j][k+1] - b.Ey[i][j][k] - b.Ez[i][j+1][k] + b.Ez[i][j][k] );
            }
        }
    }
    // -------------------------------- Hy --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x; i < top.x; ++i) {
        for (unsigned j = bottom.y; j < top.y; ++j) {
            for (unsigned k = bottom.z; k < top.z; ++k) {
                b.Hy[i][j][k] = b.Hy[i][j][k] + b.Dby[i][j][k] *
                    ( b.Ez[i+1][j][k] - b.Ez[i][j][k] - b.Ex[i][j][k+1] + b.Ex[i][j][k] );
            }
        }
    }
    // -------------------------------- Hz --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x; i < top.x; ++i) {
        for (unsigned j = bottom.y; j < top.y; ++j) {
            for (unsigned k = bottom.z; k < top.z; ++k) {
                b.Hz[i][j][k] = b.Hz[i][j][k] + b.Dbz[i][j][k] *
                    ( b.Ex[i][j+1][k] - b.Ex[i][j][k] - b.Ey[i+1][j][k] + b.Ey[i][j][k] );
            }
        }
    }
}


void basicElectricDebyeAde(std::shared_ptr<Model> model)
{
    XyzPoint bottom;
    XyzPoint top = model->getVolumeSize();
    unsigned add = 1;
    if (model->getAllocatedParameters().upml) {
        bottom = bottom + model->getAbcThicknessBot();
        top = top - model->getAbcThicknessTop();
        add = 0;
    }
    BasicParameters b = model->getBasicParameters();
    DebyeAdeParameters d = model->getDebyeAdeParameters();
    float cellsize = model->getCellSize();
    // -------------------------------- Ex --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x; i < top.x; ++i) {
        for (unsigned j = bottom.y+add; j < top.y; ++j) {
            for (unsigned k = bottom.z+add; k < top.z; ++k) {
                float jsumtmp = 0.0;
                for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                    d.Jx[pole][i][j][k] = d.kpx[pole][i][j][k] * d.Jx[pole][i][j][k] + d.dbetapx[pole][i][j][k] * (b.Ex[i][j][k] - d.Exs[i][j][k]);
                    jsumtmp += (1.0 + d.kpx[pole][i][j][k]) * d.Jx[pole][i][j][k];
                }

                d.Exs[i][j][k] = b.Ex[i][j][k];
                b.Ex[i][j][k] = b.Cax[i][j][k] * b.Ex[i][j][k] + b.Cbx[i][j][k] *
                    ( b.Hz[i][j][k] - b.Hz[i][j-1][k] - b.Hy[i][j][k] + b.Hy[i][j][k-1] - 0.5 * jsumtmp * cellsize );
            }
        }
    }
    // -------------------------------- Ey --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x+add; i < top.x; ++i) {
        for (unsigned j = bottom.y; j < top.y; ++j) {
            for (unsigned k = bottom.z+add; k < top.z; ++k) {
                float jsumtmp = 0.0;
                for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                    d.Jy[pole][i][j][k] = d.kpy[pole][i][j][k] * d.Jy[pole][i][j][k] + d.dbetapy[pole][i][j][k] * (b.Ey[i][j][k] - d.Eys[i][j][k]);
                    jsumtmp += (1.0 + d.kpy[pole][i][j][k]) * d.Jy[pole][i][j][k];
                }

                d.Eys[i][j][k] = b.Ey[i][j][k];
                b.Ey[i][j][k] = b.Cay[i][j][k] * b.Ey[i][j][k] + b.Cby[i][j][k] *
                    ( b.Hx[i][j][k] - b.Hx[i][j][k-1] - b.Hz[i][j][k] + b.Hz[i-1][j][k] - 0.5 * jsumtmp * cellsize );
            }
        }
    }
    // -------------------------------- Ez --------------------------------
    #pragma omp parallel for
    for (unsigned i = bottom.x+add; i < top.x; ++i) {
        for (unsigned j = bottom.y+add; j < top.y; ++j) {
            for (unsigned k = bottom.z; k < top.z; ++k) {
                float jsumtmp = 0.0;
                for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                    d.Jz[pole][i][j][k] = d.kpz[pole][i][j][k] * d.Jz[pole][i][j][k] + d.dbetapz[pole][i][j][k] * (b.Ez[i][j][k] - d.Ezs[i][j][k]);
                    jsumtmp += (1.0 + d.kpz[pole][i][j][k]) * d.Jz[pole][i][j][k];
                }

                d.Ezs[i][j][k] = b.Ez[i][j][k];
                b.Ez[i][j][k] = b.Caz[i][j][k] * b.Ez[i][j][k] + b.Cbz[i][j][k] *
                    ( b.Hy[i][j][k] - b.Hy[i-1][j][k] - b.Hx[i][j][k] + b.Hx[i][j-1][k] - 0.5 * jsumtmp * cellsize );
            }
        }
    }
}


void basicElectricCpmlDebyeAde(std::shared_ptr<Model> model)
{
    XyzPoint top = model->getVolumeSize();
    BasicParameters b = model->getBasicParameters();
    CpmlParameters c = model->getCpmlParameters();
    DebyeAdeParameters d = model->getDebyeAdeParameters();
    float cellsize = model->getCellSize();
    // -------------------------------- Ex --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 1; j < top.y; ++j) {
            for (unsigned k = 1; k < top.z; ++k) {
                float jsumtmp = 0.0;
                for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                    d.Jx[pole][i][j][k] = d.kpx[pole][i][j][k] * d.Jx[pole][i][j][k] + d.dbetapx[pole][i][j][k] * (b.Ex[i][j][k] - d.Exs[i][j][k]);
                    jsumtmp += (1.0 + d.kpx[pole][i][j][k]) * d.Jx[pole][i][j][k];
                }

                d.Exs[i][j][k] = b.Ex[i][j][k];
                b.Ex[i][j][k] = b.Cax[i][j][k] * b.Ex[i][j][k] + b.Cbx[i][j][k] *
                    ( (b.Hz[i][j][k] - b.Hz[i][j-1][k]) * c.dkey[j] - (b.Hy[i][j][k] - b.Hy[i][j][k-1]) * c.dkez[k] - 0.5 * jsumtmp * cellsize );
            }
        }
    }
    // -------------------------------- Ey --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 1; k < top.z; ++k) {
                float jsumtmp = 0.0;
                for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                    d.Jy[pole][i][j][k] = d.kpy[pole][i][j][k] * d.Jy[pole][i][j][k] + d.dbetapy[pole][i][j][k] * (b.Ey[i][j][k] - d.Eys[i][j][k]);
                    jsumtmp += (1.0 + d.kpy[pole][i][j][k]) * d.Jy[pole][i][j][k];
                }

                d.Eys[i][j][k] = b.Ey[i][j][k];
                b.Ey[i][j][k] = b.Cay[i][j][k] * b.Ey[i][j][k] + b.Cby[i][j][k] *
                    ( (b.Hx[i][j][k] - b.Hx[i][j][k-1]) * c.dkez[k] - (b.Hz[i][j][k] - b.Hz[i-1][j][k]) * c.dkex[i] - 0.5 * jsumtmp * cellsize );
            }
        }
    }
    // -------------------------------- Ez --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < top.x; ++i) {
        for (unsigned j = 1; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                float jsumtmp = 0.0;
                for (unsigned pole = 0; pole < d.PolesNumber; ++pole) {
                    d.Jz[pole][i][j][k] = d.kpz[pole][i][j][k] * d.Jz[pole][i][j][k] + d.dbetapz[pole][i][j][k] * (b.Ez[i][j][k] - d.Ezs[i][j][k]);
                    jsumtmp += (1.0 + d.kpz[pole][i][j][k]) * d.Jz[pole][i][j][k];
                }

                d.Ezs[i][j][k] = b.Ez[i][j][k];
                b.Ez[i][j][k] = b.Caz[i][j][k] * b.Ez[i][j][k] + b.Cbz[i][j][k] *
                    ( (b.Hy[i][j][k] - b.Hy[i-1][j][k]) * c.dkex[i] - (b.Hx[i][j][k] - b.Hx[i][j-1][k]) * c.dkey[j] - 0.5 * jsumtmp * cellsize );
            }
        }
    }
}


void basicElectricCpml(std::shared_ptr<Model> model)
{
    XyzPoint top = model->getVolumeSize();
    BasicParameters b = model->getBasicParameters();
    CpmlParameters c = model->getCpmlParameters();
    // -------------------------------- Ex --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 1; j < top.y; ++j) {
            for (unsigned k = 1; k < top.z; ++k) {
                b.Ex[i][j][k] = b.Cax[i][j][k] * b.Ex[i][j][k] + b.Cbx[i][j][k] *
                    ( (b.Hz[i][j][k] - b.Hz[i][j-1][k]) * c.dkey[j] - (b.Hy[i][j][k] - b.Hy[i][j][k-1]) * c.dkez[k] );
            }
        }
    }
    // -------------------------------- Ey --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 1; k < top.z; ++k) {
                b.Ey[i][j][k] = b.Cay[i][j][k] * b.Ey[i][j][k] + b.Cby[i][j][k] *
                    ( (b.Hx[i][j][k] - b.Hx[i][j][k-1]) * c.dkez[k] - (b.Hz[i][j][k] - b.Hz[i-1][j][k]) * c.dkex[i] );
            }
        }
    }
    // -------------------------------- Ez --------------------------------
    #pragma omp parallel for
    for (unsigned i = 1; i < top.x; ++i) {
        for (unsigned j = 1; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                b.Ez[i][j][k] = b.Caz[i][j][k] * b.Ez[i][j][k] + b.Cbz[i][j][k] *
                    ( (b.Hy[i][j][k] - b.Hy[i-1][j][k]) * c.dkex[i] - (b.Hx[i][j][k] - b.Hx[i][j-1][k]) * c.dkey[j] );
            }
        }
    }
}


void basicMagneticCpml(std::shared_ptr<Model> model)
{
    XyzPoint top = model->getVolumeSize();
    BasicParameters b = model->getBasicParameters();
    CpmlParameters c = model->getCpmlParameters();
    // -------------------------------- Hx --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                b.Hx[i][j][k] =  b.Hx[i][j][k] + b.Dbx[i][j][k] *
                    ( (b.Ey[i][j][k+1] - b.Ey[i][j][k]) * c.dkhz[k] - (b.Ez[i][j+1][k] - b.Ez[i][j][k]) * c.dkhy[j] );
            }
        }
    }
    // -------------------------------- Hy --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                b.Hy[i][j][k] = b.Hy[i][j][k] + b.Dby[i][j][k] *
                    ( (b.Ez[i+1][j][k] - b.Ez[i][j][k]) * c.dkhx[i] - (b.Ex[i][j][k+1] - b.Ex[i][j][k]) * c.dkhz[k] );
            }
        }
    }
    // -------------------------------- Hz --------------------------------
    #pragma omp parallel for
    for (unsigned i = 0; i < top.x; ++i) {
        for (unsigned j = 0; j < top.y; ++j) {
            for (unsigned k = 0; k < top.z; ++k) {
                b.Hz[i][j][k] = b.Hz[i][j][k] + b.Dbz[i][j][k] *
                    ( (b.Ex[i][j+1][k] - b.Ex[i][j][k]) * c.dkhy[j] - (b.Ey[i+1][j][k] - b.Ey[i][j][k]) * c.dkhx[i] );
            }
        }
    }
}


void initBasicParameters(std::shared_ptr<Model> model)
{
    addLosslessDielectricBlockToModel(model, XyzPoint(0, 0, 0), model->getVolumeSize());
    addLosslessMagneticBlockToModel(model, XyzPoint(0, 0, 0), model->getVolumeSize());
}


void makeMetallicWallBorder(std::shared_ptr<Model> model)
{
    XyzSize volumeSize = model->getVolumeSize();
    addPerfectConductorBlockToModel(model, XyzPoint(0, 0, 0), XyzPoint(0, volumeSize.y, volumeSize.z));
    addPerfectConductorBlockToModel(model, XyzPoint(volumeSize.x, 0, 0), XyzPoint(volumeSize.x, volumeSize.y, volumeSize.z));
    addPerfectConductorBlockToModel(model, XyzPoint(0, 0, 0), XyzPoint(volumeSize.x, 0, volumeSize.z));
    addPerfectConductorBlockToModel(model, XyzPoint(0, volumeSize.y, 0), XyzPoint(volumeSize.x, volumeSize.y, volumeSize.z));
    addPerfectConductorBlockToModel(model, XyzPoint(0, 0, 0), XyzPoint(volumeSize.x, volumeSize.y, 0));
    addPerfectConductorBlockToModel(model, XyzPoint(0, 0, volumeSize.z), XyzPoint(volumeSize.x, volumeSize.y, volumeSize.z));
}


}    // namespace fdtd


///
/// \file wire.cpp
/// \brief Modeling thin wires. See Railton's articles and Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "wire.h"


namespace fdtd {


void addRailtonThinWireToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float diameter, char wireSubType)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (firstPoint.x > volumeSize.x || firstPoint.y > volumeSize.y || firstPoint.z > volumeSize.z) {
        throw std::invalid_argument("firstPoint coordinates are outside of the calculation volume (function addRailtonThinWireToModel).");
    }
    if (secondPoint.x > volumeSize.x || secondPoint.y > volumeSize.y || secondPoint.z > volumeSize.z) {
        throw std::invalid_argument("secondPoint coordinates are outside of the calculation volume (function addRailtonThinWireToModel).");
    }

    XyzDirection direction = XyzDirection::x;
    unsigned length;
    if (secondPoint.x != firstPoint.x) {
        direction = XyzDirection::x;
        if (firstPoint.x > secondPoint.x) {
            unsigned temp = secondPoint.x;
            secondPoint.x = firstPoint.x;
            firstPoint.x = temp;
        }
        length = secondPoint.x - firstPoint.x;
    }
    else if (secondPoint.y != firstPoint.y) {
        direction = XyzDirection::y;
        if (firstPoint.y > secondPoint.y) {
            unsigned temp = secondPoint.y;
            secondPoint.y = firstPoint.y;
            firstPoint.y = temp;
        }
        length = secondPoint.y - firstPoint.y;
    }
    else {
        direction = XyzDirection::z;
        if (firstPoint.z > secondPoint.z) {
            unsigned temp = secondPoint.z;
            secondPoint.z = firstPoint.z;
            firstPoint.z = temp;
        }
        length = secondPoint.z - firstPoint.z;
    }

    // railtonCoef = (pi/2) / log(dl/radius);
    // cb /= railtonCoef;       // here we need to change only one epsilon in cb -> epsilon *= railtonCoef,
                                // sigma correction is canceled by another epsilon in denominator
    // db *= railtonCoef;       // we need to change only one mu in db -> mu /= railtonCoef
    float dl = model->getCellSize();
    float railtonCoef;
    if (diameter > 0.00001*dl) {
        railtonCoef = (PI/2.0) / log(dl/(diameter*0.5));
    }
    else {
        railtonCoef = 1.0;
    }

    unsigned i = firstPoint.x;
    unsigned j = firstPoint.y;
    unsigned k = firstPoint.z;
    if (direction == XyzDirection::x) {
        for (; i < firstPoint.x + length; ++i) {
            alterWireParameters(model, direction, XyzPoint(i, j, k), railtonCoef);
        }
    }
    else if (direction == XyzDirection::y) {
        for (; j < firstPoint.y + length; ++j) {
            alterWireParameters(model, direction, XyzPoint(i, j, k), railtonCoef);
        }
    }
    else if (direction == XyzDirection::z) {
        for (; k < firstPoint.z + length; ++k) {
            alterWireParameters(model, direction, XyzPoint(i, j, k), railtonCoef);
        }
    }
    // altering parameters near the wire tip
    alterRadialElectricParameters(model, direction, XyzPoint(i, j, k), 1.0/railtonCoef);
    alterLongitudinalMagneticParameters(model, direction, XyzPoint(i, j, k), railtonCoef);
    if (wireSubType == 't') {
        alterRotorMagneticParameters(model, direction, XyzPoint(i, j, k), railtonCoef);
    }
}


void addStaircaseWireToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, bool isCorrected)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (firstPoint.x > volumeSize.x || firstPoint.y > volumeSize.y || firstPoint.z > volumeSize.z) {
        throw std::invalid_argument("firstPoint coordinates are outside of the calculation volume (function addStaircaseWireToModel).");
    }
    if (secondPoint.x > volumeSize.x || secondPoint.y > volumeSize.y || secondPoint.z > volumeSize.z) {
        throw std::invalid_argument("secondPoint coordinates are outside of the calculation volume (function addStaircaseWireToModel).");
    }

    // experimental correction of wrong propagation speed
    float efactor = 1.0;
    if (isCorrected) {
        float fxcoor = static_cast<float>(firstPoint.x);
        float fycoor = static_cast<float>(firstPoint.y);
        float fzcoor = static_cast<float>(firstPoint.z);
        float sxcoor = static_cast<float>(secondPoint.x);
        float sycoor = static_cast<float>(secondPoint.y);
        float szcoor = static_cast<float>(secondPoint.z);
        float len = sqrt( (sxcoor-fxcoor)*(sxcoor-fxcoor) + (sycoor-fycoor)*(sycoor-fycoor) + (szcoor-fzcoor)*(szcoor-fzcoor) );
        float lens = fabs(sxcoor-fxcoor) + fabs(sycoor-fycoor) + fabs(szcoor-fzcoor);
        efactor = 1.7 * (len/lens) - 0.7;
        efactor = 1.0 / efactor;
    }

    XyzPoint runningPoint = firstPoint;
    while ( ( secondPoint.x >= firstPoint.x ? runningPoint.x <= secondPoint.x : runningPoint.x >= secondPoint.x ) &&
            ( secondPoint.y >= firstPoint.y ? runningPoint.y <= secondPoint.y : runningPoint.y >= secondPoint.y ) &&
            ( secondPoint.z >= firstPoint.z ? runningPoint.z <= secondPoint.z : runningPoint.z >= secondPoint.z ) ) {
        if (runningPoint.x == secondPoint.x && runningPoint.y == secondPoint.y && runningPoint.z == secondPoint.z) {
            break;
        }

        float dist_x, dist_y, dist_z;
        Vector first(firstPoint.x, firstPoint.y, firstPoint.z);
        Vector second(secondPoint.x, secondPoint.y, secondPoint.z);
        if (secondPoint.x >= firstPoint.x) {
            dist_x = getDistanceFromPointToLine(Vector(runningPoint.x+1, runningPoint.y, runningPoint.z), first, second);
        }
        else {
            dist_x = getDistanceFromPointToLine(Vector(runningPoint.x-1, runningPoint.y, runningPoint.z), first, second);
        }
        if (secondPoint.y >= firstPoint.y) {
            dist_y = getDistanceFromPointToLine(Vector(runningPoint.x, runningPoint.y+1, runningPoint.z), first, second);
        }
        else {
            dist_y = getDistanceFromPointToLine(Vector(runningPoint.x, runningPoint.y-1, runningPoint.z), first, second);
        }
        if (secondPoint.z >= firstPoint.z) {
            dist_z = getDistanceFromPointToLine(Vector(runningPoint.x, runningPoint.y, runningPoint.z+1), first, second);
        }
        else {
            dist_z = getDistanceFromPointToLine(Vector(runningPoint.x, runningPoint.y, runningPoint.z-1), first, second);
        }

        if (dist_x <= dist_y && dist_x <= dist_z ) {
            if (secondPoint.x >= firstPoint.x) {
                model->setCax(runningPoint, 0.0);
                model->setCbx(runningPoint, 0.0);
                alterRadialElectricParameters(model, XyzDirection::x, runningPoint, efactor);
                runningPoint.x += 1;
            }
            else {
                runningPoint.x -= 1;
                model->setCax(runningPoint, 0.0);
                model->setCbx(runningPoint, 0.0);
                alterRadialElectricParameters(model, XyzDirection::x, runningPoint, efactor);
            }
        }
        else if (dist_y <= dist_x && dist_y <= dist_z) {
            if (secondPoint.y >= firstPoint.y) {
                model->setCay(runningPoint, 0.0);
                model->setCby(runningPoint, 0.0);
                alterRadialElectricParameters(model, XyzDirection::y, runningPoint, efactor);
                runningPoint.y += 1;
            }
            else {
                runningPoint.y -= 1;
                model->setCay(runningPoint, 0.0);
                model->setCby(runningPoint, 0.0);
                alterRadialElectricParameters(model, XyzDirection::y, runningPoint, efactor);
            }
        }
        else if (dist_z <= dist_x && dist_z <= dist_y) {
            if (secondPoint.z >= firstPoint.z) {
                model->setCaz(runningPoint, 0.0);
                model->setCbz(runningPoint, 0.0);
                alterRadialElectricParameters(model, XyzDirection::z, runningPoint, efactor);
                runningPoint.z += 1;
            }
            else {
                runningPoint.z -= 1;
                model->setCaz(runningPoint, 0.0);
                model->setCbz(runningPoint, 0.0);
                alterRadialElectricParameters(model, XyzDirection::z, runningPoint, efactor);
            }
        }
    }
}


void alterWireParameters(std::shared_ptr<Model> model, XyzDirection direction, XyzPoint coord, float railtonCoef)
{
    model->setCaInCell(direction, coord, 0.0);
    model->setCbInCell(direction, coord, 0.0);
    alterRadialElectricParameters(model, direction, coord, 1.0/railtonCoef);
    alterRotorMagneticParameters(model, direction, coord, railtonCoef);
    alterLongitudinalMagneticParameters(model, direction, coord, railtonCoef);
}


void alterRadialElectricParameters(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord, float coef)
{
    if (dir == XyzDirection::x) {
        model->mulCby(coord, coef);
        model->mulDbetapy(coord, 1.0/coef);
        model->mulCbz(coord, coef);
        model->mulDbetapz(coord, 1.0/coef);
        model->mulCby(XyzPoint(coord.x, coord.y-1, coord.z), coef);
        model->mulDbetapy(XyzPoint(coord.x, coord.y-1, coord.z), 1.0/coef);
        model->mulCbz(XyzPoint(coord.x, coord.y, coord.z-1), coef);
        model->mulDbetapz(XyzPoint(coord.x, coord.y, coord.z-1), 1.0/coef);
    }
    else if (dir == XyzDirection::y) {
        model->mulCbx(coord, coef);
        model->mulDbetapx(coord, 1.0/coef);
        model->mulCbz(coord, coef);
        model->mulDbetapz(coord, 1.0/coef);
        model->mulCbx(XyzPoint(coord.x-1, coord.y, coord.z), coef);
        model->mulDbetapx(XyzPoint(coord.x-1, coord.y, coord.z), 1.0/coef);
        model->mulCbz(XyzPoint(coord.x, coord.y, coord.z-1), coef);
        model->mulDbetapz(XyzPoint(coord.x, coord.y, coord.z-1), 1.0/coef);
    }
    else if (dir == XyzDirection::z) {
        model->mulCbx(coord, coef);
        model->mulDbetapx(coord, 1.0/coef);
        model->mulCby(coord, coef);
        model->mulDbetapy(coord, 1.0/coef);
        model->mulCbx(XyzPoint(coord.x-1, coord.y, coord.z), coef);
        model->mulDbetapx(XyzPoint(coord.x-1, coord.y, coord.z), 1.0/coef);
        model->mulCby(XyzPoint(coord.x, coord.y-1, coord.z), coef);
        model->mulDbetapy(XyzPoint(coord.x, coord.y-1, coord.z), 1.0/coef);
    }
}


void alterRotorMagneticParameters(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord, float coef)
{
    if (dir == XyzDirection::x) {
        model->mulDby(coord, coef);
        model->mulDbz(coord, coef);
        model->mulDby(XyzPoint(coord.x, coord.y, coord.z-1), coef);
        model->mulDbz(XyzPoint(coord.x, coord.y-1, coord.z), coef);
    }
    else if (dir == XyzDirection::y) {
        model->mulDbx(coord, coef);
        model->mulDbz(coord, coef);
        model->mulDbx(XyzPoint(coord.x, coord.y, coord.z-1), coef);
        model->mulDbz(XyzPoint(coord.x-1, coord.y, coord.z), coef);
    }
    else if (dir == XyzDirection::z) {
        model->mulDbx(coord, coef);
        model->mulDby(coord, coef);
        model->mulDbx(XyzPoint(coord.x, coord.y-1, coord.z), coef);
        model->mulDby(XyzPoint(coord.x-1, coord.y, coord.z), coef);
    }
}


void alterLongitudinalMagneticParameters(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord, float coef)
{
    if (dir == XyzDirection::x) {
        model->mulDbx(coord, coef);
        model->mulDbx(XyzPoint(coord.x, coord.y-1, coord.z), coef);
        model->mulDbx(XyzPoint(coord.x, coord.y, coord.z-1), coef);
        model->mulDbx(XyzPoint(coord.x, coord.y-1, coord.z-1), coef);
    }
    else if (dir == XyzDirection::y) {
        model->mulDby(coord, coef);
        model->mulDby(XyzPoint(coord.x-1, coord.y, coord.z), coef);
        model->mulDby(XyzPoint(coord.x, coord.y, coord.z-1), coef);
        model->mulDby(XyzPoint(coord.x-1, coord.y, coord.z-1), coef);
    }
    else if (dir == XyzDirection::z) {
        model->mulDbz(coord, coef);
        model->mulDbz(XyzPoint(coord.x-1, coord.y, coord.z), coef);
        model->mulDbz(XyzPoint(coord.x, coord.y-1, coord.z), coef);
        model->mulDbz(XyzPoint(coord.x-1, coord.y-1, coord.z), coef);
    }
}


}    // namespace fdtd


///
/// \file obliquewire.cpp
/// \brief Implementation of the oblique thin wire model.
/// See "New Oblique Thin Wire Formalism in the FDTD Method With Multiwire Junctions"
/// by C. Guiffaut, A. Reineix, B. Pecqueux;
/// "Insulated Oblique Thin Wire Formalism in the FDTD Method"
/// by C. Guiffaut, N. Rouvrais, A. Reineix, B. Pecqueux;
/// "Extension of Thin Wire Techniques in the FDTD Method for Debye Media"
/// by D. Kuklin.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "obliquewire.h"
#include "integration.h"
#include <cmath>


namespace fdtd {


SegmentPart *spsav;
float xsav, ysav, xx1, xx2, yy1, yy2, zz1, zz2;


float calcInduct(SegmentPart *spart, float point_x, float point_y, float point_z)
{
    float ro = 0.0;
    
    try {
        ro = getDistanceFromPointToLine(Vector(point_x, point_y, point_z), spart->mStart, spart->mEnd);
    }
    catch (std::exception const& e) {
        ro = 0.0;
    }
    
    if (ro > spart->mRadius) {
        return log(ro/spart->mRadius);
    }
    else {
        return 0.0;
    }
}


float f3(float z)
{
    return calcInduct(spsav, xsav, ysav, z);
}


float f2(float y)
{
    ysav = y;
    return integrateSimpson(f3, zz1, zz2, 10, 0.001);
}


float f1(float x)
{
    xsav = x;
    return integrateSimpson(f2, yy1, yy2, 10, 0.001);
}


bool getRayIntersectionWithCube(Vector orig, Vector dir, Vector cubeStart, Vector cubeEnd, XyzDirection direction, XyzPointFloat *result)
{
    Vector V0, V1;
    
    if (direction == XyzDirection::z) {
        if (dir.z >= 0) {
            V0 = Vector(0.0, 0.0, cubeEnd.z);
            V1 = Vector(0.0, 0.0, -1.0);
            if (getRayPlaneIntersectionPoint(orig, dir, V0, V1, *result)) return true;
        }
        else {
            V0 = Vector(0.0, 0.0, cubeStart.z);
            V1 = Vector(0.0, 0.0, 1.0);
            if (getRayPlaneIntersectionPoint(orig, dir, V0, V1, *result)) return true;
        }
    }
    
    if (direction == XyzDirection::x) {
        if (dir.x >= 0) {
            V0 = Vector(cubeEnd.x, 0.0, 0.0);
            V1 = Vector(-1.0, 0.0, 0.0);
            if (getRayPlaneIntersectionPoint(orig, dir, V0, V1, *result)) return true;
        }
        else {
            V0 = Vector(cubeStart.x, 0.0, 0.0);
            V1 = Vector(1.0, 0.0, 0.0);
            if (getRayPlaneIntersectionPoint(orig, dir, V0, V1, *result)) return true;
        }
    }
    
    if (direction == XyzDirection::y) {
        if (dir.y >= 0) {
            V0 = Vector(0.0, cubeEnd.y, 0.0);
            V1 = Vector(0.0, -1.0, 0.0);
            if (getRayPlaneIntersectionPoint(orig, dir, V0, V1, *result)) return true;
        }
        else {
            V0 = Vector(0.0, cubeStart.y, 0.0);
            V1 = Vector(0.0, 1.0, 0.0);
            if (getRayPlaneIntersectionPoint(orig, dir, V0, V1, *result)) return true;
        }
    }
    
    return false;
}


bool getSegmentPartIntersectionWithCube(SegmentPart &sp, XyzDirection direction, XyzPointFloat *result)
{
    Vector orig = sp.getStart();
    float cellSize = sp.getCellSize();
    
    unsigned startCellx, startCelly, startCellz;
    float shift = 1.0e-5*cellSize; // to deal with points on the border
    for (startCellx = 0; static_cast<float>(startCellx)*cellSize < (orig.x+shift)-cellSize; ++startCellx);
    for (startCelly = 0; static_cast<float>(startCelly)*cellSize < (orig.y+shift)-cellSize; ++startCelly);
    for (startCellz = 0; static_cast<float>(startCellz)*cellSize < (orig.z+shift)-cellSize; ++startCellz);
    
    Vector start(static_cast<float>(startCellx)*cellSize, static_cast<float>(startCelly)*cellSize, static_cast<float>(startCellz)*cellSize);
    Vector end(static_cast<float>(startCellx+1)*cellSize, static_cast<float>(startCelly+1)*cellSize, static_cast<float>(startCellz+1)*cellSize);

    return getRayIntersectionWithCube(orig, sp.getDirection(), start, end, direction, result);
}


WireNode::~WireNode()
{
}


SegmentPart::SegmentPart(const Model &fdtdvol, XyzPointFloat start, XyzPointFloat end, float diam, float sheath)
    : mCellSize(fdtdvol.getCellSize()), mRadius(diam/2.0), mSheathRadius(sheath/2.0)
{
    mStart = start;
    mEnd = end;
    mEx = fdtdvol.getBasicParameters().Ex;
    mEy = fdtdvol.getBasicParameters().Ey;
    mEz = fdtdvol.getBasicParameters().Ez;
    initLengthDirectionCell();
}


float SegmentPart::calcIntegralAdsimp(SegmentPart *spart, float x_coord, float y_coord, float z_coord, bool totalInductance)
{
    xx1 = x_coord;
    xx2 = x_coord + mCellSize;
    yy1 = y_coord;
    yy2 = y_coord + mCellSize;
    zz1 = z_coord;
    zz2 = z_coord + mCellSize;
    if (totalInductance) {
        spsav = spart;
        return integrateSimpson(f1, xx1, xx2, 10, 0.001);
    }
    else {
        XyzPointFloat st = spart->getStart();
        st = st + 1.0e-5*mCellSize; // to deal with points on the border
        XyzPointFloat first;
        XyzPointFloat second;
        float vol = 0.0;
        if (st.x >= xx1 && st.x < xx2 && st.y >= yy1 && st.y < yy2 && st.z >= zz1 && st.z < zz2) {
            Vector start(xx1, yy1, zz1);
            Vector end(xx2, yy2, zz2);
            XyzPointFloat result;
            if (getRayIntersectionWithCube(st, spart->getDirection(), start, end, XyzDirection::x, &result)) {
                first = result;
            }
            if (getRayIntersectionWithCube(st, spart->getDirection(), start, end, XyzDirection::y, &result)) {
                if (getDistanceBetweenPoints(st, result) < getDistanceBetweenPoints(st, first)) {
                    first = result;
                }
            }
            if (getRayIntersectionWithCube(st, spart->getDirection(), start, end, XyzDirection::z, &result)) {
                if (getDistanceBetweenPoints(st, result) < getDistanceBetweenPoints(st, first)) {
                    first = result;
                }
            }
            if (getRayIntersectionWithCube(st, spart->getDirection()*(-1.0f), start, end, XyzDirection::x, &result)) {
                second = result;
                vol = PI*mRadius*mRadius*getDistanceBetweenPoints(first, second);
            }
            if (getRayIntersectionWithCube(st, spart->getDirection()*(-1.0f), start, end, XyzDirection::y, &result)) {
                if (getDistanceBetweenPoints(st, result) < getDistanceBetweenPoints(st, second)) {
                    second = result;
                    vol = PI*mRadius*mRadius*getDistanceBetweenPoints(first, second);
                }
            }
            if (getRayIntersectionWithCube(st, spart->getDirection()*(-1.0f), start, end, XyzDirection::z, &result)) {
                if (getDistanceBetweenPoints(st, result) < getDistanceBetweenPoints(st, second)) {
                    second = result;
                    vol = PI*mRadius*mRadius*getDistanceBetweenPoints(first, second);
                }
            }
        }
        return vol;
    }
}


float SegmentPart::calcLum(FieldComponent component, bool totalInductance)
{
    float x_coord = static_cast<float>(mCell.x) * mCellSize;
    float y_coord = static_cast<float>(mCell.y) * mCellSize;
    float z_coord = static_cast<float>(mCell.z) * mCellSize;
    float half_cell = 0.5 * mCellSize;
    float result = 0.0;
    float pu[4];
    XyzPointFloat Middle;
    float dv, dw;
    Middle = (mStart + mEnd) * 0.5;
    
    if (component == FieldComponent::ex) {
        dv = Middle.y - static_cast<float>(mCell.y) * mCellSize;
        dw = Middle.z - static_cast<float>(mCell.z) * mCellSize;

        pu[0] = (1-dv/mCellSize) * (1-dw/mCellSize);
        pu[1] = (dv/mCellSize) * (1-dw/mCellSize);
        pu[2] = (1-dv/mCellSize) * (dw/mCellSize);
        pu[3] = (dv/mCellSize) * (dw/mCellSize);

        result =  pu[0] * calcIntegralAdsimp(this, x_coord, y_coord-half_cell, z_coord-half_cell, totalInductance);
        result += pu[1] * calcIntegralAdsimp(this, x_coord, y_coord+half_cell, z_coord-half_cell, totalInductance);
        result += pu[2] * calcIntegralAdsimp(this, x_coord, y_coord-half_cell, z_coord+half_cell, totalInductance);
        result += pu[3] * calcIntegralAdsimp(this, x_coord, y_coord+half_cell, z_coord+half_cell, totalInductance);
    }
    else if (component == FieldComponent::ey) {
        dv = Middle.x - static_cast<float>(mCell.x) * mCellSize;
        dw = Middle.z - static_cast<float>(mCell.z) * mCellSize;

        pu[0] = (1-dv/mCellSize) * (1-dw/mCellSize);
        pu[1] = (dv/mCellSize) * (1-dw/mCellSize);
        pu[2] = (1-dv/mCellSize) * (dw/mCellSize);
        pu[3] = (dv/mCellSize) * (dw/mCellSize);

        result =  pu[0] * calcIntegralAdsimp(this, x_coord-half_cell, y_coord, z_coord-half_cell, totalInductance);
        result += pu[1] * calcIntegralAdsimp(this, x_coord+half_cell, y_coord, z_coord-half_cell, totalInductance);
        result += pu[2] * calcIntegralAdsimp(this, x_coord-half_cell, y_coord, z_coord+half_cell, totalInductance);
        result += pu[3] * calcIntegralAdsimp(this, x_coord+half_cell, y_coord, z_coord+half_cell, totalInductance);
    }
    else if (component == FieldComponent::ez) {
        dv = Middle.y - static_cast<float>(mCell.y) * mCellSize;
        dw = Middle.x - static_cast<float>(mCell.x) * mCellSize;

        pu[0] = (1-dv/mCellSize) * (1-dw/mCellSize);
        pu[1] = (dv/mCellSize) * (1-dw/mCellSize);
        pu[2] = (1-dv/mCellSize) * (dw/mCellSize);
        pu[3] = (dv/mCellSize) * (dw/mCellSize);

        result =  pu[0] * calcIntegralAdsimp(this, x_coord-half_cell, y_coord-half_cell, z_coord, totalInductance);
        result += pu[1] * calcIntegralAdsimp(this, x_coord-half_cell, y_coord+half_cell, z_coord, totalInductance);
        result += pu[2] * calcIntegralAdsimp(this, x_coord+half_cell, y_coord-half_cell, z_coord, totalInductance);
        result += pu[3] * calcIntegralAdsimp(this, x_coord+half_cell, y_coord+half_cell, z_coord, totalInductance);
    }
    
    if (totalInductance) {
        return result * MU0 / (2.0 * PI * mCellSize * mCellSize * mCellSize);
    }
    else {
        return MU0 * log(mSheathRadius/mRadius) * (1.0 - result/(mCellSize*mCellSize*mCellSize)) / (2.0 * PI);
    }
}


void SegmentPart::initLengthDirectionCell(void)
{
    mLength = getDistanceBetweenPoints(mStart, mEnd);
    try {
        mDirection = getDirectionFromPointToPoint(mStart, mEnd);
    }
    catch (std::exception const& e) {
        mDirection = Vector(0.0, 0.0, 0.0);
    }
    // calc cell, where SegmentPart locates.
    XyzPointFloat middle = (mStart + mEnd) * 0.5;
    for (mCell.x = 0; (static_cast<float>(mCell.x) * mCellSize) < (middle.x-mCellSize); ++mCell.x);
    for (mCell.y = 0; (static_cast<float>(mCell.y) * mCellSize) < (middle.y-mCellSize); ++mCell.y);
    for (mCell.z = 0; (static_cast<float>(mCell.z) * mCellSize) < (middle.z-mCellSize); ++mCell.z);
}


void SegmentPart::calcAllLum(void)
{
    // L u,m
    mLum[0] = calcLum(FieldComponent::ex);
    mLum[1] = calcLum(FieldComponent::ey);
    mLum[2] = calcLum(FieldComponent::ez);
    if (mSheathRadius > mRadius) {
        mLums[0] = calcLum(FieldComponent::ex, false);
        mLums[1] = calcLum(FieldComponent::ey, false);
        mLums[2] = calcLum(FieldComponent::ez, false);
    }
}


float SegmentPart::getSegmentPartEk(void)
{
    float ek;
    
    ek = mPcx[0] * mEx[mCell.x][mCell.y][mCell.z] +
         mPcx[1] * mEx[mCell.x][mCell.y+1][mCell.z] +
         mPcx[2] * mEx[mCell.x][mCell.y][mCell.z+1] +
         mPcx[3] * mEx[mCell.x][mCell.y+1][mCell.z+1];
    
    ek += mPcy[0] * mEy[mCell.x][mCell.y][mCell.z] +
          mPcy[1] * mEy[mCell.x+1][mCell.y][mCell.z] +
          mPcy[2] * mEy[mCell.x][mCell.y][mCell.z+1] +
          mPcy[3] * mEy[mCell.x+1][mCell.y][mCell.z+1];
    
    ek += mPcz[0] * mEz[mCell.x][mCell.y][mCell.z] +
          mPcz[1] * mEz[mCell.x][mCell.y+1][mCell.z] +
          mPcz[2] * mEz[mCell.x+1][mCell.y][mCell.z] +
          mPcz[3] * mEz[mCell.x+1][mCell.y+1][mCell.z];

    return ek;
}


void SegmentPart::setSegmentPartIk(float Ik)
{
    mEx[mCell.x][mCell.y][mCell.z] -= Ik * mJpcx[0];
    mEx[mCell.x][mCell.y+1][mCell.z] -= Ik * mJpcx[1];
    mEx[mCell.x][mCell.y][mCell.z+1] -= Ik * mJpcx[2];
    mEx[mCell.x][mCell.y+1][mCell.z+1] -= Ik * mJpcx[3];
    
    mEy[mCell.x][mCell.y][mCell.z] -= Ik * mJpcy[0];
    mEy[mCell.x+1][mCell.y][mCell.z] -= Ik * mJpcy[1];
    mEy[mCell.x][mCell.y][mCell.z+1] -= Ik * mJpcy[2];
    mEy[mCell.x+1][mCell.y][mCell.z+1] -= Ik * mJpcy[3];
    
    mEz[mCell.x][mCell.y][mCell.z] -= Ik * mJpcz[0];
    mEz[mCell.x][mCell.y+1][mCell.z] -= Ik * mJpcz[1];
    mEz[mCell.x+1][mCell.y][mCell.z] -= Ik * mJpcz[2];
    mEz[mCell.x+1][mCell.y+1][mCell.z] -= Ik * mJpcz[3];
}




ObliqueWire::ObliqueWire(const Model &fdtdvol, XyzPointFloat start, XyzPointFloat end,
                         float diam, unsigned segnum, float sheath, float eps, float sigma)
    : mCellSize(fdtdvol.getCellSize())
{
    mStart = start;
    mEnd = end;
    mDiameter = diam;
    if (sheath > diam) {
        mSheathDiameter = sheath;
        mSheathEps = eps;
        mSheathSigma = sigma;
    }
    for (unsigned ucnt = 0; ucnt < 3; ++ucnt) {
        Volnm[ucnt] = Voln[ucnt] = Volnp[ucnt] = 0.0;
    }
    float wireLength = getDistanceBetweenPoints(mEnd, mStart);
    if (segnum > 0) {
        mNumberOfSegments = segnum;
    }
    else {
        mNumberOfSegments = static_cast<unsigned>(wireLength/(mCellSize) + 0.5);
        mNumberOfSegments++;
    }
    Dl = wireLength / static_cast<float>(mNumberOfSegments);
    
    splitWire(fdtdvol);
    initCouplingCoefficients(fdtdvol);
    initPrecalculatedParameters(fdtdvol);
}


ObliqueWire::~ObliqueWire()
{
}


float ObliqueWire::getEk(unsigned kin)
{
    float Ek = 0.0;
    for (unsigned cnt = 0; cnt < mArrayOfSegmentParts.size(); ++cnt) {
        if (kin == mArrayOfSegmentParts[cnt].k) {
            Ek += mArrayOfSegmentParts[cnt].getSegmentPartEk();
        }
    }
    return Ek / Dl;
}


void ObliqueWire::setJ(void)
{
    unsigned k;
    for (unsigned sp = 0; sp < mArrayOfSegmentParts.size(); ++sp) {
        k = mArrayOfSegmentParts[sp].k;
        mArrayOfSegmentParts[sp].setSegmentPartIk(I[k]);
    }
}


float ObliqueWire::getJr(WirePointType point)
{
    float jsumtmp = 0.0;
    if (point == WIRE_START) {
        for (unsigned pole = 0; pole < PolesNumber; ++pole) {
            jsumtmp += (1.0 + dkp[pole][0]) * J[pole][0] * coefJr[0];
        }
    }
    else {
        unsigned sn = mNumberOfSegments;
        for (unsigned pole = 0; pole < PolesNumber; ++pole) {
            jsumtmp += (1.0 + dkp[pole][sn]) * J[pole][sn] * coefJr[1];
        }
    }
    return 0.5*jsumtmp;
}


void ObliqueWire::splitWire(const Model &fdtdvol)
{
    // array of segments' points coordinates.
    std::vector<XyzPointFloat> arrayOfSegments;
    XyzPointFloat wireDirection;
    try {
        wireDirection = getDirectionFromPointToPoint(mStart, mEnd);
    }
    catch (std::exception const& e) {
        wireDirection = Vector(0.0, 0.0, 0.0);
    }
    for (unsigned segm = 0; segm < mNumberOfSegments; ++segm) {
        arrayOfSegments.push_back(mStart + wireDirection*Dl*static_cast<float>(segm));
    }
    arrayOfSegments.push_back(mEnd);
    
    for (unsigned count = 0; count < mNumberOfSegments; ++count) {
        SegmentPart segp(fdtdvol, arrayOfSegments[count], arrayOfSegments[count+1], 0.0);
        XyzPointFloat ptinters;
        int segmadded = 0;
        
        if (getSegmentPartIntersectionWithCube(segp, XyzDirection::z, &ptinters)) {
            if (getDistanceBetweenPoints(arrayOfSegments[count], ptinters) <= Dl) {
                SegmentPart segpar(fdtdvol, arrayOfSegments[count], ptinters, mDiameter, mSheathDiameter);
                segpar.k = count;
                mArrayOfSegmentParts.push_back(segpar);
                segmadded++;
            }
        }
        
        if (getSegmentPartIntersectionWithCube(segp, XyzDirection::x, &ptinters)) {
            float length = getDistanceBetweenPoints(arrayOfSegments[count], ptinters);
            if (length <= Dl) {
                if (segmadded == 1) {
                    // calc length of last mArrayOfSegmentParts
                    float length1 = getDistanceBetweenPoints(arrayOfSegments[count], mArrayOfSegmentParts.back().getEnd());
                    if (length < length1) {
                        std::vector<SegmentPart>::iterator it = mArrayOfSegmentParts.end()-1;
                        SegmentPart segpar(fdtdvol, arrayOfSegments[count], ptinters, mDiameter, mSheathDiameter);
                        segpar.k = count;
                        mArrayOfSegmentParts.insert(it, segpar);
                        mArrayOfSegmentParts.back().setStart(ptinters);
                    }
                    else {
                        SegmentPart segpar(fdtdvol, mArrayOfSegmentParts.back().getEnd(), ptinters, mDiameter, mSheathDiameter);
                        segpar.k = count;
                        mArrayOfSegmentParts.push_back(segpar);
                    }
                }
                else {
                    SegmentPart segpar(fdtdvol, arrayOfSegments[count], ptinters, mDiameter, mSheathDiameter);
                    segpar.k = count;
                    mArrayOfSegmentParts.push_back(segpar);
                }
                segmadded++;
            }
        }
        
        if (getSegmentPartIntersectionWithCube(segp, XyzDirection::y, &ptinters)) {
            float length = getDistanceBetweenPoints(arrayOfSegments[count], ptinters);
            if (length <= Dl) {
                if (segmadded == 2) {
                    // calc length of last mArrayOfSegmentParts
                    float length1 = getDistanceBetweenPoints(arrayOfSegments[count], mArrayOfSegmentParts.back().getEnd());
                    // calc length of last - 1
                    float length2 = getDistanceBetweenPoints(arrayOfSegments[count], mArrayOfSegmentParts[mArrayOfSegmentParts.size()-2].getEnd());
                    if (length < length2) {
                        std::vector<SegmentPart>::iterator it = mArrayOfSegmentParts.end()-1;
                        SegmentPart segpar(fdtdvol, arrayOfSegments[count], ptinters, mDiameter, mSheathDiameter);
                        segpar.k = count;
                        mArrayOfSegmentParts.insert(it-1, segpar);
                        mArrayOfSegmentParts[mArrayOfSegmentParts.size()-2].setStart(ptinters);
                    }
                    else if (length < length1) {
                        std::vector<SegmentPart>::iterator it = mArrayOfSegmentParts.end()-1;
                        SegmentPart segpar(fdtdvol, mArrayOfSegmentParts[mArrayOfSegmentParts.size()-2].getEnd(), ptinters, mDiameter, mSheathDiameter);
                        segpar.k = count;
                        mArrayOfSegmentParts.insert(it, segpar);
                        mArrayOfSegmentParts.back().setStart(ptinters);
                    }
                    else {
                        SegmentPart segpar(fdtdvol, mArrayOfSegmentParts.back().getEnd(), ptinters, mDiameter, mSheathDiameter);
                        segpar.k = count;
                        mArrayOfSegmentParts.push_back(segpar);
                    }
                }
                else if (segmadded == 1) {
                    float length1 = getDistanceBetweenPoints(arrayOfSegments[count], mArrayOfSegmentParts.back().getEnd());
                    if (length < length1) {
                        std::vector<SegmentPart>::iterator it = mArrayOfSegmentParts.end()-1;
                        SegmentPart segpar(fdtdvol, arrayOfSegments[count], ptinters, mDiameter, mSheathDiameter);
                        segpar.k = count;
                        mArrayOfSegmentParts.insert(it, segpar);
                        mArrayOfSegmentParts.back().setStart(ptinters);
                    }
                    else {
                        SegmentPart segpar(fdtdvol, mArrayOfSegmentParts.back().getEnd(), ptinters, mDiameter, mSheathDiameter);
                        segpar.k = count;
                        mArrayOfSegmentParts.push_back(segpar);
                    }
                }
                else {
                    SegmentPart segpar(fdtdvol, arrayOfSegments[count], ptinters, mDiameter, mSheathDiameter);
                    segpar.k = count;
                    mArrayOfSegmentParts.push_back(segpar);
                }
                segmadded++;
            }
        }
        
        if (segmadded == 0) {
            SegmentPart segpar(fdtdvol, arrayOfSegments[count], arrayOfSegments[count+1], mDiameter, mSheathDiameter);
            segpar.k = count;
            mArrayOfSegmentParts.push_back(segpar);
        }
        else {
            SegmentPart segpar(fdtdvol, mArrayOfSegmentParts.back().getEnd(), arrayOfSegments[count+1], mDiameter, mSheathDiameter);
            segpar.k = count;
            mArrayOfSegmentParts.push_back(segpar);
        }
    }
}


void ObliqueWire::initCouplingCoefficients(const Model &fdtdvol)
{
    float divD = 1.0/mCellSize;
    XyzPointFloat Middle;
    float dv, dw;
    float dirx = fabs(mArrayOfSegmentParts[0].getDirection().x);
    float diry = fabs(mArrayOfSegmentParts[0].getDirection().y);
    float dirz = fabs(mArrayOfSegmentParts[0].getDirection().z);
    
    if ((dirx > diry && dirx > dirz) ||
        (std::fpclassify(dirx-diry) == FP_ZERO && dirx > dirz) ||
        (std::fpclassify(dirx-dirz) == FP_ZERO && dirx > diry) ||
        (std::fpclassify(dirx-diry) == FP_ZERO && std::fpclassify(dirx-dirz) == FP_ZERO)) {
        for (unsigned sp = 0; sp < mArrayOfSegmentParts.size(); ++sp) {
            XyzPointFloat sdir = mArrayOfSegmentParts[sp].getDirection();
            XyzPoint cell = mArrayOfSegmentParts[sp].getCell();
            float slength = mArrayOfSegmentParts[sp].getLength();
            
            dv = mArrayOfSegmentParts[sp].getStart().y - static_cast<float>(cell.y) * mCellSize;
            dw = mArrayOfSegmentParts[sp].getStart().z - static_cast<float>(cell.z) * mCellSize;
            float dv1 = mArrayOfSegmentParts[sp].getEnd().y - static_cast<float>(cell.y) * mCellSize;
            float dw1 = mArrayOfSegmentParts[sp].getEnd().z - static_cast<float>(cell.z) * mCellSize;
            mArrayOfSegmentParts[sp].setPcxAndJpcx(0, 0.5 * ((1.0-dv*divD) * (1.0-dw*divD) + (1.0-dv1*divD) * (1.0-dw1*divD)) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(1, 0.5 * ((dv*divD) * (1.0-dw*divD) + (dv1*divD) * (1.0-dw1*divD)) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(2, 0.5 * ((1.0-dv*divD) * (dw*divD) + (1.0-dv1*divD) * (dw1*divD)) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(3, 0.5 * ((dv*divD) * (dw*divD) + (dv1*divD) * (dw1*divD)) * sdir.x * slength);
            
            Middle = (mArrayOfSegmentParts[sp].getStart() + mArrayOfSegmentParts[sp].getEnd()) * 0.5;
            dv = Middle.x - static_cast<float>(cell.x) * mCellSize;
            dw = Middle.z - static_cast<float>(cell.z) * mCellSize;
            mArrayOfSegmentParts[sp].setPcyAndJpcy(0, (1.0-dv*divD) * (1.0-dw*divD) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(1, (dv*divD) * (1.0-dw*divD) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(2, (1.0-dv*divD) * (dw*divD) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(3, (dv*divD) * (dw*divD) * sdir.y * slength);
            
            dv = Middle.y - static_cast<float>(cell.y) * mCellSize;
            dw = Middle.x - static_cast<float>(cell.x) * mCellSize;
            mArrayOfSegmentParts[sp].setPczAndJpcz(0, (1.0-dv*divD) * (1.0-dw*divD) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(1, (dv*divD) * (1.0-dw*divD) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(2, (1.0-dv*divD) * (dw*divD) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(3, (dv*divD) * (dw*divD) * sdir.z * slength);
        }
    }
    else if ((diry > dirx && diry > dirz) ||
             (std::fpclassify(diry-dirz) == FP_ZERO && diry > dirx)) {
        for (unsigned sp = 0; sp < mArrayOfSegmentParts.size(); ++sp) {
            XyzPointFloat sdir = mArrayOfSegmentParts[sp].getDirection();
            XyzPoint cell = mArrayOfSegmentParts[sp].getCell();
            float slength = mArrayOfSegmentParts[sp].getLength();
            
            dv = mArrayOfSegmentParts[sp].getStart().x - static_cast<float>(cell.x) * mCellSize;
            dw = mArrayOfSegmentParts[sp].getStart().z - static_cast<float>(cell.z) * mCellSize;
            float dv1 = mArrayOfSegmentParts[sp].getEnd().x - static_cast<float>(cell.x) * mCellSize;
            float dw1 = mArrayOfSegmentParts[sp].getEnd().z - static_cast<float>(cell.z) * mCellSize;
            mArrayOfSegmentParts[sp].setPcyAndJpcy(0, 0.5 * ((1.0-dv*divD) * (1.0-dw*divD) + (1.0-dv1*divD) * (1.0-dw1*divD)) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(1, 0.5 * ((dv*divD) * (1.0-dw*divD) + (dv1*divD) * (1.0-dw1*divD)) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(2, 0.5 * ((1.0-dv*divD) * (dw*divD) + (1.0-dv1*divD) * (dw1*divD)) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(3, 0.5 * ((dv*divD) * (dw*divD) + (dv1*divD) * (dw1*divD)) * sdir.y * slength);
            
            Middle = (mArrayOfSegmentParts[sp].getStart() + mArrayOfSegmentParts[sp].getEnd()) * 0.5;
            dv = Middle.y - static_cast<float>(cell.y) * mCellSize;
            dw = Middle.z - static_cast<float>(cell.z) * mCellSize;
            mArrayOfSegmentParts[sp].setPcxAndJpcx(0, (1.0-dv*divD) * (1.0-dw*divD) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(1, (dv*divD) * (1.0-dw*divD) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(2, (1.0-dv*divD) * (dw*divD) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(3, (dv*divD) * (dw*divD) * sdir.x * slength);
            
            dv = Middle.y - static_cast<float>(cell.y) * mCellSize;
            dw = Middle.x - static_cast<float>(cell.x) * mCellSize;
            mArrayOfSegmentParts[sp].setPczAndJpcz(0, (1.0-dv*divD) * (1.0-dw*divD) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(1, (dv*divD) * (1.0-dw*divD) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(2, (1.0-dv*divD) * (dw*divD) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(3, (dv*divD) * (dw*divD) * sdir.z * slength);
        }
    }
    else if (dirz > dirx && dirz > diry) {
        for (unsigned sp = 0; sp < mArrayOfSegmentParts.size(); ++sp) {
            XyzPointFloat sdir = mArrayOfSegmentParts[sp].getDirection();
            XyzPoint cell = mArrayOfSegmentParts[sp].getCell();
            float slength = mArrayOfSegmentParts[sp].getLength();
            
            dv = mArrayOfSegmentParts[sp].getStart().y - static_cast<float>(cell.y) * mCellSize;
            dw = mArrayOfSegmentParts[sp].getStart().x - static_cast<float>(cell.x) * mCellSize;
            float dv1 = mArrayOfSegmentParts[sp].getEnd().y - static_cast<float>(cell.y) * mCellSize;
            float dw1 = mArrayOfSegmentParts[sp].getEnd().x - static_cast<float>(cell.x) * mCellSize;
            mArrayOfSegmentParts[sp].setPczAndJpcz(0, 0.5 * ((1.0-dv*divD) * (1.0-dw*divD) + (1.0-dv1*divD) * (1.0-dw1*divD)) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(1, 0.5 * ((dv*divD) * (1.0-dw*divD) + (dv1*divD) * (1.0-dw1*divD)) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(2, 0.5 * ((1.0-dv*divD) * (dw*divD) + (1.0-dv1*divD) * (dw1*divD)) * sdir.z * slength);
            mArrayOfSegmentParts[sp].setPczAndJpcz(3, 0.5 * ((dv*divD) * (dw*divD) + (dv1*divD) * (dw1*divD)) * sdir.z * slength);
            
            Middle = (mArrayOfSegmentParts[sp].getStart() + mArrayOfSegmentParts[sp].getEnd()) * 0.5;
            dv = Middle.y - static_cast<float>(cell.y) * mCellSize;
            dw = Middle.z - static_cast<float>(cell.z) * mCellSize;
            mArrayOfSegmentParts[sp].setPcxAndJpcx(0, (1.0-dv*divD) * (1.0-dw*divD) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(1, (dv*divD) * (1.0-dw*divD) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(2, (1.0-dv*divD) * (dw*divD) * sdir.x * slength);
            mArrayOfSegmentParts[sp].setPcxAndJpcx(3, (dv*divD) * (dw*divD) * sdir.x * slength);
            
            dv = Middle.x - static_cast<float>(cell.x) * mCellSize;
            dw = Middle.z - static_cast<float>(cell.z) * mCellSize;
            mArrayOfSegmentParts[sp].setPcyAndJpcy(0, (1.0-dv*divD) * (1.0-dw*divD) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(1, (dv*divD) * (1.0-dw*divD) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(2, (1.0-dv*divD) * (dw*divD) * sdir.y * slength);
            mArrayOfSegmentParts[sp].setPcyAndJpcy(3, (dv*divD) * (dw*divD) * sdir.y * slength);
        }
    }
    
    for (unsigned sp = 0; sp < mArrayOfSegmentParts.size(); ++sp) {
        XyzPoint cell = mArrayOfSegmentParts[sp].getCell();

        float Cbx0 = fdtdvol.getCbInCell(XyzDirection::x, XyzPoint(cell.x, cell.y, cell.z));
        float Cbx1 = fdtdvol.getCbInCell(XyzDirection::x, XyzPoint(cell.x, cell.y+1, cell.z));
        float Cbx2 = fdtdvol.getCbInCell(XyzDirection::x, XyzPoint(cell.x, cell.y, cell.z+1));
        float Cbx3 = fdtdvol.getCbInCell(XyzDirection::x, XyzPoint(cell.x, cell.y+1, cell.z+1));
        mArrayOfSegmentParts[sp].mulJpcx(0, Cbx0 * divD * divD * divD * mCellSize); // * Dl[k] (divided later);
        mArrayOfSegmentParts[sp].mulJpcx(1, Cbx1 * divD * divD * divD * mCellSize);
        mArrayOfSegmentParts[sp].mulJpcx(2, Cbx2 * divD * divD * divD * mCellSize);
        mArrayOfSegmentParts[sp].mulJpcx(3, Cbx3 * divD * divD * divD * mCellSize);
        
        float Cby0 = fdtdvol.getCbInCell(XyzDirection::y, XyzPoint(cell.x, cell.y, cell.z));
        float Cby1 = fdtdvol.getCbInCell(XyzDirection::y, XyzPoint(cell.x+1, cell.y, cell.z));
        float Cby2 = fdtdvol.getCbInCell(XyzDirection::y, XyzPoint(cell.x, cell.y, cell.z+1));
        float Cby3 = fdtdvol.getCbInCell(XyzDirection::y, XyzPoint(cell.x+1, cell.y, cell.z+1));
        mArrayOfSegmentParts[sp].mulJpcy(0, Cby0 * divD * divD * divD * mCellSize);
        mArrayOfSegmentParts[sp].mulJpcy(1, Cby1 * divD * divD * divD * mCellSize);
        mArrayOfSegmentParts[sp].mulJpcy(2, Cby2 * divD * divD * divD * mCellSize);
        mArrayOfSegmentParts[sp].mulJpcy(3, Cby3 * divD * divD * divD * mCellSize);
        
        float Cbz0 = fdtdvol.getCbInCell(XyzDirection::z, XyzPoint(cell.x, cell.y, cell.z));
        float Cbz1 = fdtdvol.getCbInCell(XyzDirection::z, XyzPoint(cell.x, cell.y+1, cell.z));
        float Cbz2 = fdtdvol.getCbInCell(XyzDirection::z, XyzPoint(cell.x+1, cell.y, cell.z));
        float Cbz3 = fdtdvol.getCbInCell(XyzDirection::z, XyzPoint(cell.x+1, cell.y+1, cell.z));
        mArrayOfSegmentParts[sp].mulJpcz(0, Cbz0 * divD * divD * divD * mCellSize);
        mArrayOfSegmentParts[sp].mulJpcz(1, Cbz1 * divD * divD * divD * mCellSize);
        mArrayOfSegmentParts[sp].mulJpcz(2, Cbz2 * divD * divD * divD * mCellSize);
        mArrayOfSegmentParts[sp].mulJpcz(3, Cbz3 * divD * divD * divD * mCellSize);
    }
}


void ObliqueWire::initPrecalculatedParameters(const Model &fdtdvol)
{
    // ************************ init L, C, b, etc. ************************
    float dirx = fabs(mArrayOfSegmentParts[0].getDirection().x);
    float diry = fabs(mArrayOfSegmentParts[0].getDirection().y);
    float dirz = fabs(mArrayOfSegmentParts[0].getDirection().z);
    
    for (unsigned cnt = 0; cnt < mArrayOfSegmentParts.size(); ++cnt) {
        mArrayOfSegmentParts[cnt].calcAllLum();
    }
    
    // calc Lk
    std::vector<std::vector<float>> Luk;
    Luk.resize(3);
    for (unsigned i = 0; i < 3; ++i) {
        for (unsigned j = 0; j < mNumberOfSegments; ++j) {
            Luk[i].push_back(0.0);
        }
    }
    for (unsigned xyz = 0; xyz < 3; ++xyz) {
        for (unsigned cnt = 0; cnt < mArrayOfSegmentParts.size(); ++cnt) {
            Luk[xyz][mArrayOfSegmentParts[cnt].k] += mArrayOfSegmentParts[cnt].getLum(xyz) * mArrayOfSegmentParts[cnt].getLength();
        }
    }
    
    Lk.resize(mNumberOfSegments);
    for (unsigned cnt = 0; cnt < mNumberOfSegments; ++cnt) {
        unsigned cntin;
        for (cntin = 0; mArrayOfSegmentParts[cntin].k != cnt; ++cntin);
        float ru = mArrayOfSegmentParts[cntin].getDirection().x;
        Lk[cnt] = Luk[0][cnt] * ru*ru;
        ru = mArrayOfSegmentParts[cntin].getDirection().y;
        Lk[cnt] += Luk[1][cnt] * ru*ru;
        ru = mArrayOfSegmentParts[cntin].getDirection().z;
        Lk[cnt] += Luk[2][cnt] * ru*ru;
        Lk[cnt] /= Dl;
    }
    
    // calc Lks
    std::vector<std::vector<float>> Luks;
    Luks.resize(3);
    for (unsigned i = 0; i < 3; ++i) {
        for (unsigned j = 0; j < mNumberOfSegments; ++j) {
            Luks[i].push_back(0.0);
        }
    }
    for (unsigned xyz = 0; xyz < 3; ++xyz) {
        for (unsigned cnt = 0; cnt < mArrayOfSegmentParts.size(); ++cnt) {
            Luks[xyz][mArrayOfSegmentParts[cnt].k] += mArrayOfSegmentParts[cnt].getLums(xyz) * mArrayOfSegmentParts[cnt].getLength();
        }
    }
    
    Lks.resize(mNumberOfSegments);
    for (unsigned cnt = 0; cnt < mNumberOfSegments; ++cnt) {
        unsigned cntin;
        for (cntin = 0; mArrayOfSegmentParts[cntin].k != cnt; ++cntin);
        float ru = mArrayOfSegmentParts[cntin].getDirection().x;
        Lks[cnt] = Luks[0][cnt] * ru*ru;
        ru = mArrayOfSegmentParts[cntin].getDirection().y;
        Lks[cnt] += Luks[1][cnt] * ru*ru;
        ru = mArrayOfSegmentParts[cntin].getDirection().z;
        Lks[cnt] += Luks[2][cnt] * ru*ru;
        Lks[cnt] /= Dl;
    }
    
    // calc Lke
    Lke.resize(mNumberOfSegments);
    for (unsigned cnt = 0; cnt < mNumberOfSegments; ++cnt) {
        Lke[cnt] = Lk[cnt] - Lks[cnt];
    }
    
    
    // dispersion
    float betasum;
    float ****betax = fdtdvol.getDebyeAdeParameters().dbetapx;
    float ****betay = fdtdvol.getDebyeAdeParameters().dbetapy;
    float ****betaz = fdtdvol.getDebyeAdeParameters().dbetapz;
    float ****kdebx = fdtdvol.getDebyeAdeParameters().kpx;
    float ****kdeby = fdtdvol.getDebyeAdeParameters().kpy;
    float ****kdebz = fdtdvol.getDebyeAdeParameters().kpz;
    PolesNumber = fdtdvol.getDebyeAdeParameters().PolesNumber;
    dbetap.resize(PolesNumber);
    for (unsigned pole = 0; pole < PolesNumber; ++pole) {
        dbetap[pole].resize(mNumberOfSegments+1);
    }
    dkp.resize(PolesNumber);
    for (unsigned pole = 0; pole < PolesNumber; ++pole) {
        dkp[pole].resize(mNumberOfSegments+1);
    }
    
    
    Vw = C0;
    Dtw = fdtdvol.getTimeStepDuration(); //Dl/Vw
    
    float tmpCa, tmpCb;
    
    unsigned cntinn;
    for (cntinn = 0; mArrayOfSegmentParts[cntinn].k != mNumberOfSegments-1; ++cntinn);
    XyzPoint tcell = mArrayOfSegmentParts[cntinn].getCell();
    float tempeps = 1.0;
    float epcx0 = fabs(mArrayOfSegmentParts[cntinn].getPcx(0));
    float epcx1 = fabs(mArrayOfSegmentParts[cntinn].getPcx(1));
    float epcx2 = fabs(mArrayOfSegmentParts[cntinn].getPcx(2));
    float epcx3 = fabs(mArrayOfSegmentParts[cntinn].getPcx(3));
    float epcy0 = fabs(mArrayOfSegmentParts[cntinn].getPcy(0));
    float epcy1 = fabs(mArrayOfSegmentParts[cntinn].getPcy(1));
    float epcy2 = fabs(mArrayOfSegmentParts[cntinn].getPcy(2));
    float epcy3 = fabs(mArrayOfSegmentParts[cntinn].getPcy(3));
    float epcz0 = fabs(mArrayOfSegmentParts[cntinn].getPcz(0));
    float epcz1 = fabs(mArrayOfSegmentParts[cntinn].getPcz(1));
    float epcz2 = fabs(mArrayOfSegmentParts[cntinn].getPcz(2));
    float epcz3 = fabs(mArrayOfSegmentParts[cntinn].getPcz(3));
    betasum = 0.0;
    if (dirx >= diry && dirx >= dirz) {
        if (epcx1 >= epcx0 && epcx1 >= epcx2 && epcx1 >= epcx3)
            tcell = XyzPoint(tcell.x, tcell.y+1, tcell.z);
        else if (epcx2 >= epcx0 && epcx2 >= epcx1 && epcx2 >= epcx3)
            tcell = XyzPoint(tcell.x, tcell.y, tcell.z+1);
        else if (epcx3 >= epcx0 && epcx3 >= epcx1 && epcx3 >= epcx2)
            tcell = XyzPoint(tcell.x, tcell.y+1, tcell.z+1);
        tmpCa = fdtdvol.getCaInCell(XyzDirection::x, tcell);
        tmpCb = fdtdvol.getCbInCell(XyzDirection::x, tcell);
        for (unsigned pole = 0; pole < PolesNumber; ++pole) {
            betasum += betax[pole][tcell.x][tcell.y][tcell.z] * Dtw;
        }
    }
    else if (diry >= dirx && diry >= dirz) {
        if (epcy1 >= epcy0 && epcy1 >= epcy2 && epcy1 >= epcy3)
            tcell = XyzPoint(tcell.x+1, tcell.y, tcell.z);
        else if (epcy2 >= epcy0 && epcy2 >= epcy1 && epcy2 >= epcy3)
            tcell = XyzPoint(tcell.x, tcell.y, tcell.z+1);
        else if (epcy3 >= epcy0 && epcy3 >= epcy1 && epcy3 >= epcy2)
            tcell = XyzPoint(tcell.x+1, tcell.y, tcell.z+1);
        tmpCa = fdtdvol.getCaInCell(XyzDirection::y, tcell);
        tmpCb = fdtdvol.getCbInCell(XyzDirection::y, tcell);
        for (unsigned pole = 0; pole < PolesNumber; ++pole) {
            betasum += betay[pole][tcell.x][tcell.y][tcell.z] * Dtw;
        }
    }
    else {
        if (epcz1 >= epcz0 && epcz1 >= epcz2 && epcz1 >= epcz3)
            tcell = XyzPoint(tcell.x, tcell.y+1, tcell.z);
        else if (epcz2 >= epcz0 && epcz2 >= epcz1 && epcz2 >= epcz3)
            tcell = XyzPoint(tcell.x+1, tcell.y, tcell.z);
        else if (epcz3 >= epcz0 && epcz3 >= epcz1 && epcz3 >= epcz2)
            tcell = XyzPoint(tcell.x+1, tcell.y+1, tcell.z);
        tmpCa = fdtdvol.getCaInCell(XyzDirection::z, tcell);
        tmpCb = fdtdvol.getCbInCell(XyzDirection::z, tcell);
        for (unsigned pole = 0; pole < PolesNumber; ++pole) {
            betasum += betaz[pole][tcell.x][tcell.y][tcell.z] * Dtw;
        }
    }
    if (std::fpclassify(tmpCb) != FP_ZERO) {
        tempeps = (Dtw*(1.0+tmpCa) - betasum*tmpCb*mCellSize) / (2.0*tmpCb*EPS0*mCellSize);
    }
    EndCellVw = C0/sqrt(tempeps);
    
    // calc Clke
    Clke.resize(mNumberOfSegments);
    for (unsigned cnt = 0; cnt < mNumberOfSegments; ++cnt) {
        float tmpeps = 1.0; // possibly, calc for tmpCa==0
        unsigned cntin;
        for (cntin = 0; mArrayOfSegmentParts[cntin].k != cnt; ++cntin);
        XyzPoint cell = mArrayOfSegmentParts[cntin].getCell();
        float ppcx0 = fabs(mArrayOfSegmentParts[cntin].getPcx(0));
        float ppcx1 = fabs(mArrayOfSegmentParts[cntin].getPcx(1));
        float ppcx2 = fabs(mArrayOfSegmentParts[cntin].getPcx(2));
        float ppcx3 = fabs(mArrayOfSegmentParts[cntin].getPcx(3));
        float ppcy0 = fabs(mArrayOfSegmentParts[cntin].getPcy(0));
        float ppcy1 = fabs(mArrayOfSegmentParts[cntin].getPcy(1));
        float ppcy2 = fabs(mArrayOfSegmentParts[cntin].getPcy(2));
        float ppcy3 = fabs(mArrayOfSegmentParts[cntin].getPcy(3));
        float ppcz0 = fabs(mArrayOfSegmentParts[cntin].getPcz(0));
        float ppcz1 = fabs(mArrayOfSegmentParts[cntin].getPcz(1));
        float ppcz2 = fabs(mArrayOfSegmentParts[cntin].getPcz(2));
        float ppcz3 = fabs(mArrayOfSegmentParts[cntin].getPcz(3));
        betasum = 0.0;
        if (dirx >= diry && dirx >= dirz) {
            if (ppcx1 >= ppcx0 && ppcx1 >= ppcx2 && ppcx1 >= ppcx3)
                cell = XyzPoint(cell.x, cell.y+1, cell.z);
            else if (ppcx2 >= ppcx0 && ppcx2 >= ppcx1 && ppcx2 >= ppcx3)
                cell = XyzPoint(cell.x, cell.y, cell.z+1);
            else if (ppcx3 >= ppcx0 && ppcx3 >= ppcx1 && ppcx3 >= ppcx2)
                cell = XyzPoint(cell.x, cell.y+1, cell.z+1);
            tmpCa = fdtdvol.getCaInCell(XyzDirection::x, cell);
            tmpCb = fdtdvol.getCbInCell(XyzDirection::x, cell);
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                betasum += betax[pole][cell.x][cell.y][cell.z] * Dtw;
            }
        }
        else if (diry >= dirx && diry >= dirz) {
            if (ppcy1 >= ppcy0 && ppcy1 >= ppcy2 && ppcy1 >= ppcy3)
                cell = XyzPoint(cell.x+1, cell.y, cell.z);
            else if (ppcy2 >= ppcy0 && ppcy2 >= ppcy1 && ppcy2 >= ppcy3)
                cell = XyzPoint(cell.x, cell.y, cell.z+1);
            else if (ppcy3 >= ppcy0 && ppcy3 >= ppcy1 && ppcy3 >= ppcy2)
                cell = XyzPoint(cell.x+1, cell.y, cell.z+1);
            tmpCa = fdtdvol.getCaInCell(XyzDirection::y, cell);
            tmpCb = fdtdvol.getCbInCell(XyzDirection::y, cell);
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                betasum += betay[pole][cell.x][cell.y][cell.z] * Dtw;
            }
        }
        else {
            if (ppcz1 >= ppcz0 && ppcz1 >= ppcz2 && ppcz1 >= ppcz3)
                cell = XyzPoint(cell.x, cell.y+1, cell.z);
            else if (ppcz2 >= ppcz0 && ppcz2 >= ppcz1 && ppcz2 >= ppcz3)
                cell = XyzPoint(cell.x+1, cell.y, cell.z);
            else if (ppcz3 >= ppcz0 && ppcz3 >= ppcz1 && ppcz3 >= ppcz2)
                cell = XyzPoint(cell.x+1, cell.y+1, cell.z);
            tmpCa = fdtdvol.getCaInCell(XyzDirection::z, cell);
            tmpCb = fdtdvol.getCbInCell(XyzDirection::z, cell);
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                betasum += betaz[pole][cell.x][cell.y][cell.z] * Dtw;
            }
        }
        if (std::fpclassify(tmpCb) != FP_ZERO) {
            tmpeps = (Dtw*(1.0+tmpCa) - betasum*tmpCb*mCellSize) / (2.0*tmpCb*EPS0*mCellSize);
        }
        Clke[cnt] = tmpeps / (Vw*Vw*Lk[cnt]);
    }
    
    
    // calc Cke
    Cke.resize(mNumberOfSegments+1);
    for (unsigned cnt = 1; cnt < mNumberOfSegments; ++cnt) {
        Cke[cnt] = (Clke[cnt-1] + Clke[cnt]) * (Dl / 2.0);
    }
    Cke[0] = Clke[0] * Dl;
    Cke[mNumberOfSegments] = Clke[mNumberOfSegments-1] * Dl;
    
    
    // calc Clks
    Clks.resize(mNumberOfSegments);
    for (unsigned cnt = 0; cnt < mNumberOfSegments; ++cnt) {
        Clks[cnt] = mSheathEps / (Vw*Vw*Lks[cnt]);
    }
    
    
    // calc Cks
    Cks.resize(mNumberOfSegments+1);
    for (unsigned cnt = 1; cnt < mNumberOfSegments; ++cnt) {
        Cks[cnt] = (Clks[cnt-1] + Clks[cnt]) * (Dl / 2.0);
    }
    Cks[0] = Clks[0] * Dl;
    Cks[mNumberOfSegments] = Clks[mNumberOfSegments-1] * Dl;
    
    
    // I, Ue, Us
    for (unsigned cnt = 0; cnt < mNumberOfSegments; ++cnt) {
        I.push_back(0.0);
    }
    for (unsigned cnt = 0; cnt < mNumberOfSegments+1; ++cnt) {
        Ue.push_back(0.0);
        Us.push_back(0.0);
    }
    
    // b1I, b2I
    float R = 0.0;
    for (unsigned cnt = 0; cnt < mNumberOfSegments; ++cnt) {
        b1I.push_back( (1.0 - R*Dtw/(2.0*Lk[cnt])) / (1.0 + R*Dtw/(2.0*Lk[cnt])) );
    }
    for (unsigned cnt = 0; cnt < mNumberOfSegments; ++cnt) {
        b2I.push_back( 1.0 / (1.0 + R*Dtw/(2.0*Lk[cnt])) );
    }
    
    // b1Ue, b2Ue
    // Debye J
    J.resize(PolesNumber);
    for (unsigned pole = 0; pole < PolesNumber; ++pole) {
        for (unsigned cnt = 0; cnt < mNumberOfSegments+1; ++cnt) {
            J[pole].push_back(0.0);
        }
    }
    b1Ue.resize(mNumberOfSegments+1);
    b2Ue.resize(mNumberOfSegments+1);
    for (unsigned cnt = 0; cnt < mNumberOfSegments+1; ++cnt) {
        float tmpeps;
        unsigned cntin;
        betasum = 0.0;
        if (cnt < mNumberOfSegments)
            for(cntin = 0; mArrayOfSegmentParts[cntin].k != cnt; ++cntin);
        else
            for(cntin = 0; mArrayOfSegmentParts[cntin].k != cnt-1; ++cntin);
        XyzPoint cell = mArrayOfSegmentParts[cntin].getCell();
        float ppcx0 = fabs(mArrayOfSegmentParts[cntin].getPcx(0));
        float ppcx1 = fabs(mArrayOfSegmentParts[cntin].getPcx(1));
        float ppcx2 = fabs(mArrayOfSegmentParts[cntin].getPcx(2));
        float ppcx3 = fabs(mArrayOfSegmentParts[cntin].getPcx(3));
        float ppcy0 = fabs(mArrayOfSegmentParts[cntin].getPcy(0));
        float ppcy1 = fabs(mArrayOfSegmentParts[cntin].getPcy(1));
        float ppcy2 = fabs(mArrayOfSegmentParts[cntin].getPcy(2));
        float ppcy3 = fabs(mArrayOfSegmentParts[cntin].getPcy(3));
        float ppcz0 = fabs(mArrayOfSegmentParts[cntin].getPcz(0));
        float ppcz1 = fabs(mArrayOfSegmentParts[cntin].getPcz(1));
        float ppcz2 = fabs(mArrayOfSegmentParts[cntin].getPcz(2));
        float ppcz3 = fabs(mArrayOfSegmentParts[cntin].getPcz(3));
        
        if (dirx >= diry && dirx >= dirz) {
            if (ppcx1 >= ppcx0 && ppcx1 >= ppcx2 && ppcx1 >= ppcx3)
                cell = XyzPoint(cell.x, cell.y+1, cell.z);
            else if (ppcx2 >= ppcx0 && ppcx2 >= ppcx1 && ppcx2 >= ppcx3)
                cell = XyzPoint(cell.x, cell.y, cell.z+1);
            else if (ppcx3 >= ppcx0 && ppcx3 >= ppcx1 && ppcx3 >= ppcx2)
                cell = XyzPoint(cell.x, cell.y+1, cell.z+1);
            tmpCa = fdtdvol.getCaInCell(XyzDirection::x, cell);
            tmpCb = fdtdvol.getCbInCell(XyzDirection::x, cell);
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                betasum += betax[pole][cell.x][cell.y][cell.z] * Dtw;
            }
            if (std::fpclassify(tmpCb) != FP_ZERO)
                tmpeps = (Dtw*(1.0+tmpCa) - betasum*tmpCb*mCellSize) / (2.0*tmpCb*EPS0*mCellSize);
            else
                tmpeps = 1.0; // possibly, calc for tmpCa==0
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                if (cnt < mNumberOfSegments)
                    dbetap[pole][cnt] = betax[pole][cell.x][cell.y][cell.z] * Cke[cnt] / (tmpeps*EPS0);
                else
                    dbetap[pole][cnt] = betax[pole][cell.x][cell.y][cell.z] * Cke[cnt-1] / (tmpeps*EPS0);
                dkp[pole][cnt] = kdebx[pole][cell.x][cell.y][cell.z];
            }
        }
        else if (diry >= dirx && diry >= dirz) {
            if (ppcy1 >= ppcy0 && ppcy1 >= ppcy2 && ppcy1 >= ppcy3)
                cell = XyzPoint(cell.x+1, cell.y, cell.z);
            else if (ppcy2 >= ppcy0 && ppcy2 >= ppcy1 && ppcy2 >= ppcy3)
                cell = XyzPoint(cell.x, cell.y, cell.z+1);
            else if (ppcy3 >= ppcy0 && ppcy3 >= ppcy1 && ppcy3 >= ppcy2)
                cell = XyzPoint(cell.x+1, cell.y, cell.z+1);
            tmpCa = fdtdvol.getCaInCell(XyzDirection::y, cell);
            tmpCb = fdtdvol.getCbInCell(XyzDirection::y, cell);
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                betasum += betay[pole][cell.x][cell.y][cell.z] * Dtw;
            }
            if (std::fpclassify(tmpCb) != FP_ZERO)
                tmpeps = (Dtw*(1.0+tmpCa) - betasum*tmpCb*mCellSize) / (2.0*tmpCb*EPS0*mCellSize);
            else
                tmpeps = 1.0; // possibly, calc for tmpCa==0
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                if (cnt < mNumberOfSegments)
                    dbetap[pole][cnt] = betay[pole][cell.x][cell.y][cell.z] * Cke[cnt] / (tmpeps*EPS0);
                else
                    dbetap[pole][cnt] = betay[pole][cell.x][cell.y][cell.z] * Cke[cnt-1] / (tmpeps*EPS0);
                dkp[pole][cnt] = kdeby[pole][cell.x][cell.y][cell.z];
            }
        }
        else {
            if (ppcz1 >= ppcz0 && ppcz1 >= ppcz2 && ppcz1 >= ppcz3)
                cell = XyzPoint(cell.x, cell.y+1, cell.z);
            else if (ppcz2 >= ppcz0 && ppcz2 >= ppcz1 && ppcz2 >= ppcz3)
                cell = XyzPoint(cell.x+1, cell.y, cell.z);
            else if (ppcz3 >= ppcz0 && ppcz3 >= ppcz1 && ppcz3 >= ppcz2)
                cell = XyzPoint(cell.x+1, cell.y+1, cell.z);
            tmpCa = fdtdvol.getCaInCell(XyzDirection::z, cell);
            tmpCb = fdtdvol.getCbInCell(XyzDirection::z, cell);
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                betasum += betaz[pole][cell.x][cell.y][cell.z] * Dtw;
            }
            if (std::fpclassify(tmpCb) != FP_ZERO)
                tmpeps = (Dtw*(1.0+tmpCa) - betasum*tmpCb*mCellSize) / (2.0*tmpCb*EPS0*mCellSize);
            else
                tmpeps = 1.0; // possibly, calc for tmpCa==0
            for (unsigned pole = 0; pole < PolesNumber; ++pole) {
                if (cnt < mNumberOfSegments)
                    dbetap[pole][cnt] = betaz[pole][cell.x][cell.y][cell.z] * Cke[cnt] / (tmpeps*EPS0);
                else
                    dbetap[pole][cnt] = betaz[pole][cell.x][cell.y][cell.z] * Cke[cnt-1] / (tmpeps*EPS0);
                dkp[pole][cnt] = kdebz[pole][cell.x][cell.y][cell.z];
            }
        }
        
        if (std::fpclassify(tmpCb) != FP_ZERO) {
            float tmpsigma = (1.0-tmpCa) / (tmpCb*mCellSize);
            b1Ue[cnt] = ( 1.0 - (tmpsigma*Dtw/(2.0*tmpeps*EPS0)) + betasum/(2.0*tmpeps*EPS0) ) /
                        ( 1.0 + (tmpsigma*Dtw/(2.0*tmpeps*EPS0)) + betasum/(2.0*tmpeps*EPS0) );
            b2Ue[cnt] = 1.0 / ( 1.0 + (tmpsigma*Dtw/(2.0*tmpeps*EPS0)) + betasum/(2.0*tmpeps*EPS0) );
        }
        else {
            b1Ue[cnt] = -1.0;
            b2Ue[cnt] = 0.0;
        }
        
        float tmpsigma = (1.0-tmpCa) / (tmpCb*mCellSize);
        if (cnt == 0) {
            Ep[0] = Cke[0]*(1.0+0.5*betasum)/Dtw + Cke[0]*tmpsigma/(2.0*tmpeps*EPS0);
            Em[0] = Cke[0]*(1.0+0.5*betasum)/Dtw - Cke[0]*tmpsigma/(2.0*tmpeps*EPS0);
        }
        else if (cnt == mNumberOfSegments) {
            Ep[1] = Cke[1]*(1.0+0.5*betasum)/Dtw + Cke[1]*tmpsigma/(2.0*tmpeps*EPS0);
            Em[1] = Cke[1]*(1.0+0.5*betasum)/Dtw - Cke[1]*tmpsigma/(2.0*tmpeps*EPS0);
        }
    }
    
    
    // b1Us, b2Us
    b1Us.resize(mNumberOfSegments+1);
    b2Us.resize(mNumberOfSegments+1);
    for (unsigned cnt = 0; cnt < mNumberOfSegments+1; ++cnt) {
        b1Us[cnt] = ( 1.0 - (mSheathSigma*Dtw/(2.0*mSheathEps*EPS0)) ) /
                    ( 1.0 + (mSheathSigma*Dtw/(2.0*mSheathEps*EPS0)) );
        b2Us[cnt] = 1.0 / ( 1.0 + (mSheathSigma*Dtw/(2.0*mSheathEps*EPS0)) );
    }
    
    
    Sp[0] = Cks[0]/Dtw + Cks[0]*mSheathSigma/(2.0*mSheathEps*EPS0);
    Sp[1] = Cks[mNumberOfSegments]/Dtw + Cks[mNumberOfSegments]*mSheathSigma/(2.0*mSheathEps*EPS0);
    Sm[0] = Cks[0]/Dtw - Cks[0]*mSheathSigma/(2.0*mSheathEps*EPS0);
    Sm[1] = Cks[mNumberOfSegments]/Dtw - Cks[mNumberOfSegments]*mSheathSigma/(2.0*mSheathEps*EPS0);
    
    
    if (mSheathDiameter > mDiameter) {
        coefVn[0] = Em[0]*Sp[0]/(Ep[0]+Sp[0]);
        coefVn[1] = Em[1]*Sp[1]/(Ep[1]+Sp[1]);
        coefDiv[0] = Ep[0]*Sp[0]/(Ep[0]+Sp[0]);
        coefDiv[1] = Ep[1]*Sp[1]/(Ep[1]+Sp[1]);
        coefZk[0] = (Em[0]*Sp[0]-Sm[0]*Ep[0])/(Ep[0]+Sp[0]);
        coefZk[1] = (Em[1]*Sp[1]-Sm[1]*Ep[1])/(Ep[1]+Sp[1]);
        coefJr[0] = 1.0/(Ep[0]+Sp[0]);
        coefJr[1] = 1.0/(Ep[1]+Sp[1]);
    }
    else {
        coefVn[0] = Em[0];
        coefVn[1] = Em[1];
        coefDiv[0] = Ep[0];
        coefDiv[1] = Ep[1];
        coefZk[0] = coefZk[1] = 0.0;
        coefJr[0] = coefJr[1] = 1.0;
        Ep[0] = Ep[1] = Em[0] = Em[1] = 0.0;
    }
}


void ObliqueWire::calcCurrents(void)
{
    if (mSheathDiameter > mDiameter) {
        for (unsigned seg = 0; seg < mNumberOfSegments; ++seg) {
            I[seg] = b1I[seg] * I[seg] - b2I[seg] * ( Dtw*(Ue[seg+1]-Ue[seg])/(Lk[seg]*Dl) + Dtw*(Us[seg+1]-Us[seg])/(Lk[seg]*Dl) - Dtw*getEk(seg)/Lk[seg] );
        }
    }
    else {
        for (unsigned seg = 0; seg < mNumberOfSegments; ++seg) {
            I[seg] = b1I[seg] * I[seg] - b2I[seg] * ( Dtw*(Ue[seg+1]-Ue[seg])/(Lk[seg]*Dl) - Dtw*getEk(seg)/Lk[seg] );
        }
    }
}


void ObliqueWire::calcVoltages(void)
{
    float utemp;
    float jsumtmp;
    unsigned pole;
    SavedVoltage = Ue[mNumberOfSegments-1]; // save voltage for first order ABC
    for (unsigned seg = 1; seg < mNumberOfSegments; ++seg) {
        utemp = Ue[seg];
        jsumtmp = 0.0;
        for (pole = 0; pole < PolesNumber; ++pole) {
            jsumtmp += (1.0 + dkp[pole][seg]) * J[pole][seg];
        }
        Ue[seg] = b1Ue[seg] * Ue[seg] - b2Ue[seg] * Dtw * (I[seg] - I[seg-1] + 0.5 * jsumtmp) / Cke[seg];
        for (pole = 0; pole < PolesNumber; ++pole) {
            J[pole][seg] = dkp[pole][seg] * J[pole][seg] + dbetap[pole][seg] * (Ue[seg] - utemp);
        }
        
        Us[seg] = b1Us[seg] * Us[seg] - b2Us[seg] * Dtw * (I[seg] - I[seg-1]) / Cks[seg];
    }
}


void ObliqueWire::calcVoltagesSecondOrderAbc(void)
{
    Volnm[0] = Voln[0];
    Volnm[1] = Voln[1];
    Volnm[2] = Voln[2];
    Voln[0] = Ue[mNumberOfSegments];
    Voln[1] = Ue[mNumberOfSegments-1];
    Voln[2] = Ue[mNumberOfSegments-2];
    calcVoltages();
    Volnp[1] = Ue[mNumberOfSegments-1];
    Volnp[2] = Ue[mNumberOfSegments-2];
}


void ObliqueWire::calcVoltageInStartNode(void)
{
    float utemp = Ue[0];
    float jsumtmp = 0.0;
    for (unsigned pole = 0; pole < PolesNumber; ++pole) {
        jsumtmp += (1.0 + dkp[pole][0]) * J[pole][0];
    }
    Ue[0] = b1Ue[0]*Ue[0] - 2.0*b2Ue[0]*Dtw*I[0]/Cke[0] - b2Ue[0]*Dtw*(0.5*jsumtmp)/Cke[0];
    for (unsigned pole = 0; pole < PolesNumber; ++pole) {
        J[pole][0] = dkp[pole][0] * J[pole][0] + dbetap[pole][0] * (Ue[0] - utemp);
    }
    
    Us[0] = b1Us[0]*Us[0] - 2.0*b2Us[0]*Dtw*I[0]/Cks[0];
}


void ObliqueWire::calcVoltageInEndNode(void)
{
    unsigned sn = mNumberOfSegments;
    if (mInfiniteEnd) {
        Ue[sn] = SavedVoltage - (EndCellVw*Dtw - Dl) * (Ue[sn] - Ue[sn-1]) / (EndCellVw*Dtw + Dl);
        //second order ABC
        //float sc = EndCellVw * Dtw / Dl;
        //Ue[sn] = ( -1.0/((1.0/sc)+2.0+sc) ) * ( ((1.0/sc)-2.0+sc) * (Volnp[2] + Volnm[0]) +
        //2.0 * (sc-(1.0/sc)) * (Voln[0] + Voln[2] - Volnp[1] - Volnm[1]) - 4.0 * (sc+(1.0/sc)) * Voln[1] ) - Volnm[2];
    }
    else {
        float utemp = Ue[sn];
        float jsumtmp = 0.0;
        for (unsigned pole = 0; pole < PolesNumber; ++pole) {
            jsumtmp += (1.0 + dkp[pole][sn]) * J[pole][sn];
        }
        Ue[sn] = b1Ue[sn]*Ue[sn] + 2.0*b2Ue[sn]*Dtw*I[sn-1]/Cke[sn] - b2Ue[sn]*Dtw*(0.5*jsumtmp)/Cke[sn];
        for (unsigned pole = 0; pole < PolesNumber; ++pole) {
            J[pole][sn] = dkp[pole][sn] * J[pole][sn] +  dbetap[pole][sn] * (Ue[sn] - utemp);
        }
        
        Us[sn] = b1Us[sn]*Us[sn] + 2.0*b2Us[sn]*Dtw*I[sn-1]/Cks[sn];
    }
}


float ObliqueWire::getTipWireCapacitance(WirePointType type)
{
    if (type == WIRE_START) {
        return Clke[0] * Dl * 0.5;
    }
    else {
        return Clke[mNumberOfSegments-1] * Dl * 0.5;
    }
}

float ObliqueWire::getTipWireFirstPartOfVoltage(WirePointType type)
{
    if (type == WIRE_START) {
        return Ue[0] * b1Ue[0];
    }
    else {
        return Ue[mNumberOfSegments] * b1Ue[mNumberOfSegments];
    }
}


float ObliqueWire::getTipWireSecondPartOfVoltage(WirePointType type)
{
    if (type == WIRE_START) {
        return - b2Ue[0] * Dtw * I[0];
    }
    else {
        return b2Ue[mNumberOfSegments] * Dtw * I[mNumberOfSegments-1];
    }
}


void ObliqueWire::setResistorInSegment(int segm, float resistance)
{
    if (segm >= 0 && resistance >= 0.0) {
        float R = resistance/Dl;
        unsigned usegm = static_cast<unsigned>(segm);
        b1I[usegm] = (1.0 - R*Dtw/(2.0*Lk[usegm])) / (1.0 + R*Dtw/(2.0*Lk[usegm]));
        b2I[usegm] = 1.0 / (1.0 + R*Dtw/(2.0*Lk[usegm]));
    }
}


void ObliqueWire::setTipWireVoltage(WirePointType type, float vn, float vnprev)
{
    unsigned seg = 0;
    unsigned idx = 0;
    if (type == WIRE_END) {
        seg = mNumberOfSegments;
        idx = 1;
    }
    float veprev = Ue[seg];
    Ue[seg] = vn;
    if (mSheathDiameter > mDiameter) {
        Us[seg] = (vn*Ep[idx] - vnprev*Em[idx] + Us[seg]*(Sm[idx]+Em[idx]) - getJr(type)) / (Ep[idx]+Sp[idx]);
        Ue[seg] = vn - Us[seg];
    }

    for (unsigned pole = 0; pole < PolesNumber; ++pole) {
        J[pole][seg] = dkp[pole][seg] * J[pole][seg] + dbetap[pole][seg] * (Ue[seg] - veprev);
    }
}


void ObliqueWire::applyVoltage(void)
{
    if (mVoltageSourceSegment >= 0 && mSourceValue) {
        unsigned useg = static_cast<unsigned>(mVoltageSourceSegment);
        I[useg] += b2I[useg] * Dtw*(*mSourceValue)/(Lk[useg]*Dl);
    }
}


void ObliqueWire::applyCurrent(void)
{
    if (mCurrentSourceSegment >= 0 && mSourceValue) {
        I[static_cast<unsigned>(mCurrentSourceSegment)] = *mSourceValue;
    }
}


void ObliqueWire::outputVoltage(void)
{
    if (mFirstVoltageCalcWire && mCalcValue) {
        *mCalcValue = Ue[0];
    }
    else if (mSecondVoltageCalcWire && mCalcValue) {
        *mCalcValue -= Ue[0];
    }
}


void ObliqueWire::outputCurrent(void)
{
    if (mCurrentCalcSegment >= 0 && mCalcValue) {
        *mCalcValue = I[static_cast<unsigned>(mCurrentCalcSegment)];
    }
}




WireArea::WireArea(const Model &fdtdvol, std::vector<ObliqueWire> array)
    : mCellSize{fdtdvol.getCellSize()}, mVolSize{fdtdvol.getVolumeSize()}, mArrayOfWires{array}
{
    addNodesAutomatically();
    
    // add second voltage calculation wire
    for (unsigned node = 0; node < WiresNodes.size(); ++node) {
        for (unsigned wire = 0; wire < WiresNodes[node].WireNumber.size(); ++wire) {
            if (WiresNodes[node].WirePoint[wire] == WIRE_START &&
                mArrayOfWires[WiresNodes[node].WireNumber[wire]].getFirstVoltageCalcWire()) {
                for (unsigned wiresec = 0; wiresec < WiresNodes[node].WireNumber.size(); ++wiresec) {
                    if (WiresNodes[node].WirePoint[wiresec] == WIRE_START &&
                            !mArrayOfWires[WiresNodes[node].WireNumber[wiresec]].getFirstVoltageCalcWire() &&
                            mArrayOfWires[WiresNodes[node].WireNumber[wiresec]].getCurrentCalcSegment() == NO_SOURCE_OR_CALC &&
                            !mArrayOfWires[WiresNodes[node].WireNumber[wiresec]].isSeparatedStart()) {
                        mArrayOfWires[WiresNodes[node].WireNumber[wiresec]].setSecondVoltageCalcWire(true);
                        mArrayOfWires[WiresNodes[node].WireNumber[wiresec]].setCalcOutput(
                            mArrayOfWires[WiresNodes[node].WireNumber[wire]].getCalcOutput());
                        goto nextnode;
                    }
                }
            }
        }
        nextnode:;
    }
    
    removeSeparatedWires();

    // connect separated wires (might be needed depending on situation).
    for (unsigned wire = 0; wire < mArrayOfWires.size(); ++wire) {
        XyzPointFloat start = mArrayOfWires[wire].getStart();
        for (unsigned wirec = 0; wirec < mArrayOfWires.size(); ++wirec) {
            XyzPointFloat startc = mArrayOfWires[wirec].getStart();
            if (mArrayOfWires[wire].isSeparatedStart() && mArrayOfWires[wirec].isSeparatedStart() &&
                wirec != wire && !isWireInNodeList(wirec, WIRE_START) &&
                getDistanceBetweenPoints(start, startc) < mCellSize*CONNECTION_DISTANCE) {
                // add wire to NodeList
                if (!isWireInNodeList(wire, WIRE_START)) {
                    WiresNodes.push_back(WireNode(wire, WIRE_START));
                }
                WiresNodes[WiresNodes.size()-1].WireNumber.push_back(wirec);
                WiresNodes[WiresNodes.size()-1].WirePoint.push_back(WIRE_START);
            }
        }
    }
    
    for (unsigned node = 0; node < WiresNodes.size(); ++node) {
        WiresNodes[node].coefVn = 0.0;
        for (unsigned wire = 0; wire < WiresNodes[node].WireNumber.size(); ++wire) {
            WiresNodes[node].coefVn += mArrayOfWires[WiresNodes[node].WireNumber[wire]].getCoefVn(WiresNodes[node].WirePoint[wire]);
        }
        WiresNodes[node].coefDiv = 0.0;
        for (unsigned wire = 0; wire < WiresNodes[node].WireNumber.size(); ++wire) {
            WiresNodes[node].coefDiv += mArrayOfWires[WiresNodes[node].WireNumber[wire]].getCoefDiv(WiresNodes[node].WirePoint[wire]);
        }
    }
}


bool WireArea::isEndWireCloseToCell(unsigned x, unsigned y, unsigned z)
{
    for (unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        if ((fabs(static_cast<float>(x)*mCellSize - mArrayOfWires[cnt].getStart().x) < mCellSize ||
             fabs(static_cast<float>(x)*mCellSize - mArrayOfWires[cnt].getEnd().x) < mCellSize) &&
            (fabs(static_cast<float>(y)*mCellSize - mArrayOfWires[cnt].getStart().y) < mCellSize ||
             fabs(static_cast<float>(y)*mCellSize - mArrayOfWires[cnt].getEnd().y) < mCellSize) &&
            (fabs(static_cast<float>(z)*mCellSize - mArrayOfWires[cnt].getStart().z) < mCellSize ||
             fabs(static_cast<float>(z)*mCellSize - mArrayOfWires[cnt].getEnd().z) < mCellSize)) {
            return true;
        }
    }
    return false;
}


void WireArea::checkCurrentTraceContinuity(void)
{
    std::cout << "============= Current Trace Continuity =============" << std::endl;
    float ***pcedgex = malloc3dArray<float>(mVolSize);
    float ***pcedgey = malloc3dArray<float>(mVolSize);
    float ***pcedgez = malloc3dArray<float>(mVolSize);
    
    float divD = 1.0/mCellSize;
    
    for (unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        for (unsigned sp = 0; sp < mArrayOfWires[cnt].getNumberOfSegmentParts(); ++sp) {
            XyzPoint cell = mArrayOfWires[cnt].getSegmentPart(sp).getCell();
            
            pcedgex[cell.x][cell.y][cell.z] += mArrayOfWires[cnt].getSegmentPart(sp).getPcx(0) * divD;
            pcedgex[cell.x][cell.y+1][cell.z] += mArrayOfWires[cnt].getSegmentPart(sp).getPcx(1) * divD;
            pcedgex[cell.x][cell.y][cell.z+1] += mArrayOfWires[cnt].getSegmentPart(sp).getPcx(2) * divD;
            pcedgex[cell.x][cell.y+1][cell.z+1] += mArrayOfWires[cnt].getSegmentPart(sp).getPcx(3) * divD;
            
            pcedgey[cell.x][cell.y][cell.z] += mArrayOfWires[cnt].getSegmentPart(sp).getPcy(0) * divD;
            pcedgey[cell.x+1][cell.y][cell.z] += mArrayOfWires[cnt].getSegmentPart(sp).getPcy(1) * divD;
            pcedgey[cell.x][cell.y][cell.z+1] += mArrayOfWires[cnt].getSegmentPart(sp).getPcy(2) * divD;
            pcedgey[cell.x+1][cell.y][cell.z+1] += mArrayOfWires[cnt].getSegmentPart(sp).getPcy(3) * divD;
            
            pcedgez[cell.x][cell.y][cell.z] += mArrayOfWires[cnt].getSegmentPart(sp).getPcz(0) * divD;
            pcedgez[cell.x][cell.y+1][cell.z] += mArrayOfWires[cnt].getSegmentPart(sp).getPcz(1) * divD;
            pcedgez[cell.x+1][cell.y][cell.z] += mArrayOfWires[cnt].getSegmentPart(sp).getPcz(2) * divD;
            pcedgez[cell.x+1][cell.y+1][cell.z] += mArrayOfWires[cnt].getSegmentPart(sp).getPcz(3) * divD;
        }
    }
    
    float sum;
    for (unsigned x = 1; x < mVolSize.x; ++x) {
        for (unsigned y = 1; y < mVolSize.y; ++y) {
            for (unsigned z = 1; z < mVolSize.z; ++z) {
                sum = pcedgex[x][y][z] + pcedgey[x][y][z] + pcedgez[x][y][z] -
                    ( pcedgex[x-1][y][z] + pcedgey[x][y-1][z] + pcedgez[x][y][z-1] );
                if (!isEndWireCloseToCell(x, y, z)) {
                    if (fabs(sum) > 1.0e-5) {
                        std::cout << sum << "   x" << x << "   y" << y << "   z" << z << std::endl;
                    }
                }
            }
        }
    }
    
    free3dArray<float>(pcedgex, mVolSize);
    free3dArray<float>(pcedgey, mVolSize);
    free3dArray<float>(pcedgez, mVolSize);
    std::cout << "EOF========== Current Trace Continuity =============" << std::endl;
}


void WireArea::calcWiresCurrents(void)
{
    for(unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        mArrayOfWires[cnt].calcCurrents();
    }
}


void WireArea::calcWiresVoltages(void)
{
    for(unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        mArrayOfWires[cnt].calcVoltages();
    }
}


bool WireArea::isWireInNodeList(unsigned wireNumber, WirePointType wirePoint)
{
    for (unsigned cnt = 0; cnt < WiresNodes.size(); ++cnt) {
        for (unsigned cntw = 0; cntw < WiresNodes[cnt].WireNumber.size(); ++cntw) {
            if (wireNumber == WiresNodes[cnt].WireNumber[cntw] &&
                wirePoint == WiresNodes[cnt].WirePoint[cntw]) {
                return true;
            }
        }
    }
    return false;
}


void WireArea::calcVoltagesInWireEnds(void)
{
    // wires not in nodes
    for(unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        if (!isWireInNodeList(cnt, WIRE_START)) {
            mArrayOfWires[cnt].calcVoltageInStartNode();
        }
        if (!isWireInNodeList(cnt, WIRE_END)) {
            mArrayOfWires[cnt].calcVoltageInEndNode();
        }
    }
    
    // wires in nodes
    for(unsigned node = 0; node < WiresNodes.size(); ++node) {
        float VskZk = 0.0;
        for (unsigned wire = 0; wire < WiresNodes[node].WireNumber.size(); ++wire) {
            VskZk += mArrayOfWires[WiresNodes[node].WireNumber[wire]].getCoefVsZk(WiresNodes[node].WirePoint[wire]);
        }
        
        float I = 0.0;
        for (unsigned wire = 0; wire < WiresNodes[node].WireNumber.size(); ++wire) {
            if (WiresNodes[node].WirePoint[wire] == WIRE_START) {
                I -= mArrayOfWires[WiresNodes[node].WireNumber[wire]].getCurrent(0);
            }
            else {
                float sn = mArrayOfWires[WiresNodes[node].WireNumber[wire]].getNumberOfSegments();
                I += mArrayOfWires[WiresNodes[node].WireNumber[wire]].getCurrent(sn-1);
            }
        }
        
        float Jr = 0.0;
        for (unsigned wire = 0; wire < WiresNodes[node].WireNumber.size(); ++wire) {
            Jr += mArrayOfWires[WiresNodes[node].WireNumber[wire]].getJr(WiresNodes[node].WirePoint[wire]);
        }
        
        float savedVn = WiresNodes[node].Vn;
        WiresNodes[node].Vn = (WiresNodes[node].Vn * WiresNodes[node].coefVn - VskZk + I - Jr) / WiresNodes[node].coefDiv;
        
        for (unsigned wire = 0; wire < WiresNodes[node].WireNumber.size(); ++wire) {
            mArrayOfWires[WiresNodes[node].WireNumber[wire]].setTipWireVoltage(WiresNodes[node].WirePoint[wire], WiresNodes[node].Vn, savedVn);
        }
    }
}


void WireArea::wiresToFdtd(void)
{
    for(unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        mArrayOfWires[cnt].setJ();
    }
}


void WireArea::addNode(WireNode node)
{
    WiresNodes.push_back(node);
}


void WireArea::removeWireFromNode(unsigned wireNumber, WirePointType wirePoint)
{
    // searches combination wireNumber+wirePoint in nodes
    // if it finds a node containing wireNumber+wirePoint, and there are only 2 wires there - removes the node
    // otherwise, removes wireNumber+wirePoint from the node
    for (unsigned cnt = 0; cnt < WiresNodes.size(); ++cnt) {
        for (unsigned cntw = 0; cntw < WiresNodes[cnt].WireNumber.size(); ++cntw) {
            if (wireNumber == WiresNodes[cnt].WireNumber[cntw] &&
                wirePoint == WiresNodes[cnt].WirePoint[cntw]) {
                if (WiresNodes[cnt].WireNumber.size() == 2) {
                    // delete node
                    WiresNodes.erase(WiresNodes.begin()+static_cast<int>(cnt));
                }
                else {
                    // delete wire from node
                    WiresNodes[cnt].WireNumber.erase(WiresNodes[cnt].WireNumber.begin()+static_cast<int>(cntw));
                    WiresNodes[cnt].WirePoint.erase(WiresNodes[cnt].WirePoint.begin()+static_cast<int>(cntw));
                }
            }
        }
    }
}


void WireArea::addNodesAutomatically(void)
{
    for (unsigned wire = 0; wire < mArrayOfWires.size(); ++wire) {
        XyzPointFloat start = mArrayOfWires[wire].getStart();
        for (unsigned wirec = 0; wirec < mArrayOfWires.size(); ++wirec) {
            XyzPointFloat startc = mArrayOfWires[wirec].getStart();
            if (wirec != wire && !isWireInNodeList(wirec, WIRE_START) &&
                getDistanceBetweenPoints(start, startc) < mCellSize*CONNECTION_DISTANCE) {
                // add wire to NodeList
                if (!isWireInNodeList(wire, WIRE_START)) {
                    WiresNodes.push_back(WireNode(wire, WIRE_START));
                }
                WiresNodes[WiresNodes.size()-1].WireNumber.push_back(wirec);
                WiresNodes[WiresNodes.size()-1].WirePoint.push_back(WIRE_START);
            }
            XyzPointFloat endc = mArrayOfWires[wirec].getEnd();
            if (wirec != wire && !isWireInNodeList(wirec, WIRE_END) &&
                getDistanceBetweenPoints(start, endc) < mCellSize*CONNECTION_DISTANCE) {
                // add wire to NodeList
                if (!isWireInNodeList(wire, WIRE_START)) {
                    WiresNodes.push_back(WireNode(wire, WIRE_START));
                }
                WiresNodes[WiresNodes.size()-1].WireNumber.push_back(wirec);
                WiresNodes[WiresNodes.size()-1].WirePoint.push_back(WIRE_END);
            }
        }
        
        XyzPointFloat end = mArrayOfWires[wire].getEnd();
        for (unsigned wirec = 0; wirec < mArrayOfWires.size(); ++wirec) {
            XyzPointFloat startc = mArrayOfWires[wirec].getStart();
            if (wirec != wire && !isWireInNodeList(wirec, WIRE_START) &&
                getDistanceBetweenPoints(end, startc) < mCellSize*CONNECTION_DISTANCE) {
                // add wire to NodeList
                if (!isWireInNodeList(wire, WIRE_END)) {
                    WiresNodes.push_back(WireNode(wire, WIRE_END));
                }
                WiresNodes[WiresNodes.size()-1].WireNumber.push_back(wirec);
                WiresNodes[WiresNodes.size()-1].WirePoint.push_back(WIRE_START);
            }
            XyzPointFloat endc = mArrayOfWires[wirec].getEnd();
            if (wirec != wire && !isWireInNodeList(wirec, WIRE_END) &&
                getDistanceBetweenPoints(end, endc) < mCellSize*CONNECTION_DISTANCE) {
                // add wire to NodeList
                if (!isWireInNodeList(wire, WIRE_END)) {
                    WiresNodes.push_back(WireNode(wire, WIRE_END));
                }
                WiresNodes[WiresNodes.size()-1].WireNumber.push_back(wirec);
                WiresNodes[WiresNodes.size()-1].WirePoint.push_back(WIRE_END);
            }
        }
    }
}


void WireArea::removeSeparatedWires(void)
{
    for (unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        if (mArrayOfWires[cnt].isSeparatedStart()) {
            removeWireFromNode(cnt, WIRE_START);
        }
    }
}


void WireArea::applyVoltages(void)
{
    for(unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        mArrayOfWires[cnt].applyVoltage();
    }
}


void WireArea::applyCurrents(void)
{
    for(unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        mArrayOfWires[cnt].applyCurrent();
    }
}


void WireArea::outputVoltages(void)
{
    for(unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        mArrayOfWires[cnt].outputVoltage();
    }
}


void WireArea::outputCurrents(void)
{
    for(unsigned cnt = 0; cnt < mArrayOfWires.size(); ++cnt) {
        mArrayOfWires[cnt].outputCurrent();
    }
}


void WireArea::doFirstHalfStepJob(unsigned)
{
    wiresToFdtd();
    calcWiresVoltages();
    calcVoltagesInWireEnds();
}


void WireArea::doSecondHalfStepJob(unsigned)
{
    calcWiresCurrents();
    applyVoltages();
    applyCurrents();
    outputVoltages();
    outputCurrents();
}


}    // namespace fdtd


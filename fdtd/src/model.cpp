///
/// \file model.cpp
/// \brief Main FDTD model data.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "model.h"


namespace fdtd {


void TimeStepItem::doFirstHalfStepJob(unsigned)
{
    mFirstHalfStepFunction(mModel);
}


void TimeStepItem::doSecondHalfStepJob(unsigned)
{
    mSecondHalfStepFunction(mModel);
}


Model::Model(XyzSize size, float dl, float s, unsigned threadsNumber)
    : mVolumeSize{size}, mD{dl}, mS{s}, mDt{s*dl/C0}, mThreadsNumber{threadsNumber}
{
    omp_set_num_threads(static_cast<int>(threadsNumber));
}


Model::~Model()
{
    if (mMemoryManager) {
        freeBasicParameters();
        freeUpmlParameters();
        freeCpmlParameters();
        freeCpmlFieldParameters();
        freeDebyeAdeParameters();
        freeAlteredParameters();
    }
}


void Model::allocateBasicParameters(void)
{
    if (!mAllocatedParameters.basic) {
        mb.Ex = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        mb.Ey = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        mb.Ez = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mb.Hx = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        mb.Hy = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        mb.Hz = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        mb.Cax = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        mb.Cay = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        mb.Caz = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mb.Cbx = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        mb.Cby = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        mb.Cbz = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mb.Dbx = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        mb.Dby = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        mb.Dbz = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        mAllocatedParameters.basic = true;
    }
}


void Model::allocateUpmlParameters(void)
{
    if (!mAllocatedParameters.upml) {
        mu.Px = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        mu.Py = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        mu.Pz = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mu.Psx = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        mu.Psy = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        mu.Psz = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mu.Bx = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        mu.By = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        mu.Bz = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        mu.Bsx = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        mu.Bsy = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        mu.Bsz = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        mu.Cex = allocateCparamArrayForUpml(mVolumeSize.x, mDt);
        mu.Cey = allocateCparamArrayForUpml(mVolumeSize.y, mDt);
        mu.Cez = allocateCparamArrayForUpml(mVolumeSize.z, mDt);
        mu.Chx = allocateCparamArrayForUpml(mVolumeSize.x+1, mDt);
        mu.Chy = allocateCparamArrayForUpml(mVolumeSize.y+1, mDt);
        mu.Chz = allocateCparamArrayForUpml(mVolumeSize.z+1, mDt);
        mAllocatedParameters.upml = true;
    }
}


float** Model::allocateCparamArrayForUpml(unsigned th, float dti)
{
    float **array = new float*[6];
    for (unsigned i = 0; i < 6; ++i) {
        array[i] = new float[th];
    }

    // default initialization
    float sigma_tmp = 0.0, kappa_tmp = 1.0;
    for (unsigned i = 0; i < th; ++i) {
        array[0][i] = ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
                      ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        array[1][i] = ( 2.0 * EPS0 ) / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        array[2][i] = ( 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti ) /
                      ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        array[3][i] = 1.0 / ( 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti );
        array[4][i] = 2.0 * EPS0 * kappa_tmp + sigma_tmp * dti;
        array[5][i] = 2.0 * EPS0 * kappa_tmp - sigma_tmp * dti;
    }

    return array;
}


void Model::allocateCpmlParameters(void)
{
    if (!mAllocatedParameters.cpml) {
        mc.Bex = new float[mVolumeSize.x];
        mc.Cex = new float[mVolumeSize.x];
        mc.Bey = new float[mVolumeSize.y];
        mc.Cey = new float[mVolumeSize.y];
        mc.Bez = new float[mVolumeSize.z];
        mc.Cez = new float[mVolumeSize.z];
        mc.Bhx = new float[mVolumeSize.x];
        mc.Chx = new float[mVolumeSize.x];
        mc.Bhy = new float[mVolumeSize.y];
        mc.Chy = new float[mVolumeSize.y];
        mc.Bhz = new float[mVolumeSize.z];
        mc.Chz = new float[mVolumeSize.z];
        mc.dkex = new float[mVolumeSize.x];
        mc.dkey = new float[mVolumeSize.y];
        mc.dkez = new float[mVolumeSize.z];
        mc.dkhx = new float[mVolumeSize.x+1];
        mc.dkhy = new float[mVolumeSize.y+1];
        mc.dkhz = new float[mVolumeSize.z+1];
        mAllocatedParameters.cpml = true;
    }
}


void Model::allocateCpmlFieldParameters(void)
{
    if (!mAllocatedParameters.cpmlfield) {
        mc.PsiEyx = malloc3dArray<float>(XyzSize(mAbcThicknessBot.x + mAbcThicknessTop.x + 1, mVolumeSize.y, mVolumeSize.z));
        mc.PsiEzx = malloc3dArray<float>(XyzSize(mAbcThicknessBot.x + mAbcThicknessTop.x + 1, mVolumeSize.y, mVolumeSize.z));
        mc.PsiExy = malloc3dArray<float>(XyzSize(mVolumeSize.x, mAbcThicknessBot.y + mAbcThicknessTop.y + 1, mVolumeSize.z));
        mc.PsiEzy = malloc3dArray<float>(XyzSize(mVolumeSize.x, mAbcThicknessBot.y + mAbcThicknessTop.y + 1, mVolumeSize.z));
        mc.PsiExz = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y, mAbcThicknessBot.z + mAbcThicknessTop.z + 1));
        mc.PsiEyz = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y, mAbcThicknessBot.z + mAbcThicknessTop.z + 1));
        mc.PsiHyx = malloc3dArray<float>(XyzSize(mAbcThicknessBot.x + mAbcThicknessTop.x, mVolumeSize.y, mVolumeSize.z));
        mc.PsiHzx = malloc3dArray<float>(XyzSize(mAbcThicknessBot.x + mAbcThicknessTop.x, mVolumeSize.y, mVolumeSize.z));
        mc.PsiHxy = malloc3dArray<float>(XyzSize(mVolumeSize.x, mAbcThicknessBot.y + mAbcThicknessTop.y, mVolumeSize.z));
        mc.PsiHzy = malloc3dArray<float>(XyzSize(mVolumeSize.x, mAbcThicknessBot.y + mAbcThicknessTop.y, mVolumeSize.z));
        mc.PsiHxz = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y, mAbcThicknessBot.z + mAbcThicknessTop.z));
        mc.PsiHyz = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y, mAbcThicknessBot.z + mAbcThicknessTop.z));
        mAllocatedParameters.cpmlfield = true;
    }
}


void Model::allocateDebyeAdeParameters(void)
{
    if (!mAllocatedParameters.debye) {
        md.Jx = new float***[md.PolesNumber];
        md.Jy = new float***[md.PolesNumber];
        md.Jz = new float***[md.PolesNumber];
        md.kpx = new float***[md.PolesNumber];
        md.kpy = new float***[md.PolesNumber];
        md.kpz = new float***[md.PolesNumber];
        md.dbetapx = new float***[md.PolesNumber];
        md.dbetapy = new float***[md.PolesNumber];
        md.dbetapz = new float***[md.PolesNumber];
        for (unsigned pole = 0; pole < md.PolesNumber; ++pole) {
            md.Jx[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
            md.Jy[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
            md.Jz[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
            md.kpx[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z+1));
            md.kpy[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z+1));
            md.kpz[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z+1));
            md.dbetapx[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
            md.dbetapy[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
            md.dbetapz[pole] = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        }
        md.Exs = malloc3dArray<float>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        md.Eys = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        md.Ezs = malloc3dArray<float>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mAllocatedParameters.debye = true;
    }
}


void Model::allocateAlteredParameters(void)
{
    if (!mAllocatedParameters.altered) {
        ma.cbx = malloc3dArray<bool>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        ma.cby = malloc3dArray<bool>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        ma.cbz = malloc3dArray<bool>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        ma.dbx = malloc3dArray<bool>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        ma.dby = malloc3dArray<bool>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        ma.dbz = malloc3dArray<bool>(XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        ma.dbetapx = malloc3dArray<bool>(XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        ma.dbetapy = malloc3dArray<bool>(XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        ma.dbetapz = malloc3dArray<bool>(XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mAllocatedParameters.altered = true;
    }
}


void Model::freeBasicParameters(void)
{
    if (mAllocatedParameters.basic) {
        free3dArray<float>(mb.Ex, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        free3dArray<float>(mb.Ey, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<float>(mb.Ez, XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mb.Hx, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        free3dArray<float>(mb.Hy, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mb.Hz, XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<float>(mb.Cax, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        free3dArray<float>(mb.Cay, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<float>(mb.Caz, XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mb.Cbx, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        free3dArray<float>(mb.Cby, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<float>(mb.Cbz, XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mb.Dbx, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        free3dArray<float>(mb.Dby, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mb.Dbz, XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        mAllocatedParameters.basic = false;
    }
}


void Model::freeUpmlParameters(void)
{
    if (mAllocatedParameters.upml) {
        free3dArray<float>(mu.Px, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        free3dArray<float>(mu.Py, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<float>(mu.Pz, XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mu.Psx, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        free3dArray<float>(mu.Psy, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<float>(mu.Psz, XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mu.Bx, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        free3dArray<float>(mu.By, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mu.Bz, XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<float>(mu.Bsx, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        free3dArray<float>(mu.Bsy, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<float>(mu.Bsz, XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        freeCparamArrayForUpml(mu.Cex);
        freeCparamArrayForUpml(mu.Cey);
        freeCparamArrayForUpml(mu.Cez);
        freeCparamArrayForUpml(mu.Chx);
        freeCparamArrayForUpml(mu.Chy);
        freeCparamArrayForUpml(mu.Chz);
        mAllocatedParameters.upml = false;
    }
}


void Model::freeCparamArrayForUpml(float** array)
{
    for (unsigned i = 0; i < 6; ++i) {
        delete[] array[i];
    }
    delete[] array;
    array = nullptr;
}


void Model::freeCpmlParameters(void)
{
    if (mAllocatedParameters.cpml) {
        delete[] mc.Bex;
        delete[] mc.Cex;
        delete[] mc.Bey;
        delete[] mc.Cey;
        delete[] mc.Bez;
        delete[] mc.Cez;
        delete[] mc.Bhx;
        delete[] mc.Chx;
        delete[] mc.Bhy;
        delete[] mc.Chy;
        delete[] mc.Bhz;
        delete[] mc.Chz;
        delete[] mc.dkex;
        delete[] mc.dkey;
        delete[] mc.dkez;
        delete[] mc.dkhx;
        delete[] mc.dkhy;
        delete[] mc.dkhz;
        mAllocatedParameters.cpml = false;
    }
}


void Model::freeCpmlFieldParameters(void)
{
    if (mAllocatedParameters.cpmlfield) {
        free3dArray<float>(mc.PsiEyx, XyzSize(mAbcThicknessBot.x + mAbcThicknessTop.x + 1, mVolumeSize.y, mVolumeSize.z));
        free3dArray<float>(mc.PsiEzx, XyzSize(mAbcThicknessBot.x + mAbcThicknessTop.x + 1, mVolumeSize.y, mVolumeSize.z));
        free3dArray<float>(mc.PsiExy, XyzSize(mVolumeSize.x, mAbcThicknessBot.y + mAbcThicknessTop.y + 1, mVolumeSize.z));
        free3dArray<float>(mc.PsiEzy, XyzSize(mVolumeSize.x, mAbcThicknessBot.y + mAbcThicknessTop.y + 1, mVolumeSize.z));
        free3dArray<float>(mc.PsiExz, XyzSize(mVolumeSize.x, mVolumeSize.y, mAbcThicknessBot.z + mAbcThicknessTop.z + 1));
        free3dArray<float>(mc.PsiEyz, XyzSize(mVolumeSize.x, mVolumeSize.y, mAbcThicknessBot.z + mAbcThicknessTop.z + 1));
        free3dArray<float>(mc.PsiHyx, XyzSize(mAbcThicknessBot.x + mAbcThicknessTop.x, mVolumeSize.y, mVolumeSize.z));
        free3dArray<float>(mc.PsiHzx, XyzSize(mAbcThicknessBot.x + mAbcThicknessTop.x, mVolumeSize.y, mVolumeSize.z));
        free3dArray<float>(mc.PsiHxy, XyzSize(mVolumeSize.x, mAbcThicknessBot.y + mAbcThicknessTop.y, mVolumeSize.z));
        free3dArray<float>(mc.PsiHzy, XyzSize(mVolumeSize.x, mAbcThicknessBot.y + mAbcThicknessTop.y, mVolumeSize.z));
        free3dArray<float>(mc.PsiHxz, XyzSize(mVolumeSize.x, mVolumeSize.y, mAbcThicknessBot.z + mAbcThicknessTop.z));
        free3dArray<float>(mc.PsiHyz, XyzSize(mVolumeSize.x, mVolumeSize.y, mAbcThicknessBot.z + mAbcThicknessTop.z));
        mAllocatedParameters.cpmlfield = false;
    }
}


void Model::freeDebyeAdeParameters(void)
{
    if (mAllocatedParameters.debye) {
        for (unsigned pole = 0; pole < md.PolesNumber; pole++) {
            free3dArray<float>(md.Jx[pole], XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
            free3dArray<float>(md.Jy[pole], XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
            free3dArray<float>(md.Jz[pole], XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
            free3dArray<float>(md.kpx[pole], XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z+1));
            free3dArray<float>(md.kpy[pole], XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z+1));
            free3dArray<float>(md.kpz[pole], XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z+1));
            free3dArray<float>(md.dbetapx[pole], XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
            free3dArray<float>(md.dbetapy[pole], XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
            free3dArray<float>(md.dbetapz[pole], XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        }
        delete[] md.Jx;
        delete[] md.Jy;
        delete[] md.Jz;
        delete[] md.kpx;
        delete[] md.kpy;
        delete[] md.kpz;
        delete[] md.dbetapx;
        delete[] md.dbetapy;
        delete[] md.dbetapz;
        free3dArray<float>(md.Exs, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        free3dArray<float>(md.Eys, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<float>(md.Ezs, XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mAllocatedParameters.debye = false;
    }
}


void Model::freeAlteredParameters(void)
{
    if (mAllocatedParameters.altered) {
        free3dArray<bool>(ma.cbx, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        free3dArray<bool>(ma.cby, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<bool>(ma.cbz, XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<bool>(ma.dbx, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z));
        free3dArray<bool>(ma.dby, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z));
        free3dArray<bool>(ma.dbz, XyzSize(mVolumeSize.x, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<bool>(ma.dbetapx, XyzSize(mVolumeSize.x, mVolumeSize.y+1, mVolumeSize.z+1));
        free3dArray<bool>(ma.dbetapy, XyzSize(mVolumeSize.x+1, mVolumeSize.y, mVolumeSize.z+1));
        free3dArray<bool>(ma.dbetapz, XyzSize(mVolumeSize.x+1, mVolumeSize.y+1, mVolumeSize.z));
        mAllocatedParameters.altered = false;
    }
}


float*** Model::getFieldComponent(FieldComponent comp) const
{
    float ***fpointer = nullptr;
    switch (comp) {
        case FieldComponent::ex:
            fpointer = mb.Ex;
            break;
        case FieldComponent::ey:
            fpointer = mb.Ey;
            break;
        case FieldComponent::ez:
            fpointer = mb.Ez;
            break;
        case FieldComponent::hx:
            fpointer = mb.Hx;
            break;
        case FieldComponent::hy:
            fpointer = mb.Hy;
            break;
        case FieldComponent::hz:
            fpointer = mb.Hz;
            break;
        default:
            break;
    }
    return fpointer;
}


float Model::getFieldInCell(const FieldComponent &component, const XyzPoint &cell) const
{
    if (component == FieldComponent::ex) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Ex coordinates are outside of the calculation volume (function getFieldInCell).");
        }
        return mb.Ex[cell.x][cell.y][cell.z];
    }
    else if (component == FieldComponent::ey) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Ey coordinates are outside of the calculation volume (function getFieldInCell).");
        }
        return mb.Ey[cell.x][cell.y][cell.z];
    }
    else if (component == FieldComponent::ez) {
        if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Ez coordinates are outside of the calculation volume (function getFieldInCell).");
        }
        return mb.Ez[cell.x][cell.y][cell.z];
    }
    else if (component == FieldComponent::hx) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Hx coordinates are outside of the calculation volume (function getFieldInCell).");
        }
        return mb.Hx[cell.x][cell.y][cell.z];
    }
    else if (component == FieldComponent::hy) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Hy coordinates are outside of the calculation volume (function getFieldInCell).");
        }
        return mb.Hy[cell.x][cell.y][cell.z];
    }
    else if (component == FieldComponent::hz) {
        if (cell.x >= mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Hz coordinates are outside of the calculation volume (function getFieldInCell).");
        }
        return mb.Hz[cell.x][cell.y][cell.z];
    }
    return 0.0;
}


float Model::getCaInCell(const XyzDirection &component, const XyzPoint &cell) const
{
    if (component == XyzDirection::x) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCaInCell).");
        }
        return mb.Cax[cell.x][cell.y][cell.z];
    }
    else if (component == XyzDirection::y) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCaInCell).");
        }
        return mb.Cay[cell.x][cell.y][cell.z];
    }
    else if (component == XyzDirection::z) {
        if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCaInCell).");
        }
        return mb.Caz[cell.x][cell.y][cell.z];
    }
    return 0.0;
}


float Model::getCbInCell(const XyzDirection &component, const XyzPoint &cell) const
{
    if (component == XyzDirection::x) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCbInCell).");
        }
        return mb.Cbx[cell.x][cell.y][cell.z];
    }
    else if (component == XyzDirection::y) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCbInCell).");
        }
        return mb.Cby[cell.x][cell.y][cell.z];
    }
    else if (component == XyzDirection::z) {
        if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCbInCell).");
        }
        return mb.Cbz[cell.x][cell.y][cell.z];
    }
    return 0.0;
}


float Model::getDbInCell(const XyzDirection &component, const XyzPoint &cell) const
{
    if (component == XyzDirection::x) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getDbInCell).");
        }
        return mb.Dbx[cell.x][cell.y][cell.z];
    }
    else if (component == XyzDirection::y) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getDbInCell).");
        }
        return mb.Dby[cell.x][cell.y][cell.z];
    }
    else if (component == XyzDirection::z) {
        if (cell.x >= mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function getDbInCell).");
        }
        return mb.Dbz[cell.x][cell.y][cell.z];
    }
    return 0.0;
}


float Model::getCax(const XyzPoint &cell) const
{
    if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCax).");
    }
    else {
        return mb.Cax[cell.x][cell.y][cell.z];
    }
}


float Model::getCay(const XyzPoint &cell) const
{
    if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCay).");
    }
    else {
        return mb.Cay[cell.x][cell.y][cell.z];
    }
}


float Model::getCaz(const XyzPoint &cell) const
{
    if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCaz).");
    }
    else {
        return mb.Caz[cell.x][cell.y][cell.z];
    }
}


float Model::getCbx(const XyzPoint &cell) const
{
    if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCbx).");
    }
    else {
        return mb.Cbx[cell.x][cell.y][cell.z];
    }
}


float Model::getCby(const XyzPoint &cell) const
{
    if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCby).");
    }
    else {
        return mb.Cby[cell.x][cell.y][cell.z];
    }
}


float Model::getCbz(const XyzPoint &cell) const
{
    if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function getCbz).");
    }
    else {
        return mb.Cbz[cell.x][cell.y][cell.z];
    }
}


void Model::setFieldInCell(const FieldComponent &component, const XyzPoint &cell, const float &value)
{
    if (component == FieldComponent::ex) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Ex coordinates are outside of the calculation volume (function setFieldInCell).");
        }
        mb.Ex[cell.x][cell.y][cell.z] = value;
    }
    else if (component == FieldComponent::ey) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Ey coordinates are outside of the calculation volume (function setFieldInCell).");
        }
        mb.Ey[cell.x][cell.y][cell.z] = value;
    }
    else if (component == FieldComponent::ez) {
        if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Ez coordinates are outside of the calculation volume (function setFieldInCell).");
        }
        mb.Ez[cell.x][cell.y][cell.z] = value;
    }
    else if (component == FieldComponent::hx) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Hx coordinates are outside of the calculation volume (function setFieldInCell).");
        }
        mb.Hx[cell.x][cell.y][cell.z] = value;
    }
    else if (component == FieldComponent::hy) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Hy coordinates are outside of the calculation volume (function setFieldInCell).");
        }
        mb.Hy[cell.x][cell.y][cell.z] = value;
    }
    else if (component == FieldComponent::hz) {
        if (cell.x >= mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Hz coordinates are outside of the calculation volume (function setFieldInCell).");
        }
        mb.Hz[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setSoftFieldInCell(const FieldComponent &component, const XyzPoint &cell, const float &value)
{
    if (component == FieldComponent::ex) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Ex coordinates are outside of the calculation volume (function setSoftFieldInCell).");
        }
        mb.Ex[cell.x][cell.y][cell.z] += value;
    }
    else if (component == FieldComponent::ey) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Ey coordinates are outside of the calculation volume (function setSoftFieldInCell).");
        }
        mb.Ey[cell.x][cell.y][cell.z] += value;
    }
    else if (component == FieldComponent::ez) {
        if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Ez coordinates are outside of the calculation volume (function setSoftFieldInCell).");
        }
        mb.Ez[cell.x][cell.y][cell.z] += value;
    }
    else if (component == FieldComponent::hx) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Hx coordinates are outside of the calculation volume (function setSoftFieldInCell).");
        }
        mb.Hx[cell.x][cell.y][cell.z] += value;
    }
    else if (component == FieldComponent::hy) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The Hy coordinates are outside of the calculation volume (function setSoftFieldInCell).");
        }
        mb.Hy[cell.x][cell.y][cell.z] += value;
    }
    else if (component == FieldComponent::hz) {
        if (cell.x >= mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The Hz coordinates are outside of the calculation volume (function setSoftFieldInCell).");
        }
        mb.Hz[cell.x][cell.y][cell.z] += value;
    }
}


void Model::setCaInCell(const XyzDirection &component, const XyzPoint &cell, const float &value)
{
    if (component == XyzDirection::x) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCaInCell).");
        }
        mb.Cax[cell.x][cell.y][cell.z] = value;
    }
    else if (component == XyzDirection::y) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCaInCell).");
        }
        mb.Cay[cell.x][cell.y][cell.z] = value;
    }
    else if (component == XyzDirection::z) {
        if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCaInCell).");
        }
        mb.Caz[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setCbInCell(const XyzDirection &component, const XyzPoint &cell, const float &value)
{
    if (component == XyzDirection::x) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCbInCell).");
        }
        mb.Cbx[cell.x][cell.y][cell.z] = value;
    }
    else if (component == XyzDirection::y) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCbInCell).");
        }
        mb.Cby[cell.x][cell.y][cell.z] = value;
    }
    else if (component == XyzDirection::z) {
        if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCbInCell).");
        }
        mb.Cbz[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setDbInCell(const XyzDirection &component, const XyzPoint &cell, const float &value)
{
    if (component == XyzDirection::x) {
        if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setDbInCell).");
        }
        mb.Dbx[cell.x][cell.y][cell.z] = value;
    }
    else if (component == XyzDirection::y) {
        if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setDbInCell).");
        }
        mb.Dby[cell.x][cell.y][cell.z] = value;
    }
    else if (component == XyzDirection::z) {
        if (cell.x >= mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
            throw std::invalid_argument("The coordinates are outside of the calculation volume (function setDbInCell).");
        }
        mb.Dbz[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setCax(const XyzPoint &cell, const float &value)
{
    if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCax).");
    }
    else {
        mb.Cax[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setCay(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCay).");
    }
    else {
        mb.Cay[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setCaz(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCaz).");
    }
    else {
        mb.Caz[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setCbx(const XyzPoint &cell, const float &value)
{
    if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCbx).");
    }
    else {
        mb.Cbx[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setCby(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCby).");
    }
    else {
        mb.Cby[cell.x][cell.y][cell.z] = value;
    }
}


void Model::setCbz(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function setCbz).");
    }
    else {
        mb.Cbz[cell.x][cell.y][cell.z] = value;
    }
}


void Model::mulCbx(const XyzPoint &cell, const float &value)
{
    if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulCbx).");
    }
    else {
        allocateAlteredParameters();
        if (!ma.cbx[cell.x][cell.y][cell.z]) {
            mb.Cbx[cell.x][cell.y][cell.z] *= value;
            ma.cbx[cell.x][cell.y][cell.z] = true;
        }
    }
}


void Model::mulCby(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulCby).");
    }
    else {
        allocateAlteredParameters();
        if (!ma.cby[cell.x][cell.y][cell.z]) {
            mb.Cby[cell.x][cell.y][cell.z] *= value;
            ma.cby[cell.x][cell.y][cell.z] = true;
        }
    }
}


void Model::mulCbz(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulCbz).");
    }
    else {
        allocateAlteredParameters();
        if (!ma.cbz[cell.x][cell.y][cell.z]) {
            mb.Cbz[cell.x][cell.y][cell.z] *= value;
            ma.cbz[cell.x][cell.y][cell.z] = true;
        }
    }
}


void Model::mulDbx(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z >= mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulDbx).");
    }
    else {
        allocateAlteredParameters();
        if (!ma.dbx[cell.x][cell.y][cell.z]) {
            mb.Dbx[cell.x][cell.y][cell.z] *= value;
            ma.dbx[cell.x][cell.y][cell.z] = true;
        }
    }
}


void Model::mulDby(const XyzPoint &cell, const float &value)
{
    if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulDby).");
    }
    else {
        allocateAlteredParameters();
        if (!ma.dby[cell.x][cell.y][cell.z]) {
            mb.Dby[cell.x][cell.y][cell.z] *= value;
            ma.dby[cell.x][cell.y][cell.z] = true;
        }
    }
}


void Model::mulDbz(const XyzPoint &cell, const float &value)
{
    if (cell.x >= mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulDbz).");
    }
    else {
        allocateAlteredParameters();
        if (!ma.dbz[cell.x][cell.y][cell.z]) {
            mb.Dbz[cell.x][cell.y][cell.z] *= value;
            ma.dbz[cell.x][cell.y][cell.z] = true;
        }
    }
}


void Model::mulDbetapx(const XyzPoint &cell, const float &value)
{
    if (cell.x >= mVolumeSize.x || cell.y > mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulDbetapx).");
    }
    else {
        allocateAlteredParameters();
        if (mAllocatedParameters.debye && !ma.dbetapx[cell.x][cell.y][cell.z]) {
            for (unsigned pole = 0; pole < md.PolesNumber; ++pole) {
                md.dbetapx[pole][cell.x][cell.y][cell.z] *= value;
            }
            ma.dbetapx[cell.x][cell.y][cell.z] = true;
        }
    }
}


void Model::mulDbetapy(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y >= mVolumeSize.y || cell.z > mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulDbetapy).");
    }
    else {
        allocateAlteredParameters();
        if (mAllocatedParameters.debye && !ma.dbetapy[cell.x][cell.y][cell.z]) {
            for (unsigned pole = 0; pole < md.PolesNumber; ++pole) {
                md.dbetapy[pole][cell.x][cell.y][cell.z] *= value;
            }
            ma.dbetapy[cell.x][cell.y][cell.z] = true;
        }
    }
}


void Model::mulDbetapz(const XyzPoint &cell, const float &value)
{
    if (cell.x > mVolumeSize.x || cell.y > mVolumeSize.y || cell.z >= mVolumeSize.z) {
        throw std::invalid_argument("The coordinates are outside of the calculation volume (function mulDbetapz).");
    }
    else {
        allocateAlteredParameters();
        if (mAllocatedParameters.debye && !ma.dbetapz[cell.x][cell.y][cell.z]) {
            for (unsigned pole = 0; pole < md.PolesNumber; ++pole) {
                md.dbetapz[pole][cell.x][cell.y][cell.z] *= value;
            }
            ma.dbetapz[cell.x][cell.y][cell.z] = true;
        }
    }
}


}    // namespace fdtd


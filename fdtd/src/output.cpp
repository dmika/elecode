///
/// \file output.cpp
/// \brief Calculation results output.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "output.h"


namespace fdtd {


Output::Output(std::ostream &outputStream, float dt, unsigned interval, float dInterp)
    : mOutputStream(outputStream)
{
    mInterval = interval;
    mDInterp = dInterp;
    mDt = dt;
    mtmoment = 0;
    if (dt > 0.0) {
        std::vector<float> time;
        mOutputArray.push_back(time);
    }
}


Output::Output(std::string fileName, float dt, unsigned interval, float dInterp)
    : mOutputFileStream(fileName), mOutputStream(mOutputFileStream)
{
    mFileName = fileName;
    mInterval = interval;
    mDInterp = dInterp;
    mDt = dt;
    mtmoment = 0;
    if (dt > 0.0) {
        std::vector<float> time;
        mOutputArray.push_back(time);
    }
}


Output::Output(std::shared_ptr<Model> model, std::string fileName, unsigned interval, float dInterp)
    : Output(fileName, model->getTimeStepDuration(), interval, dInterp)
{
    mFileName = fileName;
    mModel = model;
}


Output::Output(std::shared_ptr<Model> model, FieldComponent component, XyzPoint coordinates,
               std::string fileName, unsigned interval, float dInterp, float sign)
    : Output(fileName, model->getTimeStepDuration(), interval, dInterp)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (coordinates.x > volumeSize.x || coordinates.y > volumeSize.y || coordinates.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of Output).");
    }

    mFileName = fileName;
    mModel = model;
    addFieldPoint(component, coordinates, sign);
}


Output::~Output()
{
    save();
    if (mOutputFileStream.is_open()) {
        mOutputFileStream.close();
    }
}


void Output::doFirstHalfStepJob(unsigned)
{
}


void Output::doSecondHalfStepJob(unsigned timeStepNumber)
{
    for (unsigned point = 0; point < mFieldPointArray.size(); ++point) {
        XyzPoint coord = mFieldPointArray[point].mCellCoordinates;
        *mDataNow[mFieldPointArray[point].mIndex] = mFieldPointArray[point].mFieldArrayPointer[coord.x][coord.y][coord.z] *
                                                    mFieldPointArray[point].mSign;
    }
    
    if (mDInterp > 0.0) {
        while (mtmoment*mDInterp <= timeStepNumber*mDt && timeStepNumber) {
            mOutputArray[0].push_back(mtmoment*mDInterp);
            for(unsigned column = 0; column < mDataNow.size(); ++column) {
                float dts = mtmoment*mDInterp - (timeStepNumber-1)*mDt;
                float dI = *mDataNow[column] - mDataPrevious[column];
                float dIs = dts * dI / mDt;
                mOutputArray[column+1].push_back(mDataPrevious[column] + dIs);
            }
            mtmoment++;
        }
    }
    else {
        mOutputArray[0].push_back(mDt*timeStepNumber);
        for (unsigned column = 0; column < mDataNow.size(); ++column) {
            mOutputArray[column+1].push_back(*mDataNow[column]);
        }
    }
    
    for (unsigned column = 0; column < mDataNow.size(); column++) {
        mDataPrevious[column] = *mDataNow[column];
    }
    
    if (mInterval && timeStepNumber && !(timeStepNumber%mInterval)) {
        save();
    }
}


void Output::addFieldPoint(FieldPoint point)
{
    if (!point.mFieldArrayPointer) {
        point.mFieldArrayPointer = mModel->getFieldComponent(point.mComponent);
    }
    point.mIndex = mDataNow.size();
    mDataNow.push_back(std::make_shared<float>(0.0));
    mDataPrevious.push_back(0.0);
    mFieldPointArray.push_back(point);
    mOutputArray.push_back(std::vector<float>());
}


void Output::addFieldPoint(FieldComponent component, XyzPoint coordinates, float sign, float ***arrayPointer)
{
    addFieldPoint(FieldPoint(component, coordinates, sign, arrayPointer));
}


void Output::addDataPointer(std::shared_ptr<float> datapointer)
{
    mDataNow.push_back(datapointer);
    mDataPrevious.push_back(0.0);
    mOutputArray.push_back(std::vector<float>());
}


std::shared_ptr<float> Output::getAddedDataPointer(void)
{
    std::shared_ptr<float> datapointer = std::make_shared<float>(0.0);
    mDataNow.push_back(datapointer);
    mDataPrevious.push_back(0.0);
    mOutputArray.push_back(std::vector<float>());
    return datapointer;
}


void Output::save(void)
{
    mOutputStream.seekp(std::ios_base::beg);
    for (unsigned row = 0; row < mOutputArray[0].size(); ++row) {
        for (unsigned column = 0; column < mOutputArray.size(); ++column) {
            mOutputStream << mOutputArray[column][row];
            if (column < mOutputArray.size()-1) {
                mOutputStream << "    ";
            }
        }
        mOutputStream << '\n';
    }
    mOutputStream << std::flush;
}


VoltagePath::VoltagePath(float d)
    : mD{d}
{
    mCurrentVoltage = std::make_shared<float>(0.0);
}


void VoltagePath::doFirstHalfStepJob(unsigned)
{
    *mCurrentVoltage = 0.0;
    for (unsigned item = 0; item < mPointsArray.size(); ++item) {
        XyzPoint coord = mPointsArray[item].mCellCoordinates;
        *mCurrentVoltage += mPointsArray[item].mSign * mD * mPointsArray[item].mFieldArrayPointer[coord.x][coord.y][coord.z];
    }
}


void VoltagePath::doSecondHalfStepJob(unsigned)
{
}


AmperesLawCurrent::AmperesLawCurrent(std::shared_ptr<Model> model, FieldPoint point)
    : mD{model->getCellSize()}
{
    XyzSize volumeSize = model->getVolumeSize();
    if (point.mCellCoordinates.x > volumeSize.x || point.mCellCoordinates.y > volumeSize.y || point.mCellCoordinates.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of AmperesLawCurrent).");
    }

    mCurrentCurrent = std::make_shared<float>(0.0);
    BasicParameters b = model->getBasicParameters();
    XyzPoint coord = point.mCellCoordinates;
    float sign = point.mSign;
    if (point.mComponent == FieldComponent::ex) {
        addItem(FieldPoint(FieldComponent::hz, XyzPoint(coord.x, coord.y, coord.z), sign, b.Hz));
        addItem(FieldPoint(FieldComponent::hz, XyzPoint(coord.x, coord.y-1, coord.z), -sign, b.Hz));
        addItem(FieldPoint(FieldComponent::hy, XyzPoint(coord.x, coord.y, coord.z), -sign, b.Hy));
        addItem(FieldPoint(FieldComponent::hy, XyzPoint(coord.x, coord.y, coord.z-1), sign, b.Hy));
    }
    else if (point.mComponent == FieldComponent::ey) {
        addItem(FieldPoint(FieldComponent::hx, XyzPoint(coord.x, coord.y, coord.z), sign, b.Hx));
        addItem(FieldPoint(FieldComponent::hx, XyzPoint(coord.x, coord.y, coord.z-1), -sign, b.Hx));
        addItem(FieldPoint(FieldComponent::hz, XyzPoint(coord.x, coord.y, coord.z), -sign, b.Hz));
        addItem(FieldPoint(FieldComponent::hz, XyzPoint(coord.x-1, coord.y, coord.z), sign, b.Hz));
    }
    else if (point.mComponent == FieldComponent::ez) {
        addItem(FieldPoint(FieldComponent::hy, XyzPoint(coord.x, coord.y, coord.z), sign, b.Hy));
        addItem(FieldPoint(FieldComponent::hy, XyzPoint(coord.x-1, coord.y, coord.z), -sign, b.Hy));
        addItem(FieldPoint(FieldComponent::hx, XyzPoint(coord.x, coord.y, coord.z), -sign, b.Hx));
        addItem(FieldPoint(FieldComponent::hx, XyzPoint(coord.x, coord.y-1, coord.z), sign, b.Hx));
    }
}


void AmperesLawCurrent::doFirstHalfStepJob(unsigned)
{
}


void AmperesLawCurrent::doSecondHalfStepJob(unsigned)
{
    *mCurrentCurrent = 0.0;
    for (unsigned item = 0; item < mPointsArray.size(); ++item) {
        XyzPoint coord = mPointsArray[item].mCellCoordinates;
        *mCurrentCurrent += mPointsArray[item].mSign * mD * mPointsArray[item].mFieldArrayPointer[coord.x][coord.y][coord.z];
    }
}


FieldOutput::FieldOutput(std::shared_ptr<Model> model, Vector point, FieldComponent field)
{
    float dl = model->getCellSize();
    XyzSize volumeSize = model->getVolumeSize();
    if (point.x > volumeSize.x*dl || point.y > volumeSize.y*dl || point.z > volumeSize.z*dl) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of FieldOutput).");
    }

    mCurrentField = std::make_shared<float>(0.0);
    float ***fptr = nullptr;
    XyzPoint coord;
    float dx = 0.0, dy = 0.0, dz = 0.0, coef;
    if (field == FieldComponent::ex) {
        fptr = model->getBasicParameters().Ex;
        coord.x = point.x/dl-0.5;
        coord.y = point.y/dl;
        coord.z = point.z/dl;
        dx = point.x - (static_cast<float>(coord.x)+0.5)*dl;
        dy = point.y - static_cast<float>(coord.y)*dl;
        dz = point.z - static_cast<float>(coord.z)*dl;
    }
    else if (field == FieldComponent::ey) {
        fptr = model->getBasicParameters().Ey;
        coord.x = point.x/dl;
        coord.y = point.y/dl-0.5;
        coord.z = point.z/dl;
        dx = point.x - static_cast<float>(coord.x)*dl;
        dy = point.y - (static_cast<float>(coord.y)+0.5)*dl;
        dz = point.z - static_cast<float>(coord.z)*dl;
    }
    else if (field == FieldComponent::ez) {
        fptr = model->getBasicParameters().Ez;
        coord.x = point.x/dl;
        coord.y = point.y/dl;
        coord.z = point.z/dl-0.5;
        dx = point.x - static_cast<float>(coord.x)*dl;
        dy = point.y - static_cast<float>(coord.y)*dl;
        dz = point.z - (static_cast<float>(coord.z)+0.5)*dl;
    }
    else if (field == FieldComponent::hx) {
        fptr = model->getBasicParameters().Hx;
        coord.x = point.x/dl;
        coord.y = point.y/dl-0.5;
        coord.z = point.z/dl-0.5;
        dx = point.x - static_cast<float>(coord.x)*dl;
        dy = point.y - (static_cast<float>(coord.y)+0.5)*dl;
        dz = point.z - (static_cast<float>(coord.z)+0.5)*dl;
    }
    else if (field == FieldComponent::hy) {
        fptr = model->getBasicParameters().Hy;
        coord.x = point.x/dl-0.5;
        coord.y = point.y/dl;
        coord.z = point.z/dl-0.5;
        dx = point.x - (static_cast<float>(coord.x)+0.5)*dl;
        dy = point.y - static_cast<float>(coord.y)*dl;
        dz = point.z - (static_cast<float>(coord.z)+0.5)*dl;
    }
    else if (field == FieldComponent::hz) {
        fptr = model->getBasicParameters().Hz;
        coord.x = point.x/dl-0.5;
        coord.y = point.y/dl-0.5;
        coord.z = point.z/dl;
        dx = point.x - (static_cast<float>(coord.x)+0.5)*dl;
        dy = point.y - (static_cast<float>(coord.y)+0.5)*dl;
        dz = point.z - static_cast<float>(coord.z)*dl;
    }
    coef = (1.0-dx/dl)*(1.0-dy/dl)*(1.0-dz/dl);
    addItem(FieldPoint(field, XyzPoint(coord.x, coord.y, coord.z), coef, fptr));
    coef = (dx/dl)*(1.0-dy/dl)*(1.0-dz/dl);
    addItem(FieldPoint(field, XyzPoint(coord.x+1, coord.y, coord.z), coef, fptr));
    coef = (1.0-dx/dl)*(dy/dl)*(1.0-dz/dl);
    addItem(FieldPoint(field, XyzPoint(coord.x, coord.y+1, coord.z), coef, fptr));
    coef = (dx/dl)*(dy/dl)*(1.0-dz/dl);
    addItem(FieldPoint(field, XyzPoint(coord.x+1, coord.y+1, coord.z), coef, fptr));
    coef = (1.0-dx/dl)*(1.0-dy/dl)*(dz/dl);
    addItem(FieldPoint(field, XyzPoint(coord.x, coord.y, coord.z+1), coef, fptr));
    coef = (dx/dl)*(1.0-dy/dl)*(dz/dl);
    addItem(FieldPoint(field, XyzPoint(coord.x+1, coord.y, coord.z+1), coef, fptr));
    coef = (1.0-dx/dl)*(dy/dl)*(dz/dl);
    addItem(FieldPoint(field, XyzPoint(coord.x, coord.y+1, coord.z+1), coef, fptr));
    coef = (dx/dl)*(dy/dl)*(dz/dl);
    addItem(FieldPoint(field, XyzPoint(coord.x+1, coord.y+1, coord.z+1), coef, fptr));
}


void FieldOutput::doFirstHalfStepJob(unsigned)
{
}


void FieldOutput::doSecondHalfStepJob(unsigned)
{
    *mCurrentField = 0.0;
    for (unsigned item = 0; item < mPointsArray.size(); ++item) {
        XyzPoint coord = mPointsArray[item].mCellCoordinates;
        *mCurrentField += mPointsArray[item].mSign * mPointsArray[item].mFieldArrayPointer[coord.x][coord.y][coord.z];
    }
}


}    // namespace fdtd


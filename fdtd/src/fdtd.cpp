///
/// \file fdtd.cpp
/// \brief FDTD class for the main calculation loop.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "fdtd.h"


namespace fdtd {


void CalcLoop::runLoopTillIteration(unsigned number)
{
    for (; timeStep < number; ++timeStep) {
        for (const auto &item : mTimeStepItemsDeque) {
            item->doFirstHalfStepJob(timeStep);
        }
        for (const auto &item : mTimeStepItemsDeque) {
            item->doSecondHalfStepJob(timeStep);
        }
        if (stopCalculations) {
            break;
        }
    }
}


void CalcLoop::pushFrontTimeStepItem(std::shared_ptr<TimeStepItem> item)
{
    mTimeStepItemsDeque.push_front(item);
}


void CalcLoop::pushBackTimeStepItem(std::shared_ptr<TimeStepItem> item)
{
    mTimeStepItemsDeque.push_back(item);
}


void CalcLoop::insertTimeStepItem(std::shared_ptr<TimeStepItem> item, unsigned position)
{
    std::deque<std::shared_ptr<TimeStepItem>>::iterator it = mTimeStepItemsDeque.begin() + static_cast<int>(position);
    mTimeStepItemsDeque.insert(it, item);
}


void CalcLoop::setStopCalculations(bool stop)
{
    stopCalculations = stop;
}


Fdtd::Fdtd(XyzSize size, float dl, float s, unsigned threadsNum)
    : mModel(std::make_shared<Model>(size, dl, s, threadsNum))
{
    mCalcLoop.pushBackTimeStepItem(std::make_shared<Basic>(mModel));
}


std::shared_ptr<Model> Fdtd::getModel(void) const
{
    return mModel;
}


void Fdtd::addTimeStepItem(std::shared_ptr<TimeStepItem> item)
{
    mCalcLoop.pushBackTimeStepItem(item);
}


void Fdtd::addAbc(AbcType type, unsigned abcthickness, float sigma_coef,
                  float kappa_max, float a_max, float m, float ma)
{
    if (type == AbcType::upml) {
        mCalcLoop.pushBackTimeStepItem(std::make_shared<UpmlAbc>(mModel, abcthickness));
    }
    else if (type == AbcType::cpml) {
        mCpmlAbc = std::make_shared<CpmlAbc>(mModel, abcthickness, sigma_coef, kappa_max, a_max, m, ma);
        mCalcLoop.pushBackTimeStepItem(mCpmlAbc);
    }
}


void Fdtd::initCpmlParameters(unsigned thickness, float sigma_coef, float kappa_max,
                              float a_max, float m, float ma, std::string border)
{
    if (mCpmlAbc) {
        mCpmlAbc->initParameters(thickness, sigma_coef, kappa_max, a_max, m, ma, border);
    }
    else {
        mCpmlAbc = std::make_shared<CpmlAbc>(mModel, thickness, sigma_coef, kappa_max, a_max, m, ma, border);
        mCalcLoop.pushBackTimeStepItem(mCpmlAbc);
    }
}


void Fdtd::addDispersion(unsigned polesnumber)
{
    mCalcLoop.pushBackTimeStepItem(std::make_shared<DebyeAde>(mModel, polesnumber));
}


void Fdtd::addPointSource(FieldComponent comp, XyzPoint coord, std::function<float(float)> func, FieldSourceType srcType)
{
    mCalcLoop.pushBackTimeStepItem(std::make_shared<PointSource>(mModel, comp, coord, func, srcType));
}


void Fdtd::addPointOutput(FieldComponent component, XyzPoint coordinates, std::string fileName,
                          unsigned interval, float dInterp, float sign)
{
    mCalcLoop.pushBackTimeStepItem(std::make_shared<Output>(mModel, component, coordinates, fileName, interval, dInterp, sign));
}


void Fdtd::addPointOutput(std::string fileName, unsigned interval, float dInterp)
{
    mCalcLoop.pushBackTimeStepItem(std::make_shared<Output>(mModel, fileName, interval, dInterp));
}


void Fdtd::addResistor(XyzDirection direction, XyzPoint coordinates, float resistance)
{
    addResistorToModel(mModel, direction, coordinates, resistance);
}


void Fdtd::addResistor(XyzPoint firstPoint, XyzPoint secondPoint, float resistance)
{
    addResistorToModel(mModel, firstPoint, secondPoint, resistance);
}


void Fdtd::addCapacitor(XyzDirection direction, XyzPoint coordinates, float capacitance, float conductance)
{
    addCapacitorToModel(mModel, direction, coordinates, capacitance, conductance);
}


void Fdtd::addCapacitor(XyzPoint firstPoint, XyzPoint secondPoint, float capacitance, float conductance)
{
    addCapacitorToModel(mModel, firstPoint, secondPoint, capacitance, conductance);
}


void Fdtd::addLossyDielectricBlock(XyzPoint firstPoint, XyzPoint secondPoint, float epsilon, float sigma,
                                   std::vector<float> deltaEpsilon, std::vector<float> tau)
{
    addLossyDielectricBlockToModel(mModel, firstPoint, secondPoint, epsilon, sigma, deltaEpsilon, tau);
}


void Fdtd::addRailtonThinWire(XyzPoint firstPoint, XyzPoint secondPoint, float diameter, char wireSubType)
{
    addRailtonThinWireToModel(mModel, firstPoint, secondPoint, diameter, wireSubType);
}


void Fdtd::addStaircaseWire(XyzPoint firstPoint, XyzPoint secondPoint, bool isCorrected)
{
    addStaircaseWireToModel(mModel, firstPoint, secondPoint, isCorrected);
}


void Fdtd::calculateTillTime(float timeMoment)
{
    mCalcLoop.setStopCalculations(false);
    mCalcLoop.runLoopTillIteration(static_cast<unsigned>(timeMoment / mModel->getTimeStepDuration())+2);
}


void Fdtd::calculateTillIteration(unsigned iteration)
{
    mCalcLoop.setStopCalculations(false);
    mCalcLoop.runLoopTillIteration(iteration);
}


void Fdtd::stopCalculation(void)
{
    mCalcLoop.setStopCalculations(true);
}


}    // namespace fdtd


///
/// \file source.cpp
/// \brief EM field sources.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "source.h"


namespace fdtd {


Source::Source(std::shared_ptr<Waveform> waveform, float dt)
    : mWaveform{waveform}, mDt{dt}
{
    mCurrentValue = std::make_shared<float>(0.0);
}


void Source::doFirstHalfStepJob(unsigned timeStepNumber)
{
    *mCurrentValue = mWaveform->getValue(timeStepNumber*mDt);
}


void Source::doSecondHalfStepJob(unsigned)
{
}


PointSource::PointSource(std::shared_ptr<Model> model, FieldComponent comp, XyzPoint coord,
                         std::function<float(float)> func, FieldSourceType srcType)
    : mComponent{comp}, mCoordinates{coord}, mFieldSourceFunction{func}, mSourceType{srcType},
      mDt{model->getTimeStepDuration()}, mFieldArrayPointer{model->getFieldComponent(comp)}
{
    XyzSize volumeSize = model->getVolumeSize();
    if (coord.x > volumeSize.x || coord.y > volumeSize.y || coord.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of PointSource).");
    }
}


PointSource::PointSource(std::shared_ptr<Model> model, FieldComponent comp, XyzPoint coord,
                         std::vector<float> array, FieldSourceType srcType)
    : mComponent{comp}, mCoordinates{coord}, mFieldSourceArray{array}, mSourceType{srcType},
      mDt{model->getTimeStepDuration()}, mFieldArrayPointer{model->getFieldComponent(comp)}
{
    XyzSize volumeSize = model->getVolumeSize();
    if (coord.x > volumeSize.x || coord.y > volumeSize.y || coord.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of PointSource).");
    }
}


PointSource::PointSource(std::shared_ptr<Model> model, FieldComponent comp, XyzPoint coord,
                         std::shared_ptr<Waveform> waveform, FieldSourceType srcType)
    : mComponent{comp}, mCoordinates{coord}, mWaveform{waveform}, mSourceType{srcType},
      mDt{model->getTimeStepDuration()}, mFieldArrayPointer{model->getFieldComponent(comp)}
{
    XyzSize volumeSize = model->getVolumeSize();
    if (coord.x > volumeSize.x || coord.y > volumeSize.y || coord.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of PointSource).");
    }
}


PointSource::~PointSource()
{
}


void PointSource::setField(unsigned timeStepNumber)
{
    float field;
    if (mFieldSourceFunction) {
        field = mFieldSourceFunction(mDt*timeStepNumber);
    }
    else if (mFieldSourceArray.size()) {
        if (timeStepNumber < mFieldSourceArray.size()) {
            field = mFieldSourceArray[timeStepNumber];
        }
        else {
            field = mFieldSourceArray[mFieldSourceArray.size()-1];
        }
    }
    else if (mWaveform) {
        field = mWaveform->getValue(mDt*timeStepNumber);
    }
    else {
        return;
    }

    if (mSourceType == FieldSourceType::hard) {
        mFieldArrayPointer[mCoordinates.x][mCoordinates.y][mCoordinates.z] = field * mFieldCoefficient;
    }
    else if (mSourceType == FieldSourceType::soft) {
        mFieldArrayPointer[mCoordinates.x][mCoordinates.y][mCoordinates.z] += field * mFieldCoefficient;
    }
}


void PointSource::doFirstHalfStepJob(unsigned timeStepNumber)
{
    if (mComponent == FieldComponent::ex || mComponent == FieldComponent::ey || mComponent == FieldComponent::ez) {
        setField(timeStepNumber);
    }
}


void PointSource::doSecondHalfStepJob(unsigned timeStepNumber)
{
    if (mComponent == FieldComponent::hx || mComponent == FieldComponent::hy || mComponent == FieldComponent::hz) {
        setField(timeStepNumber);
    }
}


VoltageSource::VoltageSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                             std::function<float(float)> func, float resistance)
    : mDirection{dir}, mResistance{resistance}
{
    mFieldSourceFunction = func;
    mCoordinates = coord;
    initModel(model);
}


VoltageSource::VoltageSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                             std::vector<float> array, float resistance)
    : mDirection{dir}, mResistance{resistance}
{
    mFieldSourceArray = array;
    mCoordinates = coord;
    initModel(model);
}


VoltageSource::VoltageSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                             std::shared_ptr<Waveform> waveform, float resistance)
    : mDirection{dir}, mResistance{resistance}
{
    mWaveform = waveform;
    mCoordinates = coord;
    initModel(model);
}


void VoltageSource::initModel(std::shared_ptr<Model> model)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (mCoordinates.x > volumeSize.x || mCoordinates.y > volumeSize.y || mCoordinates.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of VoltageSource).");
    }

    mSourceType = FieldSourceType::soft;
    mDt = model->getTimeStepDuration();
    if (mDirection == XyzDirection::x) {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ex);
    }
    else if (mDirection == XyzDirection::y) {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ey);
    }
    else {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ez);
    }

    float dl = model->getCellSize();
    float dt = model->getTimeStepDuration();
    float ca = 0.0, cb = 0.0;
    if (mResistance > 0.0) {
        float gsigma = 1.0 / (mResistance*dl);
        ca = (1.0 - (gsigma*dt)/(2.0*EPS0)) / (1.0 + (gsigma*dt)/(2.0*EPS0));
        cb = (dt/(EPS0*dl)) / (1.0 + (gsigma*dt)/(2.0*EPS0));
        mFieldCoefficient = -(gsigma*dt/(EPS0*dl)) / (1.0 + (gsigma*dt)/(2.0*EPS0));
    }
    else {
        ca = -1.0;
        cb = 0.0;
        mFieldCoefficient = -2.0/dl;
    }
    if (mDirection == XyzDirection::x) {
        model->setCax(mCoordinates, ca);
        model->setCbx(mCoordinates, cb);
        mComponent = FieldComponent::ex;
    }
    else if (mDirection == XyzDirection::y) {
        model->setCay(mCoordinates, ca);
        model->setCby(mCoordinates, cb);
        mComponent = FieldComponent::ey;
    }
    else {
        model->setCaz(mCoordinates, ca);
        model->setCbz(mCoordinates, cb);
        mComponent = FieldComponent::ez;
    }
}


void VoltageSource::doFirstHalfStepJob(unsigned timeStepNumber)
{
    setField(timeStepNumber);
}


void VoltageSource::doSecondHalfStepJob(unsigned)
{
}


CurrentSource::CurrentSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                             std::function<float(float)> func, float conductance)
    : mDirection{dir}, mConductance{conductance}
{
    mFieldSourceFunction = func;
    mCoordinates = coord;
    initModel(model);
}


CurrentSource::CurrentSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                             std::vector<float> array, float conductance)
    : mDirection{dir}, mConductance{conductance}
{
    mFieldSourceArray = array;
    mCoordinates = coord;
    initModel(model);
}


CurrentSource::CurrentSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                             std::shared_ptr<Waveform> waveform, float conductance)
    : mDirection{dir}, mConductance{conductance}
{
    mWaveform = waveform;
    mCoordinates = coord;
    initModel(model);
}


void CurrentSource::initModel(std::shared_ptr<Model> model)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (mCoordinates.x > volumeSize.x || mCoordinates.y > volumeSize.y || mCoordinates.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of CurrentSource).");
    }

    mSourceType = FieldSourceType::soft;
    mDt = model->getTimeStepDuration();
    if (mDirection == XyzDirection::x) {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ex);
    }
    else if (mDirection == XyzDirection::y) {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ey);
    }
    else {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ez);
    }

    float dl = model->getCellSize();
    float dt = model->getTimeStepDuration();
    float gsigma = mConductance / dl;
    float ca = (1.0 - (gsigma*dt)/(2.0*EPS0)) / (1.0 + (gsigma*dt)/(2.0*EPS0));
    float cb = (dt/(EPS0*dl)) / (1.0 + (gsigma*dt)/(2.0*EPS0));
    mFieldCoefficient = -cb*dl/(dl*dl); // amperes to field
    if (mDirection == XyzDirection::x) {
        model->setCax(mCoordinates, ca);
        model->setCbx(mCoordinates, cb);
        mComponent = FieldComponent::ex;
    }
    else if (mDirection == XyzDirection::y) {
        model->setCay(mCoordinates, ca);
        model->setCby(mCoordinates, cb);
        mComponent = FieldComponent::ey;
    }
    else {
        model->setCaz(mCoordinates, ca);
        model->setCbz(mCoordinates, cb);
        mComponent = FieldComponent::ez;
    }
}


void CurrentSource::doFirstHalfStepJob(unsigned timeStepNumber)
{
    setField(timeStepNumber);
}


void CurrentSource::doSecondHalfStepJob(unsigned)
{
}


TfsfArea::TfsfArea(std::shared_ptr<Model> model, std::function<float(unsigned)> func, float theta, float phi, float psi, unsigned borderdist)
{
    mFieldSourceFunction = func;
    initParams(model, theta, phi, psi, borderdist);
}


TfsfArea::TfsfArea(std::shared_ptr<Model> model, std::shared_ptr<Waveform> waveform, float theta, float phi, float psi, unsigned borderdist)
{
    mWaveform = waveform;
    initParams(model, theta, phi, psi, borderdist);
}


void TfsfArea::initParams(std::shared_ptr<Model> model, float theta, float phi, float psi, unsigned borderdist)
{
    mDt = model->getTimeStepDuration();

    mHconst = model->getTimeStepDuration() / (MU0 * model->getCellSize());
    mEconst = model->getTimeStepDuration() / (EPS0 * model->getCellSize());
    
    mb = model->getBasicParameters();
    
    mxTFsize = model->getVolumeSize().x - (borderdist*2);
    myTFsize = model->getVolumeSize().y - (borderdist*2);
    mzTFsize = model->getVolumeSize().z - (borderdist*2);
    
    mIncFieldArraySize = 2000 + static_cast<unsigned>(sqrt((static_cast<float>(mxTFsize)*mxTFsize) +
                         (static_cast<float>(myTFsize)*myTFsize) + (static_cast<float>(mzTFsize)*mzTFsize)));
    mEinc = std::shared_ptr<float[]>(new float[mIncFieldArraySize]); //std::make_shared<float[]>(mIncFieldArraySize);
    mHinc = std::shared_ptr<float[]>(new float[mIncFieldArraySize]);
    mEincConst = mEconst / 0.9999987;
    mHincConst = mHconst / 0.9999987;
    
    mTheta = theta;
    mSinTheta = sin(theta*std::numbers::pi/180.0);
    mCosTheta = cos(theta*std::numbers::pi/180.0);
    mPhi = phi;
    mSinPhi = sin(phi*std::numbers::pi/180.0);
    mCosPhi = cos(phi*std::numbers::pi/180.0);
    mPsi = psi;
    mSinPsi = sin(psi*std::numbers::pi/180.0);
    mCosPsi = cos(psi*std::numbers::pi/180.0);
    
    mii0 = borderdist;
    mjj0 = borderdist;
    mkk0 = borderdist;
    mii1 = mii0 + mxTFsize - 1;
    mjj1 = mjj0 + myTFsize - 1;
    mkk1 = mkk0 + mzTFsize - 1;
    mm0 = 2;
    for (unsigned cntr = 0; cntr < mIncFieldArraySize; ++cntr) {
        mEinc[cntr] = mHinc[cntr] = 0.0;
    }
}


TfsfArea::~TfsfArea()
{
}


void TfsfArea::dCalcWithShift(float ishift, float jshift, float kshift)
{
    float ii = static_cast<float>(mii0);
    float jj = static_cast<float>(mjj0);
    float kk = static_cast<float>(mkk0);
    if (mPhi >= 0.0 && mPhi <= 90.0) {
        ii = static_cast<float>(mii0);
        jj = static_cast<float>(mjj0);
    }
    else if (mPhi > 90.0 && mPhi <= 180.0) {
        ii = static_cast<float>(mii1);
        jj = static_cast<float>(mjj0);
    }
    else if (mPhi > 180.0 && mPhi <= 270.0) {
        ii = static_cast<float>(mii1);
        jj = static_cast<float>(mjj1);
    }
    else if (mPhi > 270.0 && mPhi < 360.0) {
        ii = static_cast<float>(mii0);
        jj = static_cast<float>(mjj1);
    }
    
    if (mTheta > 0.0 && mTheta <= 90.0) {
        kk = static_cast<float>(mkk0);
    }
    else if (mTheta > 90.0 && mTheta < 180.0) {
        kk = static_cast<float>(mkk1);
    }

    mdist = mSinTheta * mCosPhi * (static_cast<float>(mi) - ii + ishift) +
            mSinTheta * mSinPhi * (static_cast<float>(mj) - jj + jshift) +
            mCosTheta * (static_cast<float>(mk) - kk + kshift);
}


void TfsfArea::eIncLinearInterpolation(void)
{
    mifixd = static_cast<int>(mdist);
    mfractdist = mdist - mifixd;
    mlineinc = (1-mfractdist) * mEinc[static_cast<int>(mm0)+mifixd] + mfractdist * mEinc[static_cast<int>(mm0)+mifixd+1];
}


void TfsfArea::hIncLinearInterpolation(void)
{
    mifixd = static_cast<int>(mdist+0.5);
    mfractdist = (mdist+0.5) - mifixd;
    mlinhinc = (1.0-mfractdist) * mHinc[static_cast<int>(mm0)+mifixd-1] + mfractdist * mHinc[static_cast<int>(mm0)+mifixd];
}


float TfsfArea::getHx(void)
{
    hIncLinearInterpolation();
    return mlinhinc * (mSinPsi*mSinPhi + mCosPsi*mCosTheta*mCosPhi);
}


float TfsfArea::getHy(void)
{
    hIncLinearInterpolation();
    return mlinhinc * (-mSinPsi*mCosPhi + mCosPsi*mCosTheta*mSinPhi);
}


float TfsfArea::getHz(void)
{
    hIncLinearInterpolation();
    return mlinhinc * (-mCosPsi*mSinTheta);
}


float TfsfArea::getEx(void)
{
    eIncLinearInterpolation();
    return mlineinc * (mCosPsi*mSinPhi - mSinPsi*mCosTheta*mCosPhi);
}


float TfsfArea::getEy(void)
{
    eIncLinearInterpolation();
    return mlineinc * (-mCosPsi*mCosPhi - mSinPsi*mCosTheta*mSinPhi);
}


float TfsfArea::getEz(void)
{
    eIncLinearInterpolation();
    return mlineinc * (mSinPsi*mSinTheta);
}


void TfsfArea::calcEinc(void)
{
    for (unsigned cnt = 1; cnt < mIncFieldArraySize; ++cnt) {
        mEinc[cnt] = mEinc[cnt] + mEincConst * (mHinc[cnt-1] - mHinc[cnt]);
    }
}


void TfsfArea::applyESource(float value)
{
    mEinc[0] = value;
}


void TfsfArea::calcHinc(void)
{
    for (unsigned cnt = 0; cnt < mIncFieldArraySize-1; ++cnt) {
        mHinc[cnt] = mHinc[cnt] + mHincConst * (mEinc[cnt] - mEinc[cnt+1]);
    }
}


void TfsfArea::calcEfieldTFSF(void)
{
    //-------------------------------------------------------------
    mj = mjj0;      // Ex
    for (mi = mii0; mi <= mii1-1; ++mi) {
        for (mk = mkk0; mk <= mkk1; ++mk) {
            dCalcWithShift(0.5, -0.5, 0);
            mb.Ex[mi][mj][mk] -= mEconst * getHz();
        }
    }
                    // Ez
    for (mi = mii0; mi <= mii1; ++mi) {
        for (mk = mkk0; mk <= mkk1-1; ++mk) {
            dCalcWithShift(0, -0.5, 0.5);
            mb.Ez[mi][mj][mk] += mEconst * getHx();
        }
    }
    mj = mjj1;      // Ex
    for (mi = mii0; mi <= mii1-1; ++mi) {
        for (mk = mkk0; mk <= mkk1; ++mk) {
            dCalcWithShift(0.5, 0.5, 0);
            mb.Ex[mi][mj][mk] += mEconst * getHz();
        }
    }
                    // Ez
    for (mi = mii0; mi <= mii1; ++mi) {
        for (mk = mkk0; mk <= mkk1-1; ++mk) {
            dCalcWithShift(0, 0.5, 0.5);
            mb.Ez[mi][mj][mk] -= mEconst * getHx();
        }
    }
    //-------------------------------------------------------------
    mk = mkk0;      // Ex
    for (mi = mii0; mi <= mii1-1; ++mi) {
        for (mj = mjj0; mj <= mjj1; ++mj) {
            dCalcWithShift(0.5, 0, -0.5);
            mb.Ex[mi][mj][mk] += mEconst * getHy();
        }
    }
                    // Ey
    for (mi = mii0; mi <= mii1; ++mi) {
        for (mj = mjj0; mj <= mjj1-1; ++mj) {
            dCalcWithShift(0, 0.5, -0.5);
            mb.Ey[mi][mj][mk] -= mEconst * getHx();
        }
    }
    mk = mkk1;      // Ex
    for (mi = mii0; mi <= mii1-1; ++mi) {
        for (mj = mjj0; mj <= mjj1; ++mj) {
            dCalcWithShift(0.5, 0, 0.5);
            mb.Ex[mi][mj][mk] -= mEconst * getHy();
        }
    }
                    // Ey
    for (mi = mii0; mi <= mii1; ++mi) {
        for (mj = mjj0; mj <= mjj1-1; ++mj) {
            dCalcWithShift(0, 0.5, 0.5);
            mb.Ey[mi][mj][mk] += mEconst * getHx();
        }
    }
    //-------------------------------------------------------------
    mi = mii0;      // Ey
    for (mj = mjj0; mj <= mjj1-1; ++mj) {
        for (mk = mkk0; mk <= mkk1; ++mk) {
            dCalcWithShift(0.5, 0.5, 0);
            mb.Ey[mi][mj][mk] += mEconst * getHz();
        }
    }
                    // Ez
    for (mj = mjj0; mj <= mjj1; ++mj) {
        for (mk = mkk0; mk <= mkk1-1; ++mk) {
            dCalcWithShift(0.5, 0, 0.5);
            mb.Ez[mi][mj][mk] -= mEconst * getHy();
        }
    }
    mi = mii1;      // Ey
    for (mj = mjj0; mj <= mjj1-1; ++mj) {
        for (mk = mkk0; mk <= mkk1; ++mk) {
            dCalcWithShift(0.5, 0.5, 0);
            mb.Ey[mi][mj][mk] -= mEconst * getHz();
        }
    }
                    // Ez
    for (mj = mjj0; mj <= mjj1; ++mj) {
        for (mk = mkk0; mk <= mkk1-1; ++mk) {
            dCalcWithShift(0.5, 0, 0.5);
            mb.Ez[mi][mj][mk] += mEconst * getHy();
        }
    }
}


void TfsfArea::calcHfieldTFSF(void)
{
    //-------------------------------------------------------------
    mj = mjj0-1;    // Hz
    for (mi = mii0; mi <= mii1-1; ++mi) {
        for (mk = mkk0; mk <= mkk1; ++mk) {
            dCalcWithShift(0.5, 0, 0);
            mb.Hz[mi][mj][mk] -= mHconst * getEx();
        }
    }
                    // Hx
    for (mi = mii0; mi <= mii1; ++mi) {
        for (mk = mkk0; mk <= mkk1-1; ++mk) {
            dCalcWithShift(0, 0, 0.5);
            mb.Hx[mi][mj][mk] += mHconst * getEz();
        }
    }
    mj = mjj1;      // Hz
    for (mi = mii0; mi <= mii1-1; ++mi) {
        for (mk = mkk0; mk <= mkk1; ++mk) {
            dCalcWithShift(0.5, 0, 0);
            mb.Hz[mi][mj][mk] += mHconst * getEx();
        }
    }
                    // Hx
    for (mi = mii0; mi <= mii1; ++mi) {
        for (mk = mkk0; mk <= mkk1-1; ++mk) {
            dCalcWithShift(0, 0, 0.5);
            mb.Hx[mi][mj][mk] -= mHconst * getEz();
        }
    }
    //-------------------------------------------------------------
    mk = mkk0-1;    // Hy
    for (mi = mii0; mi <= mii1-1; ++mi) {
        for (mj = mjj0; mj <= mjj1; ++mj) {
            dCalcWithShift(0.5, 0, 0);
            mb.Hy[mi][mj][mk] += mHconst * getEx();
        }
    }
                    // Hx
    for (mi = mii0; mi <= mii1; ++mi) {
        for (mj = mjj0; mj <= mjj1-1; ++mj) {
            dCalcWithShift(0, 0.5, 0);
            mb.Hx[mi][mj][mk] -= mHconst * getEy();
        }
    }
    mk = mkk1;      // Hy
    for (mi = mii0; mi <= mii1-1; ++mi) {
        for (mj = mjj0; mj <= mjj1; ++mj) {
            dCalcWithShift(0.5, 0, 0);
            mb.Hy[mi][mj][mk] -= mHconst * getEx();
        }
    }
                    // Hx
    for (mi = mii0; mi <= mii1; ++mi) {
        for (mj = mjj0; mj <= mjj1-1; ++mj) {
            dCalcWithShift(0, 0.5, 0);
            mb.Hx[mi][mj][mk] += mHconst * getEy();
        }
    }
    //-------------------------------------------------------------
    mi = mii0-1;    // Hz
    for (mj = mjj0; mj <= mjj1-1; ++mj) {
        for (mk = mkk0; mk <= mkk1; ++mk) {
            dCalcWithShift(0, 0.5, 0);
            mb.Hz[mi][mj][mk] += mHconst * getEy();
        }
    }
                    // Hy
    for (mj = mjj0; mj <= mjj1; ++mj) {
        for (mk = mkk0; mk <= mkk1-1; ++mk) {
            dCalcWithShift(0, 0, 0.5);
            mb.Hy[mi][mj][mk] -= mHconst * getEz();
        }
    }
    mi = mii1;      // Hz
    for (mj = mjj0; mj <= mjj1-1; ++mj) {
        for (mk = mkk0; mk <= mkk1; ++mk) {
            dCalcWithShift(0, 0.5, 0);
            mb.Hz[mi][mj][mk] -= mHconst * getEy();
        }
    }
                    // Hy
    for (mj = mjj0; mj <= mjj1; ++mj) {
        for (mk = mkk0; mk <= mkk1-1; ++mk) {
            dCalcWithShift(0, 0, 0.5);
            mb.Hy[mi][mj][mk] += mHconst * getEz();
        }
    }
}


void TfsfArea::doFirstHalfStepJob(unsigned timeStepNumber)
{
    calcEinc();
    if (mFieldSourceFunction) {
        applyESource(mFieldSourceFunction(timeStepNumber));
    }
    else if (mWaveform) {
        applyESource(mWaveform->getValue(mDt*timeStepNumber));
    }
    calcEfieldTFSF();
}


void TfsfArea::doSecondHalfStepJob(unsigned)
{
    calcHinc();
    calcHfieldTFSF();
}


}    // namespace fdtd


///
/// \file lumped.cpp
/// \brief Implementation of lumped circuit elements. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "lumped.h"


namespace fdtd {


void addResistorToModel(std::shared_ptr<Model> model, XyzDirection direction, XyzPoint coordinates, float resistance)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (coordinates.x > volumeSize.x || coordinates.y > volumeSize.y || coordinates.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (function addResistorToModel).");
    }

    float dl = model->getCellSize();
    float dt = model->getTimeStepDuration();

    float sigma = 1.0 / (resistance*dl);
    float ca = (1.0 - (sigma*dt)/(2.0*EPS0)) / (1.0 + (sigma*dt)/(2.0*EPS0));
    float cb = (dt/(EPS0*dl)) / (1.0 + (sigma*dt)/(2.0*EPS0));

    model->setCaInCell(direction, coordinates, ca);
    model->setCbInCell(direction, coordinates, cb);
}


void addResistorToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float resistance)
{
    XyzDirection direction = XyzDirection::x;
    if (secondPoint.x != firstPoint.x) {
        direction = XyzDirection::x;
        if (firstPoint.x > secondPoint.x) {
            firstPoint.x = secondPoint.x;
        }
    }
    else if (secondPoint.y != firstPoint.y) {
        direction = XyzDirection::y;
        if (firstPoint.y > secondPoint.y) {
            firstPoint.y = secondPoint.y;
        }
    }
    else {
        direction = XyzDirection::z;
        if (firstPoint.z > secondPoint.z) {
            firstPoint.z = secondPoint.z;
        }
    }
    addResistorToModel(model, direction, firstPoint, resistance);
}


void addCapacitorToModel(std::shared_ptr<Model> model, XyzDirection direction, XyzPoint coordinates, float capacitance, float conductance)
{
    XyzSize volumeSize = model->getVolumeSize();
    if (coordinates.x > volumeSize.x || coordinates.y > volumeSize.y || coordinates.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (function addCapacitorToModel).");
    }

    float dl = model->getCellSize();
    float dt = model->getTimeStepDuration();

    float sigma = conductance / dl;
    float ca = (1.0 - (sigma*dt)/(2.0*EPS0) + capacitance/(EPS0*dl)) /
               (1.0 + (sigma*dt)/(2.0*EPS0) + capacitance/(EPS0*dl));
    float cb = (dt/(EPS0*dl)) / (1.0 + (sigma*dt)/(2.0*EPS0) + capacitance/(EPS0*dl));

    model->setCaInCell(direction, coordinates, ca);
    model->setCbInCell(direction, coordinates, cb);
}


void addCapacitorToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float capacitance, float conductance)
{
    XyzDirection direction = XyzDirection::x;
    if (secondPoint.x != firstPoint.x) {
        direction = XyzDirection::x;
        if (firstPoint.x > secondPoint.x) {
            firstPoint.x = secondPoint.x;
        }
    }
    else if (secondPoint.y != firstPoint.y) {
        direction = XyzDirection::y;
        if (firstPoint.y > secondPoint.y) {
            firstPoint.y = secondPoint.y;
        }
    }
    else {
        direction = XyzDirection::z;
        if (firstPoint.z > secondPoint.z) {
            firstPoint.z = secondPoint.z;
        }
    }
    addCapacitorToModel(model, direction, firstPoint, capacitance, conductance);
}


LumpedInductor::LumpedInductor(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord, float inductance)
    : mDirection{dir}, mCoordinates{coord}, mInductance{inductance}
{
    XyzSize volumeSize = model->getVolumeSize();
    if (coord.x > volumeSize.x || coord.y > volumeSize.y || coord.z > volumeSize.z) {
        throw std::invalid_argument("coordinates are outside of the calculation volume (constructor of LumpedInductor).");
    }
    
    if (mDirection == XyzDirection::x) {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ex);
    }
    else if (mDirection == XyzDirection::y) {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ey);
    }
    else {
        mFieldArrayPointer = model->getFieldComponent(FieldComponent::ez);
    }
    
    float dl = model->getCellSize();
    float dt = model->getTimeStepDuration();
    float ca = 1.0;
    float cb = dt/(EPS0*dl);
    mFieldCoefficient = - (cb*dl/(dl*dl)) * (dl*dt/mInductance);
    if (mDirection == XyzDirection::x) {
        model->setCax(mCoordinates, ca);
        model->setCbx(mCoordinates, cb);
    }
    else if (mDirection == XyzDirection::y) {
        model->setCay(mCoordinates, ca);
        model->setCby(mCoordinates, cb);
    }
    else {
        model->setCaz(mCoordinates, ca);
        model->setCbz(mCoordinates, cb);
    }
}


LumpedInductor::~LumpedInductor()
{
}


void LumpedInductor::doFirstHalfStepJob(unsigned)
{
    mFieldArrayPointer[mCoordinates.x][mCoordinates.y][mCoordinates.z] += mFieldCoefficient * mEsum;
    mEsum += mFieldArrayPointer[mCoordinates.x][mCoordinates.y][mCoordinates.z];
}


void LumpedInductor::doSecondHalfStepJob(unsigned)
{
}


}    // namespace fdtd


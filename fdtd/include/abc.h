///
/// \file abc.h
/// \brief Absorbing boundary conditions for the FDTD. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef ABC_H__
#define ABC_H__


#include "model.h"
#include <memory>


namespace fdtd {


enum class AbcType {
    noAbc,
    upml,
    cpml
};


void upmlElectric(std::shared_ptr<Model> model);
void upmlMagnetic(std::shared_ptr<Model> model);
void upmlElectricDebyeAde(std::shared_ptr<Model> model);
void cpmlElectric(std::shared_ptr<Model> model);
void cpmlMagnetic(std::shared_ptr<Model> model);
void initUpmlParameters(std::shared_ptr<Model> model);
void initCpmlParameters(std::shared_ptr<Model> model, float sigma_coef, float kappa_max, float a_max, float m, float ma, std::string border);
void initBottomCpmlParameters(float sigma_coef, float kappa_max, float a_max, float m, float ma,
                             float dl, float dti, unsigned abcThickness, unsigned volumeSize,
                             float *Be, float *Ce, float *Bh, float *Ch, float *dke, float *dkh);
void initTopCpmlParameters(float sigma_coef, float kappa_max, float a_max, float m, float ma,
                           float dl, float dti, unsigned abcThickness, unsigned volumeSize,
                           float *Be, float *Ce, float *Bh, float *Ch, float *dke, float *dkh);


class UpmlAbc : public TimeStepItem {
public:
    UpmlAbc(std::shared_ptr<Model> model, unsigned thickness = 10);
    
    virtual void init(void);
};


class CpmlAbc : public TimeStepItem {
public:
    CpmlAbc(std::shared_ptr<Model> model, unsigned abcthickness = 10,
            float sigma_coef = 1.0, float kappa_max = 10.0,
            float a_max = 0.01, float m = 3.0, float ma = 1.0, std::string border = "all");
    
    virtual void init(void);
    
    void initParameters(unsigned abcthickness = 10, float sigma_coef = 1.0, float kappa_max = 10.0,
                        float a_max = 0.01, float m = 3.0, float ma = 1.0, std::string border = "all");
};


}    // namespace fdtd


#endif    // ABC_H__


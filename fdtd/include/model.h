///
/// \file model.h
/// \brief Main FDTD model data.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef MODEL_H__
#define MODEL_H__


#include "constants.h"
#include "vectors.h"
#include <omp.h>
#include <string>
#include <vector>
#include <iostream>
#include <memory>
#include <functional>


namespace fdtd {


using XyzPoint = Xyz<unsigned>;
using XyzSize = Xyz<unsigned>;
class TimeStepItem;
class Model;


const float S_DEFAULT = 0.5658032638;      // 0.98 / sqrt(3.0)


template <typename T>
T*** malloc3dArray(XyzSize size)
{
    T ***array;
    array = new T**[size.x];
    for (unsigned i = 0; i < size.x; ++i) {
        array[i] = new T*[size.y];
        for (unsigned j = 0; j < size.y; ++j) {
            array[i][j] = new T[size.z];
            std::fill_n(array[i][j], size.z, 0);
        }
    }
    return array;
}


template <typename T>
void free3dArray(T*** array, XyzSize size)
{
    for (unsigned i = 0; i < size.x; ++i) {
        for (unsigned j = 0; j < size.y; ++j) {
            delete[] array[i][j];
        }
        delete[] array[i];
    }
    delete[] array;
    array = nullptr;
}


enum class FieldComponent {
    ex,
    ey,
    ez,
    hx,
    hy,
    hz
};


struct FieldPoint {
    FieldPoint() = default;
    FieldPoint(FieldComponent comp, XyzPoint coordinates, float sign = 1.0, float ***arrayPointer = nullptr)
        : mComponent{comp}, mCellCoordinates{coordinates}, mFieldArrayPointer{arrayPointer}, mSign{sign} {}

    FieldComponent mComponent{};
    XyzPoint mCellCoordinates{};
    float ***mFieldArrayPointer = nullptr;
    float mSign{1.0};
    unsigned mIndex{0};
};


/// TimeStepItem is a class for every item of CalcLoop iteration.
/// One Loop Iteration == One Time Step. I.e. it works something like that (pseudocode):
/// \code
/// for (RunSomeTimeSteps) // CalcLoop
/// {
///     FirstTimeStepItem.doFirstHalfStepJob();
///     SecondTimeStepItem.doFirstHalfStepJob();
///     ThirdTimeStepItem.doFirstHalfStepJob();
///
///     FirstTimeStepItem.doSecondHalfStepJob();
///     SecondTimeStepItem.doSecondHalfStepJob();
///     ThirdTimeStepItem.doSecondHalfStepJob();
/// }
/// \endcode
class TimeStepItem {
public:
    TimeStepItem() = default;
    virtual ~TimeStepItem() {}

    virtual void init() {}
    virtual void doFirstHalfStepJob(unsigned);
    virtual void doSecondHalfStepJob(unsigned);

protected:
    void (*mFirstHalfStepFunction)(std::shared_ptr<Model> model) = nullptr;
    void (*mSecondHalfStepFunction)(std::shared_ptr<Model> model) = nullptr;
    std::shared_ptr<Model> mModel{};
};


struct BasicParameters {
    float ***Ex = nullptr, ***Ey = nullptr, ***Ez = nullptr;
    float ***Hx = nullptr, ***Hy = nullptr, ***Hz = nullptr;
    float ***Cax = nullptr, ***Cay = nullptr, ***Caz = nullptr;
    float ***Cbx = nullptr, ***Cby = nullptr, ***Cbz = nullptr;
    float ***Dbx = nullptr, ***Dby = nullptr, ***Dbz = nullptr;
};


struct UpmlParameters {
    float **Cex = nullptr, **Cey = nullptr, **Cez = nullptr;
    float **Chx = nullptr, **Chy = nullptr, **Chz = nullptr;
    float ***Px = nullptr, ***Py = nullptr, ***Pz = nullptr;
    float ***Psx = nullptr, ***Psy = nullptr, ***Psz = nullptr;
    float ***Bx = nullptr, ***By = nullptr, ***Bz = nullptr;
    float ***Bsx = nullptr, ***Bsy = nullptr, ***Bsz = nullptr;
};


struct CpmlParameters {
    float *dkex = nullptr, *dkey = nullptr, *dkez = nullptr, *dkhx = nullptr, *dkhy = nullptr, *dkhz = nullptr;
    float *Bex = nullptr, *Bey = nullptr, *Bez = nullptr, *Bhx = nullptr, *Bhy = nullptr, *Bhz = nullptr;
    float *Cex = nullptr, *Cey = nullptr, *Cez = nullptr, *Chx = nullptr, *Chy = nullptr, *Chz = nullptr;
    float ***PsiEyx = nullptr, ***PsiEzx = nullptr, ***PsiExy = nullptr, ***PsiEzy = nullptr, ***PsiExz = nullptr, ***PsiEyz = nullptr;
    float ***PsiHyx = nullptr, ***PsiHzx = nullptr, ***PsiHxy = nullptr, ***PsiHzy = nullptr, ***PsiHxz = nullptr, ***PsiHyz = nullptr;
};


struct DebyeAdeParameters {
    unsigned PolesNumber = 0;
    float ****kpx = nullptr, ****kpy = nullptr, ****kpz = nullptr;
    float ****dbetapx = nullptr, ****dbetapy = nullptr, ****dbetapz = nullptr;
    float ****Jx = nullptr, ****Jy = nullptr, ****Jz = nullptr;
    float ***Exs = nullptr, ***Eys = nullptr, ***Ezs = nullptr;
};


struct AlteredParameters {
    bool ***cbx = nullptr, ***cby = nullptr, ***cbz = nullptr;
    bool ***dbx = nullptr, ***dby = nullptr, ***dbz = nullptr;
    bool ***dbetapx = nullptr, ***dbetapy = nullptr, ***dbetapz = nullptr;
};


struct AllocatedParameters {
    bool basic{false};
    bool upml{false};
    bool cpml{false};
    bool cpmlfield{false};
    bool debye{false};
    bool altered{false};
};


/// Model is a class where pointers to main FDTD arrays are stored.
/// It is responsible for the allocation and deallocation of memory for the arrays.
/// The FDTD model data is passed to threads and other important objects.
/// It helps to avoid global variables. At the same time, it minimizes the number of arguments 
/// for functions that use the FDTD model data for calculations (or other purposes).
class Model {
public:
    Model() = default;
    Model(XyzSize size, float dl, float s = S_DEFAULT, unsigned threadsNumber = 1);
    ~Model();
    Model(const Model& sd) { *this = sd; }
    Model& operator=(const Model& sd) { mb = sd.mb; mu = sd.mu; mc = sd.mc; md = sd.md;
                                        mVolumeSize = sd.mVolumeSize; mD = sd.mD; mS = sd.mS; mDt = sd.mDt;
                                        mAbcThicknessBot = sd.mAbcThicknessBot; mAbcThicknessTop = sd.mAbcThicknessTop;
                                        mThreadsNumber = sd.mThreadsNumber; mItemsToInit = sd.mItemsToInit;
                                        mMemoryManager = false; return *this; }
    Model(Model&&) = default;
    Model& operator=(Model&&) = default;

    void allocateBasicParameters(void);
    void allocateUpmlParameters(void);
    float** allocateCparamArrayForUpml(unsigned th, float dti);
    void allocateCpmlParameters(void);
    void allocateCpmlFieldParameters(void);
    void allocateDebyeAdeParameters(void);
    void allocateAlteredParameters(void);
    void freeBasicParameters(void);
    void freeUpmlParameters(void);
    void freeCparamArrayForUpml(float** array);
    void freeCpmlParameters(void);
    void freeCpmlFieldParameters(void);
    void freeDebyeAdeParameters(void);
    void freeAlteredParameters(void);

    XyzSize getVolumeSize(void) const { return mVolumeSize; }
    float getCellSize(void) const { return mD; }
    float getCourantNumber(void) const { return mS; }
    float getTimeStepDuration(void) const { return mDt; }
    XyzSize getAbcThicknessBot(void) const { return mAbcThicknessBot; }
    XyzSize getAbcThicknessTop(void) const { return mAbcThicknessTop; }
    BasicParameters getBasicParameters(void) const { return mb; }
    UpmlParameters getUpmlParameters(void) const { return mu; }
    CpmlParameters getCpmlParameters(void) const { return mc; }
    DebyeAdeParameters getDebyeAdeParameters(void) const { return md; }
    float*** getFieldComponent(FieldComponent comp) const;
    unsigned getPolesNumber(void) const { return md.PolesNumber; }
    unsigned getThreadsNumber(void) const { return mThreadsNumber; }
    float getFieldInCell(const FieldComponent &component, const XyzPoint &cell) const;
    float getCaInCell(const XyzDirection &component, const XyzPoint &cell) const;
    float getCbInCell(const XyzDirection &component, const XyzPoint &cell) const;
    float getDbInCell(const XyzDirection &component, const XyzPoint &cell) const;
    float getCax(const XyzPoint &cell) const;
    float getCay(const XyzPoint &cell) const;
    float getCaz(const XyzPoint &cell) const;
    float getCbx(const XyzPoint &cell) const;
    float getCby(const XyzPoint &cell) const;
    float getCbz(const XyzPoint &cell) const;
    AllocatedParameters getAllocatedParameters(void) const { return mAllocatedParameters; }

    void setPolesNumber(unsigned poles) { md.PolesNumber = poles; }
    void setAbcThickness(unsigned th) { mAbcThicknessBot = XyzSize(th, th, th); mAbcThicknessTop = XyzSize(th, th, th); }
    void setAbcThicknessBot(XyzSize th) { mAbcThicknessBot = th; }
    void setAbcThicknessTop(XyzSize th) { mAbcThicknessTop = th; }
    void setFieldInCell(const FieldComponent &component, const XyzPoint &cell, const float &value);
    void setSoftFieldInCell(const FieldComponent &component, const XyzPoint &cell, const float &value);
    void setCaInCell(const XyzDirection &component, const XyzPoint &cell, const float &value);
    void setCbInCell(const XyzDirection &component, const XyzPoint &cell, const float &value);
    void setDbInCell(const XyzDirection &component, const XyzPoint &cell, const float &value);
    void setCax(const XyzPoint &cell, const float &value);
    void setCay(const XyzPoint &cell, const float &value);
    void setCaz(const XyzPoint &cell, const float &value);
    void setCbx(const XyzPoint &cell, const float &value);
    void setCby(const XyzPoint &cell, const float &value);
    void setCbz(const XyzPoint &cell, const float &value);
    void mulCbx(const XyzPoint &cell, const float &value);
    void mulCby(const XyzPoint &cell, const float &value);
    void mulCbz(const XyzPoint &cell, const float &value);
    void mulDbx(const XyzPoint &cell, const float &value);
    void mulDby(const XyzPoint &cell, const float &value);
    void mulDbz(const XyzPoint &cell, const float &value);
    void mulDbetapx(const XyzPoint &cell, const float &value);
    void mulDbetapy(const XyzPoint &cell, const float &value);
    void mulDbetapz(const XyzPoint &cell, const float &value);
    void addItemToInit(TimeStepItem *item) { mItemsToInit.push_back(item); }
    void initTimeStepItems(void) { for (const auto &item : mItemsToInit) item->init(); }

private:
    BasicParameters mb{};
    UpmlParameters mu{};
    CpmlParameters mc{};
    DebyeAdeParameters md{};
    AlteredParameters ma{};
    XyzSize mVolumeSize{};
    float mD{}, mS{}, mDt{};
    XyzSize mAbcThicknessBot{};
    XyzSize mAbcThicknessTop{};
    unsigned mThreadsNumber{1};
    std::vector<TimeStepItem*> mItemsToInit{};
    bool mMemoryManager{true};
    AllocatedParameters mAllocatedParameters{};
};


}    // namespace fdtd


#endif    // MODEL_H__


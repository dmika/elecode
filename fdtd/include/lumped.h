///
/// \file lumped.h
/// \brief Implementation of lumped circuit elements. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef LUMPED_H__
#define LUMPED_H__


#include "model.h"


namespace fdtd {


void addResistorToModel(std::shared_ptr<Model> model, XyzDirection direction, XyzPoint coordinates, float resistance);
void addResistorToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float resistance);
void addCapacitorToModel(std::shared_ptr<Model> model, XyzDirection direction, XyzPoint coordinates, float capacitance, float conductance = 0.0);
void addCapacitorToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float capacitance, float conductance = 0.0);


class LumpedInductor : public TimeStepItem {
public:
    LumpedInductor() = default;
    LumpedInductor(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord, float inductance);
    ~LumpedInductor();
    LumpedInductor(const LumpedInductor&) = default;
    LumpedInductor& operator=(const LumpedInductor&) = default;
    LumpedInductor(LumpedInductor&&) = default;
    LumpedInductor& operator=(LumpedInductor&&) = default;
    
    virtual void doFirstHalfStepJob(unsigned);
    virtual void doSecondHalfStepJob(unsigned);
    
protected:
    XyzDirection mDirection{};
    XyzPoint mCoordinates{};
    float mInductance{1.0};
    float mEsum{0.0};
    float ***mFieldArrayPointer{nullptr};
    float mFieldCoefficient{1.0};
};


}    // namespace fdtd


#endif    // LUMPED_H__


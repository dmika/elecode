///
/// \file wire.h
/// \brief Modeling thin wires. See Railton's articles and Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef WIRE_H__
#define WIRE_H__


#include "model.h"
#include <cmath>


namespace fdtd {


void addRailtonThinWireToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float diameter = 0.0, char wireSubType = 'n');
void addStaircaseWireToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, bool isCorrected = false);
void alterWireParameters(std::shared_ptr<Model> model, XyzDirection direction, XyzPoint coord, float railtonCoef);
void alterRadialElectricParameters(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord, float coef);
void alterRotorMagneticParameters(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord, float coef);
void alterLongitudinalMagneticParameters(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord, float coef);


}    // namespace fdtd


#endif    // WIRE_H__


///
/// \file obliquewire.h
/// \brief Implementation of the oblique thin wire model.
/// See "New Oblique Thin Wire Formalism in the FDTD Method With Multiwire Junctions"
/// by C. Guiffaut, A. Reineix, B. Pecqueux;
/// "Insulated Oblique Thin Wire Formalism in the FDTD Method"
/// by C. Guiffaut, N. Rouvrais, A. Reineix, B. Pecqueux;
/// "Extension of Thin Wire Techniques in the FDTD Method for Debye Media"
/// by D. Kuklin.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef OBLIQUEWIRE_H__
#define OBLIQUEWIRE_H__


#include "model.h"
#include "vectors.h"
#include <iostream>
#include <vector>


namespace fdtd {


class WireNode;
class SegmentPart;
class WireArea;
void updateCurrentsAndVoltages(unsigned timeStepNumber);
float f3(float z);
float f2(float y);
float f1(float x);
bool getRayIntersectionWithCube(Vector orig, Vector dir, Vector cubeStart, Vector cubeEnd, XyzDirection direction, XyzPointFloat *result);
bool getSegmentPartIntersectionWithCube(SegmentPart &sp, XyzDirection direction, XyzPointFloat *result);


const int NO_SOURCE_OR_CALC = -1;
const float CONNECTION_DISTANCE = 0.01;


enum WirePointType {
    WIRE_START,
    WIRE_END
};


class WireNode {
public:
    WireNode(unsigned wireNumber, WirePointType wirePoint) {
        WireNumber.push_back(wireNumber);
        WirePoint.push_back(wirePoint);
    };
    ~WireNode();
    
    std::vector<unsigned> WireNumber{};
    std::vector<WirePointType> WirePoint{};
    
    float coefVn{};
    float coefDiv{};
    float Vn{};
};


class SegmentPart {
public:
    SegmentPart(const Model &fdtdvol, XyzPointFloat start, XyzPointFloat end, float diam, float sheath = 0.0);
    
    float calcLum(FieldComponent component, bool totalInductance = true);
    void calcAllLum(void);
    float calcIntegralAdsimp(SegmentPart *spart, float x_coord, float y_coord, float z_coord, bool totalInductance = true);
    friend float calcInduct(SegmentPart *spart, float point_x, float point_y, float point_z);
    XyzPointFloat getStart(void) const { return mStart; }
    XyzPointFloat getEnd(void) const { return mEnd; }
    XyzPointFloat getDirection(void) const { return mDirection; }
    XyzPoint getCell(void) const { return mCell; }
    float getCellSize(void) const { return mCellSize; }
    float getLength(void) const { return mLength; }
    float getLum(unsigned coord) const { if (coord < 3) return mLum[coord]; return 0.0; }
    float getLums(unsigned coord) const { if (coord < 3) return mLums[coord]; return 0.0; }
    float getPcx(unsigned num) { if (num < 4) return mPcx[num]; return 0.0; }
    float getPcy(unsigned num) { if (num < 4) return mPcy[num]; return 0.0; }
    float getPcz(unsigned num) { if (num < 4) return mPcz[num]; return 0.0; }
    float getSegmentPartEk(void);
    void setSegmentPartIk(float Ik);
    void setStart(XyzPointFloat start) { mStart = start; initLengthDirectionCell(); }
    void setEnd(XyzPointFloat end) { mEnd = end; initLengthDirectionCell(); }
    void setPcxAndJpcx(unsigned num, float value) { if (num < 4) mPcx[num] = mJpcx[num] = value; }
    void setPcyAndJpcy(unsigned num, float value) { if (num < 4) mPcy[num] = mJpcy[num] = value; }
    void setPczAndJpcz(unsigned num, float value) { if (num < 4) mPcz[num] = mJpcz[num] = value; }
    void mulJpcx(unsigned num, float value) { if (num < 4) mJpcx[num] *= value; }
    void mulJpcy(unsigned num, float value) { if (num < 4) mJpcy[num] *= value; }
    void mulJpcz(unsigned num, float value) { if (num < 4) mJpcz[num] *= value; }
    
    unsigned k{};            // segment number
    
protected:
    float mCellSize;
    float ***mEx{nullptr};
    float ***mEy{nullptr};
    float ***mEz{nullptr};
    float mRadius;
    float mSheathRadius{};
    XyzPointFloat mStart{};
    XyzPointFloat mEnd{};
    XyzPointFloat mDirection{};
    XyzPoint mCell{};        // it belongs to
    float mLength{};
    float mLum[3];           // inductance of the segment part of u direction
    float mLums[3];
    float mPcx[4] = {0.0, 0.0, 0.0, 0.0};
    float mPcy[4] = {0.0, 0.0, 0.0, 0.0};
    float mPcz[4] = {0.0, 0.0, 0.0, 0.0};
    float mJpcx[4] = {0.0, 0.0, 0.0, 0.0};
    float mJpcy[4] = {0.0, 0.0, 0.0, 0.0};
    float mJpcz[4] = {0.0, 0.0, 0.0, 0.0};
    void initLengthDirectionCell(void);
};


class ObliqueWire {
public:
    ObliqueWire(const Model &fdtdvol, XyzPointFloat start, XyzPointFloat end,
                float diam, unsigned segnum = 0, float sheath = 0.0, float eps = 1.0, float sigma = 0.0);
    ~ObliqueWire();
    
    float getEk(unsigned int k);
    void setJ(void);
    SegmentPart getSegmentPart(unsigned num) const { return mArrayOfSegmentParts[num]; }
    unsigned getNumberOfSegmentParts(void) const { return mArrayOfSegmentParts.size(); }
    unsigned getNumberOfSegments(void) const { return mNumberOfSegments; }
    XyzPointFloat getStart(void) const { return mStart; }
    XyzPointFloat getEnd(void) const { return mEnd; }
    float getCurrent(unsigned cnt) const { if (cnt < mNumberOfSegments) return I[cnt]; return 0.0; }
    float getVoltage(unsigned cnt) const { if (cnt <= mNumberOfSegments) return Ue[cnt]; return 0.0; }
    float getCoefVn(WirePointType point) const { if (point == WIRE_START) return coefVn[0]; else return coefVn[1]; }
    float getCoefDiv(WirePointType point) const { if (point == WIRE_START) return coefDiv[0]; else return coefDiv[1]; }
    float getCoefVsZk(WirePointType point) const { if (point == WIRE_START) return coefZk[0]*Us[0]; else return coefZk[1]*Us[mNumberOfSegments]; }
    float getJr(WirePointType point);
    
    void setCurrent(unsigned cnt, float value) { if (cnt < mNumberOfSegments) I[cnt] = value; }
    void calcCurrents(void);
    void calcVoltages(void);
    void calcVoltagesSecondOrderAbc(void);
    void calcVoltageInStartNode(void);
    void calcVoltageInEndNode(void);
    float getTipWireCapacitance(WirePointType type);
    float getTipWireFirstPartOfVoltage(WirePointType type);
    float getTipWireSecondPartOfVoltage(WirePointType type);
    bool isSeparatedStart(void) const { return mSeparatedStart; }
    bool isInfiniteEnd(void) const { return mInfiniteEnd; }
    int getCurrentCalcSegment(void) const { return mCurrentCalcSegment; }
    bool getFirstVoltageCalcWire(void) const { return mFirstVoltageCalcWire; }
    std::shared_ptr<float> getCalcOutput(void) { return mCalcValue; }
    
    void setResistorInSegment(int segm, float resistance);
    void setTipWireVoltage(WirePointType type, float vn, float vnprev);
    void setCurrentSourceSegment(int segm) { if (segm >= -1) mCurrentSourceSegment = segm; }
    void setCurrentCalcSegment(int segm) { if (segm >= -1) mCurrentCalcSegment = segm; }
    void setVoltageSourceSegment(int segm) { if (segm >= -1) mVoltageSourceSegment = segm; }
    void setInfiniteEnd(bool inf) { mInfiniteEnd = inf; }
    void setSeparatedStart(bool sep) { mSeparatedStart = sep; }
    void setFirstVoltageCalcWire(bool val) { mFirstVoltageCalcWire = val; }
    void setSecondVoltageCalcWire(bool val) { mSecondVoltageCalcWire = val; }
    void setSourceValuePointer(std::shared_ptr<float> val) { mSourceValue = val; }
    void setCalcOutput(std::shared_ptr<float> val) { mCalcValue = val; }
    void applyVoltage(void);
    void applyCurrent(void);
    void outputVoltage(void);
    void outputCurrent(void);
    
protected:
    float mCellSize;
    int mCurrentSourceSegment{NO_SOURCE_OR_CALC};
    int mCurrentCalcSegment{NO_SOURCE_OR_CALC};
    int mVoltageSourceSegment{NO_SOURCE_OR_CALC};
    bool mInfiniteEnd{false};
    bool mSeparatedStart{false};
    bool mFirstVoltageCalcWire{false};
    bool mSecondVoltageCalcWire{false};
    std::shared_ptr<float> mSourceValue{nullptr};
    std::shared_ptr<float> mCalcValue{nullptr};
    float Dl{};
    std::vector<float> Lk{};   // inductance per unit length
    std::vector<float> Lks{};
    std::vector<float> Lke{};
    std::vector<float> Cke{};  // "node" capacitance (not per unit length)
    std::vector<float> Clke{}; // capacitance per unit length
    std::vector<float> Cks{};
    std::vector<float> Clks{};
    std::vector<float> I{};
    std::vector<float> Ue{};
    std::vector<float> Us{};
    std::vector<float> b1I{};
    std::vector<float> b2I{};
    std::vector<float> b1Ue{};
    std::vector<float> b2Ue{};
    std::vector<float> b1Us{};
    std::vector<float> b2Us{};
    std::vector<std::vector<float>> J{};      // Debye
    std::vector<std::vector<float>> dbetap{}; // Debye
    std::vector<std::vector<float>> dkp{};    // Debye
    unsigned PolesNumber{};                   // Debye
    float Dtw{};
    float Vw{};
    float EndCellVw{};
    float SavedVoltage{};
    float Volnm[3];   // n-1 step voltage
    float Voln[3];    // n step voltage
    float Volnp[3];   // n+1 step voltage
    float Sp[2];
    float Sm[2];
    float Ep[2];
    float Em[2];
    float coefVn[2];
    float coefDiv[2];
    float coefZk[2];
    float coefJr[2];
    unsigned mNumberOfSegments{};
    XyzPointFloat mStart{};
    XyzPointFloat mEnd{};
    float mDiameter{};
    std::vector<SegmentPart> mArrayOfSegmentParts{};
    float mSheathDiameter{};
    float mSheathEps{};
    float mSheathSigma{};
    void splitWire(const Model &fdtdvol);
    void initCouplingCoefficients(const Model &fdtdvol);
    void initPrecalculatedParameters(const Model &fdtdvol);
};


class WireArea : public TimeStepItem {
public:
    WireArea(const Model &fdtdvol, std::vector<ObliqueWire> array);
    
    void calcWiresCurrents(void);
    void calcWiresVoltages(void);
    void calcVoltagesInWireEnds(void);
    void wiresToFdtd(void);
    void applyVoltages(void);
    void applyCurrents(void);
    void outputVoltages(void);
    void outputCurrents(void);
    
    void checkCurrentTraceContinuity(void);
    void addNode(WireNode node);
    bool isWireInNodeList(unsigned wireNumber, WirePointType wirePoint);
    void removeWireFromNode(unsigned wireNumber, WirePointType wirePoint);
    void addNodesAutomatically(void);
    void removeSeparatedWires(void);
    bool isEndWireCloseToCell(unsigned x, unsigned y, unsigned z);
    
    ObliqueWire getWire(unsigned int num) const { return mArrayOfWires[num]; }
    std::vector<ObliqueWire> getArrayOfWires(void) const { return mArrayOfWires; }
    
    virtual void doFirstHalfStepJob(unsigned);
    virtual void doSecondHalfStepJob(unsigned);
    
protected:
    float mCellSize;
    XyzSize mVolSize;
    std::vector<ObliqueWire> mArrayOfWires;
    std::vector<WireNode> WiresNodes{};
};


}    // namespace fdtd


#endif // OBLIQUEWIRE_H__


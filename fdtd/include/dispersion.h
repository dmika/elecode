///
/// \file dispersion.h
/// \brief Implementation of the ADE method. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef DISPERSION_H__
#define DISPERSION_H__


#include "model.h"
#include <memory>


namespace fdtd {


class DebyeAde : public TimeStepItem {
public:
    DebyeAde(std::shared_ptr<Model> model, unsigned polesnumber);
    
    virtual void doFirstHalfStepJob(unsigned) {}
    virtual void doSecondHalfStepJob(unsigned) {}
};


}    // namespace fdtd


#endif    // DISPERSION_H__


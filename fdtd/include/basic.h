///
/// \file basic.h
/// \brief Core FDTD parameters and functions. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef BASIC_H__
#define BASIC_H__


#include "model.h"
#include "shape.h"
#include <memory>


namespace fdtd {


void basicElectric(std::shared_ptr<Model> model);
void basicMagnetic(std::shared_ptr<Model> model);
void basicElectricCpml(std::shared_ptr<Model> model);
void basicMagneticCpml(std::shared_ptr<Model> model);
void basicElectricDebyeAde(std::shared_ptr<Model> model);
void basicElectricCpmlDebyeAde(std::shared_ptr<Model> model);
void initBasicParameters(std::shared_ptr<Model> model);
void makeMetallicWallBorder(std::shared_ptr<Model> model);


class Basic : public TimeStepItem {
public:
    Basic(std::shared_ptr<Model> model);
    
    virtual void init();
};


}    // namespace fdtd


#endif    // BASIC_H__


///
/// \file fdtd.h
/// \brief FDTD class for the main calculation loop.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef FDTD_H__
#define FDTD_H__


#include "model.h"
#include "basic.h"
#include "abc.h"
#include "dispersion.h"
#include "source.h"
#include "output.h"
#include "lumped.h"
#include "shape.h"
#include "wire.h"
#include <deque>


namespace fdtd {


/// Class for the main FDTD calculation loop. It can add items to the loop, start and stop calculations.
class CalcLoop {
public:
    CalcLoop() = default;

    void runLoopTillIteration(unsigned number);
    void pushFrontTimeStepItem(std::shared_ptr<TimeStepItem> item);
    void pushBackTimeStepItem(std::shared_ptr<TimeStepItem> item);
    void insertTimeStepItem(std::shared_ptr<TimeStepItem> item, unsigned position);
    void setStopCalculations(bool stop);
    
private:
    bool stopCalculations{false};
    unsigned timeStep{0};
    std::deque<std::shared_ptr<TimeStepItem>> mTimeStepItemsDeque{};
};


/// The class makes calculations more convenient (setting parameters, running calculations).
/// It helps to hide unnecessary details and makes usage of the code simpler.
class Fdtd {
public:
    Fdtd(XyzSize size, float dl, float s = S_DEFAULT, unsigned threadsNum = 1);
    
    std::shared_ptr<Model> getModel(void) const;
    
    void addTimeStepItem(std::shared_ptr<TimeStepItem> item);
    void addAbc(AbcType type, unsigned abcthickness = 10, float sigma_coef = 1.0,
                float kappa_max = 10.0, float a_max = 0.01, float m = 3.0, float ma = 1.0);
    void initCpmlParameters(unsigned abcthickness = 10, float sigma_coef = 1.0, float kappa_max = 10.0,
                            float a_max = 0.01, float m = 3.0, float ma = 1.0, std::string border = "all");
    void addDispersion(unsigned polesnumber);
    void addPointSource(FieldComponent comp, XyzPoint coord, std::function<float(float)> func, FieldSourceType srcType = FieldSourceType::hard);
    void addPointOutput(FieldComponent component, XyzPoint coordinates, std::string fileName = "output.txt",
                        unsigned interval = 0, float dInterp = 0.0, float sign = 1.0);
    void addPointOutput(std::string fileName = "output.txt", unsigned interval = 0, float dInterp = 0.0);
    void addResistor(XyzDirection direction, XyzPoint coordinates, float resistance);
    void addResistor(XyzPoint firstPoint, XyzPoint secondPoint, float resistance);
    void addCapacitor(XyzDirection direction, XyzPoint coordinates, float capacitance, float conductance = 0.0);
    void addCapacitor(XyzPoint firstPoint, XyzPoint secondPoint, float capacitance, float conductance = 0.0);
    void addLossyDielectricBlock(XyzPoint firstPoint, XyzPoint secondPoint, float epsilon = 1.0, float sigma = 0.0,
                                 std::vector<float> deltaEpsilon = {}, std::vector<float> tau = {});
    void addRailtonThinWire(XyzPoint firstPoint, XyzPoint secondPoint, float diameter = 0.0, char wireSubType = 'n');
    void addStaircaseWire(XyzPoint firstPoint, XyzPoint secondPoint, bool isCorrected = false);
    void calculateTillTime(float timeMoment);
    void calculateTillIteration(unsigned iteration);
    void stopCalculation(void);
    
private:
    std::shared_ptr<Model> mModel;
    std::shared_ptr<CpmlAbc> mCpmlAbc{nullptr};
    CalcLoop mCalcLoop{};
};


}    // namespace fdtd


#endif    // FDTD_H__


///
/// \file output.h
/// \brief Calculation results output.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef OUTPUT_H__
#define OUTPUT_H__


#include "model.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <memory>


namespace fdtd {


class Output : public TimeStepItem {
public:
    Output() = default;
    Output(std::ostream &outputStream, float dt = 0.0, unsigned interval = 1, float dInterp = 0.0);
    Output(std::string fileName = "output.txt", float dt = 0.0, unsigned interval = 0, float dInterp = 0.0);
    Output(std::shared_ptr<Model> model, std::string fileName = "output.txt", unsigned interval = 0, float dInterp = 0.0);
    Output(std::shared_ptr<Model> model, FieldComponent component, XyzPoint coordinates,
           std::string fileName = "output.txt", unsigned interval = 0, float dInterp = 0.0, float sign = 1.0);
    ~Output();
    Output(const Output&) = default;
    Output& operator=(const Output&) = default;
    Output(Output&&) = default;
    Output& operator=(Output&&) = default;
    
    virtual void doFirstHalfStepJob(unsigned);
    virtual void doSecondHalfStepJob(unsigned timeStepNumber);
    
    void addFieldPoint(FieldPoint point);
    void addFieldPoint(FieldComponent component, XyzPoint coordinates, float sign = 1.0, float ***arrayPointer = nullptr);
    void addDataPointer(std::shared_ptr<float> datapointer);
    std::shared_ptr<float> getAddedDataPointer(void);
    void save(void);
    
    std::vector<std::vector<float>> getOutputArray(void) const { return mOutputArray; }
    float getDInterp(void) const { return mDInterp; }
    std::string getFileName(void) const { return mFileName; }
    
private:
    std::vector<std::vector<float>> mOutputArray{};
    std::vector<std::shared_ptr<float>> mDataNow{};
    std::vector<float> mDataPrevious{};
    std::vector<FieldPoint> mFieldPointArray{};
    unsigned mInterval{};
    float mDInterp{};
    float mDt{};
    unsigned mtmoment{0};
    std::ofstream mOutputFileStream{};
    std::ostream &mOutputStream;
    std::string mFileName{};
};


class VoltagePath : public TimeStepItem {
public:
    VoltagePath(float d);

    virtual void doFirstHalfStepJob(unsigned);
    virtual void doSecondHalfStepJob(unsigned);

    std::shared_ptr<float> getCurrentVoltagePointer(void) { return mCurrentVoltage; }
    void addItem(FieldPoint item) { mPointsArray.push_back(item); }

private:
    float mD{};
    std::vector<FieldPoint> mPointsArray{};
    std::shared_ptr<float> mCurrentVoltage{nullptr};
};


class AmperesLawCurrent : public TimeStepItem {
public:
    AmperesLawCurrent(std::shared_ptr<Model> model, FieldPoint point);

    virtual void doFirstHalfStepJob(unsigned);
    virtual void doSecondHalfStepJob(unsigned);

    std::shared_ptr<float> getCurrentCurrentPointer(void) { return mCurrentCurrent; }
    void addItem(FieldPoint item) { mPointsArray.push_back(item); }

private:
    float mD{};
    std::vector<FieldPoint> mPointsArray{};
    std::shared_ptr<float> mCurrentCurrent{nullptr};
};


class FieldOutput : public TimeStepItem {
public:
    FieldOutput(std::shared_ptr<Model> model, Vector point, FieldComponent field);

    virtual void doFirstHalfStepJob(unsigned);
    virtual void doSecondHalfStepJob(unsigned);

    std::shared_ptr<float> getCurrentFieldPointer(void) { return mCurrentField; }
    void addItem(FieldPoint item) { mPointsArray.push_back(item); }

private:
    std::vector<FieldPoint> mPointsArray{};
    std::shared_ptr<float> mCurrentField{nullptr};
};


}    // namespace fdtd


#endif    // OUTPUT_H__


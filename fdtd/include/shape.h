///
/// \file shape.h
/// \brief Modeling different shapes. See Taflove's book.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef SHAPE_H__
#define SHAPE_H__


#include "model.h"


namespace fdtd {


void addLossyDielectricBlockToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint,
                                    float epsilon = 1.0, float sigma = 0.0, std::vector<float> deltaEpsilon = {}, std::vector<float> tau = {});
void addLosslessDielectricBlockToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float epsilon = 1.0);
void addLosslessMagneticBlockToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint, float mu = 1.0);
void addPerfectConductorBlockToModel(std::shared_ptr<Model> model, XyzPoint firstPoint, XyzPoint secondPoint);


}    // namespace fdtd


#endif    // SHAPE_H__


///
/// \file source.h
/// \brief EM field sources.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef SOURCE_H__
#define SOURCE_H__


#include "model.h"
#include "waveform.h"
#include <cmath>


namespace fdtd {


enum class FieldSourceType {
    hard,
    soft
};


class Source : public TimeStepItem {
public:
    Source(std::shared_ptr<Waveform> waveform, float dt);

    std::shared_ptr<float> getCurrentValuePointer() { return mCurrentValue; }

    virtual void doFirstHalfStepJob(unsigned timeStepNumber);
    virtual void doSecondHalfStepJob(unsigned timeStepNumber);

protected:
    std::shared_ptr<Waveform> mWaveform{nullptr};
    std::shared_ptr<float> mCurrentValue{nullptr};
    float mDt{0.0};
};


class PointSource : public TimeStepItem {
public:
    PointSource() = default;
    PointSource(std::shared_ptr<Model> model, FieldComponent comp, XyzPoint coord,
                std::function<float(float)> func, FieldSourceType srcType = FieldSourceType::hard);
    PointSource(std::shared_ptr<Model> model, FieldComponent comp, XyzPoint coord,
                std::vector<float> array, FieldSourceType srcType = FieldSourceType::hard);
    PointSource(std::shared_ptr<Model> model, FieldComponent comp, XyzPoint coord,
                std::shared_ptr<Waveform> waveform, FieldSourceType srcType = FieldSourceType::hard);
    ~PointSource();
    PointSource(const PointSource&) = default;
    PointSource& operator=(const PointSource&) = default;
    PointSource(PointSource&&) = default;
    PointSource& operator=(PointSource&&) = default;
    
    void setField(unsigned timeStepNumber);
    
    virtual void doFirstHalfStepJob(unsigned timeStepNumber);
    virtual void doSecondHalfStepJob(unsigned timeStepNumber);
    
protected:
    FieldComponent mComponent{};
    XyzPoint mCoordinates{};
    std::function<float(float)> mFieldSourceFunction{};
    std::vector<float> mFieldSourceArray{};
    std::shared_ptr<Waveform> mWaveform{nullptr};
    FieldSourceType mSourceType{};
    float mDt{0.0};
    float ***mFieldArrayPointer{nullptr};
    float mFieldCoefficient{1.0};
};


class VoltageSource : public PointSource {
public:
    VoltageSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                  std::function<float(float)> func, float resistance = 0.0);
    VoltageSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                  std::vector<float> array, float resistance = 0.0);
    VoltageSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                  std::shared_ptr<Waveform> waveform, float resistance = 0.0);

    virtual void doFirstHalfStepJob(unsigned timeStepNumber);
    virtual void doSecondHalfStepJob(unsigned timeStepNumber);

protected:
    void initModel(std::shared_ptr<Model> model);

    XyzDirection mDirection{};
    float mResistance{0.0};
};


class CurrentSource : public PointSource {
public:
    CurrentSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                  std::function<float(float)> func, float conductance = 0.0);
    CurrentSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                  std::vector<float> array, float conductance = 0.0);
    CurrentSource(std::shared_ptr<Model> model, XyzDirection dir, XyzPoint coord,
                  std::shared_ptr<Waveform> waveform, float conductance = 0.0);

    virtual void doFirstHalfStepJob(unsigned timeStepNumber);
    virtual void doSecondHalfStepJob(unsigned timeStepNumber);

protected:
    void initModel(std::shared_ptr<Model> model);

    XyzDirection mDirection{};
    float mConductance{0.0};
};


class TfsfArea : public TimeStepItem {
public:
    TfsfArea() = default;
    TfsfArea(std::shared_ptr<Model> model, std::function<float(unsigned)> func,
             float theta = 90.0, float phi = 45.0, float psi = 0.0, unsigned borderdist = 20);
    TfsfArea(std::shared_ptr<Model> model, std::shared_ptr<Waveform> waveform,
             float theta = 90.0, float phi = 45.0, float psi = 0.0, unsigned borderdist = 20);
    ~TfsfArea();
    TfsfArea(const TfsfArea& ta) = default;
    TfsfArea& operator=(const TfsfArea& ta) = default;
    TfsfArea(TfsfArea&&) = default;
    TfsfArea& operator=(TfsfArea&&) = default;
    
    void dCalcWithShift(float ishift, float jshift, float kshift);
    void eIncLinearInterpolation (void);
    void hIncLinearInterpolation (void);
    float getHx(void);
    float getHy(void);
    float getHz(void);
    float getEx(void);
    float getEy(void);
    float getEz(void);
    void calcEinc(void);
    void applyESource(float value);
    void calcHinc(void);
    void calcEfieldTFSF(void);
    void calcHfieldTFSF(void);
    
    virtual void doFirstHalfStepJob(unsigned timeStepNumber);
    virtual void doSecondHalfStepJob(unsigned);
    
protected:
    void initParams(std::shared_ptr<Model> model, float theta = 90.0, float phi = 45.0,
                    float psi = 0.0, unsigned borderdist = 20);
    
    std::function<float(float)> mFieldSourceFunction{};
    std::shared_ptr<Waveform> mWaveform{nullptr};
    unsigned mi{}, mj{}, mk{};
    float mHconst{}, mEconst{};
    BasicParameters mb{};
    unsigned mxTFsize{}, myTFsize{}, mzTFsize{}, mIncFieldArraySize{};
    std::shared_ptr<float[]> mEinc{nullptr};
    std::shared_ptr<float[]> mHinc{nullptr};
    float mEincConst{}, mHincConst{};
    float mTheta{}, mPhi{}, mPsi{};
    float mSinTheta{}, mCosTheta{}, mSinPhi{}, mCosPhi{}, mSinPsi{}, mCosPsi{}, mdist{};
    unsigned mii0{}, mjj0{}, mkk0{}, mii1{}, mjj1{}, mkk1{}, mm0{};
    int mifixd{};
    float mfractdist{};
    float mlineinc{}, mlinhinc{};
    bool mMemoryManager{true};
    float mDt{0.0};
};


}    // namespace fdtd


#endif    // SOURCE_H__


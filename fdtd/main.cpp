///
/// \file main.cpp
/// \brief Example.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "fdtd.h"


int main (void)
{
    using namespace fdtd;
    const float cellsize = 0.05;
    auto fieldSource = [&](float time) noexcept
    {
        const float T = 2.0e-9;
        return (10.0e-10) * exp(-pow(((time-3.0*T)/T), 2.0)) / (EPS0*pow(cellsize, 3.0));
    };
    auto fdtdcalc = std::make_unique<Fdtd>(XyzSize(40, 40, 40), cellsize);
    float sigmacoef = 1.0;
    float kappamax = 10.0;
    float alphamax = 0.01;
    fdtdcalc->addAbc(AbcType::cpml, 8, sigmacoef, kappamax, alphamax);
    fdtdcalc->addPointOutput(FieldComponent::ez, XyzPoint(20, 30, 20), "cpml.txt");
    fdtdcalc->addPointSource(FieldComponent::ez, XyzPoint(20, 20, 20), fieldSource, FieldSourceType::hard);
    fdtdcalc->calculateTillTime(22.0e-9);
    return 0;
}


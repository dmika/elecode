
CC = g++

WARNINGS = -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wformat=2 -Winit-self -Wlogical-op \
    -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls \
    -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef \
    -Wchar-subscripts -Wcomment -Wdisabled-optimization -Wformat -Wformat-nonliteral -Wformat-security -Wformat-y2k \
    -Wimport -Winline -Winvalid-pch -Wunsafe-loop-optimizations -Wlong-long -Wmissing-braces -Wparentheses \
    -Wmissing-field-initializers -Wmissing-format-attribute -Wmissing-noreturn -Wpacked -Wpointer-arith -Wreturn-type \
    -Wsequence-point -Wsign-compare -Wstack-protector -Wstrict-aliasing -Wstrict-aliasing=2 -Wswitch -Wswitch-enum \
    -Wtrigraphs -Wuninitialized -Wunreachable-code -Wunused -Wunused-function -Wunused-label -Wunused-parameter \
    -Wunknown-pragmas -Wunused-value -Wunused-variable -Wvariadic-macros -Wvolatile-register-var -Wwrite-strings \
    -Weffc++ -Wfloat-equal

CFLAGS = -O3 -std=c++20 -I. -I./fdtd/include -I./common/include -I./permittivity/include -I./flashover/include -fopenmp $(WARNINGS)
CFLAGS += --param max-inline-insns-single=800

SOURCE = ./common/src/fft.cpp ./common/src/integration.cpp ./common/src/interpolation.cpp \
    ./common/src/pso.cpp ./common/src/vectors.cpp ./common/src/waveform.cpp \
    ./permittivity/src/apparent.cpp ./permittivity/src/common.cpp ./permittivity/src/doi.cpp \
    ./permittivity/src/properties.cpp ./permittivity/src/psols.cpp ./permittivity/src/soilmodel.cpp \
    ./fdtd/src/abc.cpp ./fdtd/src/basic.cpp ./fdtd/src/dispersion.cpp ./fdtd/src/fdtd.cpp ./fdtd/src/lumped.cpp \
    ./fdtd/src/model.cpp ./fdtd/src/output.cpp ./fdtd/src/shape.cpp ./fdtd/src/source.cpp \
    ./fdtd/src/wire.cpp ./fdtd/src/obliquewire.cpp ./flashover/src/breakdown.cpp ./flashover/src/bfp.cpp

all:
	$(CC) -g -c $(SOURCE) $(CFLAGS)
	ar ruv elecode.a *.o
	ranlib elecode.a
	rm *.o

dev:
	$(CC) -g -c $(SOURCE) $(CFLAGS) -Werror -pedantic-errors
	ar ruv elecode.a *.o
	ranlib elecode.a
	rm *.o

clean:
	touch makefile
	rm elecode.a *.o


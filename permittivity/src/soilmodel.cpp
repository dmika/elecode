///
/// \file model.cpp
/// \brief Models for soil properties.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "soilmodel.h"


std::vector<ComplexPermittivity> modelAlipioVisacro(float rho0, float minfreq, float maxfreq, unsigned fnum)
{
    std::vector<float> wi = calcLogFrequencies(minfreq, maxfreq, fnum, FreqType::hz, FreqType::rads);
    std::vector<ComplexPermittivity> eps;
    for (unsigned cnt = 0; cnt < wi.size(); ++cnt) {
        float freqhz = wi[cnt]/(2.0*PI);
        float epsinf = 12.0;
        float gamma = 0.54;
        float sigma0 = (1.0/rho0) * 1000.0;
        float h = 1.26 * pow(sigma0, -0.73);
        float im = pow(freqhz/1.0e6, gamma) * (sigma0*h*1.0e-3) / (2.0*PI*freqhz*EPS0);
        float re = epsinf + sigma0*h*pow(freqhz, gamma-1.0) * (tan(PI*gamma/2.0)*1.0e-3) / (2.0*PI*EPS0*pow(1.0e6, gamma));
        eps.push_back(ComplexPermittivity(wi[cnt], std::complex<float>(re, im)));
    }
    return eps;
}


std::vector<ComplexPermittivity> modelPortela(float rho0, float minfreq, float maxfreq, unsigned fnum)
{
    std::vector<float> wi = calcLogFrequencies(minfreq, maxfreq, fnum, FreqType::hz, FreqType::rads);
    std::vector<ComplexPermittivity> eps;
    for (unsigned cnt = 0; cnt < wi.size(); ++cnt) {
        float sigma0 = 1.0/rho0;
        float alfa = 0.706;
        float deltai = 11.71e-3;
        float sigma = sigma0 + deltai * (1.0/tan(alfa*PI/2.0)) * pow(wi[cnt]/(2.0*PI*1.0e6), alfa);
        float rho = 1.0/sigma;
        float im = (rho0-rho) / (rho0*rho*wi[cnt]*EPS0);
        float re = deltai * pow(wi[cnt]/(2.0*PI*1.0e6), alfa) / (wi[cnt]*EPS0);
        eps.push_back(ComplexPermittivity(wi[cnt], std::complex<float>(re, im)));
    }
    return eps;
}


std::vector<ComplexPermittivity> modelMessier(float rho0, float minfreq, float maxfreq, unsigned fnum)
{
    std::vector<float> wi = calcLogFrequencies(minfreq, maxfreq, fnum, FreqType::hz, FreqType::rads);
    std::vector<ComplexPermittivity> eps;
    for (unsigned cnt = 0; cnt < wi.size(); ++cnt) {
        float freqhz = wi[cnt]/(2.0*PI);
        float sigma0 = 1.0/rho0;
        float epsinf = 8.0*EPS0;
        float sigma = sigma0 * (1.0 + sqrt((4.0*PI*freqhz*epsinf)/sigma0));
        float rho = 1.0/sigma;
        float im = (rho0-rho) / (rho0*rho*wi[cnt]*EPS0);
        float re = (epsinf/EPS0) * (1.0 + sqrt(sigma0/(PI*freqhz*epsinf)));
        eps.push_back(ComplexPermittivity(wi[cnt], std::complex<float>(re, im)));
    }
    return eps;
}


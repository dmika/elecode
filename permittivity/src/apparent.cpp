///
/// \file apparent.cpp
/// \brief Apparent resistivity calculation. See "Analysis of apparent resistivity in a multi-layer earth structure" by T. Takahashi and T. Kawase.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "apparent.h"


float Apparent::wenner(float a)
{
    return mLayers[0].rho * (1.0 + 2.0*FN(a) - FN(2.0*a));
}


float Apparent::dipole(float a, float b)
{
    float k = calcPerpDipoleArrayCoef(a, b);
    
    float V21 = -getV(sqrt((a+b)*(a+b) + b*b));
    float V24 = getV(b*sqrt(2.0));
    float V31 = -getV((a+b)*sqrt(2.0));
    float V34 = getV(sqrt((a+b)*(a+b) + b*b));

    float Vn = V21 + V24 - (V31 + V34);

    float rhoa = Vn * k;

    return rhoa;
}


float Apparent::getV(float x)
{
    return (1.0 + FN(x)) * mLayers[0].rho / (2.0*PI*x);
}


float Apparent::calcPerpDipoleArrayCoef(float dipolesize, float dist)
{
    float a = dipolesize;
    float b = dist;
    return 2.0*PI/(1.0/((a+b)*sqrt(2.0)) + 1.0/(b*sqrt(2.0)) - 2.0/sqrt((a+b)*(a+b)+b*b));
}


float Apparent::K(float m, float n, float lambda, float h)
{
    return (m + n*exp(-2.0*lambda*h)) / (1.0 + m*n*exp(-2.0*lambda*h));
}


float Apparent::FN(float x)
{
    auto integr = [&](float lambda)
    {
        if (mLayers.size() < 2) return 0.0;

        std::vector<float> k;
        for (unsigned cnt = 0; cnt < mLayers.size()-1; ++cnt) {
            k.push_back((mLayers[cnt+1].rho-mLayers[cnt].rho)/(mLayers[cnt+1].rho+mLayers[cnt].rho));
        }

        float KN = k[0];
        if (k.size() >= 2) {
            KN = K(k[k.size()-2], k[k.size()-1], lambda, mLayers[k.size()-1].h);
        }
        if (k.size() >= 3) {
            for (int cnt = k.size()-3; cnt >= 0; --cnt) {
                KN = K(k[static_cast<unsigned>(cnt)], KN, lambda, mLayers[static_cast<unsigned>(cnt+1)].h);
            }
        }

        return std::cyl_bessel_jf(0.0, lambda*x)*(KN*exp(-2.0*lambda*mLayers[0].h)) / (1.0 - KN*exp(-2.0*lambda*mLayers[0].h));
    };

    return 2.0 * x * integrateTrapezoidal(integr, 0.0, 2.0e2, 10e4);
}


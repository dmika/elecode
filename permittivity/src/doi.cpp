///
/// \file doi.cpp
/// \brief Depth of investigation calculation. See "Depth of investigation in direct current methods" by A. Roy and A. Apparao;
/// "Depth of investigation of collinear symmetrical four‐electrode arrays" by R. Barker.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "doi.h"


double DoiMedianDipole(XyPointDouble cur1, XyPointDouble cur2, XyPointDouble pot1, XyPointDouble pot2, double delta)
{
    int cnt = 1;
    for (double result = 0.0; result < 0.5; ++cnt) {
        result += DicDipole(cur1, cur2, pot1, pot2, (static_cast<double>(cnt)-0.5)*delta)*delta;
    }
    return static_cast<double>(cnt-1)*delta;
}


double DicDipole(XyPointDouble cur1, XyPointDouble cur2, XyPointDouble pot1, XyPointDouble pot2, double z)
{
    double p1c1 = getDistanceBetweenPoints(pot1, cur1);
    double p1c2 = getDistanceBetweenPoints(pot1, cur2);
    double p2c1 = getDistanceBetweenPoints(pot2, cur1);
    double p2c2 = getDistanceBetweenPoints(pot2, cur2);
    
    double M = 1.0/p1c1 - 1.0/p1c2 - 1.0/p2c1 + 1.0/p2c2;

    return (4.0*z/M) * ( 1.0/pow(p1c1*p1c1 + 4.0*z*z, 1.5) - 1.0/pow(p1c2*p1c2 + 4.0*z*z, 1.5)
                       - 1.0/pow(p2c1*p2c1 + 4.0*z*z, 1.5) + 1.0/pow(p2c2*p2c2 + 4.0*z*z, 1.5) );
}


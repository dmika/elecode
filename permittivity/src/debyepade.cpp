///
/// \file debyepade.cpp
/// \brief See "Consistent FDTD Modeling of Dispersive Dielectric Media
/// via Multiple Debye Terms Derived Rigorously by Pade Approximants" by Ioannis T. Rekanos.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "debyepade.h"


EpsTauDebyeParameters calcDebyeParameters(unsigned M, double deps, double tau, double a, double b, double x)
{
    std::vector<long double> wk(M);
    std::vector<std::vector<long double>> wkm(M, std::vector<long double>(M));
    for (long unsigned k = M; k <= 2*M-1; ++k) {
        wk[k-M] = -calcWk(k, x, a, b);
        for (long unsigned m = 1; m <= M; ++m) {
            wkm[k-M][m-1] = calcWk(k-m, x, a, b);
        }
    }
    std::vector<long double> qq = solveMatrixEquation<long double>(wkm, wk);
    std::vector<long double> q = {1.0};
    for (auto i : qq) {
        q.push_back(i);
    }
    
    std::vector<long double> p(M);
    for (long unsigned k = 0; k <= M-1; ++k) {
        long double sum = 0.0;
        for (long unsigned m = 0; m <= k; ++m) {
            sum += calcWk(k-m, x, a, b)*q[m];
        }
        p[k] = sum;
    }
    
    
    std::vector<long double> roots = findRootsWithDeflation(findRootNewton, q);
    for (auto &i : roots) {
        i = -i;
    }
    std::vector<std::vector<long double>> mat(M, std::vector<long double>(M));
    for (long unsigned j = 0; j < M; ++j) {
        std::vector<long double> tmp;
        for (long unsigned cnt = 0; cnt < M; ++cnt) {
            if (cnt != j) {
                tmp.push_back(roots[cnt]);
            }
        }
        
        std::vector<long double> polin = {tmp[0], 1.0};
        for (long unsigned t = 1; t < M-1; ++t) {
            std::vector<long double> tempol = {tmp[t], 1.0};
            polin = multiplyPolynomials(polin, tempol);
        }
        
        for (long unsigned i = 0; i < M; ++i) {
            mat[i][j] = polin[M-1-i];
        }
    }
    
    std::vector<long double> coef(M);
    for (long unsigned j = 0; j < M; ++j) {
        coef[j] = p[M-1-j];
    }
    std::vector<long double> A = solveMatrixEquation<long double>(mat, coef);
    for (long unsigned i = 0; i < M; ++i) {
        A[i] /= q[M];
    }
    
    
    std::vector<float> depsd(M);
    std::vector<float> taud(M);
    for (long unsigned i = 0; i < M; ++i) {
        depsd[i] = deps * A[i] / (roots[i] - x);
        taud[i] = tau / (roots[i] - x);
    }
    
    return EpsTauDebyeParameters(depsd, taud);
}


long double calcWk(long unsigned k, long double x, long double a, long double b)
{
    return getNthDerivativeHnType(k, x, a, b) / static_cast<long double>(factorial(k));
}


long double getNthDerivativeHnType(long unsigned n, long double xin, long double a, long double b)
{
    std::complex<long double> x = xin;
    std::complex<long double> result = 0.0;
    for (long unsigned k = 0; k <= n; ++k) {
        for (long unsigned j = 0; j <= k; ++j) {
            long double kd = static_cast<long double>(k);
            long double nd = static_cast<long double>(n);
            long double jd = static_cast<long double>(j);
            std::complex<long double> tmp = powl(-1.0L, j) * pow(x, std::complex<long double>(a*kd-nd,0.0)) *
                pow(std::complex<long double>(1.0L,0.0)+pow(x,a), -b-k) * risingFactorial(1.0L-b-kd, kd) * risingFactorial(1.0L-a*j+a*kd-nd, nd);
            if (1.0-a*j+a*kd-nd < 0.0 && ceill(1.0L-a*j+a*kd-nd) == 1.0L-a*j+a*kd-nd) {
                tmp = 0.0;
            }
            tmp /= static_cast<long double>(factorial(j)) * static_cast<long double>(factorial(-j+k));
            result += tmp;
        }
    }
    return result.real();
}


long double risingFactorial(long double x, long double n)
{
    return tgamma(x+n)/tgamma(x);
}


long unsigned factorial(long unsigned x)
{
    long unsigned fact = 1;
    for(long unsigned i = 1; i <= x; ++i) {
        fact *= i;
    }
    return fact;
}


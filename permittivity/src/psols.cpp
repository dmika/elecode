///
/// \file psols.cpp
/// \brief See "Debye Function Expansions of Complex Permittivity Using a Hybrid Particle Swarm-Least Squares
/// Optimization Approach" by David F. Kelley, Timothy J. Destan, Raymond J. Luebbers.
/// Not all features of the algorithm were implemented (if there are cases
/// for which those features are necessary, please let me know).
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "psols.h"
#include "constants.h"
#include <cmath>


SwarmPsols::SwarmPsols(SampledPermittivity eps, unsigned dimensNum, unsigned particlesNum)
    : mReferencePermittivity{eps}, mDimensNum{dimensNum}
{
    std::random_device rd;
    mGen = std::make_shared<std::mt19937>(rd());
    init(particlesNum);
}


SwarmPsols::SwarmPsols(SampledPermittivity eps)
    : mReferencePermittivity{eps}
{
    std::random_device rd;
    mGen = std::make_shared<std::mt19937>(rd());
    mDimensNum = static_cast<unsigned>(0.5 + static_cast<float>(eps.samples.size()) / 4.0);
    init(30);
}


void SwarmPsols::init(unsigned particlesNum)
{
    if (particlesNum == 0) {
        throw std::invalid_argument("Zero number of particles.");
    }
    if (mReferencePermittivity.samples.size() == 0) {
        throw std::invalid_argument("Zero ReferencePermittivity size.");
    }
    if (mDimensNum == 0) {
        throw std::invalid_argument("Zero DimensNum.");
    }

    if (mReferencePermittivity.freq == FreqType::hz) {
        for (auto &rp : mReferencePermittivity.samples) {
            rp.w *= 2.0*PI;
        }
        mReferencePermittivity.freq = FreqType::rads;
    }
    gBest.clear();
    mParticles.clear();
    gBest.resize(mDimensNum, 0.0);
    for (unsigned cnt = 0; cnt < particlesNum; ++cnt) {
        mParticles.push_back(ParticlePsols(this));
    }
    gBestFitness = std::numeric_limits<float>::max();
    mMaxVeolcity = 0.1*mReferencePermittivity.samples[mReferencePermittivity.samples.size()-1].w; // 100.0*eps[0].w;
}


void SwarmPsols::makeStep(void)
{
    for (auto &particle : mParticles) {
        particle.makeStep();
    }
}


void SwarmPsols::makeIterations(unsigned iterationsNum)
{
    if (iterationsNum == 0) {
        throw std::invalid_argument("Zero iterations.");
    }

    for (unsigned cnt = 0; cnt < iterationsNum; ++cnt) {
        makeStep();
    }
}


// choose best fit parameters from n repeats (also, parameters should be positive)
DebyeParameters SwarmPsols::calcParametersWithRepeats(unsigned repeats, unsigned iterationsNum)
{
    if (repeats == 0) {
        throw std::invalid_argument("Zero repeats.");
    }

    DebyeParameters dpsave;
    for (unsigned cnt = 0; cnt < repeats; ++cnt) {
        makeIterations(iterationsNum);
        DebyeParameters dp = getDebyeParameters();
        bool good = true;
        for (const auto &dpi : dp.eps) {
            if (dpi < 0.0) {
                good = false;
            }
        }
        if (dpsave.fit > dp.fit && good) {
            dpsave = dp;
        }
        init(mParticles.size());
    }
    return dpsave;
}


DebyeParameters SwarmPsols::getDebyeParameters(void)
{
    float bestFitness = std::numeric_limits<float>::max();
    unsigned bestParticle = 0;
    for (unsigned part = 0; part < mParticles.size(); ++part) {
        if (mParticles[part].getFitness() < bestFitness) {
            bestFitness = mParticles[part].getFitness();
            bestParticle = part;
        }
    }
    std::vector<float> epsdel = mParticles[bestParticle].getWeight();
    for (auto &ed : epsdel) {
        ed *= mPermittivityS-mPermittivityInf;
    }
    std::vector<float> tau = mParticles[bestParticle].getTau();
    float epstot = mParticles[bestParticle].getDe() + mPermittivityInf;
    return DebyeParameters(epsdel, tau, epstot, bestFitness);
}


ParticlePsols::ParticlePsols(SwarmPsols *swarm)
{
    mSwarm = swarm;
    std::vector<ComplexPermittivity> eps = mSwarm->getReferencePermittivity().samples;
    unsigned dimensNum = mSwarm->getDimensNum();
    mVmax = mSwarm->getMaxVeolcity();
    unsigned Nf = eps.size();
    for (unsigned cnt = 0; cnt < Nf; ++cnt) {
        mwi.push_back(eps[cnt].w);
        mc.push_back(eps[cnt].epsilon.imag() / (mSwarm->getPermittivityS()-mSwarm->getPermittivityInf()));
    }

    std::uniform_real_distribution<float> dismx(mwi[0], mwi[mwi.size()-1]);
    std::uniform_real_distribution<float> dismv(0.0, mVmax);
    for (unsigned cnt = 0; cnt < dimensNum; ++cnt) {
        mx.push_back(dismx(*mSwarm->getGen()));
        mv.push_back(dismv(*mSwarm->getGen()));
    }

    pBest.resize(dimensNum, 0.0);
    ma.resize(dimensNum, 0.0);
    mD.resize(Nf, std::vector<float>(dimensNum, 0.0));

    mI.resize(dimensNum, std::vector<float>(dimensNum, 0.0));
    for (unsigned cnti = 0; cnti < mI.size(); ++cnti) {
        mI[cnti][cnti] = 1.0;
    }
}


void ParticlePsols::makeStep(void)
{
    // apply boundary conditions, if needed
    // check the proximity of relaxation frequencies, if needed
    // handle negative weights, if needed

    float fitn = evaluateFitness();
    if (fitn < mSwarm->getBestFitness()) {
        mSwarm->setBestFitness(fitn);
        mSwarm->setBest(mx);
    }
    if (fitn < pBestFitness) {
        pBest = mx;
        pBestFitness = fitn;
    }

    updateVelocity();
    updatePosition();
}


float ParticlePsols::evaluateFitness(void)
{
    // calc D
    for (unsigned cntf = 0; cntf < mD.size(); ++cntf) {
        for (unsigned cntp = 0; cntp < mD[0].size(); ++cntp) {
            float widx = mwi[cntf]/mx[cntp];
            mD[cntf][cntp] = widx / (1.0 + widx*widx);
        }
    }

    // calc DTD
    std::vector<std::vector<float>> DTD(mD[0].size(), std::vector<float>(mD[0].size(), 0.0));
    for (unsigned cnti = 0; cnti < DTD.size(); ++cnti) {
        for (unsigned cntj = 0; cntj < DTD[0].size(); ++cntj) {
            for (unsigned cntf = 0; cntf < mD.size(); ++cntf) {
                DTD[cnti][cntj] += mD[cntf][cnti]*mD[cntf][cntj];
            }
        }
    }

    // calc DTD+mGamma*mI
    // currently, this has no effect
    for (unsigned cnti = 0; cnti < DTD.size(); ++cnti) {
        for (unsigned cntj = 0; cntj < DTD[0].size(); ++cntj) {
            DTD[cnti][cntj] += mGamma*mI[cnti][cntj];
        }
    }

    // calc DTD^-1
    std::vector<std::vector<float>> IDTD = getInvertedMatrix<float>(DTD, true);

    // calc (DTD^-1)DT
    std::vector<std::vector<float>> DTIDTD(mD[0].size(), std::vector<float>(mD.size(), 0.0));
    for (unsigned cnti = 0; cnti < DTIDTD.size(); ++cnti) {
        for (unsigned cntj = 0; cntj < DTIDTD[0].size(); ++cntj) {
            for (unsigned cntf = 0; cntf < IDTD.size(); ++cntf) {
                DTIDTD[cnti][cntj] += IDTD[cnti][cntf]*mD[cntj][cntf];
            }
        }
    }

    // calc a
    for (unsigned cnti = 0; cnti < ma.size(); ++cnti) {
        ma[cnti] = 0.0;
        for (unsigned cntf = 0; cntf < DTIDTD[0].size(); ++cntf) {
            ma[cnti] += DTIDTD[cnti][cntf]*mc[cntf];
        }
    }

    float error = 0.0;
    for (unsigned cnti = 0; cnti < mc.size(); ++cnti) {
        float cc = 0.0;
        for (unsigned cnta = 0; cnta < ma.size(); ++cnta) {
            float widx = mwi[cnti]/mx[cnta];
            cc += ma[cnta] * widx / (1.0 + widx*widx);
        }
        error += (mc[cnti]-cc)*(mc[cnti]-cc);
    }

    return error;
}


void ParticlePsols::updateVelocity(void)
{
    std::uniform_real_distribution<float> dis(0.0, 1.0);
    for (unsigned cnt = 0; cnt < mv.size(); ++cnt) {
        mv[cnt] = mw*mv[cnt] + mc1*dis(*mSwarm->getGen())*(pBest[cnt]-mx[cnt]) +
                               mc2*dis(*mSwarm->getGen())*(mSwarm->getBest()[cnt]-mx[cnt]);
    }

    // constrain veolcity if needed
    float vsum = 0.0;
    for (const auto &mvi : mv) {
        vsum += mvi*mvi;
    }
    vsum = sqrt(vsum);
    if (vsum > mVmax) {
        float mul = mVmax / vsum;
        //mul *= 0.3; // coefficient can be adjusted
        for (auto &mvi : mv) {
            mvi *= mul;
        }
    }
}


void ParticlePsols::updatePosition(void)
{
    for (unsigned cnt = 0; cnt < mx.size(); ++cnt) {
        float dt = 1.0;
        mx[cnt] += mv[cnt]*dt;
    }
}


float ParticlePsols::getDe(void) const
{
    // calc delta epsilon
    float sum = 0.0;
    std::vector<ComplexPermittivity> eps = mSwarm->getReferencePermittivity().samples;
    float es = mSwarm->getPermittivityS();
    float einf = mSwarm->getPermittivityInf();
    for (unsigned cntf = 0; cntf < mwi.size(); ++cntf) {
        float sumnp = 0.0;
        for (unsigned cntp = 0; cntp < mx.size(); ++cntp) {
            sumnp += (es - einf) * ma[cntp] / (1.0 + (mwi[cntf]/mx[cntp])*(mwi[cntf]/mx[cntp]));
        }
        sum += eps[cntf].epsilon.real() - einf - sumnp;
    }
    return sum / static_cast<float>(mwi.size());
}


///
/// \file properties.cpp
/// \brief Calculation of the soil properties (resistivity, permittivity).
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "properties.h"


std::vector<RhoEpsilon> calcRhoEpsilon(CurrentVoltage curvol, float arraycoef)
{
    std::vector<std::complex<float>> curt(curvol.cur.size());
    std::vector<std::complex<float>> volt(curvol.vol.size());
    for(unsigned cnt = 0; cnt < curvol.cur.size(); ++cnt) {
        curt[cnt] = curvol.cur[cnt];
        volt[cnt] = curvol.vol[cnt];
    }
    float ratio = 1.0;
    auto curf = fft(curt, &ratio);
    auto volf = fft(volt);
    float df = 1.0 / (curvol.dt*ratio*curf.size());
    std::vector<RhoEpsilon> rhoeps;
    for (unsigned cnt = 0; cnt < curf.size()/2; ++cnt) {
        std::complex<float> yf = curf[cnt]/volf[cnt];
        RhoEpsilon re;
        re.freq = df*cnt;
        re.rho = 1.0/yf.real();
        if (cnt > 0) re.eps = yf.imag()/(df*cnt*EPS0*2.0*PI);
        re.rho *= arraycoef;
        re.eps /= arraycoef;
        rhoeps.push_back(re);
    }
    return rhoeps;
}


std::vector<RhoEpsilon> calcRhoEpsilon(float rho0, std::vector<ComplexPermittivity> eps)
{
    std::vector<RhoEpsilon> rhoeps;
    for (unsigned cnt = 0; cnt < eps.size(); ++cnt) {
        RhoEpsilon re;
        re.freq = eps[cnt].w;
        re.eps = eps[cnt].epsilon.real();
        re.rho = rho0/(1.0 + rho0*2.0*PI*re.freq*EPS0*eps[cnt].epsilon.imag());
        rhoeps.push_back(re);
    }
    return rhoeps;
}


std::vector<ComplexPermittivity> calcComplexEpsilon(float rho0, std::vector<RhoEpsilon> rhoeps, bool effective)
{
    std::vector<ComplexPermittivity> eps;
    for (unsigned cnt = 0; cnt < rhoeps.size(); ++cnt) {
        float epw = rhoeps[cnt].freq;
        float epre = rhoeps[cnt].eps;
        float epim = (rho0-rhoeps[cnt].rho)/(rho0*rhoeps[cnt].rho*2.0*PI*rhoeps[cnt].freq*EPS0);
        if (effective) {
            epim += 1.0/(rho0*2.0*PI*epw*EPS0);
        }
        eps.push_back(ComplexPermittivity(epw, std::complex<float>(epre, epim)));
    }
    return eps;
}


std::vector<ComplexPermittivity> calcComplexEpsilonWithFitFunc(std::vector<ComplexPermittivity> eps, FitFunc func)
{
    for (unsigned cnt = 0; cnt < eps.size(); ++cnt) {
        float epim = func.m*pow(eps[cnt].w, func.p);
        float epre = func.epsinf - epim/tan(func.p*PI/2.0);
        eps[cnt].epsilon = std::complex<float>(epre, epim);
    }
    return eps;
}


std::vector<ComplexPermittivity> calcReferenceComplexEpsilon(std::vector<ComplexPermittivity> epsall, float minfreq, float maxfreq, unsigned Np)
{
    // calculate log frequencies and corresponding permittivity values
    std::vector<float> wi = calcLogFrequencies(minfreq, maxfreq, 4*Np, FreqType::hz, FreqType::rads);
    std::vector<ComplexPermittivity> eps;
    for (unsigned cnt = 0; cnt < wi.size(); ++cnt) {
        float diff = std::numeric_limits<float>::max();
        ComplexPermittivity tmpre;
        for (unsigned wall = 0; wall < epsall.size(); ++wall) {
            if (diff > fabs(epsall[wall].w - wi[cnt]/(2.0*PI))) {
                tmpre = epsall[wall];
                diff = fabs(epsall[wall].w - wi[cnt]/(2.0*PI));
            }
        }
        eps.push_back(tmpre);
    }
    return eps;
}


FitFunc calcFitFunc(std::vector<ComplexPermittivity> eps, float freqmin, float freqmax)
{
    FitFunc fit;
    fit.freqmin = freqmin;
    fit.freqmax = freqmax;
    float minfitcoef = 1.0e20;
    float minm = 0.0;
    float minp = -0.8;
    float mine = 1.0;
    float dm = 200.0;
    float dp = 0.0005;
    float de = 0.02;
    unsigned fmin = 0;
    unsigned fmax = eps.size()-1;
    for (; fmin < eps.size() && freqmin > eps[fmin].w; ++fmin);
    for (; fmax > 1 && freqmax < eps[fmax].w; --fmax);
    for (unsigned mc = 0; mc < 1000; ++mc) {
        for (unsigned pc = 0; pc < 1000; ++pc) {
            float m = minm+mc*dm;
            float p = minp+pc*dp;
            float fitcoef = calcFitCoef(eps, m, p, fmin, fmax);
            if (fitcoef < minfitcoef) {
                minfitcoef = fitcoef;
                fit.fitcoef = fitcoef;
                fit.m = m;
                fit.p = p;
            }
        }
    }
    minfitcoef = 1.0e20;
    for (unsigned me = 0; me < 5000; ++me) {
        float epsinf = mine+me*de;
        float fitcoef = calcEpsFitCoef(eps, fit.m, fit.p, epsinf, fmin, fmax);
        if (fitcoef < minfitcoef) {
            minfitcoef = fitcoef;
            fit.epsfitcoef = fitcoef;
            fit.epsinf = epsinf;
        }
    }
    return fit;
}


float calcFitCoef(std::vector<ComplexPermittivity> eps, float m, float p, unsigned fmin, unsigned fmax)
{
    float coef = 0.0;
    for (unsigned cnt = fmin; cnt < fmax; ++cnt) {
        float diff = m*pow(eps[cnt].w, p) - eps[cnt].epsilon.imag();
        coef += diff*diff;
    }
    return coef;
}


float calcEpsFitCoef(std::vector<ComplexPermittivity> eps, float m, float p, float epsinf, unsigned fmin, unsigned fmax)
{
    float coef = 0.0;
    for (unsigned cnt = fmin; cnt < fmax; ++cnt) {
        float diff = epsinf - m*pow(eps[cnt].w, p)/tan(p*PI/2.0) - eps[cnt].epsilon.real();
        coef += diff*diff;
    }
    return coef;
}


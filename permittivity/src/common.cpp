///
/// \file common.cpp
/// \brief Shared code for soil properties.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "common.h"
#include "constants.h"


std::vector<float> calcLogFrequencies(float lowestFreq, float highestFreq, unsigned fnum,
                                      FreqType inFreqType, FreqType outFreqType)
{
    if (inFreqType == FreqType::rads) {
        lowestFreq /= 2.0*PI;
        highestFreq /= 2.0*PI;
    }
    std::vector<float> wi;
    float low = log10(lowestFreq);
    float high = log10(highestFreq);
    if (fnum == 0) {
        fnum = static_cast<unsigned>(1.0 + high/low);
    }
    float spacing = (high-low) / static_cast<float>(fnum-1);
    for (unsigned cnt = 0; cnt < fnum; ++cnt) {
        float freq = pow(10.0, low);
        if (outFreqType == FreqType::rads) {
            freq *= 2.0*PI;
        }
        wi.push_back(freq);
        low += spacing;
    }
    return wi;
}


///
/// \file debyepade.h
/// \brief See "Consistent FDTD Modeling of Dispersive Dielectric Media
/// via Multiple Debye Terms Derived Rigorously by Pade Approximants" by Ioannis T. Rekanos.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef DEBYEPADE_H__
#define DEBYEPADE_H__


#include "polynomial.h"
#include "matrixgsl.h"
#include <cmath>
#include <complex>


struct EpsTauDebyeParameters;


EpsTauDebyeParameters calcDebyeParameters(unsigned M, double deps, double tau, double a, double b, double x = 100.0);
long double calcWk(long unsigned k, long double x, long double a, long double b);
long double getNthDerivativeHnType(long unsigned n, long double xin, long double a, long double b);
long double risingFactorial(long double x, long double n);
long unsigned factorial(long unsigned x);


struct EpsTauDebyeParameters {
    EpsTauDebyeParameters() = default;
    EpsTauDebyeParameters(std::vector<float> epsin, std::vector<float> tauin)
        : eps{epsin}, tau{tauin} {}
    ~EpsTauDebyeParameters() {}
    std::vector<float> eps{};
    std::vector<float> tau{};
};


#endif    // DEBYEPADE_H__


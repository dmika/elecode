///
/// \file common.h
/// \brief Shared code for soil properties.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef COMMON_H__
#define COMMON_H__


#include <complex>
#include <vector>


enum class FreqType;
struct ComplexPermittivity;


enum class FreqType {
    hz,
    rads
};


std::vector<float> calcLogFrequencies(float lowestFreq, float highestFreq, unsigned fnum = 0,
                                      FreqType inFreqType = FreqType::hz, FreqType outFreqType = FreqType::hz);


struct ComplexPermittivity {
    ComplexPermittivity() = default;
    ComplexPermittivity(float win, std::complex<float> eps) : w{win}, epsilon{eps} {}
    float w{0.0};
    std::complex<float> epsilon{1.0, 0.0};
};


#endif    // COMMON_H__


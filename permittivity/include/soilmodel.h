///
/// \file model.h
/// \brief Models for soil properties.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef SOILMODEL_H__
#define SOILMODEL_H__


#include "common.h"
#include "constants.h"
#include <cmath>


// R.Alipio, S.Visacro, "Modeling the frequency dependence of electrical parameters of soil", 2014.
std::vector<ComplexPermittivity> modelAlipioVisacro(float rho0, float minfreq, float maxfreq, unsigned fnum);
std::vector<ComplexPermittivity> modelPortela(float rho0, float minfreq, float maxfreq, unsigned fnum);
std::vector<ComplexPermittivity> modelMessier(float rho0, float minfreq, float maxfreq, unsigned fnum);


#endif    // SOILMODEL_H__


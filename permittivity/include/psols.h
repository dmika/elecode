///
/// \file psols.h
/// \brief See "Debye Function Expansions of Complex Permittivity Using a Hybrid Particle Swarm-Least Squares
/// Optimization Approach" by David F. Kelley, Timothy J. Destan, Raymond J. Luebbers.
/// Not all features of the algorithm were implemented (if there are cases
/// for which those features are necessary, please let me know).
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef PSOLS_H__
#define PSOLS_H__


#include "common.h"
#include "matrix.h"
#include <random>
#include <memory>


class SwarmPsols;
class ParticlePsols;
struct SampledPermittivity;
struct DebyeParameters;


struct SampledPermittivity {
    SampledPermittivity() = default;
    SampledPermittivity(std::vector<ComplexPermittivity> sam, FreqType fr = FreqType::hz) : samples{sam}, freq{fr} {}
    std::vector<ComplexPermittivity> samples{};
    FreqType freq{FreqType::hz};
};


class SwarmPsols {
public:
    SwarmPsols(SampledPermittivity eps, unsigned dimensNum, unsigned particlesNum = 30);
    SwarmPsols(SampledPermittivity eps);

    void makeStep(void);
    void makeIterations(unsigned iterationsNum);
    DebyeParameters calcParametersWithRepeats(unsigned repeats = 100, unsigned iterationsNum = 1000);
    DebyeParameters getDebyeParameters(void);

    SampledPermittivity getReferencePermittivity(void) const { return mReferencePermittivity; }
    unsigned getDimensNum(void) const { return mDimensNum; }
    float getBestFitness(void) const { return gBestFitness; }
    std::vector<float> getBest(void) const { return gBest; }
    float getPermittivityInf(void) const { return mPermittivityInf; }
    float getPermittivityS(void) const { return mPermittivityS; }
    float getMaxVeolcity(void) const { return mMaxVeolcity; }
    std::shared_ptr<std::mt19937> getGen(void) const { return mGen; }
    void setBestFitness(float gbf) { gBestFitness = gbf; }
    void setBest(std::vector<float> gb) { gBest = gb; }
    void setPermittivityInf(float epsInf) { mPermittivityInf = epsInf; }
    void setPermittivityS(float epsZero) { mPermittivityS = epsZero; }
    void setMaxVeolcity(float maxVel) { mMaxVeolcity = maxVel; }

protected:
    void init(unsigned particlesNum);
    SampledPermittivity mReferencePermittivity{};
    float mPermittivityInf{1.0};
    float mPermittivityS{2.0};
    float mMaxVeolcity{};
    unsigned mDimensNum{};
    std::vector<ParticlePsols> mParticles{};
    std::vector<float> gBest{};
    float gBestFitness{std::numeric_limits<float>::max()};
    std::shared_ptr<std::mt19937> mGen{nullptr};
};


class ParticlePsols {
public:
    ParticlePsols(SwarmPsols *swarm);
    ~ParticlePsols() {}
    ParticlePsols(const ParticlePsols&) = default;
    ParticlePsols& operator=(const ParticlePsols&) = default;

    void makeStep(void);
    std::vector<float> getWeight(void) const { return ma; }
    std::vector<float> getTau(void) const { std::vector<float> tau = mx; for (auto &t : tau) t = 1.0/t; return tau; }
    float getDe(void) const;
    float getFitness(void) const { return pBestFitness; }

protected:
    float evaluateFitness(void);
    void updateVelocity(void);
    void updatePosition(void);
    SwarmPsols *mSwarm{nullptr};
    std::vector<float> pBest{};
    float pBestFitness{std::numeric_limits<float>::max()};
    std::vector<float> mx{};
    std::vector<float> mv{};
    std::vector<float> mwi{};
    std::vector<float> ma{};
    std::vector<std::vector<float>> mD{};
    std::vector<float> mc{};
    float mVmax{};
    float mc1{1.4};
    float mc2{1.4};
    float mw{0.5};
    float mGamma{0.0};
    std::vector<std::vector<float>> mI{};
};


struct DebyeParameters {
    DebyeParameters() = default;
    DebyeParameters(std::vector<float> epsin, std::vector<float> tauin, float epsinfin, float fitin)
        : eps{epsin}, tau{tauin}, epsinf{epsinfin}, fit{fitin} {}
    ~DebyeParameters() {}
    std::vector<float> eps{};
    std::vector<float> tau{};
    float epsinf{1.0};
    float fit{std::numeric_limits<float>::max()};
};


#endif    // PSOLS_H__


///
/// \file doi.h
/// \brief Depth of investigation calculation. See "Depth of investigation in direct current methods" by A. Roy and A. Apparao;
/// "Depth of investigation of collinear symmetrical four‐electrode arrays" by R. Barker.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef DOI_H__
#define DOI_H__


#include <cmath>
#include "vectors.h"

/// The function calculates depth of investigation for the dipole-dipole array
/// (median value of the depth investigation characteristic is used)
/// cur1 and cur2 are coordinates of the first and second current electrode
/// pot1 and pot2 are coordinates of the first and second potential electrode
double DoiMedianDipole(XyPointDouble cur1, XyPointDouble cur2, XyPointDouble pot1, XyPointDouble pot2, double delta = 1.0e-7);
double DicDipole(XyPointDouble cur1, XyPointDouble cur2, XyPointDouble pot1, XyPointDouble pot2, double z);


#endif    // DOI_H__


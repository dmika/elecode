///
/// \file properties.h
/// \brief Calculation of the soil properties (resistivity, permittivity).
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef PROPERTIES_H__
#define PROPERTIES_H__


#include <cmath>
#include "fft.h"
#include "constants.h"
#include "psols.h"


struct RhoEpsilon {
    float freq = 0.0;
    float rho = 0.0;
    float eps = 0.0;
};


struct CurrentVoltage {
    float dt;
    std::vector<float> cur;
    std::vector<float> vol;
};


// f = m*x^p
struct FitFunc {
    float m = 0.0;
    float p = 0.0;
    float epsinf = 0.0;
    float freqmin = 0.0;
    float freqmax = 0.0;
    float fitcoef = 0.0;
    float epsfitcoef = 0.0;
};


std::vector<RhoEpsilon> calcRhoEpsilon(CurrentVoltage curvol, float arraycoef = 1.0);
std::vector<RhoEpsilon> calcRhoEpsilon(float rho0, std::vector<ComplexPermittivity> eps);
std::vector<ComplexPermittivity> calcComplexEpsilon(float rho0, std::vector<RhoEpsilon> rhoeps, bool effective = false);
std::vector<ComplexPermittivity> calcComplexEpsilonWithFitFunc(std::vector<ComplexPermittivity> eps, FitFunc func);
std::vector<ComplexPermittivity> calcReferenceComplexEpsilon(std::vector<ComplexPermittivity> epsall, float minfreq, float maxfreq, unsigned Np);
FitFunc calcFitFunc(std::vector<ComplexPermittivity> eps, float freqmin, float freqmax);
float calcFitCoef(std::vector<ComplexPermittivity> eps, float m, float p, unsigned fmin, unsigned fmax);
float calcEpsFitCoef(std::vector<ComplexPermittivity> eps, float m, float p, float epsinf, unsigned fmin, unsigned fmax);


#endif    // PROPERTIES_H__


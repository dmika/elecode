///
/// \file apparent.h
/// \brief Apparent resistivity calculation. See "Analysis of apparent resistivity in a multi-layer earth structure" by T. Takahashi and T. Kawase.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2021, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef APPARENT_H__
#define APPARENT_H__


#include "integration.h"
#include "constants.h"
#include "common.h"


struct SoilLayer {
    SoilLayer() = default;
    SoilLayer(float rhoin, float hin) : rho{rhoin}, h{hin} {}
    float rho;
    float h;
};


class Apparent {
public:
    Apparent(std::vector<SoilLayer> layers) : mLayers{layers} {}

    void setLayers(std::vector<SoilLayer> layers) { mLayers = layers; }
    float wenner(float a);
    float dipole(float a, float b);

protected:
    float calcPerpDipoleArrayCoef(float dipolesize, float dist);
    float getV(float x);
    float FN(float x);
    float K(float m, float n, float lambda, float h);
    std::vector<SoilLayer> mLayers;
};


#endif    // APPARENT_H__


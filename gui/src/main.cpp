///
/// \file main.cpp
/// \brief Main file.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "cli.h"
#include "mainframe.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QSurfaceFormat>


int main(int argc, char *argv[])
{
    if (argc > 1) {
        interpretCommand(argc, argv);
    }
    else {
        Q_INIT_RESOURCE(elecode);
    
        QApplication app(argc, argv);
    
        QSurfaceFormat format;
        format.setDepthBufferSize(24);
        //format.setSamples(4);
        format.setVersion(3, 3);
        format.setProfile(QSurfaceFormat::CoreProfile);
        QSurfaceFormat::setDefaultFormat(format);
    
        MainWindow mainWindow;
        
        return app.exec();
    }
    
    return 0;
}


///
/// \file modelview.cpp
/// \brief 3D model.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "mainframe.h"
#include "modelview.h"
#include "vectors.h"
#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QCoreApplication>
#include <QMenu>
#include <cmath>
#include <numbers>


const int SHADER_ID = 1;


void OpenGlBuffer::prepareVertex(QOpenGLBuffer &vbo, QVector<GLfloat> &data)
{
    vbo.create();
    vbo.bind();
    vbo.allocate(data.constData(), data.size()*sizeof(GLfloat));
    vbo.bind();
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glEnableVertexAttribArray(0);
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), 0);
    f->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), reinterpret_cast<void*>(3*sizeof(GLfloat)));
    vbo.release();
}

void OpenGlBuffer::prepareForDrawing(QOpenGLBuffer &vbo, ModelView &modelView, double *color, int size, int stride, int num)
{
    float fcolor[3] = {static_cast<float>(color[0]), static_cast<float>(color[1]), static_cast<float>(color[2])};
    modelView.glUseProgram(SHADER_ID);
    modelView.glUniform3fv(modelView.glGetUniformLocation(SHADER_ID, "setcolor"), 1, fcolor);
    vbo.bind();
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glVertexAttribPointer(0, size, GL_FLOAT, GL_FALSE, stride*sizeof(GLfloat), 0);
    if (num == 2) {
        f->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), reinterpret_cast<void*>(3*sizeof(GLfloat)));
    }
}


void SelectionBuffer::draw(ModelView &modelView)
{
    Xy<float> size(modelView.width(), modelView.height());
    Xy<float> rotpos(modelView.mCurRotPos.x(), modelView.mCurRotPos.y());
    Xy<float> pos(modelView.mCurPos.x(), modelView.mCurPos.y());

    QVector<GLfloat> vertices{rotpos.x*2.0f/size.x-1.0f, (size.y-pos.y)*2.0f/size.y-1.0f, pos.x*2.0f/size.x-1.0f, (size.y-pos.y)*2.0f/size.y-1.0f,
                              rotpos.x*2.0f/size.x-1.0f, (size.y-rotpos.y)*2.0f/size.y-1.0f, rotpos.x*2.0f/size.x-1.0f, (size.y-pos.y)*2.0f/size.y-1.0f,
                              pos.x*2.0f/size.x-1.0f, (size.y-rotpos.y)*2.0f/size.y-1.0f, pos.x*2.0f/size.x-1.0f, (size.y-pos.y)*2.0f/size.y-1.0f,
                              rotpos.x*2.0f/size.x-1.0f, (size.y-rotpos.y)*2.0f/size.y-1.0f, pos.x*2.0f/size.x-1.0f, (size.y-rotpos.y)*2.0f/size.y-1.0f};
    mVbo.resize(1);
    prepareVertex(mVbo[0], vertices);

    double color[3] = {0.6, 0.6, 0.6};
    prepareForDrawing(mVbo[0], modelView, color, 2, 2, 1);
    modelView.glDrawArrays(GL_LINES, 0, 8);
}


void NodeLineBuffer::draw(ModelView &modelView)
{
    QVector<GLfloat> vertices{modelView.mCurSelectedWireEnd.x()*2.0f/modelView.width() - 1.0f,
                              modelView.mCurSelectedWireEnd.y()*2.0f/modelView.height() - 1.0f,
                              modelView.mCurMousePos.x()*2.0f/modelView.width() - 1.0f,
                              (modelView.height()-modelView.mCurMousePos.y())*2.0f/modelView.height() - 1.0f};
    mVbo.resize(1);
    prepareVertex(mVbo[0], vertices);

    double color[3] = {0.6, 0.6, 0.6};
    prepareForDrawing(mVbo[0], modelView, color, 2, 2, 1);
    modelView.glDrawArrays(GL_LINES, 0, 2);
}


NoBuffer::NoBuffer(unsigned position)
{
    mType = ObjectType::object;
    mPositionInFile = position;
}


BlockBuffer::BlockBuffer(unsigned position, Vector first, Vector second, bool frameOnly)
    : mFirst{first}, mSecond{second}, mFrameOnly{frameOnly}
{
    mType = ObjectType::block;
    mPositionInFile = position;
    int count = 0;
    mVbo.resize(2);
    QVector3D n = QVector3D::normal(QVector3D(0.0f, 0.0f, 0.0f), QVector3D(0.0f, 0.0f, mSecond.z));
    mVertexCount = 24;

    QVector<GLfloat> data;
    data.resize(mVertexCount*6);
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mSecond.z), &n);

    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mFirst.z), &n);

    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mFirst.z), &n);

    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mSecond.z), &n);

    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mSecond.z), &n);

    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mFirst.z), &n);
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mSecond.z), &n);
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mFirst.z), &n);

    prepareVertex(mVbo[0], data);


    data.resize(mVertexCount*3);
    count = 0;
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mSecond.z));
    
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mSecond.z));
    
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mFirst.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mFirst.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mFirst.x, mSecond.y, mSecond.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mFirst.z));
    addVertexToBuffer(data, count, QVector3D(mSecond.x, mSecond.y, mSecond.z));

    prepareVertex(mVbo[1], data);
}


void BlockBuffer::draw(ModelView &modelView)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if (!mFrameOnly) {
        double color[3];
        modelView.mMainWindow->getModelViewSettings().mBlock.getRgbF(color, color+1, color+2);
        if (modelView.mMainWindow->isObjectSelected(mPositionInFile)) {
            modelView.mMainWindow->getModelViewSettings().mChosenObject.getRgbF(color, color+1, color+2);
        }
        prepareForDrawing(mVbo[0], modelView, color, 3, 6);
        modelView.glDrawArrays(GL_QUADS, 0, mVertexCount);
    }

    glDisable(GL_BLEND);
    double color[3] = {0.0, 0.0, 0.0};
    prepareForDrawing(mVbo[1], modelView, color, 3, 3, 1);
    modelView.glDrawArrays(GL_LINES, 0, mVertexCount);
}


BaseWireBuffer::BaseWireBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision)
    : mStart{start}, mEnd{end}, mDiameter{diam}
{
    mPositionInFile = position;
    mVbo.resize(1);
    addCylinder(mVbo[0], precision, mStart, mEnd, mDiameter);
}


void BaseWireBuffer::addCylinder(QOpenGLBuffer &vbo, unsigned sectors, Vector start, Vector end, float diam)
{
    using namespace std::numbers;
    mVertexCount = sectors*18;
    QVector<GLfloat> data(mVertexCount*6);
    int count = 0;
    float length = getDistanceBetweenPoints(start, end);

    for (unsigned i = 0; i < sectors; ++i) {
        float angle = (i*2.0*pi) / sectors;
        float x1 = diam*0.5*sin(angle);
        float y1 = diam*0.5*cos(angle);
        angle = ((i+1)*2.0*pi) / sectors;
        float x2 = diam*0.5*sin(angle);
        float y2 = diam*0.5*cos(angle);
        addSectorToCylinderData(data, count, x1, y1, x2, y2, length);
    }

    Vector toOrigin = end - start;
    float xy = sqrt(toOrigin.x*toOrigin.x + toOrigin.y*toOrigin.y);
    float psi = pi/2.0 - acos(toOrigin.z/length);
    float phi;
    if (xy == 0.0 || toOrigin.x/xy >= 1.0) {
        phi = 0.0;
    }
    else if (toOrigin.x/xy <= -1.0 ) {
        phi = pi;
    }
    else {
        phi = acos(toOrigin.x/xy);
        if (start.y > end.y) {
            phi = -phi;
        }
    }

    for (int cnt = 0; cnt < count; cnt+=3) {
        Vector tempVec = rotateVectorWithEulerAngles(Vector(data[cnt], data[cnt+1], data[cnt+2]), pi/2.0, psi, -phi);
        data[cnt] = tempVec.x;
        data[cnt+1] = tempVec.y;
        data[cnt+2] = tempVec.z;
    }

    for (int cnt = 0; cnt < count; cnt+=6) {
        data[cnt] += start.x;
        data[cnt+1] += start.y;
        data[cnt+2] += start.z;
    }

    prepareVertex(vbo, data);
}


void BaseWireBuffer::draw(ModelView &modelView)
{
    if (modelView.mMainWindow->isObjectSelected(mPositionInFile)) {
        modelView.mMainWindow->getModelViewSettings().mChosenObject.getRgbF(mColor, mColor+1, mColor+2);
    }
    glDisable(GL_BLEND);
    for (unsigned cnt = 0; cnt < mVbo.size(); ++cnt) {
        prepareForDrawing(mVbo[cnt], modelView, mColor, 3, 6);
        modelView.glDrawArrays(GL_TRIANGLES, 0, mVertexCount);
    }
}


ObliqueWireBuffer::ObliqueWireBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision)
    : BaseWireBuffer(position, start, end, diam, precision)
{
    mType = ObjectType::obliqueThinWire;
    mVboSphere.push_back(QOpenGLBuffer());
    mVboSphere.push_back(QOpenGLBuffer());
    addSphere(mVboSphere[0], mDiameter, precision/3, precision/3, mStart);
    addSphere(mVboSphere[1], mDiameter, precision/3, precision/3, mEnd);
    addSphereIndices(precision/3, precision/3);
}


void ObliqueWireBuffer::addSphere(QOpenGLBuffer &vbo, float radius, unsigned sectors, unsigned rings, Vector center)
{
    using namespace std::numbers;
    QVector<GLfloat> data((rings+1)*(sectors+1)*6);
    int count = 0;
    for (unsigned i = 0; i <= rings; ++i) {
        float u = pi/2.0 - i*pi/rings;
        float cosu = cos(u);
        float z = sin(u);
        for (unsigned j = 0; j <= sectors; ++j) {
            float v = j*2.0*pi/sectors;
            float x = cosu * cos(v);
            float y = cosu * sin(v);
            QVector3D n(x, y, z);
            addVertexToBuffer(data, count, QVector3D(x*radius+center.x, y*radius+center.y, z*radius+center.z), &n);
        }
    }
    prepareVertex(vbo, data);
}


void ObliqueWireBuffer::addSphereIndices(unsigned sectors, unsigned rings)
{
    mSphereIdx.clear();
    for (unsigned i = 0; i < rings; ++i) {
        unsigned start = i*(sectors+1);
        for (unsigned j = start; j < start+sectors; ++j) {
            if (i > 0) {
                mSphereIdx.push_back(j);
                mSphereIdx.push_back(sectors+j+1);
                mSphereIdx.push_back(j+1);
            }
            if (i < rings-1) {
                mSphereIdx.push_back(j+1);
                mSphereIdx.push_back(sectors+j+1);
                mSphereIdx.push_back(sectors+j+2);
            }
        }
    }
}


void ObliqueWireBuffer::draw(ModelView &modelView)
{
    modelView.mMainWindow->getModelViewSettings().mObliqueWire.getRgbF(mColor, mColor+1, mColor+2);
    BaseWireBuffer::draw(modelView);

    std::vector<double> color = {0.6, 0.6, 0.6};
    if (modelView.mMainWindow->isObjectSelected(mPositionInFile) && modelView.mMainWindow->getTipOfSelectedObliqueWire() == WirePointTypeDraw::wireStart) {
        color = {0.9, 0.6, 0.6};
    }
    prepareForDrawing(mVboSphere[0], modelView, color.data(), 3, 6);
    modelView.glDrawElements(GL_TRIANGLES, mSphereIdx.size(), GL_UNSIGNED_SHORT, mSphereIdx.constData());

    if (modelView.mMainWindow->isObjectSelected(mPositionInFile) && modelView.mMainWindow->getTipOfSelectedObliqueWire() == WirePointTypeDraw::wireEnd) {
        color = {0.9, 0.6, 0.6};
    }
    else {
        color = {0.6, 0.6, 0.6};
    }
    prepareForDrawing(mVboSphere[1], modelView, color.data(), 3, 6);
    modelView.glDrawElements(GL_TRIANGLES, mSphereIdx.size(), GL_UNSIGNED_SHORT, mSphereIdx.constData());
}


ThinWireBuffer::ThinWireBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision)
    : BaseWireBuffer(position, start, end, diam, precision)
{
    mType = ObjectType::thinWire;
}


void ThinWireBuffer::draw(ModelView &modelView)
{
    modelView.mMainWindow->getModelViewSettings().mThinWire.getRgbF(mColor, mColor+1, mColor+2);
    BaseWireBuffer::draw(modelView);
}


StaircaseWireBuffer::StaircaseWireBuffer(unsigned position, Vector start, Vector end, float diam, float cellSize, unsigned precision)
{
    mStart = start;
    mEnd = end;
    mDiameter = diam;
    mType = ObjectType::staircaseWire;
    mPositionInFile = position;
    
    Xyz<unsigned> firstPoint(start.x/cellSize + 0.5, start.y/cellSize + 0.5, start.z/cellSize + 0.5);
    Xyz<unsigned> secondPoint(end.x/cellSize + 0.5, end.y/cellSize + 0.5, end.z/cellSize + 0.5);
    Xyz<unsigned> runningPoint = firstPoint;
    while ( ( secondPoint.x >= firstPoint.x ? runningPoint.x <= secondPoint.x : runningPoint.x >= secondPoint.x ) &&
            ( secondPoint.y >= firstPoint.y ? runningPoint.y <= secondPoint.y : runningPoint.y >= secondPoint.y ) &&
            ( secondPoint.z >= firstPoint.z ? runningPoint.z <= secondPoint.z : runningPoint.z >= secondPoint.z ) ) {
        if (runningPoint.x == secondPoint.x && runningPoint.y == secondPoint.y && runningPoint.z == secondPoint.z) {
            break;
        }

        float dist_x, dist_y, dist_z;
        Vector first(firstPoint.x, firstPoint.y, firstPoint.z);
        Vector second(secondPoint.x, secondPoint.y, secondPoint.z);
        if (secondPoint.x >= firstPoint.x) {
            dist_x = getDistanceFromPointToLine(Vector(runningPoint.x+1, runningPoint.y, runningPoint.z), first, second);
        }
        else {
            dist_x = getDistanceFromPointToLine(Vector(runningPoint.x-1, runningPoint.y, runningPoint.z), first, second);
        }
        if (secondPoint.y >= firstPoint.y) {
            dist_y = getDistanceFromPointToLine(Vector(runningPoint.x, runningPoint.y+1, runningPoint.z), first, second);
        }
        else {
            dist_y = getDistanceFromPointToLine(Vector(runningPoint.x, runningPoint.y-1, runningPoint.z), first, second);
        }
        if (secondPoint.z >= firstPoint.z) {
            dist_z = getDistanceFromPointToLine(Vector(runningPoint.x, runningPoint.y, runningPoint.z+1), first, second);
        }
        else {
            dist_z = getDistanceFromPointToLine(Vector(runningPoint.x, runningPoint.y, runningPoint.z-1), first, second);
        }

        if (dist_x <= dist_y && dist_x <= dist_z ) {
            if (secondPoint.x >= firstPoint.x) {
                mVbo.push_back(QOpenGLBuffer());
                addCylinder(mVbo.back(), precision, Vector(runningPoint.x*cellSize, runningPoint.y*cellSize, runningPoint.z*cellSize),
                            Vector((runningPoint.x+1)*cellSize, runningPoint.y*cellSize, runningPoint.z*cellSize), mDiameter);
                runningPoint.x += 1;
            }
            else {
                runningPoint.x -= 1;
                mVbo.push_back(QOpenGLBuffer());
                addCylinder(mVbo.back(), precision, Vector(runningPoint.x*cellSize, runningPoint.y*cellSize, runningPoint.z*cellSize),
                            Vector((runningPoint.x+1)*cellSize, runningPoint.y*cellSize, runningPoint.z*cellSize), mDiameter);
            }
        }
        else if (dist_y <= dist_x && dist_y <= dist_z) {
            if (secondPoint.y >= firstPoint.y) {
                mVbo.push_back(QOpenGLBuffer());
                addCylinder(mVbo.back(), precision, Vector(runningPoint.x*cellSize, runningPoint.y*cellSize, runningPoint.z*cellSize),
                            Vector(runningPoint.x*cellSize, (runningPoint.y+1)*cellSize, runningPoint.z*cellSize), mDiameter);
                runningPoint.y += 1;
            }
            else {
                runningPoint.y -= 1;
                mVbo.push_back(QOpenGLBuffer());
                addCylinder(mVbo.back(), precision, Vector(runningPoint.x*cellSize, runningPoint.y*cellSize, runningPoint.z*cellSize),
                            Vector(runningPoint.x*cellSize, (runningPoint.y+1)*cellSize, runningPoint.z*cellSize), mDiameter);
            }
        }
        else if (dist_z <= dist_x && dist_z <= dist_y) {
            if (secondPoint.z >= firstPoint.z) {
                mVbo.push_back(QOpenGLBuffer());
                addCylinder(mVbo.back(), precision, Vector(runningPoint.x*cellSize, runningPoint.y*cellSize, runningPoint.z*cellSize),
                            Vector(runningPoint.x*cellSize, runningPoint.y*cellSize, (runningPoint.z+1)*cellSize), mDiameter);
                runningPoint.z += 1;
            }
            else {
                runningPoint.z -= 1;
                mVbo.push_back(QOpenGLBuffer());
                addCylinder(mVbo.back(), precision, Vector(runningPoint.x*cellSize, runningPoint.y*cellSize, runningPoint.z*cellSize),
                            Vector(runningPoint.x*cellSize, runningPoint.y*cellSize, (runningPoint.z+1)*cellSize), mDiameter);
            }
        }
    }
}


void StaircaseWireBuffer::draw(ModelView &modelView)
{
    modelView.mMainWindow->getModelViewSettings().mStaircaseWire.getRgbF(mColor, mColor+1, mColor+2);
    BaseWireBuffer::draw(modelView);
}


SourceObjectBuffer::SourceObjectBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision)
    : BaseWireBuffer(position, start, end, diam, precision)
{
    mType = ObjectType::source;
}


void SourceObjectBuffer::draw(ModelView &modelView)
{
    modelView.mMainWindow->getModelViewSettings().mCurrentSource.getRgbF(mColor, mColor+1, mColor+2);
    BaseWireBuffer::draw(modelView);
}


CalculateObjectBuffer::CalculateObjectBuffer(unsigned position, std::vector<XyzPointFloat> coord, float diam, std::string calculateType, unsigned precision)
{
    mCoordinates = coord;
    mDiameter = diam;
    mType = ObjectType::calculate;
    mPositionInFile = position;
    mCalculateType = calculateType;
    if (mCoordinates.size() > 0) {
        mVbo.resize(mCoordinates.size()-1);
        for (unsigned cnt = 0; cnt < mCoordinates.size()-1; ++cnt) {
            addCylinder(mVbo[cnt], precision, mCoordinates[cnt], mCoordinates[cnt+1], mDiameter);
        }
    }
}


void CalculateObjectBuffer::draw(ModelView &modelView)
{
    if (mCalculateType == "current") {
        modelView.mMainWindow->getModelViewSettings().mCurrentCalc.getRgbF(mColor, mColor+1, mColor+2);
    }
    else if (mCalculateType == "voltage") {
        modelView.mMainWindow->getModelViewSettings().mVoltagePathCalc.getRgbF(mColor, mColor+1, mColor+2);
    }
    BaseWireBuffer::draw(modelView);
}


ResistorBuffer::ResistorBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision)
    : BaseWireBuffer(position, start, end, diam, precision)
{
    mType = ObjectType::resistor;
}


void ResistorBuffer::draw(ModelView &modelView)
{
    modelView.mMainWindow->getModelViewSettings().mResistor.getRgbF(mColor, mColor+1, mColor+2);
    BaseWireBuffer::draw(modelView);
}


CapacitorBuffer::CapacitorBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision)
    : BaseWireBuffer(position, start, end, diam, precision)
{
    mType = ObjectType::capacitor;
}


void CapacitorBuffer::draw(ModelView &modelView)
{
    modelView.mMainWindow->getModelViewSettings().mResistor.getRgbF(mColor, mColor+1, mColor+2);
    BaseWireBuffer::draw(modelView);
}


ModelView::ModelView(MainWindow *parent, std::vector<std::shared_ptr<ModelObject>> &objects)
    : QOpenGLWidget(parent), mProgram(0), mObjects(objects)
{
    mMainWindow = parent;
    setMouseTracking(true);
    QSurfaceFormat format;
    format.setSamples(2);
    setFormat(format);
}


ModelView::~ModelView()
{
    makeCurrent();
    delete mProgram;
    mProgram = nullptr;
    doneCurrent();
}


QSize ModelView::minimumSizeHint() const
{
    return QSize(50, 50);
}


QSize ModelView::sizeHint() const
{
    return QSize(400, 400);
}


void ModelView::refreshBuffers(void)
{
    mBuffers.clear();
    for (const auto &object : mObjects) {
        switch (object->getObjectType()) {
        case ObjectType::obliqueThinWire:
        {
            ObliqueThinWire *wire = static_cast<ObliqueThinWire*>(object.get());
            float diameter = wire->getDiameter();
            adjustDiameter(diameter);
            mBuffers.push_back(std::make_shared<ObliqueWireBuffer>(wire->getPositionInFile(), wire->getStartPoint(), wire->getEndPoint(),
                                                                   diameter, mMainWindow->getModelViewSettings().mPrecision));
            break;
        }
        case ObjectType::thinWire:
        {
            ThinWire *wire = static_cast<ThinWire*>(object.get());
            float diameter = wire->getDiameter();
            adjustDiameter(diameter);
            mBuffers.push_back(std::make_shared<ThinWireBuffer>(wire->getPositionInFile(), wire->getStartPoint(), wire->getEndPoint(),
                                                                diameter, mMainWindow->getModelViewSettings().mPrecision));
            break;
        }
        case ObjectType::staircaseWire:
        {
            StaircaseWire *wire = static_cast<StaircaseWire*>(object.get());
            float diameter = mCellSize/8.0;
            adjustDiameter(diameter);
            mBuffers.push_back(std::make_shared<StaircaseWireBuffer>(wire->getPositionInFile(), wire->getStartPoint(), wire->getEndPoint(),
                                                                     diameter, mCellSize, mMainWindow->getModelViewSettings().mPrecision));
            break;
        }
        case ObjectType::source:
        {
            SourceObject *src = static_cast<SourceObject*>(object.get());
            float diameter = mCellSize/8.0;
            adjustDiameter(diameter);
            mBuffers.push_back(std::make_shared<SourceObjectBuffer>(src->getPositionInFile(), src->getStartPoint(), src->getEndPoint(),
                                                                    diameter, mMainWindow->getModelViewSettings().mPrecision));
            break;
        }
        case ObjectType::calculate:
        {
            CalculateObject *calc = static_cast<CalculateObject*>(object.get());
            float diameter = mCellSize/8.0;
            adjustDiameter(diameter);
            std::vector<XyzPointFloat> coord = calc->getCoordinates();
            mBuffers.push_back(std::make_shared<CalculateObjectBuffer>(calc->getPositionInFile(), coord, diameter, "voltage",
                                                                       mMainWindow->getModelViewSettings().mPrecision));
            break;
        }
        case ObjectType::resistor:
        {
            Resistor *res = static_cast<Resistor*>(object.get());
            float diameter = mCellSize/8.0;
            adjustDiameter(diameter);
            mBuffers.push_back(std::make_shared<ResistorBuffer>(res->getPositionInFile(), res->getStartPoint(), res->getEndPoint(),
                                                                diameter, mMainWindow->getModelViewSettings().mPrecision));
            break;
        }
        case ObjectType::capacitor:
        {
            Capacitor *cap = static_cast<Capacitor*>(object.get());
            float diameter = mCellSize/8.0;
            adjustDiameter(diameter);
            mBuffers.push_back(std::make_shared<CapacitorBuffer>(cap->getPositionInFile(), cap->getStartPoint(), cap->getEndPoint(),
                                                                 diameter, mMainWindow->getModelViewSettings().mPrecision));
            break;
        }
        case ObjectType::volume:
        {
            VolumeObject *volume = static_cast<VolumeObject*>(object.get());
            mBuffers.push_back(std::make_shared<BlockBuffer>(volume->getPositionInFile(), Vector(0.0, 0.0, 0.0), volume->getVolumeSize(), true));
            break;
        }
        case ObjectType::block:
        {
            Block *block = static_cast<Block*>(object.get());
            mBuffers.push_back(std::make_shared<BlockBuffer>(block->getPositionInFile(), block->getStartPoint(), block->getEndPoint()));
            break;
        }
        default:
            mBuffers.push_back(std::make_shared<NoBuffer>(object->getPositionInFile()));
            break;
        }
    }
}


void ModelView::refreshBuffer(int item)
{
    mBuffers.erase(mBuffers.begin() + item);

    switch (mObjects[item]->getObjectType()) {
    case ObjectType::obliqueThinWire:
    {
        ObliqueThinWire *wire = static_cast<ObliqueThinWire*>(mObjects[item].get());
        float diameter = wire->getDiameter();
        adjustDiameter(diameter);
        mBuffers.insert(mBuffers.begin() + item, std::make_shared<ObliqueWireBuffer>(wire->getPositionInFile(), wire->getStartPoint(), wire->getEndPoint(),
                                                                                     diameter, mMainWindow->getModelViewSettings().mPrecision));
        break;
    }
    default:
        mBuffers.insert(mBuffers.begin() + item, std::make_shared<NoBuffer>(mObjects[item]->getPositionInFile()));
        break;
    }
}


void ModelView::resetView(void)
{
    mDefaultPosition = true;
    mRotPos.setX(0);
    mRotPosSaved.setX(0);
    mRotPos.setY(0);
    mRotPosSaved.setY(0);
    mOriginPos.x = mOriginPos.y = mOriginPos.z = 0.0;
    mOriginPosSaved.x = mOriginPosSaved.y = mOriginPosSaved.z = 0.0;
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::volume) {
            XyzSizeFloat volSize = static_cast<VolumeObject*>(object.get())->getVolumeSize();
            mCellSize = static_cast<VolumeObject*>(object.get())->getCellSize();
            mOriginPos.x = mOriginPosSaved.x = - static_cast<float>(volSize.x) / 2.0;
            mOriginPos.y = mOriginPosSaved.y = - static_cast<float>(volSize.y) / 2.0;
            mOriginPos.z = mOriginPosSaved.z = - static_cast<float>(volSize.z) / 2.0;
            break;
        }
    }
    mScroll = -200.0 * mCellSize;
    mMainWindow->clearSelectedObjects();

    mProj.setToIdentity();
    mProj.perspective(mMainWindow->getModelViewSettings().mPerspective, static_cast<GLfloat>(width())/height(),
            mMainWindow->getModelViewSettings().mNearClipPlane*mCellSize,
            mMainWindow->getModelViewSettings().mFarClipPlane*mCellSize);
    update();
}


void ModelView::initializeGL()
{
    initializeOpenGLFunctions();
    glClearColor(mMainWindow->getModelViewSettings().mBackground.red()/255.0,
                 mMainWindow->getModelViewSettings().mBackground.green()/255.0,
                 mMainWindow->getModelViewSettings().mBackground.blue()/255.0, 1.0);

    mProgram = new QOpenGLShaderProgram;
    mProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/src/shader.vert");
    mProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/src/shader.frag");
    mProgram->bindAttributeLocation("vertex", 0);
    mProgram->bindAttributeLocation("normal", 1);
    mProgram->link();

    mProgram->bind();
    mProjMatrix = mProgram->uniformLocation("projMatrix");
    mMvMatrix = mProgram->uniformLocation("mvMatrix");
    mNormalMatrix = mProgram->uniformLocation("normalMatrix");
    mLightPos = mProgram->uniformLocation("lightPos");

    // light position
    mProgram->setUniformValue(mLightPos, QVector3D(350, 350, 100));

    mProgram->release();

    refreshBuffers();
}


void ModelView::paintGL()
{
    using namespace std::numbers;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    if (mDrawSelection) {
        QMatrix4x4 world;
        QMatrix4x4 proj;
        world.setToIdentity();
        proj.setToIdentity();
        mProgram->bind();
        mProgram->setUniformValue(mProjMatrix, proj);
        mProgram->setUniformValue(mMvMatrix, world);
        mSelectionBuffer.draw(*this);
        mProgram->release();
    }
    if (mDrawNodeLine && mMainWindow->getSelectedObjects().size() == 1 && mMainWindow->getSelectedObjects()[0]->getObjectType() == ObjectType::obliqueThinWire) {
        QMatrix4x4 world;
        QMatrix4x4 proj;
        world.setToIdentity();
        proj.setToIdentity();
        mProgram->bind();
        mProgram->setUniformValue(mProjMatrix, proj);
        mProgram->setUniformValue(mMvMatrix, world);
        mNodeLineBuffer.draw(*this);
        mProgram->release();
    }

    mWorld.setToIdentity();
    // zoom
    mWorld.translate(0.0, 0.0, mScroll);
    // z up
    mWorld.rotate(270.0, 1.0, 0.0, 0.0);
    // rotation
    mWorld.rotate(mRotPos.x()*mRotationSpeedCoef, 0.0, 0.0, 1.0);
    mWorld.rotate(mRotPos.y()*mRotationSpeedCoef, cos(2.0*pi*mRotPos.x()*mRotationSpeedCoef/360.0), -sin(2.0*pi*mRotPos.x()*mRotationSpeedCoef/360.0), 0.0);
    // origin shift
    mWorld.translate(mOriginPos.x, mOriginPos.y, mOriginPos.z);

    mProgram->bind();
    mProgram->setUniformValue(mProjMatrix, mProj);
    mProgram->setUniformValue(mMvMatrix, mWorld);
    QMatrix3x3 normalMatrix = mWorld.normalMatrix();
    mProgram->setUniformValue(mNormalMatrix, normalMatrix);

    drawModelObjects();

    mProgram->release();
}


void ModelView::resizeGL(int w, int h)
{
    mProj.setToIdentity();
    mProj.perspective(mMainWindow->getModelViewSettings().mPerspective, static_cast<GLfloat>(w)/h,
            mMainWindow->getModelViewSettings().mNearClipPlane*mCellSize,
            mMainWindow->getModelViewSettings().mFarClipPlane*mCellSize);
}


void ModelView::mousePressEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        mCurRotPos = event->pos();
        if (event->modifiers() & Qt::ControlModifier) {
            mDrawSelection = true;
        }
        else {
            mDrawSelection = false;
        }
    }
    else if (event->buttons() & Qt::RightButton) {
        mCurScreenPosOrigin = event->pos();
    }
    else if (event->buttons() & Qt::MiddleButton) {
        mCurRotPos = event->pos();
        mDrawSelection = true;
    }
}


void ModelView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        if (mRotPosSaved == mRotPos || (abs(mRotPosSaved.x() - mRotPos.x()) < 5 && abs(mRotPosSaved.y() - mRotPos.y()) < 5)) {
            QPoint point = event->pos();
            processWireNode(point);
            selectObjectIfItWasHit(event);
        }
        if (event->modifiers() & Qt::ControlModifier) {
            handleSeveralObjectsSelection(event);
        }
        mDrawSelection = false;
        mRotPosSaved = mRotPos;
        update();
    }
    else if (event->button() == Qt::RightButton) {
        if (mOriginPosSaved.x == mOriginPos.x && mOriginPosSaved.y == mOriginPos.y && mOriginPosSaved.z == mOriginPos.z) {
            if (mIsItFirstWire) {
                QMenu menu(this);
                QAction *toCenterAct = new QAction("To View Center (C)", this);
                connect(toCenterAct, &QAction::triggered, this, &ModelView::selectedObjectsToCenter);
                menu.addAction(toCenterAct);
                menu.exec(mapToGlobal(event->pos()));
            }

            mIsItFirstWire = true;
            mDrawNodeLine = false;
            update();
        }
        mOriginPosSaved = mOriginPos;
        update();
    }
    else if (event->button() == Qt::MiddleButton) {
        handleSeveralObjectsSelection(event);
        mDrawSelection = false;
        update();
    }
}


void ModelView::mouseMoveEvent(QMouseEvent *event)
{
    using namespace std::numbers;
    mCurMousePos = event->pos();
    if (event->buttons() & Qt::LeftButton) {
        if (event->modifiers() & Qt::ControlModifier) {
            mCurPos = event->pos();
        }
        else {
            mRotPos = event->pos() - mCurRotPos + mRotPosSaved;
        }
        update();
    }
    else if (event->buttons() & Qt::RightButton) {
        float coef = - mScroll / 400.0;
        mOriginPos.x = coef * (event->pos().x() - mCurScreenPosOrigin.x()) * cos(2.0*pi*mRotPos.x()*mRotationSpeedCoef/360.0) + mOriginPosSaved.x;
        mOriginPos.y = coef * (-event->pos().x() + mCurScreenPosOrigin.x()) * sin(2.0*pi*mRotPos.x()*mRotationSpeedCoef/360.0) + mOriginPosSaved.y;
        mOriginPos.z = coef * (-event->pos().y() + mCurScreenPosOrigin.y()) + mOriginPosSaved.z;
        update();
    }
    else if (event->buttons() & Qt::MiddleButton) {
        mCurPos = event->pos();
        update();
    }
    updateNodeLine();
}


void ModelView::wheelEvent(QWheelEvent *event)
{
    if (event->delta() > 0) {
        if (mScroll < - mMainWindow->getModelViewSettings().mMaxZoomIn * mCellSize) {
            mScroll /= mMainWindow->getModelViewSettings().mZoom;
        }
        mDefaultPosition = false;
        updateNodeLine();
    }
    else if (event->delta() < 0) {
        if (mScroll > - mMainWindow->getModelViewSettings().mMaxZoomOut * mCellSize) {
            mScroll *= mMainWindow->getModelViewSettings().mZoom;
        }
        mDefaultPosition = false;
        updateNodeLine();
    }
    update();
}


void ModelView::drawModelObjects(void)
{
    XyzSizeFloat volSize;
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::volume) {
            volSize = static_cast<VolumeObject*>(object.get())->getVolumeSize();
            mCellSize = static_cast<VolumeObject*>(object.get())->getCellSize();
        }
    }

    if (mMainWindow->getSelectedObjects().size() == 1) {
        if (mMainWindow->getSelectedObjects()[0]->getObjectType() != ObjectType::block) {
            mBuffers[mMainWindow->getSelectedObjects()[0]->getPositionInFile()]->draw(*this);
        }
    }
    for (const auto &buffer : mBuffers) {
        if (buffer->getObjectType() != ObjectType::block) {
            buffer->draw(*this);
        }
    }
    for (const auto &buffer : mBuffers) {
        if (buffer->getObjectType() == ObjectType::block) {
            buffer->draw(*this);
        }
    }
}


void ModelView::selectedObjectsToCenter(void)
{
    Vector min(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
    Vector max(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
    bool wasSet = false;
    std::vector<std::shared_ptr<ModelObject>> modelObjects = mMainWindow->getSelectedObjects();
    for (const auto &object : modelObjects) {
        Vector startp;
        Vector endp;
        if (object->getObjectType() == ObjectType::staircaseWire || object->getObjectType() == ObjectType::thinWire ||
            object->getObjectType() == ObjectType::obliqueThinWire || object->getObjectType() == ObjectType::source ||
            object->getObjectType() == ObjectType::resistor || object->getObjectType() == ObjectType::capacitor) {
            startp = static_cast<TwoPointObject*>(object.get())->getStartPoint();
            endp = static_cast<TwoPointObject*>(object.get())->getEndPoint();
            wasSet = true;
        }
        setMinVector(min, startp);
        setMinVector(min, endp);
        setMaxVector(max, startp);
        setMaxVector(max, endp);
    }
    if (wasSet) {
        mOriginPos.x = mOriginPosSaved.x = - (min.x + max.x) / 2.0;
        mOriginPos.y = mOriginPosSaved.y = - (min.y + max.y) / 2.0;
        mOriginPos.z = mOriginPosSaved.z = - (min.z + max.z) / 2.0;
        update();
    }
}


void ModelView::handleSeveralObjectsSelection(QMouseEvent *event)
{
    std::vector<std::shared_ptr<ModelObject>> selectedObjects;
    std::vector<WirePointTypeDraw> tipIfObjectIsWire;
    for (const auto &object : mObjects) {
        Vector startp;
        Vector endp;
        bool coordSet = false;
        if (object->getObjectType() == ObjectType::staircaseWire || object->getObjectType() == ObjectType::thinWire ||
            object->getObjectType() == ObjectType::obliqueThinWire || object->getObjectType() == ObjectType::source ||
            object->getObjectType() == ObjectType::resistor || object->getObjectType() == ObjectType::capacitor) {
            startp = static_cast<TwoPointObject*>(object.get())->getStartPoint();
            endp = static_cast<TwoPointObject*>(object.get())->getEndPoint();
            coordSet = true;
        }
        if (coordSet && isPointInRectangle(startp, QRect(mCurRotPos, event->pos()))) {
            tipIfObjectIsWire.push_back(WirePointTypeDraw::wireStart);
            selectedObjects.push_back(object);
        }
        else if (coordSet && isPointInRectangle(endp, QRect(mCurRotPos, event->pos()))) {
            tipIfObjectIsWire.push_back(WirePointTypeDraw::wireEnd);
            selectedObjects.push_back(object);
        }

        if (object->getObjectType() == ObjectType::calculate) {
            for (unsigned cntnum = 0; cntnum < static_cast<CalculateObject*>(object.get())->getCoordinates().size(); ++cntnum) {
                if (isPointInRectangle(static_cast<CalculateObject*>(object.get())->getCoordinates()[cntnum], QRect(mCurRotPos, event->pos()))) {
                    selectedObjects.push_back(object);
                    tipIfObjectIsWire.push_back(WirePointTypeDraw::wireStart); // check
                }
            }
        }
    }
    mMainWindow->setSelectedObjects(selectedObjects);
    mMainWindow->setTipsOfSelectedObliqueWire(tipIfObjectIsWire);
    mMainWindow->indicateSelectedObjects();
}


bool ModelView::isPointInRectangle(Vector point, QRect rect)
{
    double selectionMinX = rect.topLeft().x();
    double selectionMaxX = rect.bottomRight().x();
    double selectionMinY = static_cast<double>(height()) - static_cast<double>(rect.bottomRight().y());
    double selectionMaxY = static_cast<double>(height()) - static_cast<double>(rect.topLeft().y());

    QVector3D winPoint = QVector3D(point.x, point.y, point.z).project(mWorld, mProj, QRect(0, 0, width(), height()));

    if (selectionMinX <= winPoint.x() && winPoint.x() <= selectionMaxX &&
        selectionMinY <= winPoint.y() && winPoint.y() <= selectionMaxY &&
        winPoint.z() >= 0.0 && winPoint.z() <= 1.0) {
        return true;
    }

    return false;
}


void ModelView::processWireNode(const QPoint &point)
{
    for (unsigned cnt = 0; cnt < mObjects.size(); ++cnt) {
        if (mObjects[cnt]->getObjectType() == ObjectType::obliqueThinWire) {
            ObliqueThinWire *wire = static_cast<ObliqueThinWire*>(mObjects[cnt].get());
            XyzPointFloat start = wire->getStartPoint();
            XyzPointFloat end = wire->getEndPoint();
            float diameter = wire->getDiameter();
            adjustDiameter(diameter);

            if (isThereHit(point.x(), point.y(), start, diameter) || isThereHit(point.x(), point.y(), end, diameter)) {
                if (mIsItFirstWire) {
                    WirePointTypeDraw pointType;
                    if (isThereHit(point.x(), point.y(), start, diameter)) {
                        pointType = WirePointTypeDraw::wireStart;
                        mCurrentNode = addWiresToNodeAutomatically(mObjects, mCellSize, wire->getStartPoint());
                    }
                    else {
                        pointType = WirePointTypeDraw::wireEnd;
                        mCurrentNode = addWiresToNodeAutomatically(mObjects, mCellSize, wire->getEndPoint());
                    }
                    if ( ( mMainWindow->getSelectedObjects().size() == 1 && mMainWindow->getSelectedObjects()[0]->getObjectType() == ObjectType::obliqueThinWire &&
                           mMainWindow->getSelectedObjects()[0]->getPositionInFile() == cnt ) || mCurrentNode.WireNumber.size() == 1 ) {
                        mIsItFirstWire = false;
                        mMainWindow->gotoLine(wire->getPositionInFile());
                        mMainWindow->setSelectedObject(mObjects[cnt]);
                        mMainWindow->setTipOfSelectedObliqueWire(pointType);
                        mMainWindow->indicateSelectedObjects();
                        mDrawNodeLine = true;
                        updateNodeLine();
                        update();
                        break;
                    }
                }
                else {
                    mIsItFirstWire = true;
                    Vector pointloc;
                    if (isThereHit(point.x(), point.y(), start, diameter)) {
                        pointloc = wire->getStartPoint();
                    }
                    else {
                        pointloc = wire->getEndPoint();
                    }
                    if (mMainWindow->getTipOfSelectedObliqueWire() == WirePointTypeDraw::wireStart) {
                        static_cast<ObliqueThinWire*>(mMainWindow->getSelectedObjects()[0].get())->setStartPoint(pointloc);
                    }
                    else {
                        static_cast<ObliqueThinWire*>(mMainWindow->getSelectedObjects()[0].get())->setEndPoint(pointloc);
                    }
                    refreshBuffer(static_cast<ObliqueThinWire*>(mMainWindow->getSelectedObjects()[0].get())->getPositionInFile());
                    
                    mMainWindow->editFileForSelectedObjects();

                    mDrawNodeLine = false;
                    break;
                }
            }
        }
    }
}


void ModelView::selectObjectIfItWasHit(QMouseEvent *event)
{
    if (!(event->modifiers()&Qt::ControlModifier) && (mIsItFirstWire || !mDrawNodeLine)) {
        mMainWindow->clearSelectedObjects();
        mMainWindow->indicateSelectedObjects();
    }

    float proximitymin = std::numeric_limits<float>::max();
    float proximity = proximitymin;
    std::shared_ptr<ModelObject> hitobject;
    bool washit = false;
    for (std::vector<std::shared_ptr<ModelObject>>::reverse_iterator object = mObjects.rbegin(); object != mObjects.rend(); ++object) {
        float diameter = mCellSize/2.0;
        Vector startp;
        Vector endp;
        std::vector<XyzPointFloat> volSrcCoordinates;
        if (object->get()->getObjectType() == ObjectType::staircaseWire || object->get()->getObjectType() == ObjectType::thinWire ||
            object->get()->getObjectType() == ObjectType::obliqueThinWire || object->get()->getObjectType() == ObjectType::source ||
            object->get()->getObjectType() == ObjectType::resistor || object->get()->getObjectType() == ObjectType::capacitor) {
            startp = static_cast<TwoPointObject*>(object->get())->getStartPoint();
            endp = static_cast<TwoPointObject*>(object->get())->getEndPoint();
        }
        else if (object->get()->getObjectType() == ObjectType::calculate) {
            volSrcCoordinates = static_cast<CalculateObject*>(object->get())->getCoordinates();
        }
        if (object->get()->getObjectType() == ObjectType::thinWire) {
            diameter = static_cast<ThinWire*>(object->get())->getDiameter();
        }
        else if (object->get()->getObjectType() == ObjectType::obliqueThinWire) {
            diameter = static_cast<ObliqueThinWire*>(object->get())->getDiameter();
        }
        adjustDiameter(diameter);

        if (isWireHit(startp, endp, diameter, Vector(event->pos().x(), event->pos().y(), 0.0), proximity) && mIsItFirstWire) {
            if (proximity < proximitymin) {
                proximitymin = proximity;
                washit = true;
                hitobject = *object;
            }
        }
        else if (volSrcCoordinates.size()) {
            for (unsigned cntr = 0; cntr < volSrcCoordinates.size()-1; ++cntr) {
                if (isWireHit(volSrcCoordinates[cntr], volSrcCoordinates[cntr+1], diameter, Vector(event->pos().x(), event->pos().y(), 0.0), proximity)
                    && mIsItFirstWire && proximity < proximitymin) {
                    proximitymin = proximity;
                    washit = true;
                    hitobject = *object;
                }
            }
        }
    }
    if (washit) {
        mMainWindow->clearSelectedObjects();
        mMainWindow->gotoLine(hitobject->getPositionInFile());
        mMainWindow->centerCursor();
        mMainWindow->setSelectedObject(hitobject);
        mMainWindow->setTipOfSelectedObliqueWire(WirePointTypeDraw::wireNoPoint);
        mMainWindow->indicateSelectedObjects();
        update();
        return;
    }
}


bool ModelView::isThereHit(int mouseX, int mouseY, Vector pos, double radius)
{
    double winX = static_cast<double>(mouseX);
    double winY = height() - static_cast<double>(mouseY);
    QVector3D start = QVector3D(winX, winY, 0.0).unproject(mWorld, mProj, QRect(0, 0, width(), height()));
    QVector3D end = QVector3D(winX, winY, 1.0).unproject(mWorld, mProj, QRect(0, 0, width(), height()));

    Vector a(start.x(), start.y(), start.z());
    Vector b(end.x(), end.y(), end.z());
    Vector ab = b - a;
    double ab_square = dotProduct(ab, ab);
    Vector ap = pos - a;
    double ap_dot_ab = dotProduct(ap, ab);
    // t is a projection parameter when we project vector ap onto ab
    double t = ap_dot_ab / ab_square;
    // calculate the closest point
    Vector q = a + ab * t;

    return getDistanceBetweenPoints(q, pos) < radius;
}


bool ModelView::isWireHit(Vector start, Vector end, float diam, Vector pt, float &proximity)
{
    using namespace std::numbers;
    float pickRadius = diam*0.58;
    Vector toOrigin = end - start;
    float length = getDistanceBetweenPoints(start, end);
    float psi = pi/2.0 - acos(toOrigin.z/length);
    float phi;
    if (sqrt(toOrigin.x*toOrigin.x + toOrigin.y*toOrigin.y) == 0.0 || toOrigin.x/sqrt(toOrigin.x*toOrigin.x + toOrigin.y*toOrigin.y) >= 1.0) {
        phi = 0.0;
    }
    else if (toOrigin.x/sqrt(toOrigin.x*toOrigin.x + toOrigin.y*toOrigin.y) <= -1.0 ) {
        phi = pi;
    }
    else {
        phi = acos(toOrigin.x/sqrt(toOrigin.x*toOrigin.x + toOrigin.y*toOrigin.y));
        if (start.y > end.y) {
            phi = -phi;
        }
    }

    Vector tr1[3];
    Vector tr2[3];
    Vector tr3[3];
    Vector tr4[3];

    tr1[0] = rotateVectorWithEulerAngles(Vector(0.0, -pickRadius, 0.0), pi/2.0, psi, -phi) + start;
    tr1[1] = rotateVectorWithEulerAngles(Vector(length, pickRadius, 0.0), pi/2.0, psi, -phi) + start;
    tr1[2] = rotateVectorWithEulerAngles(Vector(0.0, pickRadius, 0.0), pi/2.0, psi, -phi) + start;

    tr2[0] = tr1[0];
    tr2[1] = tr1[1];
    tr2[2] = rotateVectorWithEulerAngles(Vector(length, -pickRadius, 0.0), pi/2.0, psi, -phi) + start;

    tr3[0] = rotateVectorWithEulerAngles(Vector(0.0, 0.0, -pickRadius), pi/2.0, psi, -phi) + start;
    tr3[1] = rotateVectorWithEulerAngles(Vector(length, 0.0, pickRadius), pi/2.0, psi, -phi) + start;
    tr3[2] = rotateVectorWithEulerAngles(Vector(0.0, 0.0, pickRadius), pi/2.0, psi, -phi) + start;

    tr4[0] = tr3[0];
    tr4[1] = tr3[1];
    tr4[2] = rotateVectorWithEulerAngles(Vector(length, 0.0, -pickRadius), pi/2.0, psi, -phi) + start;

    double winX = static_cast<double>(pt.x);
    double winY = height() - static_cast<double>(pt.y);
    QVector3D startv = QVector3D(winX, winY, 0.0).unproject(mWorld, mProj, QRect(0, 0, width(), height()));
    QVector3D endv = QVector3D(winX, winY, 1.0).unproject(mWorld, mProj, QRect(0, 0, width(), height()));

    Vector dir = getDirectionFromPointToPoint(Vector(startv.x(), startv.y(), startv.z()), Vector(endv.x(), endv.y(), endv.z()));

    Vector inters;
    if (getRayTriangleIntersectionPoint(Vector(startv.x(), startv.y(), startv.z()), dir, tr1[0], tr1[1], tr1[2], inters) ||
        getRayTriangleIntersectionPoint(Vector(startv.x(), startv.y(), startv.z()), dir, tr2[0], tr2[1], tr2[2], inters) ||
        getRayTriangleIntersectionPoint(Vector(startv.x(), startv.y(), startv.z()), dir, tr3[0], tr3[1], tr3[2], inters) ||
        getRayTriangleIntersectionPoint(Vector(startv.x(), startv.y(), startv.z()), dir, tr4[0], tr4[1], tr4[2], inters)) {
        proximity = getDistanceBetweenPoints(Vector(startv.x(), startv.y(), startv.z()), inters);
        return true;
    }

    return false;
}


void ModelView::updateNodeLine(void)
{
    if (mDrawNodeLine) {
        QVector3D winPoint;
        if (mMainWindow->getTipOfSelectedObliqueWire() == WirePointTypeDraw::wireStart) {
            Vector start = static_cast<ObliqueThinWire*>(mMainWindow->getSelectedObjects()[0].get())->getStartPoint();
            winPoint = QVector3D(start.x, start.y, start.z).project(mWorld, mProj, QRect(0, 0, width(), height()));
        }
        else {
            Vector end = static_cast<ObliqueThinWire*>(mMainWindow->getSelectedObjects()[0].get())->getEndPoint();
            winPoint = QVector3D(end.x, end.y, end.z).project(mWorld, mProj, QRect(0, 0, width(), height()));
        }
        mCurSelectedWireEnd = QPoint(winPoint.x(), winPoint.y());
        update();
    }
}


void ModelView::adjustDiameter(float &diameter)
{
    if (mMainWindow->getModelViewSettings().mWireDiameterSqrt) {
        diameter = sqrt(diameter);
    }
    if (mMainWindow->getModelViewSettings().mWireDiameterTimesCellSize) {
        diameter *= mCellSize;
    }
    diameter *= mMainWindow->getModelViewSettings().mWireDiameterCoef;
}


WireNodeDraw addWiresToNodeAutomatically(std::vector<std::shared_ptr<ModelObject>> &modelObjects, float mCellSize, Vector point)
{
    WireNodeDraw WiresNode;
    for (unsigned wirec = 0; wirec < modelObjects.size(); ++wirec) {
        if (getDistanceBetweenPoints(point, static_cast<ObliqueThinWire*>(modelObjects[wirec].get())->getStartPoint()) < mCellSize*0.01) {
            WiresNode.WireNumber.push_back(wirec);
            WiresNode.WirePoint.push_back(WirePointTypeDraw::wireStart);
        }
        if (getDistanceBetweenPoints(point, static_cast<ObliqueThinWire*>(modelObjects[wirec].get())->getEndPoint()) < mCellSize*0.01) {
            WiresNode.WireNumber.push_back(wirec);
            WiresNode.WirePoint.push_back(WirePointTypeDraw::wireEnd);
        }
    }
    for (unsigned cntw = 0; cntw < WiresNode.WireNumber.size(); ++cntw) {
        if (WiresNode.MaxDiameter < static_cast<ObliqueThinWire*>(modelObjects[WiresNode.WireNumber[cntw]].get())->getDiameter()) {
            WiresNode.MaxDiameter = static_cast<ObliqueThinWire*>(modelObjects[WiresNode.WireNumber[cntw]].get())->getDiameter();
        }
    }
    return WiresNode;
}


void addSectorToCylinderData(QVector<GLfloat> &data, int &count, float x1, float y1, float x2, float y2, float length)
{
    QVector3D n = QVector3D::normal(QVector3D(0.0f, x2 - x1, y2 - y1), QVector3D(0.0f, -x1, -y1));

    addVertexToBuffer(data, count, QVector3D(0.0, x1, y1), &n);
    addVertexToBuffer(data, count, QVector3D(0.0, x2, y2), &n);
    addVertexToBuffer(data, count, QVector3D(0.0, 0.0, 0.0), &n);

    addVertexToBuffer(data, count, QVector3D(0.0, 0.0, 0.0), &n);
    addVertexToBuffer(data, count, QVector3D(0.0, 0.0, 0.0), &n);
    addVertexToBuffer(data, count, QVector3D(0.0, x2, y2), &n);

    n = QVector3D::normal(QVector3D(0.0f, x1 - x2, y1 - y2), QVector3D(0.0f, -x2, -y2));

    addVertexToBuffer(data, count, QVector3D(length, x2, y2), &n);
    addVertexToBuffer(data, count, QVector3D(length, x1, y1), &n);
    addVertexToBuffer(data, count, QVector3D(length, 0.0, 0.0), &n);

    addVertexToBuffer(data, count, QVector3D(length, 0.0, 0.0), &n);
    addVertexToBuffer(data, count, QVector3D(length, 0.0, 0.0), &n);
    addVertexToBuffer(data, count, QVector3D(length, x2, y2), &n);

    n = QVector3D::normal(QVector3D(-0.1f, 0.0f, 0.0f), QVector3D(0.0f, x1 - x2, y1 - y2));

    addVertexToBuffer(data, count, QVector3D(length, x2, y2), &n);
    addVertexToBuffer(data, count, QVector3D(0.0, x2, y2), &n);
    addVertexToBuffer(data, count, QVector3D(length, x1, y1), &n);

    addVertexToBuffer(data, count, QVector3D(0.0, x1, y1), &n);
    addVertexToBuffer(data, count, QVector3D(length, x1, y1), &n);
    addVertexToBuffer(data, count, QVector3D(0.0, x2, y2), &n);
}


void addVertexToBuffer(QVector<GLfloat> &data, int &count, const QVector3D &v, QVector3D *n)
{
    GLfloat *p = data.data() + count;
    *p++ = v.x();
    *p++ = v.y();
    *p++ = v.z();
    count += 3;
    if (n) {
        *p++ = n->x();
        *p++ = n->y();
        *p++ = n->z();
        count += 3;
    }
}


void setMinVector(Vector &one, const Vector &two)
{
    if (one.x > two.x) {
        one.x = two.x;
    }
    if (one.y > two.y) {
        one.y = two.y;
    }
    if (one.z > two.z) {
        one.z = two.z;
    }
}


void setMaxVector(Vector &one, const Vector &two)
{
    if (one.x < two.x) {
        one.x = two.x;
    }
    if (one.y < two.y) {
        one.y = two.y;
    }
    if (one.z < two.z) {
        one.z = two.z;
    }
}


///
/// \file mainframe.cpp
/// \brief Main window.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "mainframe.h"
#include "settings.h"
#include <QtWidgets>
#include <QDebug>
#include <thread>
#include <chrono>


ModelEditor::ModelEditor(QWidget *parent)
    : QPlainTextEdit(parent)
{
    mLineNumber = new LineNumber(this);
    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateViewportMargins()));
    connect(this, SIGNAL(updateRequest(QRect, int)), this, SLOT(updateLineNumber(QRect)));
    updateViewportMargins();
}


void ModelEditor::mousePressEvent(QMouseEvent *event)
{
    QPlainTextEdit::mousePressEvent(event);
}


void ModelEditor::resizeEvent(QResizeEvent *event)
{
    QPlainTextEdit::resizeEvent(event);
    mLineNumber->setGeometry(QRect(contentsRect().left(), contentsRect().top(), mLineNumber->getWidth(), contentsRect().height()));
}


void ModelEditor::updateViewportMargins(void)
{
    setViewportMargins(mLineNumber->getWidth(), 0, 0, 0);
}


void ModelEditor::updateLineNumber(const QRect &rect)
{
    mLineNumber->update(0, rect.y(), mLineNumber->width(), rect.height());
}


int LineNumber::getWidth(void) const
{
    int num = 1;
    for (int max = qMax(1, mModelEditor->blockCount()); max >= 10; max /= 10, ++num);
    return fontMetrics().width(QLatin1Char('0'))*num + 12;
}


void LineNumber::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.fillRect(event->rect(), QColor(230, 230, 230));
    unsigned firstNum = mModelEditor->firstBlockNum();
    unsigned countNum = 0;
    unsigned height = mModelEditor->getBlockHeight();
    for (unsigned y = mModelEditor->getYcoord(); y <= static_cast<unsigned>(event->rect().bottom()); ++countNum, y += height);
    for (unsigned cnt = firstNum, y = mModelEditor->getYcoord(); cnt < firstNum+countNum; ++cnt, y += height) {
        painter.setPen(QColor(50, 50, 50));
        painter.drawText(0, y, width(), fontMetrics().height(), Qt::AlignCenter, QString::number(cnt+1));
    }
}


MainWindow::MainWindow()
    : mObjects(mFile.getObjects())
{
    createActions();
    createStatusBar();
    createDockWindows();

    setWindowTitle(tr("elecode"));

    setUnifiedTitleAndToolBarOnMac(true);

    std::vector<std::shared_ptr<ModelObject>> &modelObjects = mFile.getObjects();
    modelObjects.push_back(std::make_shared<ObliqueThinWire>(Vector(0, 0, 0), Vector(1e-6, 1e-6, 1e-6), 1e-6));
    mModelView = new ModelView(this, mObjects);
    setCentralWidget(mModelView);

    readConfigurationFile();

    mModelEditor->clear();
    mNoteList->clear();
    mFilePath = "";

    setupEventHandlers();

    show();
    
    mObjects.clear();
    mModelView->resetView();
    mModelView->refreshBuffers();
}


MainWindow::~MainWindow()
{
    writeConfigurationFile();

    close();
}


void MainWindow::onRunCalculations(void)
{
    QString program = mGeneralSettings.mElecodePath;
    QStringList arg(getFilePath());
    mElecodeProcess = new QProcess(this);
    mElecodeProcess->startDetached(program, arg);

    // watch *.elo file
    QString eloFilePath = mFilePath.left(mFilePath.lastIndexOf(QChar('.'))) + ".elo";
    mEloFileWatcher = new QFileSystemWatcher(this);
    mEloFileWatcher->addPath(eloFilePath);
    connect(mEloFileWatcher, SIGNAL(fileChanged(QString)), this, SLOT(onOutputFileChanged(QString)));
}


void MainWindow::onOutputFileChanged(const QString &)
{
    deleteLastLineInConsole();
    QString fpath = mEloFileWatcher->files().last();
    std::ifstream eloFile(fpath.toStdString());
    std::string line;
    while (eloFile >> std::ws && std::getline(eloFile, line));
    writeStringToConsole(QString::fromStdString(line));
}


void MainWindow::deleteLastLineInConsole(void)
{
    QTextCursor cursor = mConsole->textCursor();
    cursor.movePosition(QTextCursor::End);
    cursor.select(QTextCursor::LineUnderCursor);
    cursor.removeSelectedText();
    cursor.deletePreviousChar();
    mConsole->setTextCursor(cursor);
}


void MainWindow::clearConsole(void)
{
    mConsole->clear();
}


void MainWindow::setupEventHandlers(void)
{
    connect(mModelEditor, &ModelEditor::cursorPositionChanged, this, &MainWindow::onChangedCursorPosition);
}


void MainWindow::openFileWithPath(QString path)
{
    mFilePath = path;
    QFile file(mFilePath);
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QByteArray a = file.readAll();
        QString s1 = a;
        mModelEditor->setPlainText(s1);
    }

    setWindowTitle("elecode - " + mFilePath);

    std::istringstream instr(mModelEditor->toPlainText().toStdString());
    mNoteList->clear();
    mObjects.clear();
    try {
        mFile.readDataFromStream(instr);
    }
    catch (const InputFileException &e) {
        std::stringstream ss;
        ss << e.getMessage();
        while (ss) {
            std::string tempString;
            std::getline(ss, tempString);
            if (tempString != "") {
                mNoteList->addItem(QString::fromStdString(tempString));
            }
        }
    }
    mModelView->resetView();
    mModelView->refreshBuffers();
}


void MainWindow::onOpen()
{
    openFileWithPath(QFileDialog::getOpenFileName(this, "Open Model File", ".", "*.elm"));
}


void MainWindow::onSave()
{
    QMimeDatabase mimeDatabase;
    if (mFilePath == "") {
        mFilePath = QFileDialog::getSaveFileName(this, tr("Choose a file name"), ".", "*.elm");
    }

    if (mFilePath.isEmpty()) {
        return;
    }
    QFile file(mFilePath);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("elecode"), tr("Cannot write file %1:\n%2.").arg(QDir::toNativeSeparators(mFilePath), file.errorString()));
        return;
    }

    QTextStream out(&file);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    out << mModelEditor->toPlainText();
    QApplication::restoreOverrideCursor();
    file.close();

    statusBar()->showMessage(tr("Saved '%1'").arg(mFilePath), 2000);
    
    openFileWithPath(mFilePath);
}


void MainWindow::onAbout(void)
{
    QMessageBox::about(this, tr("About elecode"), tr("elecode v1.0\nCopyright (c) Dmitry Kuklin"));
}


void MainWindow::onSettings(void)
{
    ConfigureElecode *settings = new ConfigureElecode(this, mGeneralSettings, mModelViewSettings);
    settings->show();
}


void MainWindow::createActions(void)
{
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    QToolBar *fileToolBar = addToolBar(tr("File"));
    fileToolBar->setObjectName("File");

    const QIcon openIcon = QIcon::fromTheme("document-open", QIcon(":/images/open.png"));
    QAction *openFileAct = new QAction(openIcon, tr("&Open..."), this);
    openFileAct->setShortcuts(QKeySequence::Open);
    openFileAct->setStatusTip(tr("Open model file"));
    connect(openFileAct, &QAction::triggered, this, &MainWindow::onOpen);
    fileMenu->addAction(openFileAct);
    fileToolBar->addAction(openFileAct);

    const QIcon saveIcon = QIcon::fromTheme("document-save", QIcon(":/images/save.png"));
    QAction *saveAct = new QAction(saveIcon, tr("&Save..."), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the current form letter"));
    connect(saveAct, &QAction::triggered, this, &MainWindow::onSave);
    fileMenu->addAction(saveAct);
    fileToolBar->addAction(saveAct);

    fileMenu->addSeparator();

    QAction *quitAct = fileMenu->addAction(tr("&Quit"), this, &QWidget::close);
    quitAct->setShortcuts(QKeySequence::Quit);
    quitAct->setStatusTip(tr("Quit the application"));

    mViewMenu = menuBar()->addMenu(tr("&View"));

    mRunMenu = menuBar()->addMenu(tr("&Run"));
    QToolBar *runToolBar = addToolBar(tr("Run"));
    runToolBar->setObjectName("Run");
    QAction *runAct = new QAction(QIcon(":/images/run.png"), tr("&Run calculations"), this);
    connect(runAct, &QAction::triggered, this, &MainWindow::onRunCalculations);
    mRunMenu->addAction(runAct);
    runToolBar->addAction(runAct);

    mSettingsMenu = menuBar()->addMenu(tr("&Settings"));
    QAction *settingsAct = mSettingsMenu->addAction(tr("&Configure elecode"), this, &MainWindow::onSettings);
    settingsAct->setStatusTip(tr("Configure elecode program"));

    menuBar()->addSeparator();

    mHelpMenu = menuBar()->addMenu(tr("&Help"));
    QAction *aboutAct = mHelpMenu->addAction(tr("&About"), this, &MainWindow::onAbout);
    aboutAct->setStatusTip(tr("Show the application's About box"));
}


void MainWindow::createStatusBar(void)
{
    statusBar()->showMessage(tr("Ready"));
}


void MainWindow::createDockWindows(void)
{
    QDockWidget *dock = new QDockWidget(tr("File Editor"), this);
    dock->setObjectName("File Editor");
    dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);

    mModelEditor = new ModelEditor(dock);
    mModelEditor->setLineWrapMode(QPlainTextEdit::NoWrap);
    dock->setWidget(mModelEditor);
    addDockWidget(Qt::RightDockWidgetArea, dock);
    mViewMenu->addAction(dock->toggleViewAction());

    dock = new QDockWidget(tr("Notifications"), this);
    dock->setObjectName("Notifications");
    mNotifications = new QTabWidget(dock);
    mNoteList = new QListWidget(mNotifications);
    mNotifications->addTab(mNoteList, "Problems");
    mConsole = new QPlainTextEdit(mNotifications);
    mConsole->setReadOnly(true);
    mConsole->setLineWrapMode(QPlainTextEdit::NoWrap);
    mNotifications->addTab(mConsole, "Console");
    dock->setWidget(mNotifications);
    addDockWidget(Qt::BottomDockWidgetArea, dock);
    mViewMenu->addAction(dock->toggleViewAction());
}


void MainWindow::editFileForSelectedObjects(void)
{
    for (const auto &object : mSelectedObjects) {
        unsigned line = object->getPositionInFile();
        editFileLine(QString::fromStdString(object->writeDataToString(true)), line);
    }
}


void MainWindow::editFileLine(QString inStr, unsigned line)
{
    QTextCursor cursor = mModelEditor->textCursor();
    cursor.movePosition(QTextCursor::Start);
    cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, line);
    cursor.select(QTextCursor::LineUnderCursor);
    cursor.removeSelectedText();
    mModelEditor->setTextCursor(cursor);

    mModelEditor->insertPlainText(inStr);
}


void MainWindow::clearSelectedObjects(void)
{
    mSelectedObjects.clear();
    mTipsOfSelectedObliqueWires.clear();
}


bool MainWindow::isObjectSelected(unsigned idx)
{
    for (const auto &object : mSelectedObjects) {
        if (object->getPositionInFile() == idx) {
            return true;
        }
    }
    return false;
}


void MainWindow::gotoLine(unsigned line)
{
    mCurrentLine = static_cast<int>(line);
    QTextCursor cursor(mModelEditor->document()->findBlockByLineNumber(mCurrentLine));
    mModelEditor->setTextCursor(cursor);
}


void MainWindow::indicateSelectedObjects(void)
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    for (const auto &object : mSelectedObjects) {
        QTextEdit::ExtraSelection selection;

        QColor lineColor = QColor(Qt::yellow).lighter(180);

        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = QTextCursor(mModelEditor->document()->findBlockByLineNumber(object->getPositionInFile()));
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    mModelEditor->setExtraSelections(extraSelections);
}


void MainWindow::centerCursor(void)
{
    mModelEditor->centerCursor();
}


void MainWindow::onChangedCursorPosition(void)
{
    std::vector<std::shared_ptr<ModelObject>> &modelObjects = mFile.getObjects();
    for (const auto &object : modelObjects) {
        if (static_cast<int>(object->getPositionInFile()) == mModelEditor->textCursor().blockNumber()) {
            mSelectedObjects.clear();
            mSelectedObjects.push_back(object);
            setTipOfSelectedObliqueWire(WirePointTypeDraw::wireNoPoint);
            indicateSelectedObjects();
            mModelView->refreshBuffers();
            mModelView->update();
            return;
        }
    }
}


void MainWindow::readConfigurationFile(void)
{
    QSettings config(QSettings::IniFormat, QSettings::UserScope, "dmika", "elecode");

    bool filled = config.value("General/Filled").toBool();

    // filling by default values
    if (!filled) {
        config.setValue("General/Filled", true);
        writeCurrentConfigurationData(config);
    }
    else {
        restoreGeometry(config.value("General/Geometry").toByteArray());
        restoreState(config.value("General/State").toByteArray());
        resize(config.value("General/Size", size()).toSize());
        if (config.value("General/Maximized").toBool()) {
            showMaximized();
        }
        
        mGeneralSettings.mElecodePath = config.value("General/ElecodePath").toString();


        mModelViewSettings.mPrecision = config.value("ModelView/Precision").toUInt();
        mModelViewSettings.mWireDiameterCoef = config.value("ModelView/WireDiameterCoef").toFloat();
        mModelViewSettings.mWireDiameterTimesCellSize = config.value("ModelView/WireDiameterTimesCellSize").toBool();
        mModelViewSettings.mWireDiameterSqrt = config.value("ModelView/WireDiameterSqrt").toBool();
        mModelViewSettings.mPerspective = config.value("ModelView/Perspective").toFloat();
        mModelViewSettings.mZoom = config.value("ModelView/ZoomCoef").toFloat();
        mModelViewSettings.mMaxZoomOut = config.value("ModelView/ZoomOut").toFloat();
        mModelViewSettings.mMaxZoomIn = config.value("ModelView/ZoomIn").toFloat();
        mModelViewSettings.mFarClipPlane = config.value("ModelView/FarClipping").toFloat();
        mModelViewSettings.mNearClipPlane = config.value("ModelView/NearClipping").toFloat();

        mModelViewSettings.mCurrentSource = QColor(config.value("ModelView/CurrentSource/r").toInt(),
                                                   config.value("ModelView/CurrentSource/g").toInt(),
                                                   config.value("ModelView/CurrentSource/b").toInt());

        mModelViewSettings.mCurrentCalc = QColor(config.value("ModelView/CurrentCalc/r").toInt(),
                                                    config.value("ModelView/CurrentCalc/g").toInt(),
                                                    config.value("ModelView/CurrentCalc/b").toInt());

        mModelViewSettings.mVoltageSource = QColor(config.value("ModelView/VoltageSource/r").toInt(),
                                                   config.value("ModelView/VoltageSource/g").toInt(),
                                                   config.value("ModelView/VoltageSource/b").toInt());

        mModelViewSettings.mVoltagePathCalc = QColor(config.value("ModelView/VoltagePathCalc/r").toInt(),
                                                        config.value("ModelView/VoltagePathCalc/g").toInt(),
                                                        config.value("ModelView/VoltagePathCalc/b").toInt());

        mModelViewSettings.mCurrentSourceOblique = QColor(config.value("ModelView/CurrentSourceOblique/r").toInt(),
                                                          config.value("ModelView/CurrentSourceOblique/g").toInt(),
                                                          config.value("ModelView/CurrentSourceOblique/b").toInt());

        mModelViewSettings.mVoltageSourceOblique = QColor(config.value("ModelView/VoltageSourceOblique/r").toInt(),
                                                          config.value("ModelView/VoltageSourceOblique/g").toInt(),
                                                          config.value("ModelView/VoltageSourceOblique/b").toInt());

        mModelViewSettings.mCurrentCalcOblique = QColor(config.value("ModelView/CurrentCalcOblique/r").toInt(),
                                                           config.value("ModelView/CurrentCalcOblique/g").toInt(),
                                                           config.value("ModelView/CurrentCalcOblique/b").toInt());

        mModelViewSettings.mVoltageCalcOblique = QColor(config.value("ModelView/VoltageCalcOblique/r").toInt(),
                                                           config.value("ModelView/VoltageCalcOblique/g").toInt(),
                                                           config.value("ModelView/VoltageCalcOblique/b").toInt());

        mModelViewSettings.mThinWire = QColor(config.value("ModelView/ThinWire/r").toInt(),
                                              config.value("ModelView/ThinWire/g").toInt(),
                                              config.value("ModelView/ThinWire/b").toInt());

        mModelViewSettings.mStaircaseWire = QColor(config.value("ModelView/StaircaseWire/r").toInt(),
                                                   config.value("ModelView/StaircaseWire/g").toInt(),
                                                   config.value("ModelView/StaircaseWire/b").toInt());

        mModelViewSettings.mObliqueWire = QColor(config.value("ModelView/ObliqueWire/r").toInt(),
                                                 config.value("ModelView/ObliqueWire/g").toInt(),
                                                 config.value("ModelView/ObliqueWire/b").toInt());

        mModelViewSettings.mResistor = QColor(config.value("ModelView/Resistor/r").toInt(),
                                              config.value("ModelView/Resistor/g").toInt(),
                                              config.value("ModelView/Resistor/b").toInt());

        mModelViewSettings.mBlock = QColor(config.value("ModelView/Ground/r").toInt(),
                                           config.value("ModelView/Ground/g").toInt(),
                                           config.value("ModelView/Ground/b").toInt());

        mModelViewSettings.mChosenObject = QColor(config.value("ModelView/ChosenObject/r").toInt(),
                                                  config.value("ModelView/ChosenObject/g").toInt(),
                                                  config.value("ModelView/ChosenObject/b").toInt());

        mModelViewSettings.mBackground = QColor(config.value("ModelView/Background/r").toInt(),
                                                config.value("ModelView/Background/g").toInt(),
                                                config.value("ModelView/Background/b").toInt());
    }
}


void MainWindow::writeConfigurationFile(void)
{
    QSettings config(QSettings::IniFormat, QSettings::UserScope, "dmika", "elecode");

    writeCurrentConfigurationData(config);
}


void MainWindow::writeCurrentConfigurationData(QSettings &config)
{
    config.setValue("General/Geometry", saveGeometry());
    config.setValue("General/State", saveState());
    config.setValue("General/Maximized", isMaximized());
    if (!isMaximized()) {
        config.setValue("General/Size", size());
    }
    
    config.setValue("General/ElecodePath", mGeneralSettings.mElecodePath);


    config.setValue("ModelView/Precision", mModelViewSettings.mPrecision);
    config.setValue("ModelView/WireDiameterCoef", mModelViewSettings.mWireDiameterCoef);
    config.setValue("ModelView/WireDiameterTimesCellSize", mModelViewSettings.mWireDiameterTimesCellSize);
    config.setValue("ModelView/WireDiameterSqrt", mModelViewSettings.mWireDiameterSqrt);
    config.setValue("ModelView/Perspective", mModelViewSettings.mPerspective);
    config.setValue("ModelView/ZoomCoef", mModelViewSettings.mZoom);
    config.setValue("ModelView/ZoomOut", mModelViewSettings.mMaxZoomOut);
    config.setValue("ModelView/ZoomIn", mModelViewSettings.mMaxZoomIn);
    config.setValue("ModelView/FarClipping", mModelViewSettings.mFarClipPlane);
    config.setValue("ModelView/NearClipping", mModelViewSettings.mNearClipPlane);

    config.setValue("ModelView/CurrentSource/r", mModelViewSettings.mCurrentSource.red());
    config.setValue("ModelView/CurrentSource/g", mModelViewSettings.mCurrentSource.green());
    config.setValue("ModelView/CurrentSource/b", mModelViewSettings.mCurrentSource.blue());

    config.setValue("ModelView/CurrentCalc/r", mModelViewSettings.mCurrentCalc.red());
    config.setValue("ModelView/CurrentCalc/g", mModelViewSettings.mCurrentCalc.green());
    config.setValue("ModelView/CurrentCalc/b", mModelViewSettings.mCurrentCalc.blue());

    config.setValue("ModelView/VoltageSource/r", mModelViewSettings.mVoltageSource.red());
    config.setValue("ModelView/VoltageSource/g", mModelViewSettings.mVoltageSource.green());
    config.setValue("ModelView/VoltageSource/b", mModelViewSettings.mVoltageSource.blue());

    config.setValue("ModelView/VoltagePathCalc/r", mModelViewSettings.mVoltagePathCalc.red());
    config.setValue("ModelView/VoltagePathCalc/g", mModelViewSettings.mVoltagePathCalc.green());
    config.setValue("ModelView/VoltagePathCalc/b", mModelViewSettings.mVoltagePathCalc.blue());

    config.setValue("ModelView/CurrentSourceOblique/r", mModelViewSettings.mCurrentSourceOblique.red());
    config.setValue("ModelView/CurrentSourceOblique/g", mModelViewSettings.mCurrentSourceOblique.green());
    config.setValue("ModelView/CurrentSourceOblique/b", mModelViewSettings.mCurrentSourceOblique.blue());

    config.setValue("ModelView/VoltageSourceOblique/r", mModelViewSettings.mVoltageSourceOblique.red());
    config.setValue("ModelView/VoltageSourceOblique/g", mModelViewSettings.mVoltageSourceOblique.green());
    config.setValue("ModelView/VoltageSourceOblique/b", mModelViewSettings.mVoltageSourceOblique.blue());

    config.setValue("ModelView/CurrentCalcOblique/r", mModelViewSettings.mCurrentCalcOblique.red());
    config.setValue("ModelView/CurrentCalcOblique/g", mModelViewSettings.mCurrentCalcOblique.green());
    config.setValue("ModelView/CurrentCalcOblique/b", mModelViewSettings.mCurrentCalcOblique.blue());

    config.setValue("ModelView/VoltageCalcOblique/r", mModelViewSettings.mVoltageCalcOblique.red());
    config.setValue("ModelView/VoltageCalcOblique/g", mModelViewSettings.mVoltageCalcOblique.green());
    config.setValue("ModelView/VoltageCalcOblique/b", mModelViewSettings.mVoltageCalcOblique.blue());

    config.setValue("ModelView/ThinWire/r", mModelViewSettings.mThinWire.red());
    config.setValue("ModelView/ThinWire/g", mModelViewSettings.mThinWire.green());
    config.setValue("ModelView/ThinWire/b", mModelViewSettings.mThinWire.blue());

    config.setValue("ModelView/StaircaseWire/r", mModelViewSettings.mStaircaseWire.red());
    config.setValue("ModelView/StaircaseWire/g", mModelViewSettings.mStaircaseWire.green());
    config.setValue("ModelView/StaircaseWire/b", mModelViewSettings.mStaircaseWire.blue());

    config.setValue("ModelView/ObliqueWire/r", mModelViewSettings.mObliqueWire.red());
    config.setValue("ModelView/ObliqueWire/g", mModelViewSettings.mObliqueWire.green());
    config.setValue("ModelView/ObliqueWire/b", mModelViewSettings.mObliqueWire.blue());

    config.setValue("ModelView/Resistor/r", mModelViewSettings.mResistor.red());
    config.setValue("ModelView/Resistor/g", mModelViewSettings.mResistor.green());
    config.setValue("ModelView/Resistor/b", mModelViewSettings.mResistor.blue());

    config.setValue("ModelView/Ground/r", mModelViewSettings.mBlock.red());
    config.setValue("ModelView/Ground/g", mModelViewSettings.mBlock.green());
    config.setValue("ModelView/Ground/b", mModelViewSettings.mBlock.blue());

    config.setValue("ModelView/ChosenObject/r", mModelViewSettings.mChosenObject.red());
    config.setValue("ModelView/ChosenObject/g", mModelViewSettings.mChosenObject.green());
    config.setValue("ModelView/ChosenObject/b", mModelViewSettings.mChosenObject.blue());

    config.setValue("ModelView/Background/r", mModelViewSettings.mBackground.red());
    config.setValue("ModelView/Background/g", mModelViewSettings.mBackground.green());
    config.setValue("ModelView/Background/b", mModelViewSettings.mBackground.blue());
}


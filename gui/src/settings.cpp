///
/// \file settings.cpp
/// \brief Program settings.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "settings.h"
#include "mainframe.h"
#include <QLabel>
#include <QHeaderView>
#include <QPainter>
#include <QMessageBox>


ColorButton::ColorButton(const QColor &color, QWidget *parent)
    : QPushButton(parent)
{
    this->setMinimumWidth(50);
    mCurrentColor = color;
    connect(this, &QPushButton::clicked, this, &ColorButton::chooseColor);
}


QColor ColorButton::getColor(void)
{
    return mCurrentColor;
}


void ColorButton::setColor(const QColor &color)
{
    mCurrentColor = color;
}


void ColorButton::chooseColor(void)
{
    QColor color = QColorDialog::getColor(mCurrentColor, this);
    if (color.isValid()) {
        setColor(color);
        emit colorChanged(mCurrentColor);
    }
}


void ColorButton::paintEvent(QPaintEvent *event)
{
    QPushButton::paintEvent(event);

    int colorPadding = 5;

    QRect rect = this->rect();
    QPainter painter(this);
    painter.setBrush(QBrush(mCurrentColor));
    painter.setPen("#CECECE");
    rect.adjust(colorPadding*2, colorPadding, -1-colorPadding*2, -1-colorPadding);
    painter.drawRect(rect);
}


ScrollArea::ScrollArea(QWidget *parent)
    : QScrollArea(parent)
{
    connect(verticalScrollBar(), &QScrollBar::sliderMoved, this, &ScrollArea::onScroll);
    connect(horizontalScrollBar(), &QScrollBar::sliderMoved, this, &ScrollArea::onScroll);
}


void ScrollArea::wheelEvent(QWheelEvent *event)
{
    QScrollArea::wheelEvent(event);
    widget()->update();
}


bool ScrollArea::event(QEvent *event)
{
    widget()->update();
    return QScrollArea::event(event);
}


void ScrollArea::onScroll(int)
{
    update();
}


ConfigureElecode::ConfigureElecode(MainWindow *mainwindow, GeneralSettings &generalObj, ModelViewSettings &modelObj)
    : QWidget(mainwindow, Qt::Window | Qt::WindowStaysOnTopHint),
    mMainWindow(mainwindow), mGeneralSettings(generalObj), mModelViewSettings(modelObj)
{
    mSettingsTree = new QTreeWidget(this);
    mSettingsTree->setMaximumWidth(150);
    mSettingsTree->header()->close();
    setupGeneralSettings();
    setupModelViewSettings();
    mSettingsTree->expandAll();

    mGeneral->show();

    setWindowTitle("Configure elecode");

    setGeometry(200, 200, 700, 400);

    QVBoxLayout *topLayout = new QVBoxLayout;

    QHBoxLayout *settingsLayout = new QHBoxLayout;
    settingsLayout->addWidget(mSettingsTree, 0, Qt::AlignLeft);
    settingsLayout->addWidget(mGeneral, 1);
    settingsLayout->addWidget(mModelView, 1);
    settingsLayout->addWidget(mModelViewColor, 1);

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->setContentsMargins(0, 5, 0, 0);
    mLoadDefaultButton = new QPushButton("Load Defaults", this);
    mOkButton = new QPushButton("OK", this);
    buttonLayout->addWidget(mLoadDefaultButton);
    buttonLayout->addWidget(mOkButton);

    topLayout->addLayout(settingsLayout);
    topLayout->addLayout(buttonLayout);

    setLayout(topLayout);

    setupEventHandlers();
}


void ConfigureElecode::setupEventHandlers(void)
{
    connect(mOkButton, &QPushButton::clicked, this, &ConfigureElecode::onOk);
    connect(mLoadDefaultButton, &QPushButton::clicked, this, &ConfigureElecode::onLoadDefault);
    connect(mSettingsTree, &QTreeWidget::itemSelectionChanged, this, &ConfigureElecode::onItemSelectionChanged);

    connect(mCurrentSourceColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mCurrentCalcColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mVoltageSourceColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mVoltagePathCalcColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mCurrentSourceObliqueColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mVoltageSourceObliqueColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mCurrentCalcObliqueColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mVoltageCalcObliqueColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mThinWireColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mStaircaseWireColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mObliqueWireColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mResistorColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mBlockColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mChosenObjectColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
    connect(mBackgroundColour, &ColorButton::colorChanged, this, &ConfigureElecode::onColor);
}


void ConfigureElecode::addCommonWidgetItems(QWidget* const& widget, QVBoxLayout* const& generalTopLayout, const QString &captionText)
{
    QFont f("Arial", 12, QFont::Bold);
    QLabel *caption = new QLabel(widget);
    caption->setText(captionText);
    caption->setFont(f);
    QFrame *hFrame = new QFrame(widget);
    hFrame->setFrameShape(QFrame::HLine);
    hFrame->setFrameShadow(QFrame::Sunken);
    generalTopLayout->setContentsMargins(2, 2, 2, 0);
    generalTopLayout->addWidget(caption);
    generalTopLayout->addWidget(hFrame);
}


void ConfigureElecode::addCommonWidgetItemsBottom(QWidget* const& widget, QVBoxLayout* const& generalTopLayout)
{
    generalTopLayout->addStretch();
    QFrame *hFrame1 = new QFrame(widget);
    hFrame1->setFrameShape(QFrame::HLine);
    hFrame1->setFrameShadow(QFrame::Sunken);
    generalTopLayout->addWidget(hFrame1);
    widget->setLayout(generalTopLayout);
}


void ConfigureElecode::setupGeneralSettings(void)
{
    QTreeWidgetItem *topLevelg = new QTreeWidgetItem();
    topLevelg->setText(0, "General");
    mSettingsTree->addTopLevelItem(topLevelg);

    mGeneral = new QWidget(this);
    QVBoxLayout *generalTopLayout = new QVBoxLayout(mGeneral);
    addCommonWidgetItems(mGeneral, generalTopLayout, "General Settings");
    
    QFontMetrics fm(fontMetrics());
    int maxWidthView = fm.width("Elecode path:");
    
    QHBoxLayout *row1v = new QHBoxLayout(mGeneral);
    QLabel *elpath = new QLabel(mGeneral);
    elpath->setText("Elecode path:");
    elpath->setFixedWidth(maxWidthView);
    row1v->addWidget(elpath);
    mElecodePath = new QLineEdit(mGeneralSettings.mElecodePath, mGeneral);
    row1v->addWidget(mElecodePath, 1);
    
    generalTopLayout->addLayout(row1v);
    
    addCommonWidgetItemsBottom(mGeneral, generalTopLayout);

    mGeneral->hide();
}


void ConfigureElecode::setupModelViewSettings(void)
{
    QTreeWidgetItem *topLevelm = new QTreeWidgetItem();
    topLevelm->setText(0, "Model View");
    QTreeWidgetItem *topLevelmcol = new QTreeWidgetItem();
    topLevelmcol->setText(0, "Colors");
    topLevelm->addChild(topLevelmcol);
    mSettingsTree->addTopLevelItem(topLevelm);

    mModelView = new QWidget(this);
    QVBoxLayout *generalTopLayout = new QVBoxLayout(mModelView);
    addCommonWidgetItems(mModelView, generalTopLayout, "Model View Settings");
    QFontMetrics fm(fontMetrics());
    QWidget *scrolledWidgetGen = new QWidget(mModelView);

    // Wires
    QGroupBox *wires = new QGroupBox("Wires", scrolledWidgetGen);
    QVBoxLayout *wiresLayout = new QVBoxLayout(wires);
    int maxWidthWires = fm.width("Diameter coefficient:");

    QHBoxLayout *row1w = new QHBoxLayout(wires);
    QLabel *precis = new QLabel(wires);
    precis->setText("Drawing precision:");
    precis->setFixedWidth(maxWidthWires);
    row1w->addWidget(precis);
    mDrawingPrecision = new QSpinBox(wires);
    mDrawingPrecision->setMinimum(3);
    mDrawingPrecision->setMaximum(300);
    mDrawingPrecision->setValue(mModelViewSettings.mPrecision);
    row1w->addWidget(mDrawingPrecision, 2);
    row1w->addStretch(1);
    mWireDiameterTimesCellSize = new QCheckBox("Diameter times cell size", wires);
    row1w->addWidget(mWireDiameterTimesCellSize, 2);
    if (mModelViewSettings.mWireDiameterTimesCellSize) {
        mWireDiameterTimesCellSize->setCheckState(Qt::Checked);
    }

    QHBoxLayout *row2w = new QHBoxLayout(wires);
    QLabel *diamCoef = new QLabel(wires);
    diamCoef->setText("Diameter coefficient:");
    diamCoef->setFixedWidth(maxWidthWires);
    row2w->addWidget(diamCoef);
    mWireDiameterCoef = new QDoubleSpinBox(wires);
    mWireDiameterCoef->setValue(mModelViewSettings.mWireDiameterCoef);
    row2w->addWidget(mWireDiameterCoef, 2);
    row2w->addStretch(1);
    mWireDiameterSqrt = new QCheckBox("Square root of diameter", wires);
    row2w->addWidget(mWireDiameterSqrt, 2);
    if (mModelViewSettings.mWireDiameterSqrt) {
        mWireDiameterSqrt->setCheckState(Qt::Checked);
    }

    wiresLayout->addLayout(row1w);
    wiresLayout->addLayout(row2w);
    wires->setLayout(wiresLayout);

    // View
    QGroupBox *view = new QGroupBox("View", scrolledWidgetGen);
    QVBoxLayout *viewLayout = new QVBoxLayout(view);
    int maxWidthView = fm.width("Max zoom out:");
    int maxWidthViewLeft = fm.width("Near clipping plane:");

    QHBoxLayout *row1v = new QHBoxLayout(view);
    QLabel *zoom = new QLabel(view);
    zoom->setText("Zoom coef.:");
    zoom->setFixedWidth(maxWidthView);
    row1v->addWidget(zoom);
    mZoom = new QDoubleSpinBox(view);
    mZoom->setValue(mModelViewSettings.mZoom);
    row1v->addWidget(mZoom, 2);
    row1v->addStretch(1);
    QLabel *persp = new QLabel(view);
    persp->setText("Perspective:");
    persp->setFixedWidth(maxWidthViewLeft);
    row1v->addWidget(persp);
    mPerspective = new QDoubleSpinBox(view);
    mPerspective->setValue(mModelViewSettings.mPerspective);
    row1v->addWidget(mPerspective, 2);

    QHBoxLayout *row2v = new QHBoxLayout(view);
    QLabel *zoomIn = new QLabel(view);
    zoomIn->setText("Max zoom in:");
    zoomIn->setFixedWidth(maxWidthView);
    row2v->addWidget(zoomIn);
    mZoomIn = new QDoubleSpinBox(view); 
    mZoomIn->setValue(mModelViewSettings.mMaxZoomIn);
    row2v->addWidget(mZoomIn, 2);
    row2v->addStretch(1);
    QLabel *nearClip = new QLabel(view);
    nearClip->setText("Near clipping plane:");
    nearClip->setFixedWidth(maxWidthViewLeft);
    row2v->addWidget(nearClip);
    mNearClippingPlane = new QDoubleSpinBox(view);
    mNearClippingPlane->setValue(mModelViewSettings.mNearClipPlane);
    row2v->addWidget(mNearClippingPlane, 2);

    QHBoxLayout *row3v = new QHBoxLayout(view);
    QLabel *zoomOut = new QLabel(view);
    zoomOut->setText("Max zoom out:");
    zoomOut->setFixedWidth(maxWidthView);
    row3v->addWidget(zoomOut);
    mZoomOut = new QDoubleSpinBox(view);
    mZoomOut->setRange(0, 10000);
    mZoomOut->setValue(mModelViewSettings.mMaxZoomOut);
    row3v->addWidget(mZoomOut, 2);
    row3v->addStretch(1);
    QLabel *farClip = new QLabel(view);
    farClip->setText("Far clipping plane:");
    farClip->setFixedWidth(maxWidthViewLeft);
    row3v->addWidget(farClip);
    mFarClippingPlane = new QDoubleSpinBox(view);
    mFarClippingPlane->setRange(0, 10000);
    mFarClippingPlane->setValue(mModelViewSettings.mFarClipPlane);
    row3v->addWidget(mFarClippingPlane, 2);

    viewLayout->addLayout(row1v);
    viewLayout->addLayout(row2v);
    viewLayout->addLayout(row3v);
    view->setLayout(viewLayout);

    generalTopLayout->addWidget(wires);
    generalTopLayout->addWidget(view);

    // Top
    generalTopLayout->addWidget(scrolledWidgetGen, 1);
    addCommonWidgetItemsBottom(mModelView, generalTopLayout);


    mModelViewColor = new QWidget(this);
    QVBoxLayout *generalTopLayoutCol = new QVBoxLayout(mModelViewColor);
    addCommonWidgetItems(mModelViewColor, generalTopLayoutCol, "Model View Color Settings");
    QWidget *scrolledWidget = new QWidget(mModelViewColor);
    QVBoxLayout *scrolledGeneralTopLayoutCol = new QVBoxLayout(scrolledWidget);

    // Objects
    QGroupBox *objects = new QGroupBox("Objects", scrolledWidget);
    QVBoxLayout *objectsLayout = new QVBoxLayout(objects);
    int maxWidth = fm.width("Staircase wire:");

    QHBoxLayout *row1o = new QHBoxLayout(objects);
    QLabel *oblWire = new QLabel(objects);
    oblWire->setText("Oblique wire:");
    oblWire->setFixedWidth(maxWidth);
    row1o->addWidget(oblWire);
    mObliqueWireColour = new ColorButton(mModelViewSettings.mObliqueWire, objects);
    row1o->addWidget(mObliqueWireColour, 2);
    row1o->addStretch(1);
    QLabel *blockColor = new QLabel(objects);
    blockColor->setText("Block:");
    blockColor->setFixedWidth(maxWidth);
    row1o->addWidget(blockColor);
    mBlockColour = new ColorButton(mModelViewSettings.mBlock, scrolledWidget);
    row1o->addWidget(mBlockColour, 2);

    QHBoxLayout *row2o = new QHBoxLayout(objects);
    QLabel *thinWire = new QLabel(objects);
    thinWire->setText("Thin wire:");
    thinWire->setFixedWidth(maxWidth);
    row2o->addWidget(thinWire);
    mThinWireColour = new ColorButton(mModelViewSettings.mThinWire, objects);
    row2o->addWidget(mThinWireColour, 2);
    row2o->addStretch(1);
    QLabel *resistor = new QLabel(objects);
    resistor->setText("Resistor:");
    resistor->setFixedWidth(maxWidth);
    row2o->addWidget(resistor);
    mResistorColour = new ColorButton(mModelViewSettings.mResistor, scrolledWidget);
    row2o->addWidget(mResistorColour, 2);

    QHBoxLayout *row3o = new QHBoxLayout(objects);
    QLabel *staircaseWire = new QLabel(objects);
    staircaseWire->setText("Staircase wire:");
    staircaseWire->setFixedWidth(maxWidth);
    row3o->addWidget(staircaseWire);
    mStaircaseWireColour = new ColorButton(mModelViewSettings.mStaircaseWire, objects);
    row3o->addWidget(mStaircaseWireColour, 2);
    row3o->addStretch(1);
    QLabel *empty = new QLabel(objects);
    empty->setText("");
    empty->setFixedWidth(maxWidth);
    row3o->addWidget(empty);
    row3o->addStretch(2);

    objectsLayout->addLayout(row1o);
    objectsLayout->addLayout(row2o);
    objectsLayout->addLayout(row3o);
    objects->setLayout(objectsLayout);

    // Sources
    QGroupBox *sources = new QGroupBox("Sources", scrolledWidget);
    QVBoxLayout *sourcesLayout = new QVBoxLayout(sources);

    QHBoxLayout *row1 = new QHBoxLayout(sources);
    QLabel *sourceColor = new QLabel(sources);
    sourceColor->setText("Current:");
    row1->addWidget(sourceColor);
    mCurrentSourceColour = new ColorButton(mModelViewSettings.mCurrentSource, sources);
    row1->addWidget(mCurrentSourceColour, 2);
    row1->addStretch(1);
    QLabel *curOblSourceColor = new QLabel(sources);
    curOblSourceColor->setText("Current (oblique wire):");
    row1->addWidget(curOblSourceColor);
    mCurrentSourceObliqueColour = new ColorButton(mModelViewSettings.mCurrentSourceOblique, scrolledWidget);
    row1->addWidget(mCurrentSourceObliqueColour, 2);

    QHBoxLayout *row2 = new QHBoxLayout(sources);
    QLabel *volSourceColor = new QLabel(sources);
    volSourceColor->setText("Voltage:");
    row2->addWidget(volSourceColor);
    mVoltageSourceColour = new ColorButton(mModelViewSettings.mVoltageSource, sources);
    row2->addWidget(mVoltageSourceColour, 2);
    row2->addStretch(1);
    QLabel *volOblSourceColor = new QLabel(sources);
    volOblSourceColor->setText("Voltage (oblique wire):");
    row2->addWidget(volOblSourceColor);
    mVoltageSourceObliqueColour = new ColorButton(mModelViewSettings.mVoltageSourceOblique, scrolledWidget);
    row2->addWidget(mVoltageSourceObliqueColour, 2);

    sourcesLayout->addLayout(row1);
    sourcesLayout->addLayout(row2);
    sources->setLayout(sourcesLayout);

    // Calculate
    QGroupBox *calcs = new QGroupBox("Calculate", scrolledWidget);
    QVBoxLayout *calcsLayout = new QVBoxLayout(calcs);

    QHBoxLayout *row1m = new QHBoxLayout(calcs);
    QLabel *calcColor = new QLabel(calcs);
    calcColor->setText("Current:");
    row1m->addWidget(calcColor);
    mCurrentCalcColour = new ColorButton(mModelViewSettings.mCurrentCalc, calcs);
    row1m->addWidget(mCurrentCalcColour, 2);
    row1m->addStretch(1);
    QLabel *curOblCalcColor = new QLabel(calcs);
    curOblCalcColor->setText("Current (oblique wire):");
    row1m->addWidget(curOblCalcColor);
    mCurrentCalcObliqueColour = new ColorButton(mModelViewSettings.mCurrentCalcOblique, calcs);
    row1m->addWidget(mCurrentCalcObliqueColour, 2);

    QHBoxLayout *row2m = new QHBoxLayout(calcs);
    QLabel *volCalcColor = new QLabel(calcs);
    volCalcColor->setText("Voltage:");
    row2m->addWidget(volCalcColor);
    mVoltagePathCalcColour = new ColorButton(mModelViewSettings.mVoltagePathCalc, calcs);
    row2m->addWidget(mVoltagePathCalcColour, 2);
    row2m->addStretch(1);
    QLabel *volOblCalcColor = new QLabel(calcs);
    volOblCalcColor->setText("Voltage (oblique wire):");
    row2m->addWidget(volOblCalcColor);
    mVoltageCalcObliqueColour = new ColorButton(mModelViewSettings.mVoltageCalcOblique, calcs);
    row2m->addWidget(mVoltageCalcObliqueColour, 2);

    calcsLayout->addLayout(row1m);
    calcsLayout->addLayout(row2m);
    calcs->setLayout(calcsLayout);

    // Common
    QGroupBox *common = new QGroupBox("Common", scrolledWidget);
    QVBoxLayout *commonLayout = new QVBoxLayout(common);

    QHBoxLayout *row1c = new QHBoxLayout(common);
    QLabel *backgroundColor = new QLabel(common);
    backgroundColor->setText("Background:");
    row1c->addWidget(backgroundColor);
    mBackgroundColour = new ColorButton(mModelViewSettings.mBackground, common);
    row1c->addWidget(mBackgroundColour, 2);
    row1c->addStretch(1);
    QLabel *selColor = new QLabel(common);
    selColor->setText("Selected object:");
    row1c->addWidget(selColor);
    mChosenObjectColour = new ColorButton(mModelViewSettings.mChosenObject, common);
    row1c->addWidget(mChosenObjectColour, 2);

    commonLayout->addLayout(row1c);
    common->setLayout(commonLayout);

    // Top
    scrolledGeneralTopLayoutCol->addWidget(objects);
    scrolledGeneralTopLayoutCol->addWidget(sources);
    scrolledGeneralTopLayoutCol->addWidget(calcs);
    scrolledGeneralTopLayoutCol->addWidget(common);
    scrolledGeneralTopLayoutCol->addStretch();
    scrolledWidget->setLayout(scrolledGeneralTopLayoutCol);

    ScrollArea *scrollArea = new ScrollArea(mModelViewColor);
    scrollArea->setWidget(scrolledWidget);
    scrollArea->setWidgetResizable(true);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    generalTopLayoutCol->addWidget(scrollArea, 1);

    addCommonWidgetItemsBottom(mModelViewColor, generalTopLayoutCol);

    mModelView->hide();
    mModelViewColor->hide();
}


void ConfigureElecode::onLoadDefault(void)
{
    GeneralSettings defaultGeneralSettings;
    mElecodePath->setText(mGeneralSettings.mElecodePath);

    ModelViewSettings defaultViewSettings;
    mDrawingPrecision->setValue(defaultViewSettings.mPrecision);
    mCurrentSourceColour->setColor(defaultViewSettings.mCurrentSource);
    mCurrentCalcColour->setColor(defaultViewSettings.mCurrentCalc);
    mVoltageSourceColour->setColor(defaultViewSettings.mVoltageSource);
    mVoltagePathCalcColour->setColor(defaultViewSettings.mVoltagePathCalc);
    mResistorColour->setColor(defaultViewSettings.mResistor);
    mCurrentSourceObliqueColour->setColor(defaultViewSettings.mCurrentSourceOblique);
    mVoltageSourceObliqueColour->setColor(defaultViewSettings.mVoltageSourceOblique);
    mCurrentCalcObliqueColour->setColor(defaultViewSettings.mCurrentCalcOblique);
    mVoltageCalcObliqueColour->setColor(defaultViewSettings.mVoltageCalcOblique);
    mThinWireColour->setColor(defaultViewSettings.mThinWire);
    mStaircaseWireColour->setColor(defaultViewSettings.mStaircaseWire);
    mObliqueWireColour->setColor(defaultViewSettings.mObliqueWire);
    mBlockColour->setColor(defaultViewSettings.mBlock);
    mChosenObjectColour->setColor(defaultViewSettings.mChosenObject);
    mBackgroundColour->setColor(defaultViewSettings.mBackground);

    saveSettings();
    update();
    mMainWindow->update();
    mMainWindow->writeConfigurationFile();
}


void ConfigureElecode::saveSettings(void)
{
    mGeneralSettings.mElecodePath = mElecodePath->text();
    
    mModelViewSettings.mPrecision = static_cast<unsigned>(mDrawingPrecision->value());
    mModelViewSettings.mWireDiameterCoef = mWireDiameterCoef->value();
    if (mWireDiameterTimesCellSize->checkState() == Qt::Checked) {
        mModelViewSettings.mWireDiameterTimesCellSize = true;
    }
    else {
        mModelViewSettings.mWireDiameterTimesCellSize = false;
    }
    if (mWireDiameterSqrt->checkState() == Qt::Checked) {
        mModelViewSettings.mWireDiameterSqrt = true;
    }
    else {
        mModelViewSettings.mWireDiameterSqrt = false;
    }
    mModelViewSettings.mPerspective = mPerspective->value();
    mModelViewSettings.mZoom = mZoom->value();
    mModelViewSettings.mMaxZoomOut = mZoomOut->value();
    mModelViewSettings.mMaxZoomIn = mZoomIn->value();
    mModelViewSettings.mFarClipPlane = mFarClippingPlane->value();
    mModelViewSettings.mNearClipPlane = mNearClippingPlane->value();
    mModelViewSettings.mCurrentSource = mCurrentSourceColour->getColor();
    mModelViewSettings.mCurrentCalc = mCurrentCalcColour->getColor();
    mModelViewSettings.mVoltageSource = mVoltageSourceColour->getColor();
    mModelViewSettings.mVoltagePathCalc = mVoltagePathCalcColour->getColor();
    mModelViewSettings.mResistor = mResistorColour->getColor();
    mModelViewSettings.mCurrentSourceOblique = mCurrentSourceObliqueColour->getColor();
    mModelViewSettings.mVoltageSourceOblique = mVoltageSourceObliqueColour->getColor();
    mModelViewSettings.mCurrentCalcOblique = mCurrentCalcObliqueColour->getColor();
    mModelViewSettings.mVoltageCalcOblique = mVoltageCalcObliqueColour->getColor();
    mModelViewSettings.mThinWire = mThinWireColour->getColor();
    mModelViewSettings.mStaircaseWire = mStaircaseWireColour->getColor();
    mModelViewSettings.mObliqueWire = mObliqueWireColour->getColor();
    mModelViewSettings.mBlock = mBlockColour->getColor();
    mModelViewSettings.mChosenObject = mChosenObjectColour->getColor();
    mModelViewSettings.mBackground = mBackgroundColour->getColor();
}


void ConfigureElecode::onOk(void)
{
    saveSettings();
    mMainWindow->update();
    mMainWindow->writeConfigurationFile();
    close();
}


void ConfigureElecode::onColor(void)
{
    saveSettings();
    mMainWindow->update();
    mMainWindow->writeConfigurationFile();
}


void ConfigureElecode::onItemSelectionChanged(void)
{
    if (mSettingsTree->selectedItems()[0]->text(0) == "General") {
        mModelView->hide();
        mModelViewColor->hide();
        mGeneral->show();
    }
    else if (mSettingsTree->selectedItems()[0]->text(0) == "Model View") {
        mGeneral->hide();
        mModelViewColor->hide();
        mModelView->show();
    }
    else if (mSettingsTree->selectedItems()[0]->text(0) == "Colors") {
        mGeneral->hide();
        mModelView->hide();
        mModelViewColor->show();
    }
}


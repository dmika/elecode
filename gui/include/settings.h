///
/// \file settings.h
/// \brief Program settings.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef  SETTINGS_H__
#define  SETTINGS_H__


#include <QPushButton>
#include <QColorDialog>
#include <QWidget>
#include <QCheckBox>
#include <QSlider>
#include <QTreeWidget>
#include <QBoxLayout>
#include <QScrollArea>
#include <QScrollBar>
#include <QGroupBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLineEdit>


class MainWindow;
struct GeneralSettings;
struct ModelViewSettings;


class ColorButton : public QPushButton {
    Q_OBJECT

public:
    explicit ColorButton(const QColor &color = Qt::black, QWidget *parent = 0);
    virtual ~ColorButton() {}
    QColor getColor(void);
    void setColor(const QColor &color);

signals:
    void colorChanged(QColor);

public slots:
    void chooseColor();
    void paintEvent(QPaintEvent *event);

private:
    QColor mCurrentColor;
};


class ScrollArea : public QScrollArea {
    Q_OBJECT

public:
    ScrollArea(QWidget *parent);

protected slots:
    void onScroll(int);

public slots:
    void wheelEvent(QWheelEvent *event);
    bool event(QEvent *event);
};


class ConfigureElecode : public QWidget {
    Q_OBJECT

public:
    ConfigureElecode(MainWindow *mainwindow, GeneralSettings &generalObj, ModelViewSettings &modelObj);

public slots:
    void onOk(void);
    void onLoadDefault(void);
    void onColor(void);
    void onItemSelectionChanged(void);

protected:
    void addCommonWidgetItems(QWidget * const &widget, QVBoxLayout * const &generalTopLayout, const QString &captionText);
    void addCommonWidgetItemsBottom(QWidget * const &widget, QVBoxLayout * const &generalTopLayout);
    void setupGeneralSettings(void);
    void setupModelViewSettings(void);
    void setupEventHandlers(void);
    void saveSettings(void);
    MainWindow *mMainWindow;
    QPushButton *mOkButton;
    QPushButton *mLoadDefaultButton;
    GeneralSettings &mGeneralSettings;
    ModelViewSettings &mModelViewSettings;
    
    QTreeWidget *mSettingsTree;

    QLineEdit *mElecodePath;
    QSpinBox *mAutosave;

    QSpinBox *mDrawingPrecision;
    QDoubleSpinBox *mWireDiameterCoef;
    QCheckBox *mWireDiameterTimesCellSize;
    QCheckBox *mWireDiameterSqrt;
    QDoubleSpinBox *mPerspective;
    QDoubleSpinBox *mZoom;
    QDoubleSpinBox *mZoomOut;
    QDoubleSpinBox *mZoomIn;
    QDoubleSpinBox *mFarClippingPlane;
    QDoubleSpinBox *mNearClippingPlane;
    ColorButton *mCurrentSourceColour;
    ColorButton *mCurrentCalcColour;
    ColorButton *mVoltageSourceColour;
    ColorButton *mVoltagePathCalcColour;
    ColorButton *mCurrentSourceObliqueColour;
    ColorButton *mVoltageSourceObliqueColour;
    ColorButton *mCurrentCalcObliqueColour;
    ColorButton *mVoltageCalcObliqueColour;
    ColorButton *mThinWireColour;
    ColorButton *mStaircaseWireColour;
    ColorButton *mObliqueWireColour;
    ColorButton *mResistorColour;
    ColorButton *mBlockColour;
    ColorButton *mChosenObjectColour;
    ColorButton *mBackgroundColour;

    QCheckBox *mLimitCoordinates;

    QWidget *mGeneral;
    QWidget *mModelView;
    QWidget *mModelViewColor;
};


#endif  // SETTINGS_H__


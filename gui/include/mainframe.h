///
/// \file mainframe.h
/// \brief Main window.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef MAINFRAME_H__
#define MAINFRAME_H__


#include "constants.h"
#include "modelfile.h"
#include "modelview.h"
#include <QMainWindow>
#include <QPlainTextEdit>
#include <QTabWidget>
#include <QThread>
#include <QSettings>
#include <QProcess>
#include <QFileSystemWatcher>
#include <QTextBlock>
#include <deque>


QT_BEGIN_NAMESPACE
class QAction;
class QListWidget;
class QMenu;
class QTextEdit;
QT_END_NAMESPACE


class ModelView;
class MainWindow;
class ModelEditor;
class LineNumber;


struct GeneralSettings {
    QString mElecodePath{"elecode"};
};


struct ModelViewSettings {
    ModelViewSettings() {}

    unsigned mPrecision{20};
    float mWireDiameterCoef{1.0};
    bool mWireDiameterTimesCellSize{true};
    bool mWireDiameterSqrt{true};
    float mPerspective{45.0};
    float mZoom{1.35};
    float mMaxZoomOut{1500.0};
    float mMaxZoomIn{1.0};
    float mFarClipPlane{2000.0};
    float mNearClipPlane{0.1};
    QColor mCurrentSource{150, 0, 0};
    QColor mCurrentCalc{0, 0, 150};
    QColor mVoltageSource{150, 0, 0};
    QColor mVoltagePathCalc{0, 0, 150};
    QColor mResistor{0, 150, 0};
    QColor mCurrentSourceOblique{150, 0, 0};
    QColor mVoltageSourceOblique{150, 0, 0};
    QColor mCurrentCalcOblique{0, 0, 150};
    QColor mVoltageCalcOblique{0, 0, 150};
    QColor mThinWire{100, 100, 100};
    QColor mStaircaseWire{100, 100, 100};
    QColor mObliqueWire{100, 100, 100};
    QColor mBlock{255, 255, 255};
    QColor mChosenObject{255, 255, 0};
    QColor mBackground{255, 255, 255};
};


class ModelEditor : public QPlainTextEdit {
    Q_OBJECT

public:
    ModelEditor(QWidget *parent = nullptr);
    
    unsigned firstBlockNum(void) { return firstVisibleBlock().blockNumber(); }
    unsigned getYcoord(void) { return static_cast<unsigned>(blockBoundingGeometry(firstVisibleBlock()).translated(contentOffset()).top()); }
    unsigned getBlockHeight(void) { return static_cast<unsigned>(blockBoundingRect(firstVisibleBlock()).height()); }

protected:
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);

private slots:
    void updateViewportMargins(void);
    void updateLineNumber(const QRect &rect);

private:
    LineNumber *mLineNumber;
};


class LineNumber : public QWidget {
public:
    LineNumber(ModelEditor *medit) : QWidget(medit) {
        mModelEditor = medit;
    }

    QSize sizeHint() const { return QSize(getWidth(), 0); }
    int getWidth(void) const;

protected:
    void paintEvent(QPaintEvent *event);

private:
    ModelEditor *mModelEditor;
};


class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();

    // model view
    void refreshBuffer(int item) { mModelView->refreshBuffer(item); }
    void refreshBuffers(void) { mModelView->refreshBuffers(); }
    void updateModelView(void) { mModelView->update(); }
    void resetModelView(void) { mModelView->resetView(); }
    ModelViewSettings getModelViewSettings(void) const { return mModelViewSettings; }

    // file editor
    void gotoLine(unsigned line);
    void centerCursor(void);
    
    // notifications
    void deleteLastLineInConsole(void);
    void writeStringToConsole(const QString &str) { mConsole->appendPlainText(str); }
    void clearConsole(void);
    
    // settings
    void writeConfigurationFile(void);

    // selected group
    bool isObjectSelected(unsigned idx);
    void clearSelectedObjects(void);
    void indicateSelectedObjects(void);
    void editFileLine(QString inStr, unsigned line);
    void editFileForSelectedObjects(void);
    std::vector<std::shared_ptr<ModelObject>> getSelectedObjects(void) { return mSelectedObjects; }
    std::vector<std::shared_ptr<ModelObject>> getObjects(void) { return mFile.getObjects(); }
    void setSelectedObject(std::shared_ptr<ModelObject> object) { mSelectedObjects = {object}; }
    void setSelectedObjects(std::vector<std::shared_ptr<ModelObject>> objects) { mSelectedObjects = objects; }

    // oblique wires
    void setTipsOfSelectedObliqueWire(std::vector<WirePointTypeDraw> tips) { mTipsOfSelectedObliqueWires = tips; }
    void setTipOfSelectedObliqueWire(WirePointTypeDraw tip) { mTipsOfSelectedObliqueWires = {tip}; }
    std::vector<WirePointTypeDraw> getTipsOfSelectedObliqueWires(void) const { return mTipsOfSelectedObliqueWires; }
    WirePointTypeDraw getTipOfSelectedObliqueWire(void) const { if (mTipsOfSelectedObliqueWires.size()) return mTipsOfSelectedObliqueWires[0]; else return WirePointTypeDraw(); }

private slots:
    void onOpen();
    void onSave();
    void onAbout();
    void onSettings();

    void onRunCalculations(void);
    void onOutputFileChanged(const QString &);
    void onChangedCursorPosition(void);

private:
    void createActions(void);
    void createStatusBar(void);
    void createDockWindows(void);
    void setupEventHandlers(void);

    void openFileWithPath(QString path);
    QString getFilePath(void) const { return mFilePath; }
    void writeCurrentConfigurationData(QSettings &config);
    void readConfigurationFile(void);
    
    QString mFilePath;
    ModelFile mFile;
    std::vector<std::shared_ptr<ModelObject>> &mObjects;
    std::vector<std::shared_ptr<ModelObject>> mSelectedObjects;
    std::vector<WirePointTypeDraw> mTipsOfSelectedObliqueWires;
    int mCurrentLine{0};

    GeneralSettings mGeneralSettings;
    ModelViewSettings mModelViewSettings;
    
    QProcess *mElecodeProcess;
    QFileSystemWatcher *mEloFileWatcher;

    ModelView *mModelView;
    ModelEditor *mModelEditor;
    QTabWidget *mNotifications;
    QListWidget *mNoteList;
    QPlainTextEdit *mConsole;
    QMenu *mViewMenu;
    QMenu *mRunMenu;
    QMenu *mSettingsMenu;
    QMenu *mHelpMenu;
};


#endif  // MAINFRAME_H__


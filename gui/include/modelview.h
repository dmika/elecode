///
/// \file modelview.h
/// \brief 3D model.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef MODELVIEW_H__
#define MODELVIEW_H__


#include "modelfile.h"
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QMatrix4x4>
#include <qopengl.h>
#include <QVector>
#include <QVector3D>
#include <cmath>


class MainWindow;
class ModelView;
enum class WirePointTypeDraw;
class WireNodeDraw;


WireNodeDraw addWiresToNodeAutomatically(std::vector< std::shared_ptr<ModelObject> > &modelObjects, float mCellSize, Vector point);
void addSectorToCylinderData(QVector<GLfloat> &data, int &count, float x1, float y1, float x2, float y2, float length);
void addVertexToBuffer(QVector<GLfloat> &data, int &count, const QVector3D &v, QVector3D *n = nullptr);
void setMinVector(Vector &one, const Vector &two);
void setMaxVector(Vector &one, const Vector &two);


enum class WirePointTypeDraw {
    wireStart,
    wireEnd,
    wireNoPoint
};


class WireNodeDraw {
public:
    WireNodeDraw() {}
    WireNodeDraw(unsigned wireNumber, WirePointTypeDraw wirePoint) {
        WireNumber.push_back(wireNumber);
        WirePoint.push_back(wirePoint);
    }

    std::vector<unsigned> WireNumber;
    std::vector<WirePointTypeDraw> WirePoint;
    float MaxDiameter{0.0};
};


class OpenGlBuffer {
public:
    OpenGlBuffer(void) {}
    virtual ~OpenGlBuffer() {
        for (unsigned cnt = 0; cnt < mVbo.size(); ++cnt) {
            mVbo[cnt].destroy();
        }
    }

    ObjectType getObjectType(void) const { return mType; }

    void prepareVertex(QOpenGLBuffer &mVbo, QVector<GLfloat> &data);
    void prepareForDrawing(QOpenGLBuffer &vbo, ModelView &modelView, double *color, int size, int stride, int num = 2);

    virtual void draw(ModelView &modelView) = 0;

protected:
    ObjectType mType;
    std::vector<QOpenGLBuffer> mVbo;
    unsigned mPositionInFile{0};
    double mColor[3];
};


class SelectionBuffer : public OpenGlBuffer {
public:
    SelectionBuffer(void) {}

    virtual void draw(ModelView &modelView);
};


class NodeLineBuffer : public OpenGlBuffer {
public:
    NodeLineBuffer(void) {}

    void draw(ModelView &modelView);
};


class NoBuffer : public OpenGlBuffer {
public:
    NoBuffer(unsigned position);

    virtual void draw(ModelView &modelView) {}
};


class BlockBuffer : public OpenGlBuffer {
public:
    BlockBuffer(unsigned position, Vector first, Vector second, bool frameOnly = false);

    virtual void draw(ModelView &modelView);

protected:
    Vector mFirst;
    Vector mSecond;
    int mVertexCount;
    bool mFrameOnly;
};


class BaseWireBuffer : public OpenGlBuffer {
public:
    BaseWireBuffer() {}
    BaseWireBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision = 30);

    virtual void draw(ModelView &modelView);

    void addCylinder(QOpenGLBuffer &vbo, unsigned sectors, Vector start, Vector end, float diam);

protected:
    Vector mStart;
    Vector mEnd;
    float mDiameter{0.001};
    int mVertexCount;
};


class ObliqueWireBuffer : public BaseWireBuffer {
public:
    ObliqueWireBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision = 30);
    
    virtual void draw(ModelView &modelView);
    
    void addSphere(QOpenGLBuffer &vbo, float radius, unsigned sectors, unsigned rings, Vector center);
    void addSphereIndices(unsigned sectors, unsigned rings);
    
protected:
    std::vector<QOpenGLBuffer> mVboSphere;
    QVector<GLushort> mSphereIdx;
};


class ThinWireBuffer : public BaseWireBuffer {
public:
    ThinWireBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision = 30);

    virtual void draw(ModelView &modelView);
};


class StaircaseWireBuffer : public BaseWireBuffer {
public:
    StaircaseWireBuffer(unsigned position, Vector start, Vector end, float diam, float cellSize, unsigned precision = 30);

    virtual void draw(ModelView &modelView);
};


class SourceObjectBuffer : public BaseWireBuffer {
public:
    SourceObjectBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision = 30);

    virtual void draw(ModelView &modelView);
};


class CalculateObjectBuffer : public BaseWireBuffer {
public:
    CalculateObjectBuffer(unsigned position, std::vector<XyzPointFloat> coord, float diam, std::string calculateType, unsigned precision = 30);

    virtual void draw(ModelView &modelView);

protected:
    std::vector<XyzPointFloat> mCoordinates;
    std::string mCalculateType;
};


class ResistorBuffer : public BaseWireBuffer {
public:
    ResistorBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision = 30);

    virtual void draw(ModelView &modelView);
};


class CapacitorBuffer : public BaseWireBuffer {
public:
    CapacitorBuffer(unsigned position, Vector start, Vector end, float diam, unsigned precision = 30);

    virtual void draw(ModelView &modelView);
};


QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram)


class ModelView : public QOpenGLWidget, public QOpenGLFunctions {
    Q_OBJECT

public:
    ModelView(MainWindow*, std::vector<std::shared_ptr<ModelObject>> &objects);
    ~ModelView();

    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;

    void refreshBuffers(void);
    void refreshBuffer(int item);
    void resetView(void);

    MainWindow *mMainWindow;
    QPoint mCurRotPos;
    QPoint mCurPos;
    QPoint mCurMousePos;
    QPoint mCurSelectedWireEnd;

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    
    void drawModelObjects(void);
    void selectedObjectsToCenter(void);
    void handleSeveralObjectsSelection(QMouseEvent *event);
    bool isPointInRectangle(Vector point, QRect rect);
    void processWireNode(const QPoint &point);
    void selectObjectIfItWasHit(QMouseEvent *event);
    bool isThereHit(int mouseX, int mouseY, Vector pos, double radius);
    bool isWireHit(Vector start, Vector end, float diam, Vector pt, float &proximity);
    void updateNodeLine(void);
    void adjustDiameter(float &diameter);

    float mCellSize;
    float mScroll;
    bool mDefaultPosition;
    QPoint mRotPos;
    QPoint mRotPosSaved;
    float mRotationSpeedCoef{0.5};
    QPoint mCurScreenPosOrigin;
    Vector mOriginPos;
    Vector mOriginPosSaved;
    bool mDrawSelection{false};
    bool mIsItFirstWire{true};
    bool mDrawNodeLine{false};
    WireNodeDraw mCurrentNode;
    QOpenGLVertexArrayObject mVao;
    std::vector<std::shared_ptr<OpenGlBuffer>> mBuffers;
    SelectionBuffer mSelectionBuffer;
    NodeLineBuffer mNodeLineBuffer;
    QMatrix4x4 mWorld;
    QMatrix4x4 mProj;
    int mProjMatrix;
    int mMvMatrix;
    int mNormalMatrix;
    int mLightPos;
    QOpenGLShaderProgram *mProgram{nullptr};
    std::vector<std::shared_ptr<ModelObject>> &mObjects;
};


#endif  // MODELVIEW_H__


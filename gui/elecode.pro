QT += widgets
qtHaveModule(printsupport): QT += printsupport

TARGET = elecode

QMAKE_CXXFLAGS += -std=c++20 -O3 -DNDEBUG -D_UNICODE -DNOPCH -fopenmp -pedantic -Wall -Wextra -Wcast-align -Wcast-qual \
                  -Wformat=2 -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs \
                  -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion \
                  -Wstrict-null-sentinel -Wstrict-overflow=5 -Wundef \
                  -Wno-unused -Wno-strict-overflow -Wno-deprecated-declarations -Wno-sign-conversion
#-Wswitch-default -Wctor-dtor-privacy -Wsign-promo -Wnoexcept 

QMAKE_LFLAGS += -fopenmp
#QMAKE_LFLAGS += -no-pie

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += include ../cli/include ../common/include ../flashover/include ../permittivity/include ../fdtd/include

HEADERS = include/mainframe.h \
          include/modelview.h \
          include/settings.h \
          ../cli/include/modelfile.h \
          ../cli/include/cli.h \
          ../cli/include/runfdtd.h \
          ../cli/include/epsilon.h \
          ../cli/include/flashover.h \
          ../common/include/constants.h \
          ../common/include/vectors.h \
          ../common/include/interpolation.h \
          ../common/include/integration.h \
          ../common/include/fft.h \
          ../common/include/waveform.h \
          ../flashover/include/bfp.h \
          ../flashover/include/breakdown.h \
          ../permittivity/include/psols.h \
          ../permittivity/include/common.h \
          ../permittivity/include/soilmodel.h \
          ../permittivity/include/properties.h \
          ../permittivity/include/doi.h \
          ../fdtd/include/basic.h \
          ../fdtd/include/abc.h \
          ../fdtd/include/dispersion.h \
          ../fdtd/include/fdtd.h \
          ../fdtd/include/source.h \
          ../fdtd/include/output.h \
          ../fdtd/include/lumped.h \
          ../fdtd/include/shape.h \
          ../fdtd/include/wire.h \
          ../fdtd/include/obliquewire.h \
          ../fdtd/include/model.h

SOURCES = src/main.cpp \
          src/mainframe.cpp \
          src/modelview.cpp \
          src/settings.cpp \
          ../cli/src/modelfile.cpp \
          ../cli/src/cli.cpp \
          ../cli/src/runfdtd.cpp \
          ../cli/src/epsilon.cpp \
          ../cli/src/flashover.cpp \
          ../common/src/vectors.cpp \
          ../common/src/interpolation.cpp \
          ../common/src/integration.cpp \
          ../common/src/fft.cpp \
          ../common/src/waveform.cpp \
          ../flashover/src/bfp.cpp \
          ../flashover/src/breakdown.cpp \
          ../permittivity/src/psols.cpp \
          ../permittivity/src/common.cpp \
          ../permittivity/src/soilmodel.cpp \
          ../permittivity/src/properties.cpp \
          ../permittivity/src/doi.cpp \
          ../fdtd/src/basic.cpp \
          ../fdtd/src/abc.cpp \
          ../fdtd/src/dispersion.cpp \
          ../fdtd/src/fdtd.cpp \
          ../fdtd/src/source.cpp \
          ../fdtd/src/output.cpp \
          ../fdtd/src/lumped.cpp \
          ../fdtd/src/shape.cpp \
          ../fdtd/src/wire.cpp \
          ../fdtd/src/obliquewire.cpp \
          ../fdtd/src/model.cpp

RESOURCES = elecode.qrc



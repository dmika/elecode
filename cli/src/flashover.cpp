///
/// \file flashover.cpp
/// \brief Back flashover calculation.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2023, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "flashover.h"


float calcProbability(CurrentVoltage curvol, float gapDist, char type, float correl, std::string file)
{
    float prob = 0.0;
    std::deque<AmplitudeSteepness> asc;
    if (type == 'f') {
        file += "f";
    }
    else if (type == 's') {
        file += "s";
    }
    
    std::filesystem::path f(file);
    if (std::filesystem::exists(f)) {
        asc = readAscFile(std::filesystem::path(file));
    }
    else {
        std::vector<float> voltResponse(curvol.vol.size());
        for (unsigned cnt = 0; cnt < curvol.vol.size(); ++cnt) {
            voltResponse[cnt] = curvol.vol[cnt];
        }
        if (type == 'f') {
            asc = calcAscFast(new Cigre(1.0e-6, 77.5e-6), voltResponse, curvol.dt, gapDist, 5.0e3, 250.0e3, 5.0e9, 150.0e9);
        }
        else if (type == 's') {
            asc = calcAscFast(new Cigre(1.0e-6, 30.2e-6, 0.0, 1.0, 's'), voltResponse, curvol.dt, gapDist, 5.0e3, 250.0e3, 10.0e9, 150.0e9);
        }
        if (file != "") {
            std::ofstream outfile(file, std::fstream::out | std::fstream::app);
            for (unsigned cnt = 0; cnt < asc.size(); ++cnt) {
                outfile << asc[cnt].amplitude << "    " << asc[cnt].steepness << std::endl;
            }
            outfile.close();
        }
    }
    
    if (correl > 0.0) {
        if (type == 'f') {
            prob = calcProbabilityAscCorrel(asc, 27.7, 0.461, 24.3, 0.599, correl);
        }
        else if (type == 's') {
            prob = calcProbabilityAscCorrel(asc, 11.8, 0.530, 39.9, 0.852, correl);
        }
    }
    else {
        if (type == 'f') {
            prob = calcProbabilityAsc(asc, 27.7, 0.461, 24.3, 0.599);
        }
        else if (type == 's') {
            prob = calcProbabilityAsc(asc, 11.8, 0.530, 39.9, 0.852);
        }
    }

    return prob;
}


float calcProbabilityByAmplitude(CurrentVoltage curvol, float gapDist, char type, float front)
{
    float prob = 0;
    float amplitude = 0;
    
    std::vector<float> voltResponse;
    for (unsigned cnt = 0; cnt < curvol.vol.size(); ++cnt) {
        voltResponse.push_back(curvol.vol[cnt]);
    }

    if (type == 'f') {
        amplitude = calcAmplitude(new Cigre(1.0e-6, 77.5e-6), voltResponse, curvol.dt, gapDist, 5.0e3, 250.0e3, 24.3e9, front);
        prob = getProbability(amplitude, 27.7, 0.461);
    }
    else if (type == 's') {
        amplitude = calcAmplitude(new Cigre(1.0e-6, 30.2e-6, 0.0, 1.0, 's'), voltResponse, curvol.dt, gapDist, 5.0e3, 250.0e3, 39.9e9, front);
        prob = getProbability(amplitude, 11.8, 0.530);
    }

    return prob;
}


std::deque<AmplitudeSteepness> readAscFile(std::filesystem::path filepath)
{
    std::deque<AmplitudeSteepness> asc;
    std::ifstream infile(filepath);
    while (!infile.eof()) {
        std::string str;
        float amplitude, steepness;
        getline(infile, str);
        if (!str.empty()) {
            std::stringstream ss(str);
            ss >> amplitude;
            ss >> steepness;
            asc.push_back(AmplitudeSteepness(amplitude, steepness));
        }
    }
    return asc;
}


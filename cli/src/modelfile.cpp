///
/// \file modelfile.cpp
/// \brief Handling input model files. It reads file, performs check, and writes data to the file.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "modelfile.h"


const std::string ModelObject::mCommentSymbol = "#";
const std::string ThreadsObject::mCommandName = "threads";
const std::string VolumeObject::mCommandName = "volume";
const std::string AbcObject::mCommandName = "abc";
const std::string CalcTime::mCommandName = "calctime";
const std::string CalcOutput::mCommandName = "output";
const std::string Wire::mCommandName = "wire";
const std::string StaircaseWire::mWireTypeName = "staircase";
const std::string ThinWire::mWireTypeName = "thin";
const std::string ObliqueThinWire::mWireTypeName = "oblique";
const std::string Debye::mCommandName = "debye";
const std::string Block::mCommandName = "block";
const std::string Resistor::mCommandName = "resistor";
const std::string Capacitor::mCommandName = "capacitor";
const std::string Inductor::mCommandName = "inductor";
const std::string FunctionObject::mCommandName = "function";
const std::string SourceObject::mCommandName = "source";
const std::string CalculateObject::mCommandName = "calculate";


bool isSubstringInString(std::string subString, std::string inputString)
{
    return std::string::npos != inputString.find(subString, 0);
}


void readXyzPoint(std::istringstream &stringStream, XyzPointFloat &coord, unsigned line, unsigned firstArgument)
{
    std::string one;
    std::string two;
    std::string three;
    if (firstArgument) {
        one = std::to_string(firstArgument);
        two = std::to_string(firstArgument+1);
        three = std::to_string(firstArgument+2);
    }
    if (!(stringStream >> coord.x && coord.x >= 0.0)) {
        throw InputFileException("The argument " + one + " must be a positive number.", EXCEPT_ERROR, line);
    }
    if (!(stringStream >> coord.y && coord.y >= 0.0)) {
        throw InputFileException("The argument " + two + " must be a positive number.", EXCEPT_ERROR, line);
    }
    if (!(stringStream >> coord.z && coord.z >= 0.0)) {
        throw InputFileException("The argument " + three + " must be a positive number.", EXCEPT_ERROR, line);
    }
}


void unexpectedSymbol(std::istringstream &stringStream, unsigned line)
{
    std::string unexp;
    if (stringStream >> unexp) {
        throw InputFileException("Unexpected symbol(s) \"" + unexp + "\".", EXCEPT_ERROR, line);
    }
}


void writeParameterToStream(std::ostringstream &stringStream, unsigned param, bool chars)
{
    if (chars) stringStream << ",";
    stringStream << " " << param;
}


void writeParameterToStream(std::ostringstream &stringStream, float param, bool chars)
{
    if (chars) stringStream << ",";
    stringStream << " " << param;
}


void writeParameterToStream(std::ostringstream &stringStream, std::string param, bool chars)
{
    if (param != "") {
        if (chars) stringStream << ",";
        stringStream << " " << param;
    }
}


void ModelObject::writeComment(std::ostringstream &stringStream, bool chars)
{
    if (chars) stringStream << ")";
    if (mComment != "") {
        stringStream << " " << ModelObject::mCommentSymbol << mComment;
    }
}


void ModelObject::readDataFromString(std::string inputString)
{
    size_t commentSym = inputString.find(ModelObject::mCommentSymbol);
    if (commentSym != std::string::npos) {
        mComment = inputString.substr(commentSym + 1, std::string::npos);
        inputString = inputString.substr(0, commentSym);
    }
    std::istringstream iss(prepareStringForDataReading(inputString));
    getDataFromStringStream(iss);
}


std::string ModelObject::prepareStringForDataReading(std::string inputString)
{
    std::string spacestr{" "};
    // replace '(' at the start of the string by ' '
    size_t curr = inputString.find('(');
    if (curr != std::string::npos) {
        inputString.replace(curr, 1, spacestr);
    }
    // delete ')' and everything that is after
    inputString = inputString.substr(0, inputString.find(")"));
    // replace all ',' by ' '
    for (size_t cur = inputString.find(','); cur != std::string::npos; cur = inputString.find(',', cur+1)) {
        inputString.replace(cur, 1, spacestr);
    }
    return inputString;
}


void ThreadsObject::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    int num;
    if (stringStream >> num && num > 0) {
        mThreadsNumber = static_cast<unsigned>(num);
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
}


std::string ThreadsObject::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << mThreadsNumber;
    writeComment(oss, chars);
    return oss.str();
}


void VolumeObject::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readXyzPoint(stringStream, mVolumeSize, mPositionInFile+1, 1);
    if (!(stringStream >> mCellSize && mCellSize > 0.0)) {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (stringStream >> mCourantCoef) {
        if (mCourantCoef > 0.0 && mCourantCoef <= 1.0) {
            unexpectedSymbol(stringStream, mPositionInFile+1);
        }
        else {
            throw InputFileException("The argument must be a positive number (not greater than 1.0).", EXCEPT_ERROR, mPositionInFile+1);
        }
    }
    else {
        mCourantCoef = 1.0;
    }
}


std::string VolumeObject::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << mVolumeSize.x;
    writeParameterToStream(oss, mVolumeSize.y, chars);
    writeParameterToStream(oss, mVolumeSize.z, chars);
    writeParameterToStream(oss, mCellSize, chars);
    writeParameterToStream(oss, mCourantCoef, chars);
    writeComment(oss, chars);
    return oss.str();
}


void AbcObject::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    stringStream >> mAbcType;
    if (mAbcType != "cpml" && mAbcType != "upml") {
        throw InputFileException("Wrong ABC type. \"upml\" or \"cpml\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> mThickness && mThickness >= 0.0)) {
        throw InputFileException("The argument 2 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (mAbcType == "upml") {
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else if (mAbcType == "cpml") {
        if (!(stringStream >> mSigmaCoef && mSigmaCoef > 0.0)) {
            throw InputFileException("The argument 3 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mKappaMax && mKappaMax > 0.0)) {
            throw InputFileException("The argument 4 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mAmax && mAmax >= 0.0)) {
            throw InputFileException("The argument 5 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mM && mM > 0.0)) {
            throw InputFileException("The argument 6 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mMa && mMa > 0.0)) {
            throw InputFileException("The argument 7 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mRegion)) {
            mRegion = "all";
        }
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
}


std::string AbcObject::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << mAbcType;
    writeParameterToStream(oss, mThickness, chars);
    if (mAbcType == "cpml") {
        writeParameterToStream(oss, mSigmaCoef, chars);
        writeParameterToStream(oss, mKappaMax, chars);
        writeParameterToStream(oss, mAmax, chars);
        writeParameterToStream(oss, mM, chars);
        writeParameterToStream(oss, mMa, chars);
        writeParameterToStream(oss, mRegion, chars);
    }
    writeComment(oss, chars);
    return oss.str();
}


void CalcTime::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (stringStream >> mCalculationTime && mCalculationTime > 0.0) {
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
}


std::string CalcTime::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << mCalculationTime;
    writeComment(oss, chars);
    return oss.str();
}


void CalcOutput::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> mDataDt && mDataDt > 0.0)) {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (stringStream >> mFileName) {
        if (mFileName == "*") {
            mCalcResult = true;
        }
        int interval;
        if (stringStream >> interval) {
            if (interval < 0) {
                throw InputFileException("The argument 3 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            else {
                mWriteInterval = static_cast<unsigned>(interval);
            }
        }
    }
    unexpectedSymbol(stringStream, mPositionInFile+1);
}


std::string CalcOutput::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << mDataDt;
    writeParameterToStream(oss, mFileName, chars);
    if (mWriteInterval != 1000) {
        writeParameterToStream(oss, mWriteInterval, chars);
    }
    writeComment(oss, chars);
    return oss.str();
}


std::string TwoPointObject::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mStart.x;
    writeParameterToStream(oss, mStart.y, chars);
    writeParameterToStream(oss, mStart.z, chars);
    writeParameterToStream(oss, mEnd.x, chars);
    writeParameterToStream(oss, mEnd.y, chars);
    writeParameterToStream(oss, mEnd.z, chars);
    return oss.str();
}


void StaircaseWire::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    stringStream >> name;
    if (name != mWireTypeName) {
        throw InputFileException("Wrong wire type. \"" + mWireTypeName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readXyzPoint(stringStream, mStart, mPositionInFile+1, 2);
    readXyzPoint(stringStream, mEnd, mPositionInFile+1, 5);
    std::string corr;
    if (stringStream >> corr) {
        if (corr == "correct") {
            mWithCorrection = true;
        }
        else {
            throw InputFileException("Unexpected symbol(s) \"" + corr + "\".", EXCEPT_ERROR, mPositionInFile+1);
        }
    }
}


std::string StaircaseWire::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << Wire::mCommandName << " ";
    if (chars) oss << "(";
    oss << StaircaseWire::mWireTypeName;
    if (chars) oss << "," << " ";
    oss << TwoPointObject::writeDataToString(chars);
    if (mWithCorrection) {
        writeParameterToStream(oss, "correct", chars);
    }
    writeComment(oss, chars);
    return oss.str();
}


void ThinWire::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    stringStream >> name;
    if (name != mWireTypeName) {
        throw InputFileException("Wrong wire type. \"" + mWireTypeName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readXyzPoint(stringStream, mStart, mPositionInFile+1, 2);
    readXyzPoint(stringStream, mEnd, mPositionInFile+1, 5);
    if (!(stringStream >> mDiameter && mDiameter > 0.0)) {
        throw InputFileException("The argument 8 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (stringStream >> mSubType) {
    }
    if (stringStream >> mEpsilon) {
        if (mEpsilon >= 1.0) {
            unexpectedSymbol(stringStream, mPositionInFile+1);
        }
        else {
            throw InputFileException("The argument must be a positive number (not smaller than 1.0).", EXCEPT_ERROR, mPositionInFile+1);
        }
    }
    else {
        mEpsilon = 1.0;
    }
}


std::string ThinWire::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << Wire::mCommandName << " ";
    if (chars) oss << "(";
    oss << ThinWire::mWireTypeName;
    if (chars) oss << "," << " ";
    oss << TwoPointObject::writeDataToString(chars);
    writeParameterToStream(oss, mDiameter, chars);
    if (mSubType == 't') {
        if (chars) oss << ",";
        oss << " " << mSubType;
    }
    if (mEpsilon > 1.0) {
        writeParameterToStream(oss, mEpsilon, chars);
    }
    writeComment(oss, chars);
    return oss.str();
}


void ObliqueThinWire::readAdditionalArguments(std::istringstream &stringStream)
{
    std::string arg;
    int num;
    if (stringStream >> arg) {
        if (arg == "cursrc") {
            mCurrentSource = true;
            stringStream >> mName;
            if (stringStream >> num) {
                if (num >= 0) {
                    mNodeSegmentSource = static_cast<unsigned>(num);
                }
                else {
                    throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
                }
            }
            else {
                mNodeSegmentSource = 0;
                if (stringStream.fail()) {
                    stringStream.clear();
                }
            }
            readAdditionalArguments(stringStream);
        }
        else if (arg == "curcalc") {
            mCurrentCalc = true;
            if (stringStream >> num) {
                if (num >= 0) {
                    mNodeSegmentCalc = static_cast<unsigned>(num);
                }
                else {
                    throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
                }
            }
            else {
                mNodeSegmentCalc = 0;
                if (stringStream.fail()) {
                    stringStream.clear();
                }
            }
            readAdditionalArguments(stringStream);
        }
        else if (arg == "volsrc") {
            mVoltageSource = true;
            stringStream >> mName;
            if (stringStream >> num) {
                if (num >= 0) {
                    mNodeSegmentSource = static_cast<unsigned>(num);
                    if (stringStream >> mResistance) {
                        if (mResistance < 0.0) {
                            throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
                        }
                    }
                    else {
                        mResistance = 0.0;
                        if (stringStream.fail()) {
                            stringStream.clear();
                        }
                    }
                }
                else {
                    throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
                }
            }
            else {
                mNodeSegmentSource = 0;
                if (stringStream.fail()) {
                    stringStream.clear();
                }
            }
            readAdditionalArguments(stringStream);
        }
        else if (arg == "volcalc") {
            mVoltageCalc = true;
            readAdditionalArguments(stringStream);
        }
        else if (arg == "segnum") {
            if (stringStream >> num) {
                if (num >= 0) {
                    mNumberOfSegments = static_cast<unsigned>(num);
                }
                else {
                    throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
                }
            }
            else {
                mNumberOfSegments = 0;
                if (stringStream.fail()) {
                    stringStream.clear();
                }
            }
            readAdditionalArguments(stringStream);
        }
        else if (arg == "sheath") {
            if (!(stringStream >> mSheathDiameter && mSheathDiameter > 0.0)) {
                throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            if (!(stringStream >> mSheathEpsilon && mSheathEpsilon > 0.0)) {
                throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            if (!(stringStream >> mSheathSigma && mSheathSigma >= 0.0)) {
                throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            readAdditionalArguments(stringStream);
        }
        else if (arg == "sep") {
            mSeparated = true;
            readAdditionalArguments(stringStream);
        }
        else if (arg == "infend") {
            mInfiniteEnd = true;
            readAdditionalArguments(stringStream);
        }
        else {
            throw InputFileException("Unexpected symbol(s) \"" + arg + "\".", EXCEPT_ERROR, mPositionInFile+1);
        }
    }
}


void ObliqueThinWire::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    stringStream >> name;
    if (name != mWireTypeName) {
        throw InputFileException("Wrong wire type. \"" + mWireTypeName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readXyzPoint(stringStream, mStart, mPositionInFile+1, 2);
    readXyzPoint(stringStream, mEnd, mPositionInFile+1, 5);
    if (!(stringStream >> mDiameter && mDiameter > 0.0)) {
        throw InputFileException("The argument 8 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }

    readAdditionalArguments(stringStream);
}


std::string ObliqueThinWire::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << Wire::mCommandName << " ";
    if (chars) oss << "(";
    oss << ObliqueThinWire::mWireTypeName;
    if (chars) oss << "," << " ";
    oss << TwoPointObject::writeDataToString(chars);
    writeParameterToStream(oss, mDiameter, chars);
    if (mCurrentSource) {
        writeParameterToStream(oss, "cursrc", chars);
        writeParameterToStream(oss, mName, chars);
        writeParameterToStream(oss, mNodeSegmentSource, chars);
    }
    else if (mVoltageSource) {
        writeParameterToStream(oss, "volsrc", chars);
        writeParameterToStream(oss, mName, chars);
        writeParameterToStream(oss, mNodeSegmentSource, chars);
        writeParameterToStream(oss, mResistance, chars);
    }
    if (mCurrentCalc) {
        writeParameterToStream(oss, "curcalc", chars);
        writeParameterToStream(oss, mNodeSegmentCalc, chars);
    }
    else if (mVoltageCalc) {
        writeParameterToStream(oss, "volcalc", chars);
    }
    if (mNumberOfSegments) {
        writeParameterToStream(oss, "segnum", chars);
        writeParameterToStream(oss, mNumberOfSegments, chars);
    }
    if (mSheathDiameter > mDiameter) {
        writeParameterToStream(oss, "sheath", chars);
        writeParameterToStream(oss, mSheathDiameter, chars);
        writeParameterToStream(oss, mSheathEpsilon, chars);
        writeParameterToStream(oss, mSheathSigma, chars);
    }
    if (mSeparated) {
        writeParameterToStream(oss, "sep", chars);
    }
    if (mInfiniteEnd) {
        writeParameterToStream(oss, "infend", chars);
    }
    writeComment(oss, chars);
    return oss.str();
}


void Debye::readArguments(std::istringstream &stringStream)
{
    float eps;
    if (stringStream >> eps) {
        if (eps >= 0.0) {
            mDeltaEpsilon.push_back(eps);
            float tau;
            if (stringStream >> tau) {
                if (tau >= 0.0) {
                    mTau.push_back(tau);
                    readArguments(stringStream);
                }
                else {
                    throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
                }
            }
            else {
                throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
            }
        }
        else {
            throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
    }
}


void Debye::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> mName)) {
        throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readArguments(stringStream);
}


std::string Debye::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << mName;
    for (unsigned cnt = 0; cnt < mDeltaEpsilon.size(); ++cnt) {
        writeParameterToStream(oss, mDeltaEpsilon[cnt], chars);
        writeParameterToStream(oss, mTau[cnt], chars);
    }
    writeComment(oss, chars);
    return oss.str();
}


void Block::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readXyzPoint(stringStream, mStart, mPositionInFile+1, 1);
    readXyzPoint(stringStream, mEnd, mPositionInFile+1, 4);
    if (!(stringStream >> mEpsilon && mEpsilon >= 1.0)) {
        throw InputFileException("The argument 7 must be a positive number (not smaller than 1.0).", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> mSigma && mSigma >= 0.0)) {
        throw InputFileException("The argument 8 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    stringStream >> mDebyeName;
    unexpectedSymbol(stringStream, mPositionInFile+1);
}


std::string Block::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << TwoPointObject::writeDataToString(chars);
    writeParameterToStream(oss, mEpsilon, chars);
    writeParameterToStream(oss, mSigma, chars);
    writeParameterToStream(oss, mDebyeName, chars);
    writeComment(oss, chars);
    return oss.str();
}


void Resistor::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readXyzPoint(stringStream, mStart, mPositionInFile+1, 1);
    readXyzPoint(stringStream, mEnd, mPositionInFile+1, 4);
    if (!(stringStream >> mResistance && mResistance > 0.0)) {
        throw InputFileException("The argument 7 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    unexpectedSymbol(stringStream, mPositionInFile+1);
}


std::string Resistor::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << TwoPointObject::writeDataToString(chars);
    writeParameterToStream(oss, mResistance, chars);
    writeComment(oss, chars);
    return oss.str();
}


void Capacitor::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readXyzPoint(stringStream, mStart, mPositionInFile+1, 1);
    readXyzPoint(stringStream, mEnd, mPositionInFile+1, 4);
    if (!(stringStream >> mCapacitance && mCapacitance >= 0.0)) {
        throw InputFileException("The argument 7 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (stringStream >> mConductance) {
        if (mConductance < 0.0) {
            throw InputFileException("The argument 6 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
    }
    else {
        mConductance = 0.0;
    }
    unexpectedSymbol(stringStream, mPositionInFile+1);
}


std::string Capacitor::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << TwoPointObject::writeDataToString(chars);
    writeParameterToStream(oss, mCapacitance, chars);
    if (mConductance > 0.0) {
        writeParameterToStream(oss, mConductance, chars);
    }
    writeComment(oss, chars);
    return oss.str();
}


void Inductor::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    readXyzPoint(stringStream, mStart, mPositionInFile+1, 1);
    readXyzPoint(stringStream, mEnd, mPositionInFile+1, 4);
    if (!(stringStream >> mInductance && mInductance > 0.0)) {
        throw InputFileException("The argument 7 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    unexpectedSymbol(stringStream, mPositionInFile+1);
}


std::string Inductor::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << TwoPointObject::writeDataToString(chars);
    writeParameterToStream(oss, mInductance, chars);
    writeComment(oss, chars);
    return oss.str();
}


void FunctionObject::readHeidlerParameters(std::istringstream &stringStream, HeidlerFuncParameters &param)
{
    if (!(stringStream >> param.mTailCoef && param.mTailCoef >= 0.0)) {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> param.mExponent && param.mExponent >= 0.0)) {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> param.mAmplitude && param.mAmplitude >= 0.0)) {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    float front;
    if (stringStream >> front) {
        mHeidlerFuncParameters.push_back(HeidlerFuncParameters(front, 1.0, 10.0, 1.0));
        readHeidlerParameters(stringStream, mHeidlerFuncParameters[mHeidlerFuncParameters.size()-1]);
    }
}


void FunctionObject::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> mName)) {
        throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> mFunctionType)) {
        throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
    }

    if (mFunctionType == "heidler") {
        if (!(stringStream >> mSubType)) {
            throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
        }
        else if (mSubType == "s") {
            if (!(stringStream >> mFront && mFront >= 0.0)) {
                throw InputFileException("The argument 4 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            if (!(stringStream >> mTailCoef && mTailCoef >= 0.0)) {
                throw InputFileException("The argument 5 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            if (!(stringStream >> mCorrectionCoef && mCorrectionCoef >= 0.0)) {
                throw InputFileException("The argument 6 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            if (!(stringStream >> mExponent && mExponent >= 0.0)) {
                throw InputFileException("The argument 7 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            if (stringStream >> mAmplitude);
            else {
                mAmplitude = 1.0;
            }
            if (stringStream >> mTimeShift);
            else {
                mTimeShift = 0.0;
            }
        }
        else if (mSubType == "m") {
            if (!(stringStream >> mTimeShift)) {
                throw InputFileException("The argument 4 must be a number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            float front;
            if (stringStream >> front) {
                mHeidlerFuncParameters.push_back(HeidlerFuncParameters(front, 1.0, 10.0, 1.0));
                readHeidlerParameters(stringStream, mHeidlerFuncParameters[mHeidlerFuncParameters.size()-1]);
            }
        }
        else if (mSubType == "mtle") {
            if (!(stringStream >> mLambda)) {
                throw InputFileException("The argument 4 must be a number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            if (!(stringStream >> mSpeed)) {
                throw InputFileException("The argument 5 must be a number.", EXCEPT_ERROR, mPositionInFile+1);
            }
            float front;
            if (stringStream >> front) {
                mHeidlerFuncParameters.push_back(HeidlerFuncParameters(front, 1.0, 10.0, 1.0));
                readHeidlerParameters(stringStream, mHeidlerFuncParameters[mHeidlerFuncParameters.size()-1]);
            }
        }
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else if (mFunctionType == "cigre") {
        if (!(stringStream >> mSubType)) {
            throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mFront && mFront >= 0.0)) {
            throw InputFileException("The argument 4 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mTimeToHalf && mTimeToHalf >= 0.0)) {
            throw InputFileException("The argument 5 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mSteepness && mSteepness >= 0.0)) {
            throw InputFileException("The argument 6 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (stringStream >> mAmplitude);
        else {
            mAmplitude = 1.0;
        }
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else if (mFunctionType == "ramp") {
        if (!(stringStream >> mFront && mFront >= 0.0)) {
            throw InputFileException("The argument 3 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mTimeToHalf && mTimeToHalf >= 0.0)) {
            throw InputFileException("The argument 4 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (stringStream >> mAmplitude);
        else {
            mAmplitude = 1.0;
        }
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else if (mFunctionType == "step") {
        if (!(stringStream >> mFront && mFront > 0.0)) {
            throw InputFileException("The argument 3 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else if (mFunctionType == "sine") {
        if (!(stringStream >> mFrequency && mFrequency >= 0.0)) {
            throw InputFileException("The argument 3 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (stringStream >> mAmplitude);
        else {
            mAmplitude = 1.0;
        }
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else if (mFunctionType == "gaussian") {
        if (!(stringStream >> mB)) {
            throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mC)) {
            throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (stringStream >> mAmplitude);
        else {
            mAmplitude = 1.0;
        }
        if (stringStream >> mSubType);
        else {
            mSubType = "s";
        }
        unexpectedSymbol(stringStream, mPositionInFile+1);
    }
    else if (mFunctionType == "custom") {
        if (!(stringStream >> mDt && mDt > 0.0)) {
            throw InputFileException("The argument 3 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        float tmpfl;
        while (stringStream >> tmpfl) {
            mDataArray.push_back (tmpfl);
        }
    }
}


std::string FunctionObject::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    oss << "(";
    oss << mName;
    writeParameterToStream(oss, mFunctionType, chars);
    if (mFunctionType == "heidler") {
        writeParameterToStream(oss, mSubType, chars);
        if (mSubType == "s") {
            writeParameterToStream(oss, mFront, chars);
            writeParameterToStream(oss, mTailCoef, chars);
            writeParameterToStream(oss, mCorrectionCoef, chars);
            writeParameterToStream(oss, mExponent, chars);
            writeParameterToStream(oss, mAmplitude, chars);
            writeParameterToStream(oss, mTimeShift, chars);
        }
        else if (mSubType == "m") {
            writeParameterToStream(oss, mTimeShift, chars);
            for (unsigned cnt = 0; cnt < mHeidlerFuncParameters.size(); ++cnt) {
                writeParameterToStream(oss, mHeidlerFuncParameters[cnt].mFrontCoef, chars);
                writeParameterToStream(oss, mHeidlerFuncParameters[cnt].mTailCoef, chars);
                writeParameterToStream(oss, mHeidlerFuncParameters[cnt].mExponent, chars);
                writeParameterToStream(oss, mHeidlerFuncParameters[cnt].mAmplitude, chars);
            }
        }
        else if (mSubType == "mtle") {
            writeParameterToStream(oss, mLambda, chars);
            writeParameterToStream(oss, mSpeed, chars);
            for (unsigned cnt = 0; cnt < mHeidlerFuncParameters.size(); ++cnt) {
                writeParameterToStream(oss, mHeidlerFuncParameters[cnt].mFrontCoef, chars);
                writeParameterToStream(oss, mHeidlerFuncParameters[cnt].mTailCoef, chars);
                writeParameterToStream(oss, mHeidlerFuncParameters[cnt].mExponent, chars);
                writeParameterToStream(oss, mHeidlerFuncParameters[cnt].mAmplitude, chars);
            }
        }
    }
    else if (mFunctionType == "cigre") {
        writeParameterToStream(oss, mSubType, chars);
        writeParameterToStream(oss, mFront, chars);
        writeParameterToStream(oss, mTimeToHalf, chars);
        writeParameterToStream(oss, mSteepness, chars);
        writeParameterToStream(oss, mAmplitude, chars);
    }
    else if (mFunctionType == "ramp") {
        writeParameterToStream(oss, mFront, chars);
        writeParameterToStream(oss, mTimeToHalf, chars);
        writeParameterToStream(oss, mAmplitude, chars);
    }
    else if (mFunctionType == "step") {
        writeParameterToStream(oss, mFront, chars);
    }
    else if (mFunctionType == "sine") {
        writeParameterToStream(oss, mFrequency, chars);
        writeParameterToStream(oss, mAmplitude, chars);
    }
    else if (mFunctionType == "gaussian") {
        writeParameterToStream(oss, mB, chars);
        writeParameterToStream(oss, mC, chars);
        writeParameterToStream(oss, mAmplitude, chars);
        writeParameterToStream(oss, mSubType, chars);
    }
    else if (mFunctionType == "custom") {
        writeParameterToStream(oss, mDt, chars);
        if (chars) oss << ",";
        oss << " " << std::endl;
        unsigned cnt;
        for (cnt = 0; cnt < mDataArray.size(); ++cnt) {
            oss << mDataArray[cnt] << " ";
            if (!((cnt+1)%25)) {
                oss << '\n';
            }
        }
    }
    writeComment(oss, chars);
    return oss.str();
}


void SourceObject::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> mSourceType)) {
        throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (mSourceType == "tfsf") {
        if (!(stringStream >> mTheta && mTheta >= 0.0)) {
            throw InputFileException("The argument 2 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mPhi && mPhi >= 0.0)) {
            throw InputFileException("The argument 3 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mPsi)) {
            throw InputFileException("The argument 4 must be a number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        if (!(stringStream >> mDist && mDist >= 0)) {
            throw InputFileException("The argument 5 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        stringStream >> mName;
    }
    else {
        readXyzPoint(stringStream, mStart, mPositionInFile+1, 2);
        readXyzPoint(stringStream, mEnd, mPositionInFile+1, 5);
        if (!(stringStream >> mConductanceResistance && mConductanceResistance >= 0.0)) {
            throw InputFileException("The argument 8 must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
        }
        stringStream >> mName;
        if (stringStream >> mSubType);
        else {
            mSubType = 'n';
        }
    }
    unexpectedSymbol(stringStream, mPositionInFile+1);
}


std::string SourceObject::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << mSourceType;
    if (mSourceType == "tfsf") {
        writeParameterToStream(oss, mTheta, chars);
        writeParameterToStream(oss, mPhi, chars);
        writeParameterToStream(oss, mPsi, chars);
        writeParameterToStream(oss, mDist, chars);
    }
    else {
        if (chars) oss << ",";
        oss << " " << TwoPointObject::writeDataToString(chars);
        writeParameterToStream(oss, mConductanceResistance, chars);
        writeParameterToStream(oss, mName, chars);
        if (mSubType != 'n') {
            if (chars) oss << ",";
            oss << " " << mSubType;
        }
    }
    writeComment(oss, chars);
    return oss.str();
}


void CalculateObject::readYzCoordinates(std::istringstream &stringStream, XyzPointFloat &coord)
{
    if (!(stringStream >> coord.y && coord.y >= 0.0)) {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> coord.z && coord.z >= 0.0)) {
        throw InputFileException("The argument must be a positive number.", EXCEPT_ERROR, mPositionInFile+1);
    }
    float num;
    if (stringStream >> num) {
        mCoordinates.push_back(XyzPointFloat(num, 0.0, 0.0));
        readYzCoordinates(stringStream, mCoordinates[mCoordinates.size()-1]);
    }
}


void CalculateObject::getDataFromStringStream(std::istringstream &stringStream)
{
    std::string name;
    stringStream >> name;
    if (name != mCommandName) {
        throw InputFileException("Wrong command name. \"" + mCommandName + "\" is expected.", EXCEPT_ERROR, mPositionInFile+1);
    }
    if (!(stringStream >> mCalculateType)) {
        throw InputFileException("Wrong command.", EXCEPT_ERROR, mPositionInFile+1);
    }

    float num;
    if (stringStream >> num) {
        mCoordinates.push_back(XyzPointFloat(num, 0.0, 0.0));
        readYzCoordinates(stringStream, mCoordinates[0]);
    }
}


std::string CalculateObject::writeDataToString(bool chars)
{
    std::ostringstream oss;
    oss << mCommandName << " ";
    if (chars) oss << "(";
    oss << mCalculateType;
    for (unsigned cnt = 0; cnt < mCoordinates.size(); ++cnt) {
        writeParameterToStream(oss, mCoordinates[cnt].x, chars);
        writeParameterToStream(oss, mCoordinates[cnt].y, chars);
        writeParameterToStream(oss, mCoordinates[cnt].z, chars);
    }
    writeComment(oss, chars);
    return oss.str();
}


ModelFile::ModelFile(std::string filePath)
{
    mFilePath = filePath;
}


void ModelFile::processLineInFstream(unsigned &linenum, std::istream &inf)
{
    std::string lineString;
    getline(inf, lineString);
    
    size_t curr = lineString.find('\r');
    if (curr != std::string::npos) {
        lineString.replace(curr, 1, "");
    }
    
    std::string commandString = lineString.substr(0, lineString.find(ModelObject::mCommentSymbol)); // remove comments
    std::istringstream issc(commandString.substr(0, commandString.find("(")));
    issc >> commandString;
    
    if (commandString == ThreadsObject::mCommandName) {
        mObjects.push_back(std::make_shared<ThreadsObject>(lineString, linenum));
    }
    else if (commandString == VolumeObject::mCommandName) {
        mObjects.push_back(std::make_shared<VolumeObject>(lineString, linenum));
    }
    else if (commandString == AbcObject::mCommandName) {
        mObjects.push_back(std::make_shared<AbcObject>(lineString, linenum));
    }
    else if (commandString == CalcTime::mCommandName) {
        mObjects.push_back(std::make_shared<CalcTime>(lineString, linenum));
    }
    else if (commandString == CalcOutput::mCommandName) {
        mObjects.push_back(std::make_shared<CalcOutput>(lineString, linenum));
    }
    else if (commandString == Wire::mCommandName) {
        std::istringstream iss(ModelObject::prepareStringForDataReading(lineString));
        std::string name;
        iss >> name;
        iss >> name;
        if (name == StaircaseWire::mWireTypeName) {
            mObjects.push_back(std::make_shared<StaircaseWire>(lineString, linenum));
        }
        else if (name == ThinWire::mWireTypeName) {
            mObjects.push_back(std::make_shared<ThinWire>(lineString, linenum));
        }
        else if (name == ObliqueThinWire::mWireTypeName) {
            mObjects.push_back(std::make_shared<ObliqueThinWire>(lineString, linenum));
        }
        else {
            throw InputFileException("Wrong command.", EXCEPT_WARNING, linenum+1);
        }
    }
    else if (commandString == Debye::mCommandName) {
        mObjects.push_back(std::make_shared<Debye>(lineString, linenum));
    }
    else if (commandString == Block::mCommandName) {
        mObjects.push_back(std::make_shared<Block>(lineString, linenum));
    }
    else if (commandString == Resistor::mCommandName) {
        mObjects.push_back(std::make_shared<Resistor>(lineString, linenum));
    }
    else if (commandString == Capacitor::mCommandName) {
        mObjects.push_back(std::make_shared<Capacitor>(lineString, linenum));
    }
    else if (commandString == Inductor::mCommandName) {
        mObjects.push_back(std::make_shared<Inductor>(lineString, linenum));
    }
    else if (commandString == FunctionObject::mCommandName) {
        mObjects.push_back(std::make_shared<FunctionObject>(inf, lineString, linenum));
    }
    else if (commandString == SourceObject::mCommandName) {
        mObjects.push_back(std::make_shared<SourceObject>(lineString, linenum));
    }
    else if (commandString == CalculateObject::mCommandName) {
        mObjects.push_back(std::make_shared<CalculateObject>(lineString, linenum));
    }
    else if (commandString != "\r" && commandString != "") {
        throw InputFileException("Wrong command name?", EXCEPT_WARNING, linenum+1);
    }
    else {
        mObjects.push_back(std::make_shared<ModelObject>(lineString, linenum));
    }
}


void ModelFile::readDataFromStream(std::istream &inf)
{
    std::string errorMessage;
    unsigned linenum = 0;
    bool throwError = false;

    for (; !inf.eof(); ++linenum) {
        unsigned position = 0;
        try {
            position = inf.tellg();
            processLineInFstream(linenum, inf);
        }
        catch (const InputFileException &e) {
            errorMessage += e.getMessage() + "\n";
            if (e.getExceptionSeverity() == EXCEPT_ERROR) {
                throwError = true;
            }
            
            inf.clear();
            inf.seekg(position, std::ios::beg);
            std::string lineString;
            getline(inf, lineString);
            size_t curr = lineString.find('\r');
            if (curr != std::string::npos) {
                lineString.replace(curr, 1, "");
            }
            mObjects.push_back(std::make_shared<ModelObject>(lineString, linenum));
        }
    }

    try {
        performModelCheck();
    }
    catch (const InputFileException &e) {
        errorMessage += e.getMessage();
        if (e.getExceptionSeverity() == EXCEPT_ERROR) {
            throwError = true;
        }
    }

    if (errorMessage != "") {
        if (throwError) {
            throw InputFileException(errorMessage, EXCEPT_ERROR, 0, false);
        }
        else {
            throw InputFileException(errorMessage, EXCEPT_WARNING, 0, false);
        }
    }
}


void ModelFile::readFile(void)
{
    std::ifstream inf(mFilePath.c_str(), std::ios::binary);
    if (!inf.good()) {
        throw InputFileException("File can't be opened.");
    }
    readDataFromStream(inf);
}


void ModelFile::performModelCheck(void)
{
    bool throwError = false;
    std::string errorMessage;

    // Volume
    XyzSizeFloat volumeSize;
    float cellSize = 1.0;
    unsigned comnum = 0;
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::volume) {
            ++comnum;
            if (comnum == 1) {
                volumeSize = static_cast<VolumeObject*>(object.get())->getVolumeSize();
                cellSize = static_cast<VolumeObject*>(object.get())->getCellSize();
            }
        }
    }
    if (comnum == 0) {
        errorMessage += "Input file error. The \"volume\" command is not set.\n";
        throwError = true;
    }
    else if (comnum > 1) {
        errorMessage += "Input file warning. The \"volume\" command is set more than once.\n";
    }

    // ABC
    unsigned comnumu = 0;
    unsigned comnumc = 0;
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::abc) {
            if (static_cast<AbcObject*>(object.get())->getAbcType() == "upml") {
                ++comnumu;
            }
            else if (static_cast<AbcObject*>(object.get())->getAbcType() == "cpml") {
                ++comnumc;
            }
        }
    }
    if (comnumu == 0 && comnumc == 0) {
        errorMessage += "Input file warning. The \"abc\" command is not set.\n";
    }
    if (comnumu > 1) {
        errorMessage += "Input file warning. The \"abc\" command is used more than once for UPML.\n";
    }
    if (comnumu && comnumc) {
        errorMessage += "Input file warning. UPML is used together with CPML.\n";
    }

    // CalcTime
    comnum = 0;
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::calcTime) {
            ++comnum;
        }
    }
    if (comnum == 0) {
        errorMessage += "Input file error. The \"calctime\" command is not set.\n";
        throwError = true;
    }
    else if (comnum > 1) {
        errorMessage += "Input file warning. The \"calctime\" command is set more than once.\n";
    }

    // CalcOutput
    comnum = 0;
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::calcOutput) {
            ++comnum;
        }
    }
    if (comnum == 0) {
        errorMessage += "Input file warning. The \"output\" command is not set.\n";
    }
    else if (comnum > 1) {
        errorMessage += "Input file warning. The \"output\" command is set more than once.\n";
    }

    // Debye
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::debye) {
            comnum = 0;
            for (const auto &block : mObjects) {
                if (block->getObjectType() == ObjectType::block && static_cast<Debye*>(object.get())->getName() == static_cast<Block*>(block.get())->getDebyeName()) {
                    ++comnum;
                }
            }
            if (comnum == 0) {
                errorMessage += "Input file warning. Line " + std::to_string(static_cast<Debye*>(object.get())->getPositionInFile()+1) +
                                ". There is no block with these Debye parameters.\n";
            }
        }
    }
    
    // Check consistency of TwoPointObject coordinates
    for (const auto &object : mObjects) {
        ObjectType otype = object->getObjectType();
        if (otype == ObjectType::staircaseWire || otype == ObjectType::thinWire || otype == ObjectType::obliqueThinWire ||
            otype == ObjectType::block || otype == ObjectType::source || otype == ObjectType::resistor || otype == ObjectType::capacitor || otype == ObjectType::inductor) {
            TwoPointObject *twopointobj = static_cast<TwoPointObject*>(object.get());
            if (twopointobj->getStartPoint().x > volumeSize.x ||
                twopointobj->getStartPoint().y > volumeSize.y ||
                twopointobj->getStartPoint().z > volumeSize.z) {
                errorMessage += "Input file error. Line " + std::to_string(twopointobj->getPositionInFile()+1) +
                                ". Start point of the object is outside of the volume.\n";
                throwError = true;
            }
            if (twopointobj->getEndPoint().x > volumeSize.x ||
                twopointobj->getEndPoint().y > volumeSize.y ||
                twopointobj->getEndPoint().z > volumeSize.z) {
                errorMessage += "Input file error. Line " + std::to_string(twopointobj->getPositionInFile()+1) +
                                ". End point of the object is outside of the volume.\n";
                throwError = true;
            }
            
            // Check coordinates
            bool xDir = false, yDir = false, zDir = false;
            if (twopointobj->getStartPoint().x == twopointobj->getEndPoint().x) {
                xDir = true;
            }
            if (twopointobj->getStartPoint().y == twopointobj->getEndPoint().y) {
                yDir = true;
            }
            if (twopointobj->getStartPoint().z == twopointobj->getEndPoint().z) {
                zDir = true;
            }
            
            if (xDir && yDir && zDir) {
                if (!(otype == ObjectType::source && static_cast<SourceObject*>(object.get())->getSourceType() == "tfsf")) {
                    errorMessage += "Input file warning. Line " + std::to_string(twopointobj->getPositionInFile()+1) +
                                    ". Start point coincides with end point.\n";
                }
            }
            
            if ( ( otype == ObjectType::thinWire || otype == ObjectType::source || otype == ObjectType::resistor || otype == ObjectType::capacitor || otype == ObjectType::inductor ) &&
                 ( (!xDir && !yDir) || (!xDir && !zDir) || (!yDir && !zDir) ) ) {
                errorMessage += "Input file warning. Line " + std::to_string(static_cast<ThinWire*>(object.get())->getPositionInFile()+1) +
                                ". The object should be parallel to x, y or z-axis.\n";
            }
        }
    }

    // Blocks
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::block) {
            // Check if the Debye parameters exist
            if (static_cast<Block*>(object.get())->getDebyeName() != "") {
                comnum = 0;
                for (const auto &deb : mObjects) {
                    if (deb->getObjectType() == ObjectType::debye && static_cast<Debye*>(deb.get())->getName() == static_cast<Block*>(object.get())->getDebyeName()) {
                        ++comnum;
                    }
                }
                if (comnum == 0) {
                    errorMessage += "Input file warning. Line " + std::to_string(static_cast<Block*>(object.get())->getPositionInFile()+1) +
                                    ". There are no Debye parameters corresponding to this block.\n";
                }
            }
        }
    }

    if (errorMessage != "") {
        if (throwError) {
            throw InputFileException(errorMessage, EXCEPT_ERROR, 0, false);
        }
        else {
            throw InputFileException(errorMessage, EXCEPT_WARNING, 0, false);
        }
    }
}


void ModelFile::coutFile(void)
{
    std::cout << "------- The model data -------" << '\n';

    // threads
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::threads) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    // volume
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::volume) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    // abc
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::abc) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    // calctime
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::calcTime) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    // output
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::calcOutput) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    // blocks, resistors, capacitors, inductors
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::block) {
            std::cout << object->writeDataToString() << '\n';
        }
    }
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::debye) {
            std::cout << object->writeDataToString() << '\n';
        }
    }
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::resistor) {
            std::cout << object->writeDataToString() << '\n';
        }
    }
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::capacitor) {
            std::cout << object->writeDataToString() << '\n';
        }
    }
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::inductor) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    // wires
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::staircaseWire) {
            std::cout << object->writeDataToString() << '\n';
        }
    }
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::thinWire) {
            std::cout << object->writeDataToString() << '\n';
        }
    }
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::obliqueThinWire) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    // sources
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::source) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    // calculate
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::calculate) {
            std::cout << object->writeDataToString() << '\n';
        }
    }
    
    // waveforms
    for (const auto &object : mObjects) {
        if (object->getObjectType() == ObjectType::function) {
            std::cout << object->writeDataToString() << '\n';
        }
    }

    std::cout << "------------------------------" << std::endl;
}


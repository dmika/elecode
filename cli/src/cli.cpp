///
/// \file cli.cpp
/// \brief Handling input arguments, etc.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "cli.h"


void interpretCommand(int argc, char *argv[])
{
    if (argc == 1) {
        std::string path = "./";
        std::vector<std::filesystem::path> files;
        for (const auto &entry : std::filesystem::directory_iterator(path)) {
            files.push_back(entry.path());
        }
        unsigned elofiles = 0;
        std::filesystem::path filepath;
        bool resulttxt = false;
        for (const auto &item : files) {
            std::filesystem::path tmppath = item;
            if (tmppath.extension() == ".elo") {
                filepath = tmppath;
                elofiles++;
            }
            if (item == "./result.txt") {
                resulttxt = true;
            }
        }

        if (elofiles == 1) {
            convertFileEloToFre(filepath);
        }
        else if (elofiles == 0) {
            std::cout << "\".elo\" file not found.\n";
        }
        else if (elofiles > 1) {
            std::cout << "Too many \".elo\" files. Choose one.\n";
        }
        // if there is "result.txt" then use it as input file
        if (resulttxt) {
            convertFileFreToFee(std::filesystem::path("./result.txt"));
        }
    }
    else {
        std::filesystem::path filepath = argv[1];
        if (filepath.extension() == ".elm") {
            if (argc == 2) {
                RunFdtd calc(argv[1]);
            }
            else if (argc == 3) {
                std::string key(argv[2]);
                if (key == "-c") {
                    ModelFile file(argv[1]);
                    try {
                        file.readFile();
                    }
                    catch (InputFileException const& e) {
                        std::cout << e.getMessage();
                    }
                }
                else {
                    std::cout << "Unrecognized argument." << std::endl;
                }
            }
            else {
                std::cout << "Wrong arguments." << std::endl;
            }
        }
        else if (filepath.extension() == ".elo") {
            if (argc > 2 && std::string(argv[2]) == "-pf") {
                float gapDist = std::stod(argv[3]);
                float correl = 0.0;
                if (argc > 4) {
                    correl = std::stod(argv[4]);
                }
                CurrentVoltage curvol = readEloFile(filepath);
                float prob = calcProbability(curvol, gapDist, 'f', correl, filepath.replace_extension(".asc"));
                std::cout << "Back flashover probability is " << prob << std::endl;
            }
            else if (argc > 2 && std::string(argv[2]) == "-ps") {
                float gapDist = std::stod(argv[3]);
                float correl = 0.0;
                if (argc > 4) {
                    correl = std::stod(argv[4]);
                }
                CurrentVoltage curvol = readEloFile(filepath);
                float prob = calcProbability(curvol, gapDist, 's', correl, filepath.replace_extension(".asc"));
                std::cout << "Back flashover probability is " << prob << std::endl;
            }
            else if (argc > 2 && std::string(argv[2]) == "-paf") {
                float gapDist = std::stod(argv[3]);
                CurrentVoltage curvol = readEloFile(filepath);
                float prob = calcProbabilityByAmplitude(curvol, gapDist, 'f', 3.83e-6);
                std::cout << "Back flashover probability is " << prob << std::endl;
            }
            else if (argc > 2 && std::string(argv[2]) == "-pas") {
                float gapDist = std::stod(argv[3]);
                CurrentVoltage curvol = readEloFile(filepath);
                float prob = calcProbabilityByAmplitude(curvol, gapDist, 's', 0.67e-6);
                std::cout << "Back flashover probability is " << prob << std::endl;
            }
            else {
                float arraycoef = 1.0;
                float scale = 1.0;
                if (argc > 4 && std::string(argv[2]) == "-a") {
                    arraycoef = calcPerpDipoleArrayCoef(std::stod(argv[3]), std::stod(argv[4]));
                    if (argc > 6 && std::string(argv[5]) == "-s") {
                        scale = std::stod(argv[6]);
                    }
                }
                convertFileEloToFre(filepath, arraycoef, scale);
            }
        }
        else if (filepath.extension() == ".fre" || filepath.extension() == ".txt") {
            unsigned samples = 1;
            for (int cntargc = 0; cntargc < argc; ++cntargc) {
                if (std::string(argv[cntargc]) == "-a") {
                    samples = 10;
                }
            }
            if (argc > 2 && std::string(argv[2]) == "-s") {
                convertFileFreToFse(filepath, samples);
            }
            else {
                convertFileFreToFee(filepath, samples);
            }
        }
        else if (filepath.extension() == ".fee") {
            bool output = false;
            for (int cntargc = 0; cntargc < argc; ++cntargc) {
                if (std::string(argv[cntargc]) == "-o") {
                    output = true;
                }
            }
            float minfreq = 10.0e3;
            float maxfreq = 4.0e6;
            unsigned Np = 4;
            unsigned repeats = 150;
            if (argc == 4 && std::string(argv[2]) == "-r") {
                repeats = std::stoul(argv[3]);
            }
            if (argc > 2 && std::string(argv[2]) != "-r" && std::string(argv[2]) != "-f") {
                minfreq = std::stod(argv[2]);
                if (argc > 3) {
                    maxfreq = std::stod(argv[3]);
                    if (argc > 4) {
                        Np = std::stoul(argv[4]);
                        if (argc > 5) {
                            repeats = std::stoul(argv[5]);
                        }
                    }
                }
            }
            if (argc > 2 && std::string(argv[2]) == "-f") {
                minfreq = 10.0e3;
                maxfreq = 300.0e3;
                if (argc > 4) {
                    minfreq = std::stod(argv[3]);
                    maxfreq = std::stod(argv[4]);
                }
                std::vector<ComplexPermittivity> eps = readFeeFile(filepath);
                FitFunc fit = calcFitFunc(eps, minfreq, maxfreq);
                std::cout << "m=" << fit.m << "   p=" << fit.p << "   epsinf=" << fit.epsinf << "   fitcoef=" << fit.fitcoef << "   epsfitcoef=" << fit.epsfitcoef << '\n';
                
                if (output) {
                    filepath.replace_extension(".fre");
                    float rho0 = calcDcResistivity(filepath, 1);
                    eps = calcComplexEpsilonWithFitFunc(eps, fit);
                    writeFitFiles(filepath, rho0, eps);
                }
            }
            else {
                std::vector<ComplexPermittivity> eps = readFeeFile(filepath);
                eps = calcReferenceComplexEpsilon(eps, minfreq, maxfreq, Np);
                DebyeParameters dp = calcDebyeParameters(SampledPermittivity(eps, FreqType::hz), Np, repeats);
                outputDebyeParameters(std::cout, dp, plain | edx | plt);
            }
        }
        else {
            if (argc >= 3) {
                std::string key(argv[1]);
                float minfreq = 10.0e3;
                float maxfreq = 4.0e6;
                unsigned Np = 4;
                unsigned repeats = 150;
                if (argc > 3) {
                    minfreq = std::stod(argv[3]);
                    if (argc > 4) {
                        maxfreq = std::stod(argv[4]);
                        if (argc > 5) {
                            Np = std::stoul(argv[5]);
                            if (argc > 6) {
                                repeats = std::stoul(argv[6]);
                            }
                        }
                    }
                }
                if (key == "-mav") {
                    calcDebyeParametersForModel(alipiovisacro, std::stod(argv[2]), minfreq, maxfreq, Np, repeats);
                }
                else if (key == "-mp") {
                    calcDebyeParametersForModel(portela, std::stod(argv[2]), minfreq, maxfreq, Np, repeats);
                }
                else if (key == "-mm") {
                    calcDebyeParametersForModel(messier, std::stod(argv[2]), minfreq, maxfreq, Np, repeats);
                }
                else if (key == "-d") {
                    double a = std::stod(argv[2]);
                    double b = std::stod(argv[3]);
                    double doi = DoiMedianDipole(XyPointDouble(-a, 0), XyPointDouble(0, 0), XyPointDouble(b, b), XyPointDouble(b, b+a));
                    std::cout << "DOI = " << doi << std::endl;
                }
                else {
                    std::cout << "Unrecognized argument." << std::endl;
                }
            }
        }
    }
}


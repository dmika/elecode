///
/// \file runfdtd.cpp
/// \brief This code performs calculations for the input file.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "runfdtd.h"


RunFdtd::RunFdtd(std::string FilePath)
    : mFile(FilePath)
{
    mObliqueWireArea = nullptr;
    std::cout << "Reading file..." << std::endl;
    readFile();
    mFile.coutFile();
    std::cout << "Preparing objects..." << std::endl;
    setup();
    float calcTime = 1.0e-6;
    std::vector<std::shared_ptr<ModelObject>> modelObjects = mFile.getObjects();
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::calcTime) {
            calcTime = static_cast<CalcTime*>(object.get())->getCalculationTime();
        }
    }
    std::cout << "Calculation (" << static_cast<unsigned>(calcTime / mModel->getTimeStepDuration())+2 << " steps)..." << std::endl;
    calculate();
    std::cout << std::endl << "Writing file..." << std::endl;
    writeFile();
}


void RunFdtd::readFile(void)
{
    try {
        mFile.readFile();
    }
    catch (InputFileException const& e) {
        std::cout << e.getMessage();
        if (e.getExceptionSeverity() == EXCEPT_ERROR) {
            exit(1);
        }
    }
}


void RunFdtd::setup(void)
{
    std::vector<std::shared_ptr<ModelObject>> modelObjects = mFile.getObjects();
    
    unsigned threads = 1;
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::threads) {
            threads = static_cast<ThreadsObject*>(object.get())->getThreadsNumber();
        }
    }
    
    // basic
    float cellSize = 1.0;
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::volume) {
            VolumeObject *volume = static_cast<VolumeObject*>(object.get());
            cellSize = volume->getCellSize();
            XyzPoint volSizeInt(volume->getVolumeSize().x/cellSize + 0.5,
                                volume->getVolumeSize().y/cellSize + 0.5,
                                volume->getVolumeSize().z/cellSize + 0.5);
            mFdtd = std::make_shared<Fdtd>(volSizeInt, cellSize, S_DEFAULT * volume->getCourantCoef(), threads);
            mModel = mFdtd->getModel();
            break;
        }
    }

    // abc
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::abc) {
            AbcObject *abc = static_cast<AbcObject*>(object.get());
            unsigned thick = static_cast<unsigned>(abc->getThickness()/cellSize);
            if (abc->getAbcType() == "upml") {
                mFdtd->addAbc(AbcType::upml, thick);
            }
            else if (abc->getAbcType() == "cpml") {
                mFdtd->initCpmlParameters(thick, abc->getSigma(), abc->getKappa(), abc->getA(), abc->getM(), abc->getMa(), abc->getRegion());
            }
        }
    }

    std::shared_ptr<DebyeAde> debye;
    unsigned maxPolesNum = 0;
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::debye) {
            if (static_cast<Debye*>(object.get())->getDeltaEpsilon().size() > maxPolesNum) {
                maxPolesNum = static_cast<Debye*>(object.get())->getDeltaEpsilon().size();
            }
        }
    }
    if (maxPolesNum) {
        mFdtd->addDispersion(maxPolesNum);
    }

    // volume objects
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::block) {
            setupBlock(*static_cast<Block*>(object.get()));
        }
    }
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::resistor) {
            setupResistor(*static_cast<Resistor*>(object.get()));
        }
    }
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::capacitor) {
            setupCapacitor(*static_cast<Capacitor*>(object.get()));
        }
    }
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::inductor) {
            setupInductor(*static_cast<Inductor*>(object.get()));
        }
    }
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::staircaseWire) {
            setupStaircaseWire(*static_cast<StaircaseWire*>(object.get()));
        }
    }
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::thinWire) {
            setupThinWire(*static_cast<ThinWire*>(object.get()));
        }
    }

    // sources
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::source) {
            setupSource(*static_cast<SourceObject*>(object.get()));
        }
    }

    // calculate
    std::string path = "";
    bool ifResult = false;
    unsigned wInterval = 1000;
    float tInterval = 0.0;
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::calcOutput) {
            CalcOutput *coutput = static_cast<CalcOutput*>(object.get());
            ifResult = coutput->getCalcResult();
            wInterval = coutput->getWriteInterval();
            tInterval = coutput->getTimeInterval();
        }
    }

    path = mFile.getFilePath();
    if (static_cast<int>(path.rfind('/')) < static_cast<int>(path.rfind('.')) &&
        static_cast<int>(path.rfind('\\')) < static_cast<int>(path.rfind('.')) &&
        path.rfind('.') != std::string::npos) {
        path = path.substr(0, path.rfind('.'));
    }
    if (ifResult) {
        path += ".tmp";
    }
    path += ".elo";
    mResult = std::make_shared<Output>(mModel, path, wInterval, tInterval);
    mFdtd->addTimeStepItem(mResult);
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::calculate) {
            setupCalculate(*static_cast<CalculateObject*>(object.get()));
        }
    }

    setupObliqueWires();

    std::shared_ptr<CoutTimeStepNumber> coutTimeStep = std::make_shared<CoutTimeStepNumber>();
    coutTimeStep->setDt(mModel->getTimeStepDuration());
    float calcTime = 1.0e-6;
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::calcTime) {
            calcTime = static_cast<CalcTime*>(object.get())->getCalculationTime();
        }
    }
    coutTimeStep->setCalcTime(calcTime);
    mFdtd->addTimeStepItem(coutTimeStep);
}


void RunFdtd::calculate(void)
{
    float calcTime = 1.0e-6;
    std::vector<std::shared_ptr<ModelObject>> modelObjects = mFile.getObjects();
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::calcTime) {
            calcTime = static_cast<CalcTime*>(object.get())->getCalculationTime();
        }
    }
    mFdtd->calculateTillTime(calcTime);
}


void RunFdtd::writeFile(void)
{
    mResult->save();
}


void RunFdtd::setupBlock(Block const& block)
{
    float cellsize = mModel->getCellSize();
    XyzPoint firstPoint(block.getStartPoint().x/cellsize + 0.5, block.getStartPoint().y/cellsize + 0.5, block.getStartPoint().z/cellsize + 0.5);
    XyzPoint secondPoint(block.getEndPoint().x/cellsize + 0.5, block.getEndPoint().y/cellsize + 0.5, block.getEndPoint().z/cellsize + 0.5);
    std::vector<float> deltaEpsilon;
    std::vector<float> tau;
    std::vector<std::shared_ptr<ModelObject>> modelObjects = mFile.getObjects();
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::debye) {
            if (static_cast<Debye*>(object.get())->getName() == block.getDebyeName()) {
                deltaEpsilon = static_cast<Debye*>(object.get())->getDeltaEpsilon();
                tau = static_cast<Debye*>(object.get())->getTau();
            }
        }
    }
    mFdtd->addLossyDielectricBlock(firstPoint, secondPoint, block.getEpsilon(), block.getSigma(), deltaEpsilon, tau);
}


void RunFdtd::setupResistor(Resistor const& resistor)
{
    float cellsize = mModel->getCellSize();
    XyzPoint firstPoint(resistor.getStartPoint().x/cellsize + 0.5, resistor.getStartPoint().y/cellsize + 0.5, resistor.getStartPoint().z/cellsize + 0.5);
    XyzPoint secondPoint(resistor.getEndPoint().x/cellsize + 0.5, resistor.getEndPoint().y/cellsize + 0.5, resistor.getEndPoint().z/cellsize + 0.5);
    mFdtd->addResistor(firstPoint, secondPoint, resistor.getResistance());
}


void RunFdtd::setupCapacitor(Capacitor const& capacitor)
{
    float cellsize = mModel->getCellSize();
    XyzPoint firstPoint(capacitor.getStartPoint().x/cellsize + 0.5, capacitor.getStartPoint().y/cellsize + 0.5, capacitor.getStartPoint().z/cellsize + 0.5);
    XyzPoint secondPoint(capacitor.getEndPoint().x/cellsize + 0.5, capacitor.getEndPoint().y/cellsize + 0.5, capacitor.getEndPoint().z/cellsize + 0.5);
    mFdtd->addCapacitor(firstPoint, secondPoint, capacitor.getCapacitance(), capacitor.getConductance());
}


void RunFdtd::setupInductor(Inductor const& inductor)
{
    float cellsize = mModel->getCellSize();
    XyzPoint firstPoint(inductor.getStartPoint().x/cellsize + 0.5, inductor.getStartPoint().y/cellsize + 0.5, inductor.getStartPoint().z/cellsize + 0.5);
    XyzPoint secondPoint(inductor.getEndPoint().x/cellsize + 0.5, inductor.getEndPoint().y/cellsize + 0.5, inductor.getEndPoint().z/cellsize + 0.5);
    XyzDirection direction = XyzDirection::x;
    
    if (secondPoint.x != firstPoint.x) {
        direction = XyzDirection::x;
        if (firstPoint.x > secondPoint.x) {
            firstPoint.x = secondPoint.x;
        }
    }
    else if (secondPoint.y != firstPoint.y) {
        direction = XyzDirection::y;
        if (firstPoint.y > secondPoint.y) {
            firstPoint.y = secondPoint.y;
        }
    }
    else {
        direction = XyzDirection::z;
        if (firstPoint.z > secondPoint.z) {
            firstPoint.z = secondPoint.z;
        }
    }

    mFdtd->addTimeStepItem(std::make_shared<LumpedInductor>(mModel, direction, firstPoint, inductor.getInductance()));
}


void RunFdtd::setupStaircaseWire(const StaircaseWire &wire)
{
    float cellsize = mModel->getCellSize();
    XyzPoint firstPoint(wire.getStartPoint().x/cellsize + 0.5, wire.getStartPoint().y/cellsize + 0.5, wire.getStartPoint().z/cellsize + 0.5);
    XyzPoint secondPoint(wire.getEndPoint().x/cellsize + 0.5, wire.getEndPoint().y/cellsize + 0.5, wire.getEndPoint().z/cellsize + 0.5);
    mFdtd->addStaircaseWire(firstPoint, secondPoint, wire.isCorrected());
}


void RunFdtd::setupThinWire(ThinWire const& wire)
{
    float cellsize = mModel->getCellSize();
    XyzPoint firstPoint(wire.getStartPoint().x/cellsize + 0.5, wire.getStartPoint().y/cellsize + 0.5, wire.getStartPoint().z/cellsize + 0.5);
    XyzPoint secondPoint(wire.getEndPoint().x/cellsize + 0.5, wire.getEndPoint().y/cellsize + 0.5, wire.getEndPoint().z/cellsize + 0.5);
    mFdtd->addRailtonThinWire(firstPoint, secondPoint, wire.getDiameter(), wire.getSubType());
}


std::shared_ptr<Waveform> RunFdtd::getPreparedWaveform(std::string name, unsigned counter)
{
    std::shared_ptr<Waveform> wfdata;
    std::vector<std::shared_ptr<ModelObject>> modelObjects = mFile.getObjects();
    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::function) {
            FunctionObject *function = static_cast<FunctionObject*>(object.get());
            if (name == function->getName() || name == "") {
                if (function->getFunctionType() == "custom") {
                    wfdata = std::make_shared<ArrayWaveform>(function->getDataArray(), function->getTimeInterval());
                }
                else if (function->getFunctionType() == "heidler") {
                    if (function->getSubType() == "s") {
                        wfdata = std::make_shared<Heidler>(function->getFront(), function->getTailCoef(), function->getCorrectionCoef(),
                                                           function->getExponent(), function->getAmplitude(), function->getTimeShift());
                    }
                    else if (function->getSubType() == "m") {
                        std::vector<HeidlerFuncParameters> funpar = function->getHeidlerFuncParameters();
                        std::vector<HeidlerParameters> params;
                        for (unsigned cnt = 0; cnt < funpar.size(); ++cnt) {
                            params.push_back(HeidlerParameters(funpar[cnt].mFrontCoef, funpar[cnt].mTailCoef, funpar[cnt].mExponent, funpar[cnt].mAmplitude));
                        }
                        wfdata = std::make_shared<Heidler>(params, function->getTimeShift());
                    }
                    else if (function->getSubType() == "mtle") {
                        std::vector<HeidlerFuncParameters> funpar = function->getHeidlerFuncParameters();
                        std::vector<HeidlerParameters> params;
                        float cellsize = mModel->getCellSize();
                        float dist = cellsize*static_cast<float>(counter);
                        float atten = 1.0;
                        if (function->getLambda() > 0.0) {
                            atten = exp(-dist/function->getLambda());
                        }
                        float delay = 0.0;
                        if (function->getSpeed() > 0.0) {
                            delay = -dist/(function->getSpeed());
                        }
                        for (unsigned cnt = 0; cnt < funpar.size(); ++cnt) {
                            params.push_back(HeidlerParameters(funpar[cnt].mFrontCoef, funpar[cnt].mTailCoef, funpar[cnt].mExponent, funpar[cnt].mAmplitude*atten));
                        }
                        wfdata = std::make_shared<Heidler>(params, delay);
                    }
                }
                else if (function->getFunctionType() == "cigre") {
                    wfdata = std::make_shared<Cigre>(function->getFront(), function->getTimeToHalf(), function->getSteepness(),
                                                     function->getAmplitude(), function->getSubType()[0]);
                }
                else if (function->getFunctionType() == "ramp") {
                    wfdata = std::make_shared<Ramp>(function->getFront(), function->getTimeToHalf(), function->getAmplitude());
                }
                else if (function->getFunctionType() == "step") {
                    wfdata = std::make_shared<SineStep>(function->getFront());
                }
                else if (function->getFunctionType() == "sine") {
                    wfdata = std::make_shared<Sine>(function->getFrequency(), function->getAmplitude());
                }
                else if (function->getFunctionType() == "gaussian") {
                    wfdata = std::make_shared<Gaussian>(function->getB(), function->getC(), function->getAmplitude(), function->getSubType()[0]);
                }
            }
        }
    }
    return wfdata;
}


void RunFdtd::setupSource(SourceObject const& src)
{
    float cellsize = mModel->getCellSize();
    XyzPoint firstPoint(src.getStartPoint().x/cellsize + 0.5, src.getStartPoint().y/cellsize + 0.5, src.getStartPoint().z/cellsize + 0.5);
    XyzPoint secondPoint(src.getEndPoint().x/cellsize + 0.5, src.getEndPoint().y/cellsize + 0.5, src.getEndPoint().z/cellsize + 0.5);
    std::shared_ptr<Waveform> waveform = getPreparedWaveform(src.getName());
    XyzDirection direction = XyzDirection::x;
    FieldComponent comp = FieldComponent::ex;
    XyzPoint step(0, 0, 0);
    unsigned srcnum = 1;
    
    if (secondPoint.x != firstPoint.x) {
        direction = XyzDirection::x;
        comp = FieldComponent::ex;
        step = XyzPoint(1, 0, 0);
        if (firstPoint.x > secondPoint.x) {
            unsigned tmp = firstPoint.x;
            firstPoint.x = secondPoint.x;
            secondPoint.x = tmp;
        }
        srcnum = secondPoint.x - firstPoint.x;
    }
    else if (secondPoint.y != firstPoint.y) {
        direction = XyzDirection::y;
        comp = FieldComponent::ey;
        step = XyzPoint(0, 1, 0);
        if (firstPoint.y > secondPoint.y) {
            unsigned tmp = firstPoint.y;
            firstPoint.y = secondPoint.y;
            secondPoint.y = tmp;
        }
        srcnum = secondPoint.y - firstPoint.y;
    }
    else {
        direction = XyzDirection::z;
        comp = FieldComponent::ez;
        step = XyzPoint(0, 0, 1);
        if (firstPoint.z > secondPoint.z) {
            unsigned tmp = firstPoint.z;
            firstPoint.z = secondPoint.z;
            secondPoint.z = tmp;
        }
        srcnum = secondPoint.z - firstPoint.z;
    }

    if (src.getSourceType() == "current") {
        for (unsigned cnt = 0; cnt < srcnum; ++cnt) {
            std::shared_ptr<Waveform> waveformcnt = getPreparedWaveform(src.getName(), cnt);
            mFdtd->addTimeStepItem(std::make_shared<CurrentSource>(mModel, direction, firstPoint+step*cnt, waveformcnt, src.getConductance()));
        }
    }
    else if (src.getSourceType() == "voltage") {
        mFdtd->addTimeStepItem(std::make_shared<VoltageSource>(mModel, direction, firstPoint, waveform, src.getResistance()));
    }
    else if (src.getSourceType() == "ehard") {
        mFdtd->addTimeStepItem(std::make_shared<PointSource>(mModel, comp, firstPoint, waveform, FieldSourceType::hard));
    }
    else if (src.getSourceType() == "esoft") {
        mFdtd->addTimeStepItem(std::make_shared<PointSource>(mModel, comp, firstPoint, waveform, FieldSourceType::soft));
    }
    else if (src.getSourceType() == "tfsf") {
        unsigned dist = static_cast<unsigned>(src.getDist()/cellsize + 0.5);
        mFdtd->addTimeStepItem(std::make_shared<TfsfArea>(mModel, waveform, src.getTheta(), src.getPhi(), src.getPsi(), dist));
    }
}


void RunFdtd::setupCalculate(CalculateObject const& calc)
{
    float dl = mModel->getCellSize();
    std::vector<XyzPoint> coordinates;
    for (unsigned cnt = 0; cnt < calc.getCoordinates().size(); ++cnt) {
        coordinates.push_back(XyzPoint(calc.getCoordinates()[cnt].x/dl + 0.5,
                                       calc.getCoordinates()[cnt].y/dl + 0.5,
                                       calc.getCoordinates()[cnt].z/dl + 0.5));
    }
    if (calc.getCalculateType() == "current") {
        FieldComponent component = FieldComponent::ex;
        if (coordinates[1].x != coordinates[0].x) {
            component = FieldComponent::ex;
            if (coordinates[0].x > coordinates[1].x) {
                coordinates[0].x = coordinates[1].x;
            }
        }
        else if (coordinates[1].y != coordinates[0].y) {
            component = FieldComponent::ey;
            if (coordinates[0].y > coordinates[1].y) {
                coordinates[0].y = coordinates[1].y;
            }
        }
        else {
            component = FieldComponent::ez;
            if (coordinates[0].z > coordinates[1].z) {
                coordinates[0].z = coordinates[1].z;
            }
        }
        
        std::shared_ptr<AmperesLawCurrent> current = std::make_shared<AmperesLawCurrent>(mModel, FieldPoint(component, coordinates[0], 1.0));
        mFdtd->addTimeStepItem(current);
        mResult->addDataPointer(current->getCurrentCurrentPointer());
    }
    else if (calc.getCalculateType() == "voltage") {
        std::shared_ptr<VoltagePath> volPath = std::make_shared<VoltagePath>(mModel->getCellSize());
        for (unsigned cntnum = 1; cntnum < coordinates.size(); ++cntnum) {
            float sign = 1.0;
            if (coordinates[cntnum].x != coordinates[cntnum-1].x) {
                if (coordinates[cntnum-1].x > coordinates[cntnum].x) {
                    std::swap(coordinates[cntnum].x, coordinates[cntnum-1].x);
                    sign = -1.0;
                }
                for (unsigned cnt = 0; cnt < coordinates[cntnum].x - coordinates[cntnum-1].x; ++cnt) {
                    volPath->addItem(FieldPoint(FieldComponent::ex, XyzPoint(coordinates[cntnum-1].x+cnt, coordinates[cntnum-1].y,
                                                                   coordinates[cntnum-1].z), sign, mModel->getBasicParameters().Ex));
                }
            }
            else if (coordinates[cntnum].y != coordinates[cntnum-1].y) {
                if (coordinates[cntnum-1].y > coordinates[cntnum].y) {
                    std::swap(coordinates[cntnum].y, coordinates[cntnum-1].y);
                    sign = -1.0;
                }
                for (unsigned cnt = 0; cnt < coordinates[cntnum].y - coordinates[cntnum-1].y; ++cnt) {
                    volPath->addItem(FieldPoint(FieldComponent::ey, XyzPoint(coordinates[cntnum-1].x, coordinates[cntnum-1].y+cnt,
                                                                   coordinates[cntnum-1].z), sign, mModel->getBasicParameters().Ey));
                }
            }
            else {
                if (coordinates[cntnum-1].z > coordinates[cntnum].z) {
                    std::swap(coordinates[cntnum].z, coordinates[cntnum-1].z);
                    sign = -1.0;
                }
                for (unsigned cnt = 0; cnt < coordinates[cntnum].z - coordinates[cntnum-1].z; ++cnt) {
                    volPath->addItem(FieldPoint(FieldComponent::ez, XyzPoint(coordinates[cntnum-1].x, coordinates[cntnum-1].y,
                                                                   coordinates[cntnum-1].z+cnt), sign, mModel->getBasicParameters().Ez));
                }
            }
        }
        mFdtd->addTimeStepItem(volPath);
        mResult->addDataPointer(volPath->getCurrentVoltagePointer());
    }
    else if (calc.getCalculateType() == "exfield") {
        std::shared_ptr<FieldOutput> field = std::make_shared<FieldOutput>(mModel, calc.getCoordinates()[0], FieldComponent::ex);
        mFdtd->addTimeStepItem(field);
        mResult->addDataPointer(field->getCurrentFieldPointer());
    }
    else if (calc.getCalculateType() == "eyfield") {
        std::shared_ptr<FieldOutput> field = std::make_shared<FieldOutput>(mModel, calc.getCoordinates()[0], FieldComponent::ey);
        mFdtd->addTimeStepItem(field);
        mResult->addDataPointer(field->getCurrentFieldPointer());
    }
    else if (calc.getCalculateType() == "ezfield") {
        std::shared_ptr<FieldOutput> field = std::make_shared<FieldOutput>(mModel, calc.getCoordinates()[0], FieldComponent::ez);
        mFdtd->addTimeStepItem(field);
        mResult->addDataPointer(field->getCurrentFieldPointer());
    }
    else if (calc.getCalculateType() == "hxfield") {
        std::shared_ptr<FieldOutput> field = std::make_shared<FieldOutput>(mModel, calc.getCoordinates()[0], FieldComponent::hx);
        mFdtd->addTimeStepItem(field);
        mResult->addDataPointer(field->getCurrentFieldPointer());
    }
    else if (calc.getCalculateType() == "hyfield") {
        std::shared_ptr<FieldOutput> field = std::make_shared<FieldOutput>(mModel, calc.getCoordinates()[0], FieldComponent::hy);
        mFdtd->addTimeStepItem(field);
        mResult->addDataPointer(field->getCurrentFieldPointer());
    }
    else if (calc.getCalculateType() == "hzfield") {
        std::shared_ptr<FieldOutput> field = std::make_shared<FieldOutput>(mModel, calc.getCoordinates()[0], FieldComponent::hz);
        mFdtd->addTimeStepItem(field);
        mResult->addDataPointer(field->getCurrentFieldPointer());
    }
    else {   // if (calc.getCalculateType() == "function")
        std::shared_ptr<Waveform> wfdata = getPreparedWaveform(calc.getCalculateType());
        std::shared_ptr<Source> calculateSource = std::make_shared<Source>(wfdata, mModel->getTimeStepDuration());
        mFdtd->addTimeStepItem(calculateSource);
        mResult->addDataPointer(calculateSource->getCurrentValuePointer());
    }
}


void RunFdtd::setupObliqueWires(void)
{
    std::vector<ObliqueWire> obliqueWires;
    std::vector<std::shared_ptr<ModelObject>> modelObjects = mFile.getObjects();

    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::obliqueThinWire) {
            ObliqueThinWire *oblwire = static_cast<ObliqueThinWire*>(object.get());
            if (oblwire->hasItVoltageCalc()) {
                obliqueWires.push_back(ObliqueWire(*mModel, oblwire->getStartPoint(),
                    oblwire->getEndPoint(), oblwire->getDiameter(), oblwire->getNumberOfSegments(),
                    oblwire->getSheathDiameter(), oblwire->getSheathEpsilon(), oblwire->getSheathSigma()));
                obliqueWires[obliqueWires.size()-1].setInfiniteEnd(oblwire->isItInfiniteEnd());
                obliqueWires[obliqueWires.size()-1].setFirstVoltageCalcWire(true);
                obliqueWires[obliqueWires.size()-1].setSeparatedStart(true);
                //obliqueWires[obliqueWires.size()-1].setCalcOutput(mResult->getAddedDataPointer());
            }
        }
    }

    for (const auto &object : modelObjects) {
        if (object->getObjectType() == ObjectType::obliqueThinWire) {
            ObliqueThinWire *oblwire = static_cast<ObliqueThinWire*>(object.get());
            if (!oblwire->hasItVoltageCalc()) {
                obliqueWires.push_back(ObliqueWire(*mModel, oblwire->getStartPoint(),
                    oblwire->getEndPoint(), oblwire->getDiameter(), oblwire->getNumberOfSegments(),
                    oblwire->getSheathDiameter(), oblwire->getSheathEpsilon(), oblwire->getSheathSigma()));
                obliqueWires[obliqueWires.size()-1].setSeparatedStart(oblwire->isItSeparated());
                obliqueWires[obliqueWires.size()-1].setInfiniteEnd(oblwire->isItInfiniteEnd());
                if (oblwire->hasItCurrentSource()) {
                    obliqueWires[obliqueWires.size()-1].setCurrentSourceSegment(static_cast<int>(oblwire->getNodeSegmentSource()));
                    std::shared_ptr<Waveform> wfdata = getPreparedWaveform(oblwire->getName());
                    std::shared_ptr<Source> source = std::make_shared<Source>(wfdata, mModel->getTimeStepDuration());
                    mFdtd->addTimeStepItem(source);
                    obliqueWires[obliqueWires.size()-1].setSourceValuePointer(source->getCurrentValuePointer());
                }
                else if (oblwire->hasItVoltageSource()) {
                    obliqueWires[obliqueWires.size()-1].setVoltageSourceSegment(static_cast<int>(oblwire->getNodeSegmentSource()));
                    obliqueWires[obliqueWires.size()-1].setResistorInSegment(static_cast<int>(oblwire->getNodeSegmentSource()), oblwire->getSourceResistance());
                    std::shared_ptr<Waveform> wfdata = getPreparedWaveform(oblwire->getName());
                    std::shared_ptr<Source> source = std::make_shared<Source>(wfdata, mModel->getTimeStepDuration());
                    mFdtd->addTimeStepItem(source);
                    obliqueWires[obliqueWires.size()-1].setSourceValuePointer(source->getCurrentValuePointer());
                }
                if (oblwire->hasItCurrentCalc()) {
                    obliqueWires[obliqueWires.size()-1].setCurrentCalcSegment(static_cast<int>(oblwire->getNodeSegmentCalc()));
                    obliqueWires[obliqueWires.size()-1].setCalcOutput(mResult->getAddedDataPointer());
                }
            }
        }
    }
    
    for (auto &wire : obliqueWires) {
        if (wire.getFirstVoltageCalcWire()) {
            wire.setCalcOutput(mResult->getAddedDataPointer());
        }
    }

    if (obliqueWires.size()) {
        mObliqueWireArea = std::make_shared<WireArea>(*mModel, obliqueWires);
        mFdtd->addTimeStepItem(mObliqueWireArea);
        //mObliqueWireArea->CheckCurrentTraceContinuity();
    }
}


void CoutTimeStepNumber::doFirstHalfStepJob(unsigned)
{
}


void CoutTimeStepNumber::doSecondHalfStepJob(unsigned timeStepNumber)
{
    unsigned steps = static_cast<unsigned>(mCalcTime/mDt)+1;
    float progress = static_cast<float>(timeStepNumber)*100.0/steps;
    std::cout << "\r" << "step: " << timeStepNumber << "    progress: " << std::fixed << std::setprecision(1) << progress << "%" << std::flush;
}


///
/// \file epsilon.cpp
/// \brief Permittivity calculation.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#include "epsilon.h"


void convertFileEloToFre(std::filesystem::path filepath, float arraycoef, float scale)
{
    std::cout << "Array coefficient equals to " << arraycoef << ". Use -a key to change it.\n";
    CurrentVoltage curvol = readEloFile(filepath);
    std::vector<RhoEpsilon> rhoeps = calcRhoEpsilon(curvol, arraycoef);
    for (auto &re : rhoeps) {
        re.rho *= scale;
    }
    writeFreFile(filepath, rhoeps);
}


void convertFileFreToFee(std::filesystem::path filepath, unsigned samples)
{
    float rho0 = calcDcResistivity(filepath, samples);
    std::vector<RhoEpsilon> rhoeps = prepareRhoEpsilon(filepath);
    std::vector<ComplexPermittivity> eps = calcComplexEpsilon(rho0, rhoeps);
    writeFeeFile(filepath, eps);
}


void convertFileFreToFse(std::filesystem::path filepath, unsigned samples)
{
    float rho0 = calcDcResistivity(filepath, samples);
    std::vector<RhoEpsilon> rhoeps = prepareRhoEpsilon(filepath);
    std::vector<ComplexPermittivity> eps = calcComplexEpsilon(rho0, rhoeps, true);
    writeFeeFile(filepath, eps, ".fse");
}


float calcDcResistivity(std::filesystem::path filepath, unsigned samples)
{
    float rho0 = 0.0;
    std::vector<RhoEpsilon> rhoeps = readFreFile(filepath);
    unsigned size = rhoeps.size();
    if (size > 1 && rhoeps[size-1].freq < rhoeps[size-2].freq) {
        rho0 = rhoeps[size-1].rho;
    }
    else if (size) {
        for (unsigned cnt = 0; cnt < samples; ++cnt) {
            rho0 += rhoeps[cnt].rho;
        }
        rho0 /= static_cast<float>(samples);
        std::cout << "Low-frequency resistivity at the last line was not found. Using resistivity " <<
            rho0 << " at the first line(s). Corresponding frequency is " << rhoeps[0].freq;
        if (samples > 1) {
            std::cout << "-" << rhoeps[samples-1].freq;
        }
        std::cout << " Hz.\n";
    }
    else {
        std::cout << "Low-frequency resistivity was not found.\n";
    }
    return rho0;
}


std::vector<RhoEpsilon> prepareRhoEpsilon(std::filesystem::path filepath)
{
    std::vector<RhoEpsilon> fullrhoeps = readFreFile(filepath);
    std::vector<RhoEpsilon> rhoeps;
    for (unsigned cnt = 0; cnt < fullrhoeps.size(); ++cnt) {
        if (fullrhoeps[cnt].freq > 0.0) {
            rhoeps.push_back(fullrhoeps[cnt]);
        }
    }
    return rhoeps;
}


CurrentVoltage readEloFile(std::filesystem::path filepath)
{
    CurrentVoltage curvol;
    std::ifstream infile(filepath);
    unsigned cntr = 0;
    while (!infile.eof()) {
        std::string str;
        float cur, vol, tim;
        getline(infile, str);
        if (!str.empty()) {
            std::stringstream ss(str);
            ss >> tim;
            ss >> cur;
            ss >> vol;
            curvol.cur.push_back(cur);
            curvol.vol.push_back(vol);
            if (cntr == 1) {
                curvol.dt = tim;
            }
            cntr++;
        }
    }
    return curvol;
}


std::vector<RhoEpsilon> readFreFile(std::filesystem::path filepath)
{
    std::ifstream infile(filepath);
    std::vector<RhoEpsilon> rhoeps;
    while (!infile.eof()) {
        std::string str;
        RhoEpsilon re;
        float tmprho, tmpeps;
        getline(infile, str);
        if (!str.empty()) {
            std::stringstream ss(str);
            ss >> re.freq;
            ss >> re.rho;
            ss >> re.eps;
            if (ss >> tmprho) {
                re.rho = tmprho;
            }
            if (ss >> tmpeps) {
                re.eps = tmpeps;
            }
            rhoeps.push_back(re);
        }
    }
    return rhoeps;
}


std::vector<ComplexPermittivity> readFeeFile(std::filesystem::path filepath)
{
    std::ifstream infile(filepath);
    std::vector<ComplexPermittivity> eps;
    while (!infile.eof()) {
        std::string str;
        getline(infile, str);
        if (!str.empty()) {
            std::stringstream ss(str);
            float w, im, re;
            ss >> w;
            ss >> im;
            ss >> re;
            eps.push_back(ComplexPermittivity(w, std::complex<float>(re, im)));
        }
    }
    return eps;
}


void writeFreFile(std::filesystem::path filepath, std::vector<RhoEpsilon> rhoeps)
{
    filepath.replace_extension(".fre");
    std::ofstream ofile(filepath);
    for (unsigned cnt = 0; cnt < rhoeps.size(); ++cnt) {
        ofile << std::fixed << std::setprecision(9) << rhoeps[cnt].freq << "  " << rhoeps[cnt].rho << "  " << rhoeps[cnt].eps << '\n';
    }
}


void writeFeeFile(std::filesystem::path filepath, std::vector<ComplexPermittivity> eps, std::string ext)
{
    filepath.replace_extension(ext);
    std::ofstream ofile(filepath);
    for (unsigned cnt = 0; cnt < eps.size(); ++cnt) {
        ofile << std::fixed << std::setprecision(7) << eps[cnt].w << "  " << eps[cnt].epsilon.imag() << "  " << eps[cnt].epsilon.real() << '\n';
    }
}


void writeFitFiles(std::filesystem::path filepath, float rho0, std::vector<ComplexPermittivity> eps)
{
    std::vector<RhoEpsilon> rhoeps = calcRhoEpsilon(rho0, eps);
    filepath = filepath.replace_filename(filepath.stem().concat("_fit.fre"));
    writeFeeFile(filepath, eps);
    writeFreFile(filepath, rhoeps);
}


void calcDebyeParametersForModel(int model, float rho0, float minfreq, float maxfreq, unsigned Np, unsigned repeats)
{
    std::vector<ComplexPermittivity> eps;
    if (model & alipiovisacro) {
        eps = modelAlipioVisacro(rho0, minfreq, maxfreq, Np*4);
    }
    else if (model & portela) {
        eps = modelPortela(rho0, minfreq, maxfreq, Np*4);
    }
    else if (model & messier) {
        eps = modelMessier(rho0, minfreq, maxfreq, Np*4);
    }
    DebyeParameters dp = calcDebyeParameters(SampledPermittivity(eps, FreqType::rads), Np, repeats);
    outputDebyeParameters(std::cout, dp, plain, rho0);
    std::ofstream ofile("model.plt");
    outputDebyeParameters(ofile, dp, plt | model, rho0);
    std::ofstream ofilefull("model.txt");
    outputDebyeParameters(ofilefull, dp, plain | edx | plt | model, rho0);
}


DebyeParameters calcDebyeParameters(SampledPermittivity eps, unsigned Np, unsigned repeats)
{
    SwarmPsols swarm(eps, Np);
    return swarm.calcParametersWithRepeats(repeats);
}


void outputDebyeParameters(std::ostream &strm, DebyeParameters dp, int oflags, float rho0)
{
    if (oflags & plain) {
        strm << "############ PLAIN ############\n";
        strm << "epsinf = " << dp.epsinf << '\n';
        for (unsigned cnt = 0; cnt < dp.eps.size(); ++cnt) {
            strm << "delta eps = " << dp.eps[cnt] << "   tau = " << dp.tau[cnt] << '\n';
        }
        if (rho0 > 0.0) {
            strm << "rho0 = " << rho0 << '\n';
        }
        strm << "fit = " << dp.fit << '\n';
    }
    if (oflags & edx) {
        strm << "############ ELM ############\n";
        strm << "block (0, 0, 0, 0, 0, 0, ";
        strm << dp.epsinf << ", ";
        if (rho0 > 0.0) {
            strm << 1.0/rho0;
        }
        else {
            strm << 1.0;
        }
        strm << ", deb)\ndebye (deb";
        for (unsigned cnt = 0; cnt < dp.eps.size(); ++cnt) {
            strm << ", " << dp.eps[cnt] << ", " << dp.tau[cnt];
        }
        strm << ")\n";
    }
    if (oflags & plt) {
        strm << "############ GNUPLOT ############\n";
        strm << "i = {0.0,1.0}\n" <<
        "pi = 3.141592654\n" <<
        "eps0 = 8.854187813e-12\n" <<
        "f(x) = " << dp.epsinf;
        for (unsigned cnt = 0; cnt < dp.eps.size(); ++cnt) {
            strm << " + " << dp.eps[cnt] << " / (1 - i*2*pi*x*(" << dp.tau[cnt] << "))";
        }
        strm << '\n' << "set logscale x\n" <<
        "set yrange [0:500]\n" <<
        "set xrange [10e3:4e6]\n";
        if (oflags & alipiovisacro) {
            strm << "rho0 = " << rho0 << '\n';
            strm << "sigma0 = (1.0/rho0) * 1000.0\n";
            strm << "h = 1.26*(sigma0**(-0.73))\n";
            strm << "epsinf = 12.0\n";
            strm << "gamma = 0.54\n";
            strm << "epsre(x) = epsinf + (tan(pi*gamma/2.0)*(1.0e-3) * sigma0*h*(x**(gamma-1.0))) / (2.0*pi*eps0*((1.0e6)**gamma))\n";
            strm << "epsim(x) = (sigma0*h*(1.0e-3) * (x/1.0e6)**gamma) / (2.0*pi*x*eps0)\n";
            strm << "plot real(f(x)), imag(f(x)), epsre(x), epsim(x);\n";
        }
        if (oflags & portela) {
            strm << "rho0 = " << rho0 << '\n';
            strm << "sigma0 = (1.0/rho0)\n";
            strm << "alfa = 0.706\n";
            strm << "deltai = 11.71e-3\n";
            strm << "sigma(x) = sigma0 + deltai * (1.0/tan(alfa*pi/2.0)) * (((2.0*pi*x)/(2.0*pi*1.0e6))**alfa)\n";
            strm << "rho(x) = 1.0/sigma(x)\n";
            strm << "epsre(x) = deltai * (((2.0*pi*x)/(2.0*pi*1.0e6))**alfa) / (2.0*pi*x*eps0)\n";
            strm << "epsim(x) = (rho0-rho(x)) / (rho0*rho(x)*(2.0*pi*x)*eps0)\n";
            strm << "plot real(f(x)), imag(f(x)), epsre(x), epsim(x);\n";
        }
        if (oflags & messier) {
            strm << "rho0 = " << rho0 << '\n';
            strm << "sigma0 = (1.0/rho0)\n";
            strm << "epsinf = 8*eps0\n";
            strm << "sigma(x) = sigma0 * (1.0 + ((4.0*pi*x*epsinf)/sigma0)**0.5)\n";
            strm << "rho(x) = 1.0/sigma(x)\n";
            strm << "epsre(x) = (epsinf/eps0) * (1.0 + (sigma0/(pi*x*epsinf))**0.5)\n";
            strm << "epsim(x) = (rho0-rho(x)) / (rho0*rho(x)*(2.0*pi*x)*eps0)\n";
            strm << "plot real(f(x)), imag(f(x)), epsre(x), epsim(x);\n";
        }
        if (!(oflags & (alipiovisacro|portela|messier))) {
            strm << "plot real(f(x)), imag(f(x));\n";
        }
    }
}


float calcPerpDipoleArrayCoef(float dipolesize, float dist)
{
    float a = dipolesize;
    float b = dist;
    return 2.0*PI/(1.0/sqrt(2.0*((a+b)*(a+b))) + 1.0/(b*sqrt(2.0)) - 2.0/sqrt((a+b)*(a+b)+b*b));
}


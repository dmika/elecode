///
/// \file flashover.h
/// \brief Back flashover calculation.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2023, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef FLASHOVER_H__
#define FLASHOVER_H__


#include "bfp.h"
#include "properties.h"
#include <fstream>
#include <filesystem>


float calcProbability(CurrentVoltage curvol, float gapDist = 1.0, char type = 't', float correl = 0.0, std::string file = "");
float calcProbabilityByAmplitude(CurrentVoltage curvol, float gapDist = 1.0, char type = 't', float front = 0.0);
std::deque<AmplitudeSteepness> readAscFile(std::filesystem::path filepath);


#endif    // FLASHOVER_H__


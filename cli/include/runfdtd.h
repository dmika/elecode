///
/// \file runfdtd.h
/// \brief This code performs calculations for the input file.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef RUNFDTD_H__
#define RUNFDTD_H__


#include "constants.h"
#include "modelfile.h"
#include "fdtd.h"
#include "obliquewire.h"
#include "interpolation.h"
#include <cmath>
#include <iomanip>


using namespace fdtd;


class CoutTimeStepNumber : public TimeStepItem {
public:
    virtual void doFirstHalfStepJob(unsigned);
    virtual void doSecondHalfStepJob(unsigned timeStepNumber);
    void setDt(float dt) { mDt = dt; }
    void setCalcTime(float time) { mCalcTime = time; }

protected:
    float mDt{0.0};
    float mCalcTime{0.0};
};


class RunFdtd {
public:
    RunFdtd() {
        mObliqueWireArea = nullptr;
    }
    RunFdtd(std::string FilePath);
    ~RunFdtd() {}

    void readFile(void);
    void setup(void);
    void calculate(void);
    void writeFile(void);

protected:
    ModelFile mFile;
    std::shared_ptr<Fdtd> mFdtd;
    std::shared_ptr<Model> mModel;
    std::shared_ptr<WireArea> mObliqueWireArea;
    std::shared_ptr<Output> mResult;
    void setupBlock(Block const& block);
    void setupResistor(Resistor const& resistor);
    void setupCapacitor(Capacitor const& capacitor);
    void setupInductor(Inductor const& inductor);
    void setupStaircaseWire(StaircaseWire const& wire);
    void setupThinWire(ThinWire const& wire);
    void setupSource(SourceObject const& src);
    void setupCalculate(CalculateObject const& calc);
    void setupObliqueWires(void);
    std::shared_ptr<Waveform> getPreparedWaveform(std::string name, unsigned counter = 0);
};


#endif    // RUNFDTD_H__


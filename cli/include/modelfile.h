///
/// \file modelfile.h
/// \brief Handling input model files. It reads file, performs check, and writes data to the file.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef MODELFILE_H__
#define MODELFILE_H__


#include "vectors.h"
#include <exception>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <memory>


bool isSubstringInString(std::string subString, std::string inputString);
void readXyzPoint(std::istringstream &stringStream, XyzPointFloat &coord, unsigned line, unsigned firstArgument = 0);
void unexpectedSymbol(std::istringstream &stringStream, unsigned line);
void writeParameterToStream(std::ostringstream &stringStream, unsigned param, bool chars = true);
void writeParameterToStream(std::ostringstream &stringStream, float param, bool chars = true);
void writeParameterToStream(std::ostringstream &stringStream, std::string param, bool chars = true);


enum ExceptionSeverity {
    EXCEPT_WARNING,    // calculation results may be wrong
    EXCEPT_ERROR       // program will crash
};


class Exception : public std::exception {
public:
    Exception() = default;
    Exception(std::string message, ExceptionSeverity sev = EXCEPT_ERROR) {
        mSeverity = sev;
        if (mSeverity == EXCEPT_WARNING)
            mMessage = "warning: ";
        else if (mSeverity == EXCEPT_ERROR)
            mMessage = "error: ";
        mMessage += message;
    }
    ~Exception() = default;

    virtual std::string getMessage() const { return mMessage; }
    ExceptionSeverity getExceptionSeverity() const { return mSeverity; }
    virtual const char* what() const throw() { return mMessage.c_str(); }

protected:
    std::string mMessage{""};
    ExceptionSeverity mSeverity{EXCEPT_ERROR};
};


enum class ObjectType {
    object,
    threads,
    volume,
    abc,
    calcTime,
    calcOutput,
    staircaseWire,
    thinWire,
    obliqueThinWire,
    debye,
    block,
    resistor,
    capacitor,
    inductor,
    function,
    source,
    calculate
};


class InputFileException : public Exception {
public:
    InputFileException(std::string message = "", ExceptionSeverity sev = EXCEPT_ERROR, unsigned linenumber = 0, bool writesev = true) {
        mSeverity = sev;
        mLine = linenumber;
        if (writesev && mSeverity == EXCEPT_ERROR) {
            mMessage = "Input file error. ";
        }
        else if (writesev && mSeverity == EXCEPT_WARNING) {
            mMessage = "Input file warning. ";
        }
        if (mLine) {
            mMessage += "Line " + std::to_string(mLine) + ". ";
        }
        mMessage += message;
    }

    unsigned getLineNumber(void) const { return mLine; }

protected:
    unsigned mLine;
};


struct HeidlerFuncParameters {
    HeidlerFuncParameters() = default;
    HeidlerFuncParameters(float front, float tail, float exp, float amplitude)
        : mFrontCoef{front}, mTailCoef{tail}, mExponent{exp}, mAmplitude{amplitude} {}

    float mFrontCoef{1.0};
    float mTailCoef{1.0};
    float mExponent{10.0};
    float mAmplitude{1.0};
};


class ModelObject {
public:
    ModelObject() {
        mType = ObjectType::object;
    }
    ModelObject(std::string inputString, unsigned line) {
        mType = ObjectType::object;
        mPositionInFile = line;
        mComment = inputString;
    }
    virtual ~ModelObject() {}
    
    virtual void getDataFromStringStream(std::istringstream &) {}
    virtual std::string writeDataToString(bool chars = true) { return mComment; }
    void writeComment(std::ostringstream &stringStream, bool chars = true);
    void readDataFromString(std::string inputString);
    static std::string prepareStringForDataReading(std::string inputString);
    void setPositionInFile(unsigned pos) { mPositionInFile = pos; }
    void setComment(std::string comment) { mComment = comment; }
    ObjectType getObjectType(void) const { return mType; }
    unsigned getPositionInFile(void) const { return mPositionInFile; }
    std::string getComment(void) const { return mComment; }
    const static std::string mCommentSymbol;

protected:
    unsigned mPositionInFile{0};
    ObjectType mType;
    std::string mComment;
};


class ThreadsObject : public ModelObject {
public:
    ThreadsObject() {
        mType = ObjectType::threads;
    }
    ThreadsObject(std::string inputString, unsigned line) {
        mType = ObjectType::threads;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    ThreadsObject(unsigned threads) {
        mType = ObjectType::threads;
        mThreadsNumber = threads;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    void setThreadsNumber(unsigned threads) { mThreadsNumber = threads; }
    unsigned getThreadsNumber(void) const { return mThreadsNumber; }
    const static std::string mCommandName;

protected:
    unsigned mThreadsNumber{1};
};


class VolumeObject : public ModelObject {
public:
    VolumeObject() {
        mType = ObjectType::volume;
    }
    VolumeObject(std::string inputString, unsigned line) {
        mType = ObjectType::volume;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    VolumeObject(XyzSizeFloat size, float cellsize, float courant) {
        mType = ObjectType::volume;
        mVolumeSize = size;
        mCellSize = cellsize;
        mCourantCoef = courant;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    void setVolumeSize(XyzSizeFloat size) { mVolumeSize = size; }
    void setCellSize(float size) { mCellSize = size; }
    void setCourantCoef(float courant) { mCourantCoef = courant; }
    XyzSizeFloat getVolumeSize(void) const { return mVolumeSize; }
    float getCellSize(void) const { return mCellSize; }
    float getCourantCoef(void) const { return mCourantCoef; }
    const static std::string mCommandName;

protected:
    XyzSizeFloat mVolumeSize;
    float mCellSize{0.0};
    float mCourantCoef{0.0};
};


class AbcObject : public ModelObject {
public:
    AbcObject() {
        mType = ObjectType::abc;
    }
    AbcObject(std::string inputString, unsigned line) {
        mType = ObjectType::abc;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    AbcObject(float thick, std::string abctype, float sigma = 1.0, float kappa = 10.0,
              float amax = 0.01, float m = 3.0, float ma = 1.0, std::string region = "all") {
        mType = ObjectType::abc;
        mAbcType = abctype;
        mRegion = region;
        mThickness = thick;
        mSigmaCoef = sigma;
        mKappaMax = kappa;
        mAmax = amax;
        mM = m;
        mMa = ma;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    void setAbcType(std::string abctype) { mAbcType = abctype; }
    void setThickness(float thick) { mThickness = thick; }
    void setSigma(float sigma) { mSigmaCoef = sigma; }
    void setKappa(float kappa) { mKappaMax = kappa; }
    void setA(float amax) { mAmax = amax; }
    void setM(float m) { mM = m; }
    void setMa(float ma) { mMa = ma; }
    std::string getAbcType(void) const { return mAbcType; }
    std::string getRegion(void) const { return mRegion; }
    float getThickness(void) const { return mThickness; }
    float getSigma(void) const { return mSigmaCoef; }
    float getKappa(void) const { return mKappaMax; }
    float getA(void) const { return mAmax; }
    float getM(void) const { return mM; }
    float getMa(void) const { return mMa; }
    const static std::string mCommandName;

protected:
    std::string mAbcType;
    std::string mRegion{"all"};
    float mThickness;
    float mSigmaCoef;
    float mKappaMax;
    float mAmax;
    float mM;
    float mMa;
};


class CalcTime : public ModelObject {
public:
    CalcTime() {
        mType = ObjectType::calcTime;
    }
    CalcTime(std::string inputString, unsigned line) {
        mType = ObjectType::calcTime;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    CalcTime(float time) {
        mType = ObjectType::calcTime;
        mCalculationTime = time;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    void setCalculationTime(float time) { if (time > 0.0) mCalculationTime = time; }
    float getCalculationTime(void) { return mCalculationTime; }
    const static std::string mCommandName;

protected:
    float mCalculationTime{1.0e-6};
};


class CalcOutput : public ModelObject {
public:
    CalcOutput() {
        mType = ObjectType::calcOutput;
        mFileName = "";
    }
    CalcOutput(std::string inputString, unsigned line) {
        mType = ObjectType::calcOutput;
        mPositionInFile = line;
        mFileName = "";
        readDataFromString(inputString);
    }
    CalcOutput(float dt, std::string filename = "", unsigned interval = 1000, bool result = false) {
        mType = ObjectType::calcOutput;
        mDataDt = dt;
        mFileName = filename;
        mWriteInterval = interval;
        mCalcResult = result;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    void setTimeInterval(float dt) { mDataDt = dt; }
    void setFileName(std::string name) { mFileName = name; if (mFileName == "*") mCalcResult = true; else mCalcResult = false; }
    void setWriteInterval(unsigned interval) { mWriteInterval = interval; }
    void setCalcResult(void) { mCalcResult = true; mFileName = "*"; }
    float getTimeInterval(void) const { return mDataDt; }
    std::string getFileName(void) const { return mFileName; }
    unsigned getWriteInterval(void) const { return mWriteInterval; }
    bool getCalcResult(void) const { return mCalcResult; }
    const static std::string mCommandName;

protected:
    float mDataDt;
    std::string mFileName;
    unsigned mWriteInterval{1000};
    bool mCalcResult = false;
};


class TwoPointObject : public ModelObject {
public:
    TwoPointObject() {}
    
    virtual std::string writeDataToString(bool chars = true);
    XyzPointFloat getStartPoint(void) const { return mStart; }
    XyzPointFloat getEndPoint(void) const { return mEnd; }
    void setStartPoint(XyzPointFloat point) { mStart = point; }
    void setEndPoint(XyzPointFloat point) { mEnd = point; }

protected:
    XyzPointFloat mStart;
    XyzPointFloat mEnd;
};


class Wire : public TwoPointObject {
public:
    Wire() {}

    const static std::string mCommandName;
};


class StaircaseWire : public Wire {
public:
    StaircaseWire() {
        mType = ObjectType::staircaseWire;
    }
    StaircaseWire(std::string inputString, unsigned line) {
        mType = ObjectType::staircaseWire;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    StaircaseWire(XyzPointFloat start, XyzPointFloat end, bool correction = false) {
        mType = ObjectType::staircaseWire;
        mStart = start;
        mEnd = end;
        mWithCorrection = correction;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    void setCorrection(bool correction) { mWithCorrection = correction; }
    bool isCorrected(void) const { return mWithCorrection; }
    const static std::string mWireTypeName;

protected:
    bool mWithCorrection = false;
};


class ThinWire : public Wire {
public:
    ThinWire() {
        mType = ObjectType::thinWire;
    }
    ThinWire(std::string inputString, unsigned line) {
        mType = ObjectType::thinWire;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    ThinWire(XyzPointFloat start, XyzPointFloat end, float diam) {
        mType = ObjectType::thinWire;
        mStart = start;
        mEnd = end;
        mDiameter = diam;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    float getDiameter(void) const { return mDiameter; }
    float getEpsilon(void) const { return mEpsilon; }
    char getSubType(void) const { return mSubType; }
    const static std::string mWireTypeName;
    void setDiameter(float diam) { if (diam > 0.0) mDiameter = diam; }

protected:
    float mDiameter{0.001};
    float mEpsilon{1.0};
    char mSubType{'n'}; // n - no subtype
};


class ObliqueThinWire : public Wire {
public:
    ObliqueThinWire() {
        mType = ObjectType::obliqueThinWire;
    }
    ObliqueThinWire(std::string inputString, unsigned line) {
        mType = ObjectType::obliqueThinWire;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    ObliqueThinWire(XyzPointFloat start, XyzPointFloat end, float diam, bool separ = false, bool infend = false) {
        mType = ObjectType::obliqueThinWire;
        mStart = start;
        mEnd = end;
        mDiameter = diam;
        mSeparated = separ;
        mInfiniteEnd = infend;
    }

    void readAdditionalArguments(std::istringstream &stringStream);
    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    void setCurrentSource(bool cursrc, unsigned segm = 0) {
        mCurrentSource = cursrc;
        mNodeSegmentSource = segm; }
    void setCurrentCalc(bool curcalc, unsigned segm = 0) {
        mCurrentCalc = curcalc;
        mNodeSegmentCalc = segm; }
    void setVoltageSource(bool volsrc, unsigned segm = 0) {
        mVoltageSource = volsrc;
        mNodeSegmentSource = segm; }
    void setVoltageCalc(bool volcalc, unsigned segm = 0) {
        mVoltageCalc = volcalc;
        mNodeSegmentCalc = segm; }
    void setName(std::string name) { mName = name; }
    void setSeparated(bool sep) { mSeparated = sep; }
    void setInfiniteEnd(bool inf) { mInfiniteEnd = inf; }
    void setDiameter(float diam) { if (diam > 0.0) mDiameter = diam; }
    float getDiameter(void) const { return mDiameter; }
    float getSheathDiameter(void) const { return mSheathDiameter; }
    float getSheathEpsilon(void) const { return mSheathEpsilon; }
    float getSheathSigma(void) const { return mSheathSigma; }
    bool isItSeparated(void) const { return mSeparated; }
    bool isItInfiniteEnd(void) const { return mInfiniteEnd; }
    bool hasItCurrentSource(void) const { return mCurrentSource; }
    bool hasItVoltageSource(void) const { return mVoltageSource; }
    bool hasItCurrentCalc(void) const { return mCurrentCalc; }
    bool hasItVoltageCalc(void) const { return mVoltageCalc; }
    float getSourceResistance(void) const { return mResistance; }
    unsigned getNumberOfSegments(void) const { return mNumberOfSegments; }
    unsigned getNodeSegmentSource(void) const { return mNodeSegmentSource; }
    unsigned getNodeSegmentCalc(void) const { return mNodeSegmentCalc; }
    std::string getName(void) const { return mName; }
    const static std::string mWireTypeName;

protected:
    std::string mName;
    float mDiameter{0.001};
    float mSheathDiameter{0.0};
    float mSheathEpsilon{1.0};
    float mSheathSigma{0.0};
    bool mSeparated{false};
    bool mInfiniteEnd{false};
    bool mCurrentSource{false};
    bool mVoltageSource{false};
    bool mCurrentCalc{false};
    bool mVoltageCalc{false};
    unsigned mNumberOfSegments{0};
    unsigned mNodeSegmentSource{0};
    unsigned mNodeSegmentCalc{0};
    float mResistance{0.0};
};


class Debye : public ModelObject {
public:
    Debye() {
        mType = ObjectType::debye;
    }
    Debye(std::string inputString, unsigned line) {
        mType = ObjectType::debye;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    Debye(std::string name, std::vector<float> epsilon, std::vector<float> tau) {
        mType = ObjectType::debye;
        mName = name;
        mDeltaEpsilon = epsilon;
        mTau = tau;
    }

    void readArguments(std::istringstream &stringStream);
    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    void setName(std::string name) { mName = name; }
    void setDeltaEpsilon(std::vector<float> eps) { mDeltaEpsilon = eps; }
    void setTau(std::vector<float> tau) { mTau = tau; }
    std::vector<float> getDeltaEpsilon(void) const { return mDeltaEpsilon; }
    std::vector<float> getTau(void) const { return mTau; }
    std::string getName(void) const { return mName; }
    const static std::string mCommandName;

protected:
    std::string mName;
    std::vector<float> mDeltaEpsilon;
    std::vector<float> mTau;
};


class Block : public TwoPointObject {
public:
    Block() {
        mType = ObjectType::block;
    }
    Block(std::string inputString, unsigned line) {
        mType = ObjectType::block;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    Block(XyzPointFloat start, XyzPointFloat end, float sigma, float epsilon, std::string name) {
        mType = ObjectType::block;
        mStart = start;
        mEnd = end;
        mSigma = sigma;
        mEpsilon = epsilon;
        mDebyeName = name;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    float getSigma(void) const { return mSigma; }
    float getEpsilon(void) const { return mEpsilon; }
    std::string getDebyeName(void) const { return mDebyeName; }
    void setSigma(float sig) { mSigma = sig; }
    void setEpsilon(float eps) { mEpsilon = eps; }
    const static std::string mCommandName;

protected:
    float mSigma;
    float mEpsilon;
    std::string mDebyeName;
};


class Resistor : public TwoPointObject {
public:
    Resistor() {
        mType = ObjectType::resistor;
    }
    Resistor(std::string inputString, unsigned line) {
        mType = ObjectType::resistor;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    Resistor(XyzPointFloat start, XyzPointFloat end, float res) {
        mType = ObjectType::resistor;
        mStart = start;
        mEnd = end;
        mResistance = res;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    float getResistance(void) const { return mResistance; }
    const static std::string mCommandName;

protected:
    float mResistance;
};


class Capacitor : public TwoPointObject {
public:
    Capacitor() {
        mType = ObjectType::capacitor;
    }
    Capacitor(std::string inputString, unsigned line) {
        mType = ObjectType::capacitor;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    Capacitor(XyzPointFloat start, XyzPointFloat end, float cap, float cond) {
        mType = ObjectType::capacitor;
        mStart = start;
        mEnd = end;
        mCapacitance = cap;
        mConductance = cond;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    float getCapacitance(void) const { return mCapacitance; }
    float getConductance(void) const { return mConductance; }
    const static std::string mCommandName;

protected:
    float mCapacitance;
    float mConductance;
};


class Inductor : public TwoPointObject {
public:
    Inductor() {
        mType = ObjectType::inductor;
    }
    Inductor(std::string inputString, unsigned line) {
        mType = ObjectType::inductor;
        mPositionInFile = line;
        readDataFromString(inputString);
    }
    Inductor(XyzPointFloat start, XyzPointFloat end, float ind) {
        mType = ObjectType::inductor;
        mStart = start;
        mEnd = end;
        mInductance = ind;
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    float getInductance(void) const { return mInductance; }
    const static std::string mCommandName;

protected:
    float mInductance;
};


class FunctionObject : public ModelObject {
public:
    FunctionObject() {
        mType = ObjectType::function;
    }
    FunctionObject(std::istream &infstr, std::string inputString, unsigned &line) {
        mType = ObjectType::function;
        mPositionInFile = line;
        std::string strdata = inputString + " ", tempString;
        if (!isSubstringInString(")", inputString)) {
            while (!isSubstringInString(")", tempString)) {
                getline(infstr, tempString);
                line++;
                strdata = strdata + tempString + " ";
            }
        }
        readDataFromString(strdata);
    }

    void readHeidlerParameters(std::istringstream &stringStream, HeidlerFuncParameters &param);
    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    std::vector<float> getDataArray(void) const { return mDataArray; }
    float getTimeInterval(void) const { return mDt; }
    unsigned getDataArraySize(void) const { return mDataArray.size(); }
    std::string getName(void) const { return mName; }
    std::string getFunctionType(void) const { return mFunctionType; }
    std::string getSubType(void) const { return mSubType; }
    float getFront(void) const { return mFront; }
    float getFrequency(void) const { return mFrequency; }
    float getTailCoef(void) const { return mTailCoef; }
    float getAmplitude(void) const { return mAmplitude; }
    float getExponent(void) const { return mExponent; }
    float getB(void) const { return mB; }
    float getC(void) const { return mC; }
    float getTimeShift(void) const { return mTimeShift; }
    float getCorrectionCoef(void) const { return mCorrectionCoef; }
    float getTimeToHalf(void) const { return mTimeToHalf; }
    float getSteepness(void) const { return mSteepness; }
    std::vector<HeidlerFuncParameters> getHeidlerFuncParameters(void) { return mHeidlerFuncParameters; }
    float getLambda(void) const { return mLambda; }
    float getSpeed(void) const { return mSpeed; }
    const static std::string mCommandName;

protected:
    std::string mName;
    std::string mFunctionType;
    std::string mSubType{"s"}; // s - simple
    float mDt;
    std::vector<float> mDataArray;
    float mFront;
    float mTailCoef;
    float mAmplitude;
    float mCorrectionCoef;
    float mExponent;
    float mFrequency;
    float mB;
    float mC;
    float mTimeShift{0.0};
    float mTimeToHalf;
    float mSteepness;
    std::vector<HeidlerFuncParameters> mHeidlerFuncParameters;
    float mLambda{0.0};
    float mSpeed{0.0};
};


class SourceObject : public TwoPointObject {
public:
    SourceObject() {
        mType = ObjectType::source;
    }
    SourceObject(std::string inputString, unsigned line) {
        mType = ObjectType::source;
        mPositionInFile = line;
        readDataFromString(inputString);
    }

    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    float getConductance(void) const { return mConductanceResistance; }
    float getResistance(void) const { return mConductanceResistance; }
    std::string getName(void) const { return mName; }
    std::string getSourceType(void) const { return mSourceType; }
    char getSubType(void) const { return mSubType; }
    float getTheta(void) const { return mTheta; }
    float getPhi(void) const { return mPhi; }
    float getPsi(void) const { return mPsi; }
    float getDist(void) const { return mDist; }
    const static std::string mCommandName;

protected:
    std::string mSourceType;
    std::string mName;
    float mConductanceResistance;
    char mSubType{'n'}; // n - no subtype
    float mTheta;
    float mPhi;
    float mPsi;
    float mDist;
};


class CalculateObject : public ModelObject {
public:
    CalculateObject() {
        mType = ObjectType::calculate;
    }
    CalculateObject(std::string inputString, unsigned line) {
        mType = ObjectType::calculate;
        mPositionInFile = line;
        readDataFromString(inputString);
    }

    void readYzCoordinates(std::istringstream &stringStream, XyzPointFloat &coord);
    virtual void getDataFromStringStream(std::istringstream &stringStream);
    virtual std::string writeDataToString(bool chars = true);
    std::vector<XyzPointFloat> getCoordinates(void) const { return mCoordinates; }
    std::string getCalculateType(void) const { return mCalculateType; }
    const static std::string mCommandName;

protected:
    std::string mCalculateType;
    std::vector<XyzPointFloat> mCoordinates;
};


/// Class for handling the model file.
/// The class reads file, performs model checks (the file should describe the model properly and
/// guarantee that the model desciption in the file can be calculated).
class ModelFile {
public:
    ModelFile() {}
    ModelFile(std::string filePath);

    std::vector<std::shared_ptr<ModelObject>> & getObjects(void) { return mObjects; }
    void readDataFromStream(std::istream &inputStream);
    void readFile(void);
    void coutFile(void);
    std::string getFilePath(void) { return mFilePath; }

protected:
    std::vector<std::shared_ptr<ModelObject>> mObjects;
    void processLineInFstream(unsigned &line, std::istream &inf);
    void performModelCheck(void);
    std::string mFilePath;
};


#endif    // MODELFILE_H__


///
/// \file epsilon.h
/// \brief Permittivity calculation.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2022, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of elecode.
/// elecode is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// elecode is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with elecode. If not, see <https://www.gnu.org/licenses/>.
///


#ifndef EPSILON_H__
#define EPSILON_H__


#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <filesystem>
#include <algorithm>
#include "properties.h"
#include "psols.h"
#include "soilmodel.h"
#include "doi.h"


enum Output {
    plain = 1 << 0,
    edx = 1 << 1,
    plt = 1 << 2,
    alipiovisacro = 1 << 3,
    portela = 1 << 4,
    messier = 1 << 5,
};


void convertFileEloToFre(std::filesystem::path filepath, float arraycoef = 1.0, float scale = 1.0);
void convertFileFreToFee(std::filesystem::path filepath, unsigned samples = 1);
void convertFileFreToFse(std::filesystem::path filepath, unsigned samples = 1);
float calcDcResistivity(std::filesystem::path filepath, unsigned samples = 1);
std::vector<RhoEpsilon> prepareRhoEpsilon(std::filesystem::path filepath);
CurrentVoltage readEloFile(std::filesystem::path filepath);
std::vector<RhoEpsilon> readFreFile(std::filesystem::path filepath);
std::vector<ComplexPermittivity> readFeeFile(std::filesystem::path filepath);
void writeFreFile(std::filesystem::path filepath, std::vector<RhoEpsilon> rhoeps);
void writeFeeFile(std::filesystem::path filepath, std::vector<ComplexPermittivity> eps, std::string ext = ".fee");
void writeFitFiles(std::filesystem::path filepath, float rho0, std::vector<ComplexPermittivity> eps);
void calcDebyeParametersForModel(int model, float rho0, float minfreq, float maxfreq, unsigned Np, unsigned repeats = 150);
DebyeParameters calcDebyeParameters(SampledPermittivity eps, unsigned Np, unsigned repeats);
void outputDebyeParameters(std::ostream &strm, DebyeParameters dp, int oflags = plain, float rho0 = 0.0);
float calcPerpDipoleArrayCoef(float dipolesize, float dist);


#endif    // EPSILON_H__

